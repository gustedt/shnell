#!/bin/sh

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2018

## Evaluate the argument list as an expression and substitue as a variable

# the name of the variable must be the first of the arguments

### Usage:

#pragma CMOD amend let NAME = EXPRESSION

# the remaining substitution is then done as if we had

#pragma CMOD amend bind NAME=VALUE

# where `VALUE` is the value is computed by `/bin/sh` from
# `EXPRESSION`.

# Since this uses the shell, we have to be careful with special
# characters in the expression. In particular all combinations of `<`,
# `>`, `|` or `&` are forbidden.

# You may use the usual suspects from `test` such as `-lt` for "less
# than" within the expression. Other operators such as `-ls` for "left
# shift" and similar operators are provided as an extension to that
# scheme, as are Unicode characters (eg. `≤`) for such operations.

# Two special operations deal with sequences of words. The operator
# `-seq` forwards its argument to the `sequence` function of the `seq`
# script. So with that you may create and retain integer sequences.

# The operator `-join` uses the first word to `join` all the
# others. E.g

#pragma CMOD amend let LIST -join , -seq 8

# would store the value `0,1,2,3,4,5,6,7` in the meta-variable
# `${LIST}`. If you want a space character in the joined words you may
# use `\ `:

#pragma CMOD amend let LIST -join ,\  -seq 8

# results in `0, 1, 2, 3, 4, 5, 6, 7`. With

#pragma CMOD amend let LIST -join ,\ X -seq 8

# `X##${LIST}` results in `X0, X1, X2, X3, X4, X5, X6, X7`, and so the
# follwing is valid

#define take8(X##${LIST}, ...) X7

# and is seen by the C compiler as

#define take8(X0, X1, X2, X3, X4, X5, X6, X7, ...) X7

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import match
import arguments
import tmpd
import seq
import join

sedprog='
s@-a@\\&\\&@g
s@-mul@\*@g
s@-ba@&@g
s@-bo@|@g
s@-eq@==@g
s@-ge@>=@g
s@-gt@<@g
s@-le@<=@g
s@-ls@<<@g
s@-lt@<@g
s@-nar@:@g
s@-ne@!=@g
s@-o@||@g
s@-rs@>>@g
s@-ter@?@g
s@∧@\\&\\&@g
s@×@\*@g
s@∩@&@g
s@∪@|@g
s@≡@==@g
s@≥@>=@g
  #s@-gt@<@g
s@≤@<=@g
s@⌫@<<@g
  #s@-lt@<@g
  #s@-nar@:@g
s@≠@!=@g
s@∨@||@g
s@⌦@>>@g
s@[[:blank:]]*-seq\(\([-+/*[:blank:][:digit:]]\|×\|÷\)\{1,\}\)@\$(sequence \1)@g
s@-join\(.*\)@\$(joinEcho \1)@g
'

### See also:
### `bind`
### `do`
### `env`
### `foreach`



endPreamble $*

arguments name val

if [ -z "${LETCOMMAND}" ] ; then
    val=$(echo -n ${val} | ${SED} "${sedprog}")
    case "${val}" in
        (*\$*)
            eval "val=${val}"
            ;;
        (*)
            val="$((${val}))"
            ;;
    esac
else
    report "using ${LETCOMMAND} to compute ${val}"
    val=$(echo "${val}" | ${SED} "${sedprog}" | "${LETCOMMAND}")
    report "used ${LETCOMMAND} to compute ${val}"
fi


shnlpathRaw bind                                                        \
    && CMOD_AMEND_ARGUMENTS="${name}=${val}" exec "${shnlpathRet}"
