#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## establish startup handler functions

### Usage:
#pragma CMOD insert startup FUNCTION0 ...

# where `FUNCTIONx` are functions that have the signature

#void FUNCTIONx(void);

# This is intended to be used with the `implicit` directive.

# The startup directive establishes startup handlers that are
# intended to be run before any other code that is defined in the
# corresponding TU.

# Yet before the startup handlers of a particular TU `A` are run, all
# such handlers for all TU `B` that are used by `A` are guaranteed to
# be finished, if possible. That is, if the "used-by" relation that is
# detected by the implicit directive has no cycle, this guarantee is
# given.

# This mechanism of startup handlers is triggered by code that is
# inserted by the `implicit` directive to the beginning of
# `stdc::main`. All these handlers for all TU that are linked together
# in an executable are run by the `main` thread, before any other
# threads may have been started by the program.

# See `atexit` for an analogous directive hat establishes handlers
# that are run by the last thread of an execution on exit.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import arguments
import echo
import tmpd
import join

endPreamble $*

sufunc () {
    suname="${TMPD}/_Startup.txt"
    join "\n" $*
    echo "${joinRet}" >> "${suname}"
}

arguments argv
sufunc ${argv}
