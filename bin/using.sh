#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## Use symbols from another module

# This is intended to be used with the `implicit` directive.

### Usage:
#pragma MODULE using LONGNAME NAME0 NAME1 ...

# This is as if all occurences of `NAMEx` are replaced by
# `LONGNAME::NAMEx`.

# All names `NAME0`, `NAME1` etc are forced to be private in this TU
# and should not be used for another purpose.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import arguments
import echo
import tmpd
import tokenize
import mangle

endPreamble $*

using () {
    mesep="${SHNELL_SEPARATOR:-::}"
    pref="$1"
    shift
    sname="${TMPD}/_Sname.txt"
    compsep="::" splitlong ${pref}
    echo "#line 1 \"<${CMOD_SOURCE} using ${pref}>\""
    for n in $* ; do
        echo "$n" >> "${sname}"
        join "${markc}" ${splitlongRet} $n
        # We must be sure that the tokenization of the identifiers is
        # correct, such that `implicit` can pick up the pieces.
        echo "#define ${markl} $n ${markr} ${markl} ${joinRet} ${markr}"
    done
}

arguments argv
using ${argv}

