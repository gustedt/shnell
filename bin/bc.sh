#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## A evaluate constant expressions with POSIX' `bc` tool.

# This directive spans the whole text that it amends for expressions
# that are enclosed in special brackets and performs computation on
# them.

### Usage

#pragma CMOD amend bc [LEFTPAREN RIGHTPAREN]

# Where `LEFTPAREN` and `RIGHTPAREN` can be any strings that help you
# visually distinguish processed expressions from the rest of the
# programming text. E.g

#pragma CMOD amend bc ⟦ ⟧

# processes all expressions such as `⟦2 ^ 9⟧` or `⟦4*atan(1)⟧` through
# `bc`, and replaces them with their value, here `512` and
# `3.14159265358979323844`.

# For details on the capacity of `bc` to do computations see the POSIX
# documentation.  Beware that `bc` is C-like but not completely
# C-conforming. In particular, it has no bit operations (`^`, `|`,
# `&`, `<<` and `>>`) and the `^` character is used for the
# exponentiation operation. So in the above example the C expression
# corresponding to `⟦2 ^ 9⟧` would have been `1 << 9`.

# The spacing around such replacements should remain intact, such that
# you may suffix such expressions with the appropriate C number suffix
# that you want to give the number. E.g `⟦2 ^ 9⟧ULL` would result in
# `512ULL`, the value `512` with a type of `unsigned long long`.

### Complicated "programs"

# `bc` is a whole programming language, that allows much more than
# just evaluating simple expressions. If we detect `;`, `{` or `}` we
# switch to a "complicated" processing mode that ensures that changes
# to the state in one processed expression do not pollute the result
# of other expressions. The result that such an expression has should
# then be assigned to the `bc` variable `r`.

# You may start your `bc` programs with an `auto` list of variables,
# but beware that `bc` itself already uses the letters `a`, `c`, `e`,
# `j`, `l`, and `s` for functions, and that we internally use the
# letters `o`, `p`, `q` and `r`. So you'd better not chose a letter
# that conflicts with these.  The "variable declaration" part starting
# with the keyword `auto` must always be at the beginning of the
# expression, consist of a list of variables or arrays and be
# terminated by a `;` token.

#### Example:

#⟦scale=200 ; r=4*atan(1)⟧

# sets the precision of computation temporarily to 200 digits and
# returns the value of 𝝅 with that precision.

#⟦auto i; for (i=0; i < 10; i++) { r += 2*i + 1; }⟧

# is a complicated way to express the value `100`.

# You may use meta-variable from `do`, `let`, `foreach`, `env` or
# `bind` directives if you expand them in the right order:

#⟦auto i; for (i=0; i < ${DIM}; i++) { r += 2*i + 1; }⟧

# is a complicated way to express the value `${DIM}*${DIM}`, where
# `${DIM}` should resolve to an integer constant provided by one of
# the directives as listed above.

### User functions

# Within the limits of `bc`' capacities (one-letter names!) , you can
# define your own functions. Whenever an expression starts with the
# word `define` this is considered to contain a function defintion. It
# is passed into `bc` as-is and is by itself supposed to *not* have a
# return value. The text is replaced by the expression itself, so you
# better put all of this inside a C comment.

# As mentionned above, `bc` already uses some letters for predefined
# functions, and we restrict the choice even further. So unfortunately
# it would not be easy implement a general function library for `bc`.

#### Example:

# The follwing lines also compute 𝝅:

#// Add a function to the bc state: ⟦define q(x) { return a(x); }⟧
#double pi = ⟦4*q(1)⟧;

# but the replacement removes the brackets but conserves the text in
# the comment

#// Add a function to the bc state: define q(x) { return a(x ) ; }
#double pi = 3.14159265358979323844;

### Mathematical functions

# We use the `bc` tool with the `-l` option to ensure that the "math
# library" is effective.  This enables limited support for
# mathematical functions, but only `sqrt` is a builtin, directly
# usable as we know it. We translate the names of the following
# mathematical functions to `bc`'s supported one character function
# names: `atan`, `sin`, `cos`, `log`, `exp`, and `jn`.

BC="${BC:-bc -l}"

#

SRC="$_" . "${0%%/${0##*/}}/import.sh"
import join
import tokenize
import list
import tmpd
import server
import arguments
import ontty

### Supported `bc` implementations

# Currently we use a POSIX specific hack that ensures that `bc` is run
# with line buffering. This speculates on the fact that if `bc` also
# has an "buffered" mode with larger buffers, it will check with POSX'
# `isatty` call if it is attached to a terminal. We don't know if this
# would work with other implementations. See `ontty`.

### Implementation

# This feature is implemented by starting a `bc` "server" process and
# sending the expressions that are to be processed on separate
# lines. We hope that this should be a bit faster than launching a
# separate process of each evaluation, but we have not hard evidence
# for that, yet. You could take all of this just as an example for
# such a "server" based approach.

endPreamble $*

export beginbc="__beginbc__"
export endbc="__endbc__"

parenthesis () {
    oparen="${1:-${beginbc}}"
    cparen="${2:-${endbc}}"
}

open () {
    while [ "${openfd:=2}" -lt 50 ] ; do
        openfd="$((${openfd} + 1))"
        report "testing ${openfd}"
        suc=1
        eval "exec ${openfd}$2 && suc=0"
        if [ ${suc} -eq 0 ] ; then
            break;
        fi
    done
    eval "$1=${openfd}"
}

sedpre="${tmpd}/sedpre$$.sed"

cp "${sedunmark}" "${sedpre}"

cat <<EOF >>"${sedpre}"
# remove possible additional control characters
s![[:cntrl:]]\{1,\}! !g
# provide C-like function names
s!\<atan\>!a!g
s!\<sin\>!s!g
s!\<cos\>!c!g
s!\<log\>!ln!g
s!\<exp\>!e!g
s!\<jn\>!j!g
EOF

computebc () {
    # The server command is a pipe that runs the expressions through
    # `bc`. `bc` has to be tricked into accepting to work in
    # "interactive" mode, that is, treating one line at a time.
    server "{ ontty ${SED} ${SEDU} -f ${sedpre} | ontty ${BC} ; }"      \
           "${tmpd}/fin$$.pipe"                                         \
           "${tmpd}/fout$$.pipe"
    open fdin ">${tmpd}/fin$$.pipe"
    open fdout "<${tmpd}/fout$$.pipe"

    # Skip any prologue that bc might send us since we tricked it into
    # interactive mode.
    echo "5555555+1" >&"${fdin}"
    while read -r line <&"${fdout}" ; do
        # the prologue is finished if we ge our first valid answer
        # back
        report "header: ${line}"
        case "${line}" in
            (5555556)
                break
                ;;
        esac
    done

    # Now comes the real server loop.
    expression=
    missingNL=
    while read -r line ; do
        if match "*#*" "${line}" ; then
            # Such a line should never enter a bc expression
            [ -z "${expression}" ] && echo "${line}"
            continue
        fi
        for w in ${line} ; do
            if [ -z "${expression}" ] ; then
                case "${w}" in
                    # The start of the expression is marked with a
                    # keyword.
                    (${beginbc})
                        # Just start with a parenthesis, so the
                        # expression isn't empty anymore.
                        expression="("
                        ;;
                    # Any other word is just echoed to stdout
                    (*)
                        echo -n "${w} "
                        ;;
                esac
            else
                case "${w}" in
                    # The end of the expression is marked with a
                    # keyword.
                    (${endbc})
                        expression="${expression#\( }"
                        expect=1

                        # Replace the marker by blanks and also remove
                        # leading and trailing blanks, such that we
                        # don't disturbe `bc`.
                        sj "${markl}${markr}${markib}${markit}${markpr}" " " ${expression}
                        expression="${joinRet}"

                        # Now decide on the action to take.
                        report "handle expression ${expression}"
                        case "${expression}" in
                            # If it is a function definition we don't
                            # expect to get a result.
                            (*define*)
                                expect=0
                                report "define expression: ${expression}"
                                echo "${expression}" >&"${fdin}"
                                ;;

                            # If it has these characters, we have to
                            # make sure that we don't change the
                            # internal state of `bc`. Therefore we
                            # encapsulate within a function and
                            # execute that function, once.
                            (*[\;\{\}]*)
                                vars="o, p, q, r"
                                case "${expression}" in
                                    (*auto*)
                                        sj ' ' ' ' ${expression}
                                        expression="${joinRet}"
                                        start="${expression%%;*}"
                                        nvars="${start#auto}"
                                        vars="${vars}, ${nvars}"
                                        report "all variables ${vars}"
                                        expression="${expression#*;}"
                                    ;;
                                esac
                                echo "define p() { auto ${vars}; p=scale; q=ibase; o=obase; ${expression}; scale=p; ibase=q; obase=o; return r; } ; p()" >&"${fdin}"
                                ;;

                            # Otherwise, just feed it into `bc`.
                            (*)
                                echo "${expression}" >&"${fdin}"
                                ;;
                        esac

                        # bc plays our input back to us
                        if [ "${BC_PLAYS_BACK:-1}" -eq 1 ] ; then
                            read rep <&"${fdout}"
                            report "playback ${rep}"
                        fi

                        if [ "${expect}" -eq 1 ] ; then
                            read res <&"${fdout}"
                            report "read ${res}"
                            case "${res}" in
                                (*syntax*)
                                    complain "${res#*) } for '${expression}'"
                                    res="${oparen} ${res#*) } ${cparen}"
                                    ;;
                            esac
                            echo -n "${res}"
                        else
                            # Function definitions are left in the
                            # output stream. They are better placed
                            # inside a C comment.
                            join "${markib}" ${expression}
                            echo -n ${joinRet}
                        fi
                        expression=
                        ;;
                    (*)
                        append expression "${w}"
                        ;;
                esac
            fi
        done
        # Echo a newline character at the end of the current input
        # line, but only if there is not an open expression
        if [ -z "${expression}" ] ; then
            echo "${missingNL}"
            missingNL=
        else
            # Collect the missing newlines, such that we don't change
            # the line count for the whole.
            missingNL="${missingNL}\n"
        fi
    done
    if [ -n "${expression}" ] ; then
        # Don't forget to output the value of a trailing
        # expression.
        complain "unterminated expression: ${expression}"
    fi
}

stage1 () {
    parenthesis $*
    "${SED}" "
# replace the open and closing parenthesis by strings that are
# detectable by sh
s!${oparen}!${markl} ${beginbc} ${markr}!g
s!${cparen}!${markl} ${endbc} ${markr}!g
"
}

arguments argv
stage1 ${argv} | computebc
