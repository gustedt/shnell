#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

# This is only experimental and not suitable for real. See `export`
# instead.

SRC="$_" . "${0%%/${0##*/}}/import.sh"
import echo

export manglePrefix="${manglePrefix-}"

import mangle
import join
import arguments
import tokenize

endPreamble $*

# File names
export MODSED="${TMPD}/modsed.sed"
source="${tmpd}/modsource.c"

import getnames

# Now this character can just be used as field separator, such that
# mangleLocal receives the identifier components as separate arguments
transform () {
    local IFS="${markc}"
    mangleLocal $*
    echo -n "${mangleRet} "
}

replace () {
    while read -r line ; do
        for w in ${line} ; do
            case "${w}" in
                # Here words that contain the special character must
                # otherwise be alphanumeric.
                (*${markc}*)
                    transform ${w}
                    ;;
                (*)
                    echo -n "${w} "
                    ;;
            esac
        done
        echo
    done
}

modtmp="${tmpd}/modtmp.sed"

#cp "${sedmark}" "${modtmp}"

# Glue the `::` identifiers together, again
echo "
s!\([[:blank:]]\|${markl}\|${markr}\)\{1,\}::\([[:blank:]]\|${markl}\|${markr}\)\{1,\}!${markc}!g
" >> "${modtmp}"

arguments argv

getnames ${argv}

[ -r "${MODSED}" ] && ${SED} "s![[:blank:]]*::[[:blank:]]*!${markc}!g" "${MODSED}" >>"${modtmp}"

shnlpathDo expand | ${SED} -f "${modtmp}" | replace
