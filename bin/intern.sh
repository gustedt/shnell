#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## Make a list of variables intern to the TU

### Usage:
#pragma CMOD amned intern NAME0 NAME1 ...

# This modifies the local names in the list such that they are not
# retrievable from other TU. For functions and global variables you
# should usually not use this but declare them `static`. This is for
# macros and types that you have to expose in a `.h` file but which is
# only for use in the interfaces as you provide them.

# If you are paranoid, you could also use this for all parameter and
# local variable names of `inline` functions, such that they will not
# clash with macro names coming from third parties.


SRC="$_" . "${0%%/${0##*/}}/import.sh"

import getnames
import arguments
import echo

endPreamble $*

MODSED="${MODSED:-${TMPD}/modsed.sed}"

arguments argv

getnames "_Intern::job${JOBID}::process$$" ${argv}

