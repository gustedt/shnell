#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## Tokenize the input stream

#  This script provides means to tokenize a byte stream according to
#  C's rules about different kinds of tokens:
#

#  - number: Beware that number tokens are not always proper numbers,
#    but only a superset which is sufficiently distinct from the
#    identifiers. We also recognize the `i` suffix of complex floating
#    point literals and rewrite this to a portable form.

#  - identifier: All words starting with an alphabetic character or
#    underscore, continuing the same or decimal digits.

#  - punctuator: We recognize all C punctuators, plus the bang-bang
#    operator `!!` and opening and closing double-brackets `[[` and
#    `]]` for attributes. See `punctuators` and the Unicode section
#    below for details.

#  - string and character literals: These are encoded and escaped
#    specially such that no blank character is visible inside such a
#    token, and such that there is no direct contact between
#    alphanumeric characters and punctuaton.

#  - comment tokens: These are encoded in a similar way, such that no
#    parsing will match these.

#  After this tokenization such tokens are surrounded by spaces and a
#  lot of markers (composed of control characters, see below) such
#  that other skripts then easily can treat them on a word base. This
#  is done in a way that the spacing of the source can mostly be
#  reconstructed after parsing.

###  Interfaces

#  - `tokenize` and `untokenize` which are functions that do the job
#    of doing the tokenization on the input stream and removing the
#    markers, respectively.

#  - `${sedmark}` and `${sedunmark}` which are temporary files that
#    contain regular expressions for sed to do the job

### Unicode

# Tokenization of Unicode code points is delegated to `sed`, so all
# character classification your `sed` knows about should work. Usually
# this should work well for the "basic multilanguage plane" (BMP).
# Most modern scripts for existing languages fall into that
# category. Beware though, that C has restrictions which Unicode code
# points are permitted for identifiers, since it only allows
# alphanumeric characters. E.g a Greek alpha, α, is fine, whereas an
# infinity symbol, ∞, is not permitted.

# We also recognize certain Unicode punctuators that represent
# mathematical operators. There use should make your source easier
# readable and also avoid posible ambiguities (for the human reader)
# in cases where several punctuators are adjacent or where the same
# punctuator (such as `*`) can have very different meanings according
# to the context. The following are the `sed` replacement patterns
# that are used.

opUtf8="
 #### comparisons
 s@ ≤ @ <= @g
 s@ ≥ @ >= @g
 s@ ≟ @ == @g
 s@ ≡ @ == @g
 s@ ≠ @ != @g
 #### Boolean logic
 # Beware, & on RHS is special for sed
 s@ ¬ @ ! @g
 s@ ‼ @ !! @g
 s@ ∨ @ || @g
 s@ ∧ @ \\&\\& @g
 #### set operations
 # Beware, & on RHS is special for sed
 s@ ∪ @ | @g
 s@ ∩ @ \\& @g
 s@ ∁ @ ~ @g
 s@ ⌫ @ << @g
 s@ ⌦ @ >> @g
 #### arithmetic
 # The second line for division is in fact the Unicode point x2215,
 # which happens to have the same glyph as the usual division character
 # x2F.
 s@ × @ * @g
 s@ ÷ @ / @g
 s@ ∕ @ / @g
 s@ − @ - @g
 #### assignment ops
 # Beware, & on RHS is special for sed
 s@ ∪= @ |= @g
 s@ ∩= @ \\&= @g
 s@ ×= @ *= @g
 s@ ÷= @ /= @g
 s@ ∕= @ /= @g
 s@ ⌫= @ <<= @g
 s@ ⌦= @ >>= @g
 #### syntax specials
 s@ … @ ... @g
 s@ → @ -> @g
 #### attributes
 s@ ⟦ @ [[ @g
 s@ ⟧ @ ]] @g"
 
#

SRC="$_" . "${0%%/${0##*/}}/import.sh"
import echo
import tmpd
import match
import punctuators

#  All of this can be tuned by assigning different values to the
#  markers.

#  For procedural reasons the following must be a single control
#  character, each:

#  - Markers for inserted spaces surrounding tokens, left or right
export markl="${markl:-}"
export markr="${markr:-}"

#  - for character and string prefixes
export markpr="${markpr:-}"

#  - A marker for a double colon. If you leave this allone, the only
#    effect is that spaces arround :: (as in [[ gnu :: deprecated]])
#    will be removed
export markc="${markc:-}"

#  - for internal blanks
export markib="${markib:-}"

#  - for internal tabs
export markit="${markit:-}"

# The following now can be encoded as two control characters, they
# don't need to appear in character classes or IFS.

#  - Transient markers for periods, plus and minus within a
#    number. These are removed at the end of tokenization, so you
#    probably don't want to mess with this.
export markp="${markp:-}"
export markplus="${markplus:-}"
export markminus="${markminus:-}"

#  - string literal markers, mid, left and right
export marksm="${marksm:-}"
export marksl="${marksl:-}"
export marksr="${marksr:-}"

#  - character literal markers, mid, left and right
export markcm="${markcm:-}"
export markcl="${markcl:-}"
export markcr="${markcr:-}"

#  - for double backslash
export markbb="${markbb:-}"

#  - for single backslash
export marksb="${marksb:-}"

#  - for a backtick
export markbt="${markbt:-}"

#  - for a dollar
export markdo="${markdo:-}"

#  - for a hash
export markha="${markha:-}"

#  - for an underscore

#  These are part of identifiers, but might also be separators inside
#  numbers in the future. Therefore we use hex characters to replace
#  them.
export markun="${markun:-AbCDeF$$fEdCBa}"

#  - comment markers left, right, //-encoding
export markCL="${markCL:-}"
export markCR="${markCR:-}"
export markCMP="${markCMP:-}"


endPreamble $*

if [ -z "${TOKENIZE_SOURCE}" ] ; then
    export TOKENIZE_SOURCE="${SRC}"

    # An integer pattern for sed. This is not necessarily a valid
    # integer, but in valid C code, only integers are identified by
    # this.
    export sedint='[[:digit:]][[:alnum:]_]*'
    # An pattern for the exponent of a number
    export seddec='[[:digit:]]\{1,\}'
    export sedexpc='[eEpP]'
    export sedexp='[eEpP][+-]\{,1\}[[:digit:]]\{1,\}'

    # The possible floating point or integer suffix. Currently this
    # could be [ulfdULFD] and we support [iI] as extension, see
    # below. But this part of the standard seems merely creative at
    # the moment, so we just suppose that this can be any alphabetic
    # character.
    export sedsuff='[[:alpha:]]*'

    # An pattern for an identifier
    export sedid='[[:alpha:]_]\{1,\}[[:alnum:]_]*'
    # An pattern for the end preceeding a number
    export sedend="[^-[:alnum:]_.]"
    # An pattern for the end preceeding an identifier
    export sediend="[^-[:alnum:]_]"

    # A filename containing the pattern for tokenization
    export sedmark="${TMPD}/sedmark$$.sed"
    # A filename containing the pattern for unmarking
    export sedunmark="${TMPD}/sedunmark$$.sed"

    # Use "Echo" such that \1 etc arrive properly in the file.
    Echo "
# Don't process preprocessor lines too much
/^[[:blank:]]*\#[[:blank:]]*\(else\|pragma\|line\|endif\|include\)/{
        s!^[[:blank:]]*\#[[:blank:]]*pragma!\#pragma!
        s![\\] !${markib}!g
        s![\\]	!${markit}!g
        s![[:blank:]]\{1,\}! !g
        p
        d
}
/^[[:blank:]]*\#[[:blank:]]*\(define\)/{
        s![[:blank:]]\{1,\}! !g
}
s![\\][\\]!${markbb}!g
s![\\][\"]!${marksb}${marksm}!g
s![\\][']!${marksb}${markcm}!g
s![\\]!${marksb}!g
s![\`]!${markbt}!g
s![\#]!${markha}!g
s![_]!${markun}!g

:stringOrChar

s!\"\|'\|\/\/\|[\/][\*]!${markp}&!
/${markp}/{
        # this is a string
        /${markp}\"/{
                s!${markp}\"!${markp}!
                # replace all blanks, squote and comment start inside the string
                :markSblank
                s!\(${markp}[^[:blank:][:punct:]]*\)[ ]!\1${markib}!
                s!\(${markp}[^[:blank:][:punct:]]*\)[	]!\1${markit}!
                s!\(${markp}[^[:blank:][:punct:]]*\)'!\1${markcm}!

                # identify the end of the string
                # if it is followed directly by another string, insert a blank
                s!${markp}\([^[:blank:][:punct:]]*\)\"\"!${marksl}\1${marksr} ${markp}!
                t markSblank
                s!${markp}\([^[:blank:][:punct:]]*\)\"!${marksl}\1${marksr}!
                t stringOrChar

                s!${markp}\([^[:blank:][:punct:]]*\)\([[:punct:]]\)!${marksl}\1${markCL}\2${markCR}${marksr}${markp}!
                t markSblank
        }
        # this is a cpp comment
        /${markp}[/][/]/{
                s!${markp}[/][/]!${markp}${markCMP}!
                # replace all blanks, squote, and dquote inside the comment
                :markCPblank
                s!\(${markp}.*\)[ ]!\1${markib}!
                s!\(${markp}.*\)[	]!\1${markit}!
                s!\(${markp}.*\)'!\1${markcm}!
                s!\(${markp}.*\)\"!\1${marksm}!
                #
                /${markp}.*\([ 	'\"]\)/b markCPblank

                :markCPpunct
                s!${markp}\([^[:punct:]]*\)\([[:punct:]]\)!${markCL}\1${markCR}${markCL}\2${markCR}${markp}!
                t markCPpunct
                s!${markp}\(.\{1,\}\)!${markCL}\1${markCR}!
                s!${markp}!!
                b cppComment
        }
        # this is a C comment
        /${markp}[\/][\*]/{
                s!${markp}[/]\*!${markCL}/${markCR}${markCL}*${markCR}${markp}!

                :CCstart
                /${markp}.*[\*][\/]/!{
                        # replace all blanks, squote, and dquote inside the comment
                        :markCCblank
                        s!\(${markp}.*\)[ ]!\1${markib}!
                        s!\(${markp}.*\)[	]!\1${markit}!
                        s!\(${markp}.*\)'!\1${markcm}!
                        s!\(${markp}.*\)\"!\1${marksm}!
                        #
                        /${markp}.*\([ 	'\"]\)/b markCCblank

                        :markCCpunct
                        s!${markp}\([^[:punct:]]*\)\([[:punct:]]\)!${markCL}\1${markCR}${markCL}\2${markCR}${markp}!
                        /${markp}[^[:punct:]]*[[:punct:]]/b markCCpunct
                        s!${markp}\(.\{1,\}\)!${markCL}\1${markCR}!
                        s!${markp}!!
                        p
                        N
                        s!.*\n!${markp}!
                        b CCstart
                }
                s!\*\/!&${markCR}!
                :markCCblank2
                s!\(${markp}.*\)[ ]\(.*${markCR}\)!\1${markib}\2!
                s!\(${markp}.*\)[	]\(.*${markCR}\)!\1${markit}\2!
                s!\(${markp}.*\)'\(.*${markCR}\)!\1${markcm}\2!
                s!\(${markp}.*\)\"\(.*${markCR}\)!\1${marksm}\2!
                #
                /${markp}.*[ 	'\"].*${markCR}/b markCCblank2

                :markCCpunct2
                s!${markp}\([^[:punct:]]*\)\([[:punct:]]\)\(.*${markCR}\)!${markCL}\1${markCR}${markCL}\2${markCR}${markp}\3!
                /${markp}[^[:punct:]]*[[:punct:]].*${markCR}/b markCCpunct2
                s!${markp}!${markCL}!
                b stringOrChar
        }
        # this is a character
        /${markp}'/{
                s!${markp}'!${markp}!
                # replace a blank or dquote inside the character constant
                s!${markp}[ ]'!${markcl}${markib}${markcr}!
                s!${markp}[	]'!${markcl}${markit}${markcr}!
                s!${markp}\"'!${markcl}${marksm}${markcr}!
                s!${markp}\([^[:blank:]']*\)'!${markcl}\1${markcr}!

                /${markp}/!b stringOrChar
        }
        s!${markp}!STRING_ERROR!g
}
:cppComment

s![\$]!${markdo}!g

# Mark and protect string or character prefixes
s!^\(u\|U\|u8\|L\)\(${marksl}\|${markcl}\)!\2\1${markpr}!g
s!\([^[:alnum:]]\)\(u\|U\|u8\|L\)\(${marksl}\|${markcl}\)!\1\3\2${markpr}!g

# Put markers arround preprocessing numbers and escape period characters

# a number with a decimal point and a positive exponent at the beginning of a line
s!^\(${sedint}\)[.]\([[:alnum:]_]*\)\(\(${sedexpc}\)[+]\(${seddec}\)\(${sedsuff}\)\)!${markl}\1${markp}\2\4${markplus}\5\6${markr}!g
s!^[.]\(${sedint}\)\(\(${sedexpc}\)[+]\(${seddec}\)\(${sedsuff}\)\)!${markl}${markp}\1\3${markplus}\4\5${markr}!g

# a number with a decimal point and a positive exponent that starts a new word
s!\(${sedend}\)\(${sedint}\)[.]\([[:alnum:]_]*\)\(\(${sedexpc}\)[+]\(${seddec}\)\(${sedsuff}\)\)!\1${markl}\2${markp}\3\5${markplus}\6\7${markr}!g
s!\(${sedend}\)[.]\(${sedint}\)\(\(${sedexpc}\)[+]\(${seddec}\)\(${sedsuff}\)\)!\1${markl}${markp}\2\4${markplus}\5\6${markr}!g

# a number without a decimal point but with a positive exponent at the beginning of a line
s!^\(${sedint}\)\(\(${sedexpc}\)[+]\(${seddec}\)\(${sedsuff}\)\)!${markl}\1\3${markplus}\4\5${markr}!g

# a number without a decimal point but with a positive exponent that starts a new word
s!\(${sedend}\)\(${sedint}\)\(\(${sedexpc}\)[+]\(${seddec}\)\(${sedsuff}\)\)!\1${markl}\2\4${markplus}\5\6${markr}!g

# a number with a decimal point and negative exponent at the beginning of a line
s!^\(${sedint}\)[.]\([[:alnum:]_]*\)\(\(${sedexpc}\)[-]\(${seddec}\)\(${sedsuff}\)\)!${markl}\1${markp}\2\4${markminus}\5\6${markr}!g
s!^[.]\(${sedint}\)\(\(${sedexpc}\)[-]\(${seddec}\)\(${sedsuff}\)\)!${markl}${markp}\1\3${markminus}\4\5${markr}!g

# a number with a decimal point and negative exponent that starts a new word
s!\(${sedend}\)\(${sedint}\)[.]\([[:alnum:]_]*\)\(\(${sedexpc}\)[-]\(${seddec}\)\(${sedsuff}\)\)!\1${markl}\2${markp}\3\5${markminus}\6\7${markr}!g
s!\(${sedend}\)[.]\(${sedint}\)\(\(${sedexpc}\)[-]\(${seddec}\)\(${sedsuff}\)\)!\1${markl}${markp}\2\4${markminus}\5\6${markr}!g

# a number without a decimal point but with a negative exponent at the beginning of a line
s!^\(${sedint}\)\(\(${sedexpc}\)[-]\(${seddec}\)\(${sedsuff}\)\)!${markl}\1\3${markminus}\4\5${markr}!g

# a number without a decimal point and without exponent that starts a new word
s!\(${sedend}\)\(${sedint}\)\(\(${sedexpc}\)[-]\(${seddec}\)\(${sedsuff}\)\)!\1${markl}\2\4${markminus}\5\6${markr}!g

# a number with a decimal point and without exponent at the beginning of a line
s!^\(${sedint}\)[.]\([[:alnum:]_]*\)!${markl}\1${markp}\2${markr}!g
s!^[.]\(${sedint}\)!${markl}${markp}\1${markr}!g

# a number with a decimal point and without exponent that starts a new word
s!\(${sedend}\)\(${sedint}\)[.]\([[:alnum:]_]*\)!\1${markl}\2${markp}\3${markr}!g
s!\(${sedend}\)[.]\(${sedint}\)!\1${markl}${markp}\2${markr}!g

# a number without a decimal point and without exponent at the beginning of a line
s!^\(${sedint}\)!${markl}\1${markr}!g

# a number without a decimal and without exponent point that starts a new word
s!\(${sedend}\)\(${sedint}\)!\1${markl}\2${markr}!g

# unmark an exponent that has been identified as separate number
s!\([eEpP][+-]\{,1\}\)${markl}\([[:digit:]]\{1,\}\)${markr}${markr}!\1\2${markr}!g

# Put markers arround identifiers

# at the beginning of a line
s!^${sedid}!${markl}&${markr}!g

# at the end of a line
s!${sedid}\$!${markl}&${markr}!g

# starting a new word
s!\(${sediend}\)\(${sedid}\)!\1${markl}\2${markr}!g

# Replace the token :: by the special character, if any. This can then
# be used as a field separator in a function transform. Per default
# there is no special marker, and this just removes spacing around ::

s!${markr}[[:blank:]]*\(::\|∷\)[[:blank:]]*${markl}!${markc}!g

# We want to reestablish the space structure of the lines.  Therefore
# we mark the beginning and the end of the identifiers that are to be
# mangled and withdraw the inserted space afterwards.
s!${markl}!${markl} !g
s!${markr}! ${markr}!g

# Mark punctuators that are not inside strings
s@^\(${punctuators}\)@${markl} \1 ${markr}@
s@\([[:blank:]]\{1,\}\)\(${punctuators}\)@\1\2 ${markr}@g

:crunch
s@\(${marksr}\|${markcr}\|${markCR}\|${markr}\|${markdo}\)\(${punctuators}\)@\1${markl} \2 ${markr}@g
t crunch

# Some punctuators inside comments get falsely marked.
s@${markCL}${markl}[[:blank:]]*@${markCL}@g
s@[[:blank:]]*${markr}${markCR}@${markCR}@g

# Now fuse directly adjacent string literals that we created for punctuators
s!${markCR}${marksr}${marksl}!${markCR}!g

# starting a meta-variable
s!${markr}${markdo}!${markr}${markl} ${markdo}!g

# Also glue meta variable references back together
s!\#\([-[:blank:]]*\)${markdo}\([-[:blank:]]*\)[{][-[:blank:]]*\([[:alnum:]_]\{1,\}\)[-[:blank:]]*[}]!\1\2\#${markdo}{\3}!g
s!${markdo}\([-[:blank:]]*\)[{][-[:blank:]]*\([[:alnum:]_]\{1,\}\)[-[:blank:]]*[}]!\1${markdo}{\2}!g
s!\([[:punct:]][-]*\)\(\#\{0,1\}${markdo}{[[:alnum:]_]\{1,\}}\)!\1 \2!g
s!\(\#\{0,1\}${markdo}{[[:alnum:]_]\{1,\}}\)\([-]*[[:punct:]]\)!\1 \2!g

${opUtf8}

# revert the signs in the exponents of floating point numbers
s!${markplus}!+!g
s!${markminus}!-!g

# An extension to allows the use of the 1.i notation for the imaginary
# part of a complex number. These may only be appended to floating
# point numbers, thus they have to contain a hidden period.
s![[:blank:]]\([0-9][[:alnum:]]*${markp}[-+[:alnum:]]*\)[iI]\([[:alpha:]]*\)[[:blank:]]! \1\2 ${markr}${markl} * ${markr}${markl} I !g
s![[:blank:]]\(${markp}[0-9][-+[:alnum:]]*\)[iI]\([[:alpha:]]*\)[[:blank:]]! \1\2 ${markr}${markl} * ${markr}${markl} I !g

# now revert the hidden period in floating point numbers
s!${markp}!.!g

# revert similar hidden characters
s!${markdo}!\$!g
s!${markha}!\#!g
s!${markun}!_!g

# Now do a bit of a cleanup
s!\(${markl} \)\{2,\}!${markl} !g
s!\( ${markr}\)\{2,\}! ${markr}!g
s!${markl}[[:blank:]]\{1,\}${markr}!!g
s!\(${markCL}${markCR}\)\{2,\}!${markCL}${markCR}!g
s!${markCR}\(${markCL}${markCR}\)\{1,\}${markCL}!${markCR}${markCL}!g
" >"${sedmark}"

    Echo "
# Unprotect string and character prefixes
s!\(${marksl}\|${markcl}\)\(u\|U\|L\|u8\)${markpr}!\2\1!g
s!${markl} *!!g
s! *${markr}!!g
s!${markc}!::!g
s!${markib}! !g
s!${markit}!\t!g
s!${marksm}!\"!g
s!${marksl}!\"!g
s!${marksr}!\"!g
s!${markcm}!\'!g
s!${markcl}!'!g
s!${markcr}!'!g
s!${markbb}!\\\\\\\\!g
s!${marksb}!\\\\!g
s!${markbt}!\`!g
s!${markCMP}!//!g
s!${markCL}!!g
s!${markCR}!!g
s![[:blank:]]\{1,\}\$!!
s!${markun}!_!g
" >"${sedunmark}"

fi

TOKENIZE="${TOKENIZE-${IMPORT_SOURCE_tokenize}}"

tokenize () {
    ${SED} -f "${sedmark}"
}

untokenize () {
    ${SED} -f "${sedunmark}"
}

# The following is only used for testing.
case "${TOKENIZETEST}" in
    (1)
        tokenize
        ;;
    (2)
        untokenize
        ;;
    (3)
        tokenize | untokenize
        ;;
esac
