#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2022

## Define a holdback for a lambda expression

# This enables the definition of holdbacks, that are macro definitions
# of lambda expressions that are expanded differently if they are
# encountered with or without arguments.

### With arguments

# If encountered in a macro call with arguments the expansion first
# squeezes all evaluations of the arguments into evaluations that are
# captured with value captures and then defines local variables that
# mimic the parameters.

# To mimic the parameters declarations of array parameters, these are
# adjusted to definition of pointer variables. This only works if the
# array definition is not hidden inside a `typedef`{.C}; if so this
# results in a compilation error.

#### Type generic lambdas

# If these parameters have `auto`{.C} types, this lets you define calls
# to type-generic lambdas, even if your compiler doesn't support
# these.

#### Constant expressions

# If all arguments are constant expressions and the function call
# doesn't refer to global variables the result can also be a constant
# expression. (This works for C++ lambdas out of the box, otherwise
# you'd have to see for the gcc extension described below.)

#### Support on pre-C23 compilers

# If your platform does not support lambda expressions, yet, you may
# indicate that by setting the feature test macro `NO_LAMBDAS`{.C}. Then,
# if possible, a version that only uses gcc's compound expressions,
# `({ })`{.C}, is used. If you use that feature you'd have to have at most
# one `return`{.C} statement in the body and that statement must be the
# very last. If none of this works, a normal function call (see below)
# is used.

### Without arguments

# If such a holdback is encountered otherwise, it is just replaced by
# the lambda expression as specified. This can be used for compilers
# that support type-generic lambdas expression only in contexts where
# they are converted to a function pointer with a known prototype.

# Note that if the language is C++, for the latter the lambda
# expression is modified such that parameter declarations such as
# `double a[const static 78]`{.C} are adjusted to the pointer definition
# of the parameter object, so here `double (*const a)`{.C}. We do this
# because that notation is currently not valid in C++.

# If `NO_LAMBDAS`{.C} is defined, a `static inline`{.C} function is generated,
# instead. This has its limits and only works if the capture list is
# in fact empty. For the generated function the same adjustment of
# array parameters as described above is made if the target language
# is C++.

### Extensions

# As other extensions we also recognize `__restrict__`{.C},
# `typeof`{.C} and `__typeof__`{.C}.

### Compiler prefix

# The `shnell` distribution also has a compiler prefix `holdback` that
# uses the dialect `HOLDBACK` to apply holdups systematically to your
# code.

### Example:

# ```C
# #pragma CMOD amend holdback max
#  [](auto a, auto b) [[unsequenced]] {
#      return (a < 0)
#        ? (b < 0) ? (a < b) ? b : a : b
#        : (0 < b) ? (b < a) ? a : b : a;
# }
# #pragma CMOD done
# ```

# A call `max(23U, -21)`{.C} would expand to something like

# ```C
# (
#  [cap1 = (23U), cap2 = (-21)](void) [[unsequenced]] {
#    auto a = cap1; /* is unsigned */
#    auto b = cap2; /* is signed */
#    return (a < 0)
#      ? (b < 0) ? (a < b) ? b : a : b
#      : (0 < b) ? (b < a) ? a : b : a; /* result is unsigned */
#  }()
# )
# ```

# Note that this lambda expression now has no parameters at all, in
# particular none that would be `auto`{.C}. Also, note that besides the
# initializers of captures and pseudo-parameters, this modified lambda
# still has only one `return`{.C} statement that evaluates an
# expression. This is close to what historical versions of C++
# accepted as a `constexpr`{.C} functions or lambda expressions.

# If the macro `max`{.C} then is used otherwise in the program (that is,
# not within a macro call) it is just replaced by the orginal lambda
# expression. If your compiler supports conversion of type-generic
# lambdas to function pointers, the following line

#unsigned (*maxUI)(unsigned, signed) [[unsequenced]] = max;

# would be replaced by

# ```C
# unsigned (*maxUI)(unsigned, signed)  [[unsequenced]] = [](auto a, auto b)  [[unsequenced]] {
#  return (a < 0)
#    ? (b < 0) ? (a < b) ? b : a : b
#    : (0 < b) ? (b < a) ? a : b : a;
# };
# ```




SRC="$_" . "${0%%/${0##*/}}/import.sh"

import arguments
import echo
import tokenize
import tmpd
import ballanced

endPreamble $*

countParams () {
    local top=
    local line=
    local count="1"
    firstWord
    if [ "${top}" = "(" ] ; then
        deleteWord
    else
        complain "${name}: expecting opening parenthesis"
    fi
    skipCntrl       >>/dev/null
    while [ "${top}" ] ; do
        case "${top}" in
            ([\[\{\(]|\[\[)
                callBack >>/dev/null
                continue
                ;;
            (,)
                count=$((${count} + 1))
                ;;
            (\))
                break
                ;;
        esac
        report "countParams seeing ${top} (${count})"
        deleteSkipCntrl
    done
    echo "${count}"
}

extractQualifiers () {
    local top=
    local line=
    firstWord
    while [ "${top}" ] ; do
        report "extractQualifiers seeing ${top}"
        case "${top}" in
            (_Atomic|const|volatile|restrict|__restrict__)
                report "extractQualifiers seeing ${top}"
                popWord
                skipCntrl >>/dev/null
                ;;
            (*)
                deleteSkipCntrl
                ;;
        esac
    done
}

adjustParam () {
    local top=
    local line=
    firstWord
    newTmp bound
    newTmp item
    echo -n > "${item}"
    while [ "${top}" ] ; do
        case "${top}" in
            (\(|\*)
                report "adjustParam met top ${top}"
                popWord
                skipCntrl >>/dev/null
                continue
                ;;
        esac
        break
    done
    while [ "${top}" ] ; do
        report "adjustParam, loop, seeing ${top} ${line}"
        case "${top}" in
            (\[\[)
                # attributes are not array bounds and in general bind
                # to the previous item
                callBack >> "${item}"
                skipCntrl >>/dev/null
                continue
                ;;
            (\()
                # parenthesis start a new item
                ${CAT}  "${item}"
                callBack > "${item}"
                skipCntrl >>/dev/null
                it=$(${CAT} "${item}")
                report "adjustParam: looking for pointer ${it} : ${top} : ${line}, adjusting ?"
                case "${it}" in
                    (*\**)
                        report "adjustParam: found a pointer ${it} : ${top} : ${line}, adjusting ?"
                        echo -n > "${item}"
                        fo=$(echo "${it}" | adjustParam)
                        rest=$(${CAT})
                        report "adjustParam: ${fo} ${top} ${line} ${rest} "
                        echo -n "${fo} ${top} ${line} ${rest} "
                        return
                esac
                #echo -n " ${it} "
                report "found ${it}, not a pointer, continuing"
                continue
                ;;
            (\[)
                # this one is real
                report "adjustParam seeing ${top}"
                callBack > "${bound}"
                skipCntrl >>/dev/null
                echo >> "${bound}"
                it=$(${CAT} "${item}")
                echo -n > "${item}"
                bo=$(extractQualifiers < "${bound}")
                rest=$(${CAT})
                echo -n " (*${bo} ${it}) ${top} ${line} ${rest} "
                return
                ;;
            (\))
                # other tokens start a new item
                report "adjustParam seeing ${top}"
                ${CAT}  "${item}"
                echo -n " "
                popWord
                skipCntrl >>/dev/null
                echo -n " "
                return
                ;;
            (typeof|__typeof__)
                # a typeof needs following ()
                report "adjustParam seeing ${top} ${line}"
                ${CAT}  "${item}"
                echo -n " "
                popWord > "${item}"
                skipCntrl >>/dev/null
                if [ "${top}" = "(" ] ; then
                    callBack  >>"${item}"
                    skipCntrl >>/dev/null
                else
                    complain "${name}: expecting opening parenthesis after typeof"
                fi
                continue
                ;;
        esac
        # other tokens start a new item
        report "adjustParam seeing ${top}"
        echo -n " " $(${CAT}  "${item}")
        popWord > "${item}"
        skipCntrl >>/dev/null
    done
    echo -n  " " $(${CAT}  "${item}")
    echo -n > "${item}"
}

fakeParams () {
    local top=
    local line=
    local count="1"
    newTmp param
    echo -n > "${param}"
    firstWord
    if [ "${top}" = "(" ] ; then
        deleteSkipCntrl
    else
        complain "${name}: expecting opening parenthesis"
    fi
    echo "/* local variables that mimik parameters */"
    while [ "${top}" ] ; do
        case "${top}" in
            ([\[\{\(]|\[\[)
                callBack >> "${param}"
                skipCntrl >>/dev/null
                continue
                ;;
            (,)
                echo >> "${param}"
                p=$(adjustParam < "${param}")
                echo -n > "${param}"
                report "fakeParams parameter is ${p}"
                echo "${p} = _CAPTURE_${numb}_${count};"
                count=$((${count} + 1))
                deleteSkipCntrl
                continue
                ;;
            (\))
                echo >> "${param}"
                p=$(adjustParam < "${param}")
                echo -n > "${param}"
                report "fakeParams parameter is ${p}"
                echo "${p} = _CAPTURE_${numb}_${count};"
                return
                ;;
        esac
        report "fakeParams seeing ${top} (${count})"
        popWord  >> "${param}"
        skipCntrl       >>/dev/null
        echo -n " "
    done
    echo -n " "
}

cppParams () {
    local top=
    local line=
    local count="1"
    newTmp param
    echo -n > "${param}"
    firstWord
    if [ "${top}" = "(" ] ; then
        deleteSkipCntrl
    else
        complain "${name}: expecting opening parenthesis"
    fi
    while [ "${top}" ] ; do
        case "${top}" in
            ([\[\{\(]|\[\[)
                callBack >> "${param}"
                skipCntrl >>/dev/null
                continue
                ;;
            (,)
                echo >> "${param}"
                p=$(adjustParam < "${param}")
                echo -n > "${param}"
                report "cppParams parameter is ${p}"
                echo "${p}, "
                count=$((${count} + 1))
                deleteSkipCntrl
                continue
                ;;
            (\))
                echo >> "${param}"
                p=$(adjustParam < "${param}")
                echo -n > "${param}"
                report "cppParams parameter is ${p}"
                echo "${p}"
                return
                ;;
        esac
        report "cppParams seeing ${top} (${count})"
        popWord  >> "${param}"
        skipCntrl       >>/dev/null
        echo -n " "
    done
    echo -n " "
}

appendCaptures () {
    local top=
    local line=
    firstWord
    if [ "${top}" != "[" ] ; then
        complain "${name}: expecting opening bracket"
    fi
    popWord
    echo
    skipCntrl       >>/dev/null
    case "${top}" in
        ([=\&])
            echo "/* default capture from lambda */"
            popWord
            echo ","
            skipCntrl       >>/dev/null
            if [ "${top}" = "," ] ; then
                deleteSkipCntrl
            fi
            ;;
    esac
    echo "/* value captures for the function arguments */"
    echo -n "_CAPTURE_${numb}_1 = (_ARG_${numb}_1)"
    p="2"
    while [ "${p}" -le "${params}" ] ; do
        echo ","
        echo -n "_CAPTURE_${numb}_${p} = (_ARG_${numb}_${p})"
        p=$(($p + 1))
    done
    report "appendCaptures seeing ${top}"
    if [ "${top}" = "]" ] ; then
        echo " ]"
    else
        echo ", "
        echo "/* remaining explicit captures from lambda */"
        while [ "${top}" ] ; do
            popWord
        done
    fi
}

fakeCaptures () {
    local top=
    local line=
    firstWord
    newTmp capture
    if [ "${top}" != "[" ] ; then
        complain "${name}: expecting opening bracket"
    fi
    deleteSkipCntrl
    case "${top}" in
        ([\&])
            echo "/* ignoring default identifier capture from lambda */"
            deleteSkipCntrl
            case "${top}" in
                (,)
                    deleteSkipCntrl
                    ;;
            esac
            ;;
        ([=])
            echo "/* Warning: faking default shaddow captures is not supported without lambdas */"
            deleteSkipCntrl
            case "${top}" in
                (,)
                    deleteSkipCntrl
                    ;;
            esac
            ;;
    esac
    echo "/* value captures for the function arguments */"
    echo "auto _CAPTURE_${numb}_1 = (_ARG_${numb}_1);"
    p="2"
    while [ "${p}" -le "${params}" ] ; do
        echo "auto _CAPTURE_${numb}_${p} = (_ARG_${numb}_${p});"
        p=$(($p + 1))
    done
    report "fakeCaptures seeing ${top}"
    if [ "${top}" != "]" ] ; then
        echo "/* remaining explicit captures from lambda */"
        while [ "${top}" ] ; do
            report "fakeCaptures seeing ${top} ${line}"
            case "${top}" in
                ([\[\{\(]|\[\[)
                    callBack >> "${capture}"
                    skipCntrl >>/dev/null
                    continue
                    ;;
                (,)
                    echo >> "${capture}"
                    c=$(${CAT} "${capture}")
                    echo -n > "${capture}"
                    report "capture is ${c}"
                    case "${c}" in
                        (*=*)
                            echo "/* mimiking value capture ${c} */"
                            n="${c%%=*}"
                            e="${c##*=}"
                            ;;
                        (\&*)
                            echo "/* ignoring identifier capture ${c} */"
                            deleteSkipCntrl
                            continue
                            ;;
                        (*)
                            echo "/* mimiking shaddow capture ${c} */"
                            n="${c}"
                            e="${c}"
                            ;;
                    esac
                    report "capture is ${c} ($n = $e)"
                    echo "auto _CAPTURE_${numb}_${p} = (${e});"
                    echo "auto ${n} = _CAPTURE_${numb}_${p};"
                    p=$(($p + 1))
                    deleteSkipCntrl
                    continue
                    ;;
                (\])
                    echo >> "${capture}"
                    c=$(${CAT} "${capture}")
                    echo -n > "${capture}"
                    report "capture is ${c}"
                    case "${c}" in
                        (*=*)
                            echo "/* mimiking value capture ${c} */"
                            n="${c%%=*}"
                            e="${c##*=}"
                            ;;
                        (\&*)
                            echo "/* ignoring identifier capture ${c} */"
                            deleteSkipCntrl
                            continue
                            ;;
                        (*)
                            echo "/* mimiking shaddow capture ${c} */"
                            n="${c}"
                            e="${c}"
                            ;;
                    esac
                    report "capture is ${c} ($n = $e)"
                    echo "auto _CAPTURE_${numb}_${p} = (${e});"
                    echo "auto ${n} = _CAPTURE_${numb}_${p};"
                    p=$(($p + 1))
                    deleteSkipCntrl
                    return
                    ;;
            esac
            report "fakeParams seeing ${top} (${count})"
            popWord  >> "${capture}"
            skipCntrl       >>/dev/null
            echo -n " "
        done
    fi
}

lambdaFind () {
    newTmp captures
    newTmp parameters
    newTmp body
    newTmp attributes
    newTmp macro
    newNumber numb

    # initialize the input stream
    local top=
    local line=
    firstWord
    while [ "${top}" ] ; do
        case "${top}" in
            ("[")
                report "collecting captures"
                ballancedBracket > "${captures}"
                echo            >> "${captures}"
                skipCntrl       >>/dev/null

                if [ "${top}" = "(" ] ; then
                    report "collecting parameters"
                    ballancedParen    > "${parameters}"
                    echo             >> "${parameters}"
                    skipCntrl        >>/dev/null
                fi

                # There can be several attributes that make up a whole
                # attributes clause
                echo -n               >"${attributes}"
                while [ "${top}" = "[[" ] ; do
                    report "collecting attributes"
                    ballancedParen   >> "${attributes}"
                    echo -n " "      >> "${attributes}"
                    skipCntrl        >>/dev/null
                done
                echo             >> "${attributes}"

                report "collecting body"
                echo -n           > "${body}"
                if [ "${top}" = "{" ] ; then
                    deleteWord
                else
                    complain "${name}: expecting opening brace for body"
                fi
                skipCntrl         >>/dev/null
                # collect body until we find the corresponding "}"
                while [ "${top}" ] ; do
                    case "${top}" in
                        (\})
                            break
                            ;;
                        ([\[\{\(]|\[\[)
                            callBack  >>"${body}"
                            ;;
                        (\#*)
                            report "skipping rest of line: ${top} ${line}"
                            popLine >>/dev/null
                            ;;
                        (*)
                            popWord   >>"${body}"
                            ;;
                    esac
                    skipCntrl >>/dev/null
                done
                echo                  >>"${body}"
                deleteSkipCntrl
                if [ "${top}" ] ; then
                    complain "${name}: extra token after lambda expression: ${top} ${line}"
                fi
                break
                ;;
            (\#*)
                popLine
                ;;
            (*)
                popToken
                ;;
        esac
    done
    echo "#ifdef NO_LAMBDAS"
    echo "#ifndef __cplusplus"
    cap="$(${CAT} ${captures})"
    echo "/* captures where ${cap} */"
    echo -n "static inline auto ${name} $(${CAT} ${parameters} ${attributes}) { "
    ${CAT} "${body}"
    echo " } "
    echo "#else"
    cap="$(${CAT} ${captures})"
    echo "/* captures where ${cap} */"
    echo -n "static inline auto ${name} "
    echo -n "(" $(cppParams < "${parameters}") ")"
    echo "$(${CAT} ${attributes}) { "
    ${CAT} "${body}"
    echo " } "
    echo "#endif"

    echo "#else"

    echo "#ifndef __cplusplus"
    echo "#pragma _HOLDBACK1 amend define ${name}"
    ${CAT} "${captures}"
    echo $(${CAT} "${parameters}" "${attributes}") " { "
    ${CAT} "${body}"
    echo " } "
    echo "#pragma _HOLDBACK1 done"

    echo "#else"
    echo "#pragma _HOLDBACK1 amend define ${name}"
    ${CAT} "${captures}"
    echo -n "(" $(cppParams < "${parameters}") ")"
    echo $(${CAT} "${attributes}") " { "
    ${CAT} "${body}"
    echo " } "
    echo "#pragma _HOLDBACK1 done"
    echo "#endif"

    echo "#endif"

    params=$(countParams < "${parameters}")
    report "generating macro for ${params}"

    echo "("                                      >"${macro}"
    appendCaptures < "${captures}"               >>"${macro}"
    echo "(void) $(${CAT} "${attributes}") {"    >>"${macro}"
    fakeParams < "${parameters}"                 >>"${macro}"
    echo                                         >>"${macro}"
    echo "/* specified lambda body */"           >>"${macro}"
    untokenize < "${body}"                       >>"${macro}"
    echo -n "})()"                               >>"${macro}"

    echo "#ifndef NO_LAMBDAS"
    echo -n "#pragma _HOLDBACK0 amend define ${name} "
    echo -n "_ARG_${numb}_1"
    p="${params}"
    while [ "${p}" -gt "1" ] ; do
        echo -n " _ARG_${numb}_${p}"
        p=$(($p - 1))
    done
    echo
    untokenize < "${macro}"
    echo
    echo "#pragma _HOLDBACK0 done"

    echo "({"                                       >"${macro}"
    fakeCaptures < "${captures}"                   >>"${macro}"
    fakeParams < "${parameters}"                   >>"${macro}"
    echo                                           >>"${macro}"
    echo "/* specified lambda body */"             >>"${macro}"
    ${SED} '1,/ return/ { s! return ! !1 ; p ; d } ; p ' "${body}"           \
        | ${SED} 's! return ! __too_many_return_statements_for_this_mode__ !g' \
        | untokenize >>"${macro}"
    echo -n "})"                                   >>"${macro}"

    echo "#elif defined(__GNUC__)"
    echo -n "#pragma _HOLDBACK0 amend define ${name} "
    echo -n "_ARG_${numb}_1"
    p="${params}"
    while [ "${p}" -gt "1" ] ; do
        echo -n " _ARG_${numb}_${p}"
        p=$(($p - 1))
    done
    echo
    untokenize < "${macro}"
    echo
    echo "#pragma _HOLDBACK0 done"

    echo "#else"
    echo "#pragma _HOLDBACK0 amend define ${name}"
    echo "${name}"
    echo "#pragma _HOLDBACK0 done"
    echo "#endif"
}

arguments name

report "Hello here is ${name}"

newTmp holdback

lambdaFind
