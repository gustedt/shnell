#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2020

## A tool to

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import echo
import tmpd
import ballanced
import tokenize

endPreamble $*

# Process a declaration or statement.
declstat () {
    local body
    newTmp body
    case "${top}" in
        (\{)
            popCntrl
            REC="$(( ${REC:-0} + 1 ))" compound
            skipCntrl
            ;;
        (\#*)
            popLine
            ;;
        (*)
            echo -n " { "
            block >"${body}"
            "${blockCallback}" <"${body}"
            skipCntrl
            echo -n " } "
            ;;
    esac
}

expression () {
    local nested=0
    while [ "${top}" ] ; do
        case "${top}" in
            (\#*)
                popLine
                ;;
            (\{)
                popCntrl
                REC="$(( ${REC:-0} + 1 ))" compound
                if [ "${nested}" -eq 0 -a "${parenthesis}" -eq 1 -a "${brackets}" -eq 0 -a "${initializer}" -eq 0 ] ; then
                    function=1
                    echo -n " /* end of function */ "
                    break
                fi
                skipCntrl
                ;;
            (\()
                if [ "${nested}" -eq 0 ] ; then
                    parenthesis=1
                fi
                nested="$(( ${nested} + 1))"
                popCntrl
                ;;
            (\[|\[\[)
                if [ "${nested}" -eq 0 ] ; then
                    brackets=1
                fi
                nested="$(( ${nested} + 1))"
                popCntrl
                ;;
            ([\)\]]|\]\])
                nested="$(( ${nested} - 1))"
                popCntrl
                if [ "${nested}" -eq 0 ] ; then
                    break
                fi
                ;;
            ([\;\,])
                if [ "${nested}" -eq 0 ] ; then
                    break
                fi
                popCntrl
                ;;
            (*)
                if [ "${nested}" -eq 0 ] ; then
                    case "${top}" in
                        ("=")
                            initializer=1
                            ;;
                    esac
                fi
                popCntrl
                ;;
        esac
    done
    skipCntrl
}

# Treat a block of C code that can be either a single statement, a
# declaration or a sequence of function definitions. This
# distinguishes control structures (if, switch, for, while, do) and
# collects their pieces. If dependend statements of these constructs
# are not compound statements, such a compound statement is added
# surrounding this statement.
#
# Other blocks are supposed to be expression statements or
# declarations that terminate with a semicolon.
#
# A block would not always terminate with a newline. To be able to
# launch a callback on each such block we need to add such a newline
# artificially at the end. All newlines that we create by this are
# then removed by a final sed command.
block () {
    skipCntrl
    local first="${top}"
    # After a `=` has been detected within a block at top level, a
    # `{}` expression cannot be the body of a function declaration.
    local initializer=0
    # To be a function body, a `{}` expression must be preceeded by
    # parenthesis
    local parenthesis=0
    # A function body is not preceeded by `[]` unless it is a lambda.
    local brackets=0
    # If we found a function, we have to return.
    local function=0
    case "${first}" in
        ("switch"|"for"|"while")
            popCntrl
            [ "${top}" != "(" ] && echo -n " /* missing parenthesis for \"${first}\" */ "
            expression
            declstat
            ;;
        ("if")
            popCntrl
            [ "${top}" != "(" ] && echo -n " /* missing parenthesis for \"${first}\" */ "
            expression
            declstat
            if [ "${top}" = "else" ] ; then
                popCntrl
                declstat
            fi
            ;;
        ("do")
            popCntrl
            declstat
            if [ "${top}" = "while" ] ; then
                popCntrl
                [ "${top}" != "(" ] && echo -n " /* missing parenthesis */ "
                expression
            else
                echo -n " /* missing \"while\" for \"do\" */ "
            fi
            if [ "${top}" = ";" ] ; then
                popCntrl
            else
                echo -n " /* missing semicolon after \"while\" ... \"do\" */ "
            fi
            ;;
        (\#*)
            popLine
            ;;
        (*)
            while [ "${top}" ] ; do
                case "${top}" in
                    ([\;\,])
                        popCntrl
                        break
                        ;;
                    (\#*)
                        popLine
                        ;;
                    (*)
                        expression
                        if [ "${function}" -eq 1 ] ; then
                            break
                        fi
                        ;;
                esac
            done
            ;;
    esac
    echo " /*__SEPEOL__*/ "
}

# Deal with the interior of a compound statement or the whole file.
# This cuts that contents recursively down into nested compound
# statements or into blocks.
#
# Here, more or less accidentally because of the weird syntax, a
# sequence of function definitions would appear as one block because
# there is no separating semicolon. Therefore we have to do some
# lifting to detect such function definitions.
compound () {
    local body
    newTmp body
    while [ "${top}" ] ; do
        case "${top}" in
            (\{)
                popCntrl
                REC="$(( ${REC:-0} + 1 ))" compound
                skipCntrl
                ;;
            (\})
                popWord
                return
                ;;
            (\#*)
                popLine
                ;;
            (*)
                block >"${body}"
                "${blockCallback}" <"${body}"
                ;;
        esac
    done
}

if [ ! "${blockCallback}" ] ; then
    blockCallback="${CAT}"
fi

newTmp all

firstWord > "${all}"
skipCntrl >>"${all}"
compound  >>"${all}"
skipCntrl >>"${all}"

# Remove the artificial newlines that we introduced for each block.
untokenize <"${all}" | ${SED} '
    :LOOP
    /\/\*__SEPEOL__\*\/[[:space:]]*$/{
      s![[:space:]]*\/\*__SEPEOL__\*\/[[:space:]]*! !g
      N
      # do not delete newlines before hashes
      /\n[[:space:]]*\#/!{
        s!\n!!
        b LOOP
      }
   }
' | tokenize
