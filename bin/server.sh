#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## Launch a server process that is connected to one or two pipes

### Usage

#server PROGRAM INPUT OUTPUT

# Where `PROGRAM` is a filter program that will listen on `INPUT` and
# write to `OUTPUT`. `INPUT` and `OUTPUT` should be filenames in the
# ${tmpd} or ${TMPD} directories. Named pipes will be enabled at these
# names, if they do not yet exist. If you just want to have these as
# temporary files and not as named pipes, you'd have to ensure that
# they already exist.

# One of `INPUT` or `OUTPUT` but not both may be an empty string. If
# `INPUT` is empty, this results in process that produces data on the
# `OUTPUT` named pipe. IF `OUTPUT` is empty, the process receives data
# through `INPUT` and does whatever it is designed to do with it.

### Example

#npipe="${tmpd}/npipe$$.pipe"
#server "sed 's![[:cntrl;]]! !g'" "${npipe}" ""
#exec >"${npipe}"

# establishes a filter process that removes all control characters
# from the stdout stream of the current process.

SRC="$_" . "${0%%/${0##*/}}/import.sh"
import tmpd

MKNOD=${MKNOD:=mknod}

endPreamble $*

server () {
    local serv="$1"
    if [ -n "$2" ] ; then
        test ! -r "$2" && ${MKNOD} "$2" p
        if [ -n "$3" ] ; then
            test ! -r "$3" && ${MKNOD} "$3" p
            report "switching input and ouput through pipe ${serv}" >&2
            eval "${serv} <'${2}' >'${3}' 2>'${3}' &"
        else
            report "switching input through pipe ${serv}" >&2
            eval "${serv} <'${2}' &"
        fi
    else
        if [ -n "$3" ] ; then
            test ! -r "$3" && ${MKNOD} "$3" p
            report "switching output through pipe ${serv}" >&2
            eval "${serv} >'${3}' 2>'${3}' &"
        else
            complain "at least an input or output pipe name must be given for ${serv}"
            exit 1
        fi
    fi
    report "process $!"
}
