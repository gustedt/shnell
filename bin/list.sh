#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2017

SRC="$_" . "${0%%/${0##*/}}/import.sh"
import match

endPreamble $*

assignInner () {
    eval "${listVar}='$*'"
}

append () {
    listVar="$1"
    shift
    eval "listCont=\${${listVar}}"
    assignInner ${listCont} "$*"
}

push () {
    listVar="$1"
    shift
    eval "listCont=\"\${${listVar}}\""
    assignInner $* "${listCont}"
}

popInner () {
    local listVar="$2"
    eval "$1=$3"
    shift 3
    assignInner "$*"
}

pop () {
    eval "listCont=\${$2}"
    popInner $1 $2 ${listCont}
}

revert () {
    eval "listCont=\${$1}"
    eval "$1="
    local el
    for el in ${listCont} ; do
        push $1 ${el}
    done
}
