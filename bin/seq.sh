#!/bin/sh

## print a sequence of numbers

### Usage:
# `$0 START END OPERATION`

# with `START` and `END` decimal numbers, possibly prefixed with `+`
# or `-`, and `OPERATION` of the form `@NUMBER`, where `@` is one of
# the operators `[-+*/]`.

### Description
# This prints the numbers

# `START`

# `START @NUMBER`

# `START @NUMBER @NUMBER`

# etc. that are strictly less (for operation `@` being `+` and `*`)
# respectively strictly greater (for `-` and `/`) than `END`.

# If `@` is omitted it defaults to '+'.

# If `OPERATION` is omitted it defaults to `+1`

# If `START` is also omitted it defaults to `0`.

### Examples:

# `5              &rarr; 0 1 2 3 4`

# `1 5 2          &rarr; 1 3`

# `3 -3 -1        &rarr; 3 2 1 0 -1 -2`

# `32 0 /2        &rarr; 32 16 8 4 2 1`

### Restrictions

# This tool is relatively picky. It should not accept anything else
# than numbers and the resulting range must not be empty.

SRC="$_" . "${0%%/${0##*/}}/import.sh"
import echo
import list

endPreamble $*

sequence () {

case $# in
    (1)
        start=0
        end="$1"
        inc="1"
        ;;
    (2)
        start="$1"
        end="$2"
        inc="1"
        ;;
    (3)
        start="$1"
        end="$2"
        inc="$3"
        ;;
    (*)
        echo "$0: expecting minimum 1 and maximum 3 arguments, having $# ($*) , aborting" 1>&2
        exit 1
        ;;
esac

case "${start} ${end}" in
    ([-+0-9]*\ [-+0-9]*)
        true
        ;;
    (*)
        echo "$0: first two arguments must be decimal numbers, have '${start} ${end}', aborting" 1>&2
        exit 1
        ;;
esac

comp='-lt'

case "${inc}" in
    ([0-9]*)
        op='+'
        ;;
    (+[0-9]*)
        op='+'
        inc="${inc#+}"
        ;;
    (-[0-9]*)
        comp='-gt'
        op='-'
        inc="${inc#-}"
        ;;
    (\*[0-9]*)
        op='*'
        inc="${inc#\*}"
        ;;
    (/[0-9]*)
        comp='-gt'
        op='/'
        inc="${inc#/}"
        ;;
    (*)
        echo "$0: wrong increment, have '${inc}', aborting" 1>&2
        exit 1
        ;;
esac

start="$((${start}))"
end="$((${end}))"
inc="$((${inc}))"

if [ ! "${start}" "${comp}" "${end}" ] ; then
    if [ ! "${CMOD_EMPTY}" ] ; then
        echo "$0: empty range $*, have '${start} ${comp} ${end}' operation '${op}${inc}', aborting" 1>&2
        exit 1
    fi
fi

while [ "${start}" "${comp}" "${end}" ]; do
    echo -n " ${start}"
    start=$((${start} ${op} ${inc}))
done

echo

}

if [ "$(rl $0)" = "${IMPORT_SOURCE_seq}" ] ; then
    sequence $*
fi
