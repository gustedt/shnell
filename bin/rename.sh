#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## Rename a module prefix

# This is intended to be used with the `module` directive. The
# `module` shnl file uses this with the name `alias`.

### Usage:
#pragma MODULE alias LONGNAME ABREV

# Where `LONGNAME` can be any longer module name and `ABREV` should be
# a simple name without `::`.

### Example:

#pragma MODULE	alias	ho::ho		hui
#typedef hui::type type;

# This will be replaced by

#typedef ho::ho::type type;

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import getnames
import arguments
import echo

endPreamble $*

MODSED="${MODSED:-${TMPD}/modsed.sed}"

rename () {
    report "renaming $2 to $1"
    echo "s! $2::! $1::!g" >> "${MODSED}"
    shnlpathRaw using                                           \
        && CMOD_AMEND_ARGUMENTS="$1" exec "${shnlpathRet}"
    return 1
}

arguments argv

rename ${argv}
