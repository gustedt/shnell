#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## Get the current git ID hash

# An `insert` directive that provides the bytes of the git hash for
# the source file as sequence of integer values. The result is a macro
# definition with the name that is passed to the directive.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

endPreamble $*

cat 1>&2

case "${CMOD_AMEND_ARGUMENTS}" in
    (*[[:blank:]]*)
        echo "#error cmod amendment gitID takes only one argument"
        ;;
    (*[[:alnum:]]*)
        res=$(git log --pretty=format:%H -- "${CMOD_SOURCE}" | head -1)
        if [ -z "${res}" ] ; then
            res="0000000000000000000000000000000000000000"
        fi
        echo "${res}" | sed "
  s![[:xdigit:]]\$!-&!
  s![[:xdigit:]][[:xdigit:]]!0x&, !g
  s!\([[:xdigit:]]\)-\([[:xdigit:]]\)!0x\1\2 !
  s!.*!#define ${CMOD_AMEND_ARGUMENTS} &!
"
        ;;
    (*)
        echo "#error cmod amendment gitID needs ID as an argument"
        ;;
esac
