#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## A tool to import headers from other TU implicitly.

### Usage:

#pragma CMOD amend implicit [LPREFIX] [ : [ PREFIX0 PREFIX1 ...] ]

# This directive will seek for all composed identifiers that are
# neither derived from the short nor the long prefix. These composed
# identifiers are used to extract the names of all TU that are
# implicitly used. Then it will look for a `.h` or `.o` file to
# include. The base name of such files is formed from the TU's prefix
# where the separators are replaced by `-`.

# For the role of the parameters and identifier naming conventions see
# `export`. Both are best used indirectly through the dialect `TRADE`
# and even better through the command line prefix `trade`.

### Functionality

# An identifier `A0::...::AN::AN1` that is encountered by scanning the
# source could come from two different other TU: `A0::...::AN::AN1`
# itself (if it represents the main feature of that TU) or
# `A0::...::AN` (if it is a secondary feature of that) . Both are
# searched in the current directory and `SHNELL_INCLUDE` if such a
# file exists.

# A list of legacy pseudo-TU can be kept for which it is assumed that
# they use top level identifiers for their symbols. Use this feature
# only if you must, notably for legacy interfaces that you can't
# change. The `stdc` prefix for the standard C library is always
# included in that list. Identifiers found in such a legacy realm are
# translated back to that toplevel form by a set of macros that is put
# in front of the source code. You may add legacy realms to the list
# by using the `legacy` directive in a scan of the source that
# preceeds the scan for `implicit`.

# It is the responsibility of the code using this feature to ensure
# that the symbols that are used in this way are really declared by
# the header file. A typical example would be `stdc::printf` which
# would trigger the integration of an include line

#`#include "stdc.h"`

# This include file then gives access to all the C library headers,
# where a function `printf` should indeed be provided.

### `main` is special

# With all of this you would never be able to produce a function with
# the bare name `main`, and thus you would never be able to produce a
# working executable. In fact, the name `main` itself is reserved by
# the C standard for the entry point of the program, and so you should
# define such a function as `stdc::main` instead of `main`. This
# guarantees that we do not rewrite that name, and that the compiler
# sees bare `main` as he should.

### `startup` and `atexit` handlers

# TU that use some resources in the backgroung may need to enable such
# resources at startup (e.g open a log file) or release them at exit
# time (e.g print a time stamp to a log file). This can be achived by
# instauring `startup` and `atexit` handlers, respectively.

### Makefile

# If the environment variables `SHNELL_MAKEFILE` is set to a directory
# name, this directive places a makefile, there, that reflects all the
# implicit dependencies that were found. If the file exist, it is only
# modified if there was a change in the dependencies.

## Implementation considerations

# We use sorting to have unique lists of symbols. Therefore we must
# ensure that the collating sequence of the locale is ignored.
export LC_COLLATE=C
SORT="${SORT:-sort}"
SORTUNIQ="${SORTUNIQ:--u}"

#

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import arguments
import tmpd
import tokenize
import match
import echo
import mangle
import join
import list

endPreamble $*

## The actual work of this shnell-module is done here.
#
implicit () {
    # the external separator that is used for exporting symbols of
    # this TU
    mesep="${SHNELL_SEPARATOR:-::}"

    report "export seeing $*"

    #checkNames $*

    # the internal separator that is used in this TU
    misep="::"
    tisep="${markc}"

    sname="${TMPD}/_Sname.txt"

    # the short name of the TU, in case of ambiguities this can be
    # used to prefix local names
    if [ "$1" -a ":" != "$1" ] ; then
        mname="$1"
        shift
        [ ":" = "$1" ] && shift
    fi

    # but we already have a name, that overwrites this.
    if [ -r "${sname}" ] ; then
        read -r mname < "${sname}"
    else
        [ "${mname}" ] && echo "${mname}" > "${sname}"
    fi

    lname="${TMPD}/_Legacy.txt"
    echo "stdc" >> "${lname}"

    # we need some temporary files
    local syms
    local tus
    local compos
    local sfile
    newTmp syms txt
    newTmp compos txt
    newTmp heads txt
    newTmp tus txt
    newTmp sedext txt
    newTmp sfile
    newTmp cfile
    newTmp mfile

    # Dump stdin to a file. This will be read several times.
    ${CAT} > "${sfile}"

    ###########################################################################
    # Start processing the remaining arguments, the list of prefix
    # components,
    all="$*"
    ppath="${all%%:*}"
    join "${misep}" ${ppath}
    prefix="${joinRet}"
    join "-" ${ppath}
    bname="${joinRet}"
    all=${all#*:}

    if [ "${prefix}" = "${misep}" ] ; then
        if [ "${CMOD_SOURCE}" ] ; then
            bname="${CMOD_SOURCE##*/}"
            bname="${bname%.c}"
            sj "-" "${misep}" ${bname}
            prefix="${joinRet}"
        else
            prefix="${mname}"
            bname="${mname}"
        fi
    fi

    # The local name defaults to the last component of the prefix, if
    # any.
    if ! [ "${mname}" ] ; then
        mname="${bname##*-}"
        echo "${mname}" > "${sname}"
    fi


    # Find all used identifiers
    "${SED}" "
s! \(${sedid}\(${tisep}${sedid}\)\{,\}\) !\n_IMPLICIT_IDENTIFIER \1\n!g
" "${sfile}" \
        | "${SED}" "-n" "
/^_IMPLICIT_IDENTIFIER ${mname}\(${tisep}\)\{,1\}/d
/^_IMPLICIT_IDENTIFIER ${prefix}/d
# print names from before
/^_IMPLICIT_IDENTIFIER/{
        s!^_IMPLICIT_IDENTIFIER !!
        p
        d
}
" >> "${syms}"

    join " " $(LC_ALL=C "${SORT}" "${SORTUNIQ}" "${syms}")
    echo "${joinRet}" > "${syms}"
    syms="${joinRet}"

    cat <<EOF >"${compos}"
stdc${tisep}size_t
stdc${tisep}puts
stdc${tisep}strcmp
EOF

    for n in ${syms} ; do
        case "${n}" in
            (${mname}|${mname}${tisep}*|${prefix}*)
                report "own: ${n}"
                ;;
            (_[A-Z_]*)
                report "reserved: ${n}"
                ;;
            (*${tisep}*)
                report "foreign: ${n}"
                echo "${n}" >>"${compos}"
                ;;
            (*)
                report "local: ${n}"
                ;;
        esac
    done

    join " " $(LC_ALL=C "${SORT}" "${SORTUNIQ}" "${compos}")
    echo "${joinRet}" > "${compos}"
    compos="${joinRet}"

    for n in ${compos} ; do
        report "handle: $n"
        checkMangle "${tisep}" "${mesep}" "${n}"
        maker="${maker}
s! ${n} ! ${mangleRet} !g"
    done

    join "|" $(LC_ALL=C "${SORT}" "${SORTUNIQ}" "${lname}")
    legacy="${joinRet}"
    legundef="#line 1 \"<${prefix} undef legacy macros>\""
    legdefine="#line 1 \"<${prefix} define legacy macros>\""

    echo "#line 1 \"<${prefix} implicit includes>\""
    for sym in ${compos} ; do
        compsep="${tisep}" splitlong ${sym}
        join "-" ${splitlongRet}
        mm="${joinRet}"
        echo "${mm}" >>"${tus}"
        if match "*-*" "${mm}" ; then
            leg="${mm%-*}"
            echo "${leg}" >>"${tus}"
            if match "${legacy}" "${leg}" ; then
                checkMangle "${tisep}" "${mesep}" "${sym}"
                legdefine="${legdefine}
#define ${mangleRet}\t${markl}${mm#*-}${markr}"
                legundef="${legundef}
#undef ${mangleRet}"
            fi
        fi
    done

    join " " $(LC_ALL=C "${SORT}" "${SORTUNIQ}" "${tus}")
    echo "${joinRet}" > "${tus}"
    tus="${joinRet}"

    if [ "${SHNELL_MAKEFILE}" ] ; then
        if [ -d "${SHNELL_MAKEFILE}" ] ; then
            cat <<EOF > "${mfile}"
`rp ${bname}.o` `rp ${SHNELL_MAKEFILE}/${bname}.mk` : `rp ${bname}.c`
EOF
            [ -d "SHNELL_INCLUDE" ] && cat <<EOF > "${mfile}"
`rp ${SHNELL_INCLUDE}/${bname}.h` : `rp ${bname}.c`
EOF
        else
            complain "make directory ${SHNELL_MAKEFILE} doesn't exist, aborting"
            exit 1
        fi
    fi
    depends=
    for tu in ${tus} ; do
        if ! [ "${tu}" = "${bname}" ] ;  then
            if [                                        \
                -r "${tu}.c"                            \
                -o -r "${tu}.h"                         \
                -o -r "${tu}.o"                         \
                -o -r "${SHNELL_INCLUDE}/${tu}.h"       \
                -o -r "${SHNELL_LEGACY}/${tu}.h"        \
                -o -r "${SHNELL_INCLUDE}/${tu}.o"       \
                ] ; then
                echo "#include \"${tu}.h\""
                append depends "${tu}"
            fi
            if [ -r "${tu}.c" -o -r "${SHNELL_INCLUDE}/${tu}.h" ] ; then
                cat <<EOF >> "${mfile}"
`rp ${bname}.o` : `rp ${SHNELL_INCLUDE}/${tu}.h`
EOF
            else
                if [ -r "${SHNELL_LEGACY}/${tu}.h" ] ; then
                    cat <<EOF >> "${mfile}"
`rp ${bname}.o` : `rp ${SHNELL_LEGACY}/${tu}.h`
EOF
                fi
            fi
        fi
    done
    loadfile="${TMPD}/_Load"
    if [ -s "${loadfile}" ] ; then
        join " " $(LC_ALL=C "${SORT}" "${SORTUNIQ}" "${loadfile}")
        echo "${joinRet}" > "${loadfile}"
        report "load dependencies: ${joinRet}"
        for f in ${joinRet} ; do
            cat <<EOF >> "${mfile}"
`rp ${bname}.o` `rp ${SHNELL_MAKEFILE}/${bname}.mk` : ${f}
EOF
        done
    fi

    # Only copy the makefile if there were changes.
    if [ -d "${SHNELL_MAKEFILE}" ] ; then
        mmfile="${SHNELL_MAKEFILE}/${bname}.mk"
        if [ ! -r "${mmfile}" -o "${mmfile}" -ot "${CMOD_SOURCE}" ]         \
               || ! cmp -s "${mfile}" "${mmfile}" ; then
            cp "${mfile}" "${mmfile}"
        fi
    fi

    echo "${legundef}"
    echo "${legdefine}"

    suname="${TMPD}/_Startup.txt"
    aename="${TMPD}/_Atexit.txt"
    newTmp cb
    ${CAT} <<EOF >>"${cb}"
#line 1 "<${prefix} generated startup>"
#define __startup __startup
#ifdef __STDC_NO_ATOMICS__
size_t __started = 0;
#else
atomic_flag __started = ATOMIC_FLAG_INIT;
#endif
inline void __startup(void) {
#ifdef __STDC_NO_ATOMICS__
     if (__started++) return;
#else
     if (atomic_flag_test_and_set_explicit(&__started, memory_order_relaxed)) return;
#endif
EOF
    for d in ${depends} ; do
        sj "-" "${misep}" ${d}
        checkMangle "${misep}" "${mesep}" "${joinRet}${misep}__startup"
        report "dependency ${d} resolves to ${mangleRet}, (${prefix})"
        ${CAT} <<EOF >>"${cb}"
#ifdef ${mangleRet}
                ${mangleRet}();
#endif
EOF
    done
    if [ -s "${aename}" ] ; then
        join " " $(LC_ALL=C "${SORT}" "${SORTUNIQ}" "${aename}")
        echo "${joinRet}" > "${aename}"
        ae="${joinRet}"
        for f in ${ae} ; do
            ${CAT} <<EOF >>"${cb}"
            atexit(${f});
EOF
        done
    fi
    if [ -s "${suname}" ] ; then
        join " " $(LC_ALL=C "${SORT}" "${SORTUNIQ}" "${suname}")
        echo "${joinRet}" > "${suname}"
        su="${joinRet}"
        for f in ${su} ; do
            ${CAT} <<EOF >>"${cb}"
                 ${mangleRet}();
EOF
        done
    fi
    ${CAT} <<EOF >>"${cb}"
}  /* __startup(void) */
EOF
    tokenize <"${cb}" >>"${sfile}"
    maker="
# Instrument the user main to launch the startup code
/ stdc${tisep}main /s! { ! {\n#line 1 \"<${prefix} startup trigger>\"\nextern void __startup ( void ) ;\n __startup ( ) ; !
${maker}
"
    ${SED} -e "${maker}" "${sfile}"

}

arguments argv

implicit ${argv}
