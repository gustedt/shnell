#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2020

## A tool to emulate lambda expressions in C by means of gcc's nested functions

# This implements a rewrite of C code with lambdas to C code using gcc features

# - nested functions

# - the `__typeof__` operator

# This implements function literals as normal function pointers, so
# these should also work outside the scope where the lambda expression
# is evaluated.
#
# Closures are also implemented as function pointers, but these point
# to a trampoline on the stack, so such a lambda value should never
# leave the scope of the evaluation.

# This rewrite can do a lot of things, but there are still some
# restrictions.  In particular, the lambda expression must be movable
# to the surrounding scope, so this may not work for lambda
# expressions within `for`-loop definitions, in a statement controlled
# by an `if` or similar where there are no scoping `{}`, or that are
# used to initialize a variable that is defined as subsequent
# definition after a comma.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

# Load the corresponding callback that prefixes a block with the
# definitions of its lambdas.
import lambdablock

endPreamble $*

# switch off the preamble mechanism
endPreamble () {
    true
}

export blockCallback=lambdaBlock

. ${EXEDIR}/normalize.sh

