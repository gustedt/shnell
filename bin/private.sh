#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## Make a list of variables private to the TU

### Usage:
#pragma CMOD amend private NAME0 NAME1 ...

# If you use the `TRADE` dialect (or the `trade` executable) you should use

#pragma TRADE private NAME0 NAME1 ...

# This registers local names in the argument list as being private
# such that they are not retrievable from other TU. `static` objects
# and functions are always private, so there is no need to used this
# feature here for them.

# You can use this to make a global symbol private that should not be
# used (by the name) by other TU, but that could be used indirectly
# and therefore need external linkage.

# This directive may appear multiple times in a TU, as long as the
# position is after the `export` directive, or uses the `TRADE`
# dialect.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import join
import arguments
import echo

endPreamble $*

stats="${TMPD}/_Stats.txt"

arguments argv

join "\n" ${argv}

echo "${argv}" >> "${stats}"
