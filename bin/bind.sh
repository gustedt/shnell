#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2018

## Directive to bind variables to values

# This directive expects arguments in the form

# `NAME0=VALUE0 [TOK0_0 ...] NAME1=VALUE1 [TOK1_0 ...] ...`

# (without special characters) and replaces all occurrences of
# `${NAME0}` (with the dollar and the braces) in the submitted text by
# `VALUE0 TOK0_0 ...` etc. So all tokens that do not contain an `=`
# sign are collected into a list of value tokens for `NAME0` etc. A
# token on the line that contains a `=` starts the list for the next
# meta-variable `NAME1` etc.

# All occurences of `#${NAME0}` (with the
# hash, the dollar and the braces) are replaced by
# `"VALUE0 TOK0_0 ..."`, that is the value put inside a C string.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import match
import arguments
import echo
import regVar

### See also:
### `do`
### `env`
### `foreach`
### `let`

endPreamble $*

prog=""
name=""

addProg () {
    # add an "s" rule to the sed program
    prog="${prog}
$(regVar $*)"
}

arguments args

if [ -z "${args}" ] ; then
    args="$*"
fi

for comb in ${args} ; do
    case "${comb}" in
        # each argument has the form NAME=VALUE
        (*=*)
            if [ -n "${name}" ] ; then
                addProg "${name}" ${val}
            fi
            name="${comb%%[[:blank:]=]*}"
            val="${comb#*[[:blank:]=]}"
            ;;
        # if not, this a trailing word to a previous NAME
        (*)
            val="${val} ${comb}"
            ;;
    esac
done

if [ -n "${name}" ] ; then
    addProg "${name}" ${val}
fi

exec ${SED} "${prog}"
