#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2018

## Generate sed expression to replace a meta-variable

# This produces a regular expression to evaluate different forms and
# surroundings of meta-variables given as `${NAME}`

# `#${NAME}` is stringification of the content

# `## ${NAME}` joins the content to the left

# `## ${NAME} ##` joins the content to the left and to the right

# `${NAME} ##` joins the content to the right

# `${NAME}` without any surrounding `#` or `##` operators is simple
# replacement with the contents

### String literal and character literal prefixes

# Sometimes we might have `${NAME}` representing a string or character
# prefix (such as `L`) as in `${NAME} ## "something"` or
# `${NAME} ## 'z'`. The prefix is then joined to the string literal to
# form a single string token `L"something"` or `L'z'`.

# Otherwise, remember that C string literals don't need concatenation:
# two string literals that directly follow each other are joined later
# by the preprocessor.

### Precedence

# In theory, `##` binds to the meta-variable that was defined
# first. E.g `${HEI} ## ${HOI}` binds to the variable `HOI` if that
# was defined in an outer scope (or previous in the same `bind`
# directive) than `HEI`. But under normal circumstances you should not
# notice this, the contents of the two variables should just be glued
# together.

# There is ambiguity for `${HEI}###${HOI}`. This stringifies `${HOI}`
# regardles of the order of definition and then glues `${HEI}` to it
# as a string prefix.

# There should never be more than three `#` in a row.

### Tokens and white space

# Generally, `shnell` pragmas are broken into tokens by white
# space. Sometimes it may be convenient to also include spaces or tabs
# into words, e.g if we want to assign a whole list of tokens to a
# meta-variable, or if some data naturally contains spaces. This can
# be achieved by placing a backspace character `\` in front of a space
# such as in `L\ u\ U\ u8` for a list of string prefixes, or as in
# `Elster,\ Frau` for a combination of data that belongs to a single
# item.

# Such escaped spaces behave differently when such a meta-variable is
# expanded:

# - If expanded directly, whitout stringification, the result is a
#   list of tokens that are split at the escaped spaces.

# - When stringified, the resulting string is still one single token.

### Examples

# with `${A}` containing `5`, `${NAME}` containing `top`, `${EXT}`
# containing `U`, `${FN}` containing `Elster\ ,\ Frau` we obtain

# <table>

# <tr><th>source</th><th>&nbsp;</th><th>replacement</th><th>&nbsp;</th><th></th></tr>

# <tr><td>`int a = ${A};`</td><td>&nbsp;</td><td>`int a = 5;`</td><td>&nbsp;</td><td>simple token</td></tr>

# <tr><td>`long a = ${A}L;`</td><td>&nbsp;</td><td>`long a = 5L;`</td><td>&nbsp;</td><td>variable and alnum, 1 token</td></tr>

# <tr><td>`long a = ${A}##L;`</td><td>&nbsp;</td><td>`long a = 5L;`</td><td>&nbsp;</td><td>same with `##`, 1 token</td></tr>

# <tr><td>`unsigned u = ${A}${EXT};`</td><td>&nbsp;</td><td>`unsigned u = 5 U;`</td><td>&nbsp;</td><td>invalid C, 2 token</td></tr>

# <tr><td>`unsigned u = ${A}##${EXT};`</td><td>&nbsp;</td><td>`unsigned u = 5U;`</td><td>&nbsp;</td><td>`##` necessary, 1 token</td></tr>

# <tr><td>`var${A}`</td><td>&nbsp;</td><td>`var5`</td><td>&nbsp;</td><td>alnum and variable, 1 token</td></tr>

# <tr><td>`var ## ${A}`</td><td>&nbsp;</td><td>`var5`</td><td>&nbsp;</td><td>same with `##`, 1 token</td></tr>

# <tr><td>`var ${A}`</td><td>&nbsp;</td><td>`var 5`</td><td>&nbsp;</td><td>invalid C, 2 token</td></tr>

# <tr><td>`${NAME}${A}`</td><td>&nbsp;</td><td>`top 5`</td><td>&nbsp;</td><td>invalid C, 2 token</td></tr>

# <tr><td>`${NAME} ${A}`</td><td>&nbsp;</td><td>`top 5`</td><td>&nbsp;</td><td>invalid C, 2 token</td></tr>

# <tr><td>`${NAME} ## ${A}`</td><td>&nbsp;</td><td>`top5`</td><td>&nbsp;</td><td>`##` necessary, 1 token</td></tr>

# <tr><td>`char s[] = #${NAME};`</td><td>&nbsp;</td><td>`char s[] = "top";`</td><td>&nbsp;</td><td>stringification</td></tr>

# <tr><td>`wchar s[] = L#${NAME};`</td><td>&nbsp;</td><td>`wchar s[] = L"top";`</td><td>&nbsp;</td><td>string prefix, 1 token</td></tr>

# <tr><td>`char32_t su[] = ${EXT}## #${NAME};`</td><td>&nbsp;</td><td>`char32_t su[] = U"top";`</td><td>&nbsp;</td><td>`##` necessary, 1 token</td></tr>

# <tr><td>`char s[] = #${FN};`</td><td>&nbsp;</td><td>`char s[] = "Elster , Frau";`</td><td>&nbsp;</td><td>stringification, 1 token</td></tr>

# <tr><td>`enum { ${FN} };`</td><td>&nbsp;</td><td>`enum { Elster , Frau };`</td><td>&nbsp;</td><td>3 token, including comma</td></tr>

# </table>

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import echo
import join

endPreamble $*

regVar () {
    name="$1"
    shift
    report "// \${${name}} -> $*"
    sj "${markib}${markit} 	" " " $*
    # produce "s" rules for the sed program
    Echo "

# First look at triple #, if we find them that is stringification
# prefixed with a join operation that can only be to the left.
s!\([[:blank:][:cntrl:]]*\#[[:blank:][:cntrl:]]*\#[[:blank:][:cntrl:]]*\)\#[[:blank:][:cntrl:]]*\$[[:blank:][:cntrl:]]*{[[:blank:][:cntrl:]]*${name}[[:blank:][:cntrl:]]*}!\1${marksl}$*${marksr}!g

# Now look at a join operation that is followed by a string. This
# should be a prefix that is joined to the string.
s!\$[[:blank:][:cntrl:]]*{[[:blank:][:cntrl:]]*${name}[[:blank:][:cntrl:]]*}[[:blank:][:cntrl:]]*\#[[:blank:][:cntrl:]]*\#[[:blank:][:cntrl:]]*\(${marksl}\|${markcl}\)!\1${joinRet}${markpr}!g

# A join operation before the meta-variable and than another optional
# join thereafter.
s![[:blank:][:cntrl:]]*\#[[:blank:][:cntrl:]]*\#[[:blank:][:cntrl:]]*\$[[:blank:][:cntrl:]]*{[[:blank:][:cntrl:]]*${name}[[:blank:][:cntrl:]]*}\([[:blank:][:cntrl:]]*\#[[:blank:][:cntrl:]]*\#[[:blank:][:cntrl:]]*\)\{,1\}!${joinRet}!g

# A join operation thereafter.
s!\$[[:blank:][:cntrl:]]*{[[:blank:][:cntrl:]]*${name}[[:blank:][:cntrl:]]*}[[:blank:][:cntrl:]]*\#[[:blank:][:cntrl:]]*\#[[:blank:][:cntrl:]]*!${joinRet}!g

# A separator before the meta-variable and another thereafter.
s![[:blank:][:cntrl:]]*:[[:blank:][:cntrl:]]*:[[:blank:][:cntrl:]]*\$[[:blank:][:cntrl:]]*{[[:blank:][:cntrl:]]*${name}[[:blank:][:cntrl:]]*}[[:blank:][:cntrl:]]*:[[:blank:][:cntrl:]]*:[[:blank:][:cntrl:]]*!${markc}${joinRet}${markc}!g

# A separator before the meta-variable
s![[:blank:][:cntrl:]]*:[[:blank:][:cntrl:]]*:[[:blank:][:cntrl:]]*\$[[:blank:][:cntrl:]]*{[[:blank:][:cntrl:]]*${name}[[:blank:][:cntrl:]]*}!${markc}${joinRet}!g

# A separator after the meta-variable
s!\$[[:blank:][:cntrl:]]*{[[:blank:][:cntrl:]]*${name}[[:blank:][:cntrl:]]*}[[:blank:][:cntrl:]]*:[[:blank:][:cntrl:]]*:[[:blank:][:cntrl:]]*!${joinRet}${markc}!g

# A stringification
s!\#[[:blank:][:cntrl:]]*\$[[:blank:][:cntrl:]]*{[[:blank:][:cntrl:]]*${name}[[:blank:][:cntrl:]]*}!${marksl}$*${marksr}!g

# A simple evalution
s!\$[[:blank:][:cntrl:]]*{[[:blank:][:cntrl:]]*${name}[[:blank:][:cntrl:]]*}!${joinRet}!g"
}
