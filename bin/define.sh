#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2020

## A directive to put all depending code into a macro definition on a logical line, joined by terminating backslashes

### Usage:

#pragma CMOD amend define NAME [PARAM1, [, PARAM2 ... ] ]

# With this you may place the body of a macro definiton inside a
# source file and delay its usage by the processor to later
# compilation phases.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import arguments
import echo

endPreamble $*

doDefine () {
    name="$1"
    shift
    if [ $# -eq 0 ] ; then
        echo "#define ${name}"
    else
        echo -n "#define ${name}($1"
        shift
        while [ $# -ne 0 ] ; do
            echo -n ", $1"
            shift
        done
        echo ")"
    fi
    ${CAT}
}

arguments args

doDefine ${args} | ${EXEDIR}/logicalline.sh

