#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2017

com_plain () {
    echo "${IMPORT_PATH}: $*" 1>&2
}
level=1
com_bash () {
    echo "${IMPORT_PATH}:${FUNCNAME[${level}]}:${BASH_LINENO[$((${level}-1))]}: $*" 1>&2
}

if [ -z "${BASH_VERSION}" ] ; then
    alias complain=com_plain
else
    shopt -s expand_aliases
    alias complain=com_bash
fi

timing_show () {
    level=$((${level}+1))
    complain `date -Ins` "$*"
    level=$((${level}-1))
}

if [ -n "${CMOD_TIMING}" ] ; then
    alias timing=timing_show
else
    alias timing=true
fi

if [ -z "${CMOD_VERBOSE}" ] ; then
    alias report=true
else
    alias report=complain
fi

# We expect echo to accept -n as an argument and to transscribe
# backslash escape sequences. If this is not the case we use the
# following fallback. This still would not be completely portable,
# because the %b option for printf may interpret some escape sequences
# differently.
echoFallback () {
    if [ $# = 0 ] ; then
        printf "\n"
    else
        case "$1" in
            (-n)
                shift
                printf "%b" "$*"
                ;;
            (*)
                printf "%b\n" "$*"
                ;;
        esac
    fi
}

# If echo doesn't know about the -n option, the test will result in
# "-n a" plus the newline character(s).
test="`echo -n a`"
if [ ${#test} -gt 1 ] ; then
    alias echo=echoFallback
else
    # If echo accepts the "-e" argument add it
    if [ "`echo -n -e`" != "-e" ] ; then
        alias echo="echo -e"
    fi
    alias Echo="/bin/echo -E"
    # Ensure that echo interprets escaped characters such as \t as
    # special.
    test=`echo "\t"`
    if [ ${#test} -eq 2 ] ; then
        alias echo=echoFallback
    fi
fi
