#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## Implement C2x' new attribute syntax

# This relies on legacy methods provided by common compilers such as
# `gcc`'s `__attribute__`.

# You should provide your idea of the legacy compiler to use through
# the environment variable `ATTRIBUTES`. This should provide the
# implementation specific prefix and is used to guess the
# implementation-specific syntax that implement attributes.

# We currently support the following values:

# - `gnu` is the GNU compiler `gcc`. It supports all current standard
#    attributes as well as large number of implementation specific
#    ones. The syntax is rewritten to something like
#    `__attribute__((tokens))`.

# - `clang` is treated a superset to `gnu`.

# - `std` is meant for compilers that don't need rewriting, because
#   they fully implement the attribute feature. Attribute tokens are
#   rewritten to the form that has leading and trailing double
#   underscores. Syntactically correct implementation-specific
#   attributes are otherwise passed through. Other unknown attributes
#   are removed.

# - `portability` is the same as `std` but all implementation-specific
#   attributes are removed.

# - `gsl` is for the Microsoft compilers. The support the standard
#   attributes but not much more. The resulting syntax is
#   `__declspec`.

SRC="$_" . "${0%%/${0##*/}}/import.sh"
import join
import tokenize
import ballanced
import list
import tmpd

endPreamble $*

markAttr () {
    cumul=$(cat)
    echo -n "/*attribute*/${cumul}/*end*/"
}

markParen () {
    cumul=$(cat)
    echo -n "/*parenthesis*/${cumul}/*end*/"
}

std2gnu="
s![[:blank:][:cntrl:]]*deprecated!__gnu__${markc}__deprecated__!g
s![[:blank:][:cntrl:]]*maybe_unused!__gnu__${markc}__unused__!g
s![[:blank:][:cntrl:]]*fallthrough!__gnu__${markc}__fallthrough__!g
s![[:blank:][:cntrl:]]*nodiscard!__gnu__${markc}__warn_unused_result__!g
"

gnuSyntax="
s!^[[:blank:][:cntrl:]]*gnu[[:blank:][:cntrl:]]*${markc}!__gnu__${markc}!g
# delete attributes that are not known to us
/^[[:blank:][:cntrl:]]*__gnu__/!d
s![[:blank:][:cntrl:]]*__gnu__[[:blank:][:cntrl:]]*${markc}\(__[[:alnum:]_]*__.*\)!__attribute__((\1))!g
s![[:blank:][:cntrl:]]*__gnu__[[:blank:][:cntrl:]]*${markc}[[:blank:][:cntrl:]]*\([[:alnum:]_]*\)\(.*\)!__attribute__((__\1__\2))!g
"

std2clang="${std2gnu}"
clangSyntax="
s!clang!gnu!g
${gnuSyntax}
"

std2std="
s![[:blank:][:cntrl:]]*deprecated\(.*\)![[__deprecated__\1]]!g
s![[:blank:][:cntrl:]]*maybe_unused\(.*\)![[__maybe_unused__]\1]]!g
s![[:blank:][:cntrl:]]*fallthrough\(.*\)![[__fallthrough__]]!g
s![[:blank:][:cntrl:]]*nodiscard![[__nodiscard__]]!
"

std2portability="${std2std}"
portabilitySyntax="
/\[\[/!d
"

stdSyntax="
/${markc}/{
        s![[:blank:][:cntrl:]]*__\([[:alnum:]_]\{1,\}\)__[[:blank:][:cntrl:]]*${markc}!\1${markc}!
        s!${markc}[[:blank:][:cntrl:]]*__\([[:alnum:]_]\{1,\}\)__!${markc}\1!
        s!\([[:alnum:]_]\{1,\}\)${markc}\([[:alnum:]_]\{1,\}\)!__\1__${markc}__\2__!
        s!.*![[&]]!
}
${portabilitySyntax}
"


std2gsl="
s![[:blank:][:cntrl:]]*deprecated!__gsl__${markc}__deprecated__!g
s![[:blank:][:cntrl:]]*maybe_unused!__gsl__${markc}__unused__!g
s![[:blank:][:cntrl:]]*fallthrough!__gsl__${markc}__fallthrough__!g
s![[:blank:][:cntrl:]]*nodiscard!__gsl__${markc}__warn_unused_result__!g
"

gslSyntax="
s!^[[:blank:][:cntrl:]]*gsl[[:blank:][:cntrl:]]*${markc}!__gsl__${markc}!g
# delete attributes that are not known to us
/^[[:blank:][:cntrl:]]*__gsl__/!d
s![[:blank:][:cntrl:]]*__gsl__[[:blank:][:cntrl:]]*${markc}\(__[[:alnum:]_]*__.*\)!__declspec(\1)!g
s![[:blank:][:cntrl:]]*__gsl__[[:blank:][:cntrl:]]*${markc}[[:blank:][:cntrl:]]*\([[:alnum:]_]*\)\(.*\)!__declspec(__\1__\2)!g
"

case "${ATTRIBUTES:=gnu}" in
    (*[![:alnum:]_]*)
        complain "ATTRIBUTES has invalid value"
        ;;
    (*)
        eval "attributesStd=\${std2${ATTRIBUTES}}"
        eval "attributesSyntax=\${${ATTRIBUTES}Syntax}"
        ;;
esac

if [ -z "${attributesSyntax}" ] ; then
    # If we don't know the attribute syntax, delete
    attributesSyntax="d"
fi

report "std: ${attributesStd}"

report "syn: ${attributesSyntax}"

processAttr () {
    ${SED} "
s!^[[:blank:][:cntrl:]]*\[[[:blank:][:cntrl:]]*\[!!g
s!\][[:blank:][:cntrl:]]*\][[:blank:][:cntrl:]]*!!g
${attributesStd}
${attributesSyntax}
"
}

popWord

callbackAttr=processAttr
#callbackParen=markParen
ballanced
