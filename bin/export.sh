#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## A tool to introduce prefix naming conventions.

### Usage:

#pragma CMOD amend export [ LPREFIX ] [ : [ PREFIX0 PREFIX1 ... ] ]

# with the following definitions:

# - `LPREFIX` is the local name prefix. All identifiers that are
#   prefixed with `LPREFIX::` in their declarations (or that are just
#   this `LPREFIX`) are considered to have external linking, unless
#   they are `static` objects or functions or they are explicitly made
#   private, see below. (Example: `string::length` for the combination
#   of `string` with `::` and `length`.)

# - `PREFIXx` are identifiers that are used to compose the external
#   name prefix.

# All of these have suitable defaults and this directive is best used
# indirectly together with the `implicit` directive through the
# `TRADE` dialect or the `trade` compiler prefix.

### Defaults

# None of the identifiers used above must be strictly reserved,
# that is none of
#LPREFIX PREFIX0 PREFIX1 ...
# shall start with an underscore that is followed by a capital letter
# or a second underscore.

# Any of the two parts above may be empty:

# - If expanded in a scan prior to this one, the `alias` directive can
#   be used to set `LPREFIX`.

# - If there is no `PREFIXx`, the filename is split at `-`
#   characters.

# - If the filename is not known, `LPREFIX` is used.

# - If `LPREFIX` is empty, the last component of `PREFIX0 PREFIX1 ...`
#   is used.

# So if neither `LPREFIX` or `PREFIXx` are given (directly or via
# `alias`) the filename must be known and is used to determine all
# naming conventions.

### Linkage of identifiers

# Not concerned by this tool are objects, functions and types that are
# just used (including tag names) but not declared or defined. In
# particular types that are just forward declared (such as in `struct
# toto;`) . For them we suppose that naming issues are taken care of
# by whichever TU defines them.

# There are several categories of local identifiers, that is
# identifiers that are *defined* in the current TU:

# 0 Some globally exposed identifiers without linkage are unchanged by
#   this tool. Per default these are `struct` or `union` members,
#   function parameters and `struct`, `union` or `enum` tags that are
#   only declared but not defined. If you want to bless them with
#   linkage, you'd have to use one of the methods above to force it.

# 1 Identifiers with internal linkage. Per default these are all
#   global `static` objects or functions. Identifiers can be added to
#   that by using the `private` directive.

# 2 Identifiers with external linkage. Per default these are all
#   declared global objects and functions that are not `static`. To
#   that are added all other identifiers for which short or long
#   identifier variants (see below) are used.

# 3 If not made external by one of the rules above, `typedef` names,
#   macros and enumeration constants have internal linkage. The same
#   holds for `struct`, `union` and `enum` tags for types that are
#   defined within this TU.

# If an identifier could have internal or external linkage by (1) or
# (2), internal linkage prevails. This is so that you may make an
# identifier private that otherwise would have external linkage. To
# simplify the use of such an internal identifer you are still able to
# use the short form, it is rewritten to the internal form.

# All identifiers that have internal linkage by these rules are
# "private", those that have external linkage are "public". Those with
# no linkage in (0) are in a gray zone, but since we force privacy on
# all macros, there should be no bad interaction between foreign
# macros and such local identifiers.

### Composite identifiers

# There are several types of identifiers that are dealt by this tool:

# - *Local identifiers* are usual global C identifiers that are
#   defined within this TU. They have no linkage, if none of the
#   above provided some.

#   Without linkage can only be members or function
#   parameters. Variables, functions, macros, types and enumeration
#   constants always have linkage.

# - *Short identifiers* are composed of two parts, `LPREFIX` and a
#   local identifier `ID`, that are joined by the `::`. Declaring a
#   local identifier that is not `static` or explicitly private with
#   such a form automatically elevates it to have external linkage in
#   the whole TU. If you prefix, *e.g*, an enumeration constant in its
#   definition with the local prefix, it becomes globally visible and
#   usable by others without creating naming conflicts. For the whole
#   TU, the local identifier `ID` can be used instead of the short
#   identifier `LPREFIX::ID`, and is replaced by it accordingly. Then,
#   for the output of this tool, all short identifiers are replaced by
#   the corresponding long identifiers.

# - *Long identifiers* come in several flavors, for external linkage,
#   internal linkage, and may be different as used inside the code and
#   as they are visible to the outside. Seen from inside the TU, for
#   external linkage an `ID` is prefixed with the list `PREFIX0`,
#   `PREFIX1` ... and the parts are joined with `::`. As for short
#   idenfiers this form in the declaration of the identifiers makes
#   `ID` an identifier with external linkage, and the short and local
#   forms can be used interchangeably. To make an external symbol from
#   a long identifier, `::` is replaced by `SHNELL_SEPARATOR` and,
#   unless `SHNELL_SEPARATOR` is "`::`", this is how long identifiers
#   are seen by the outside. E.g if `SHNELL_SEPARATOR` is `_`,
#   `test::string::length` is presented to the outside as
#   `test_string_length`.

#   If `SHNELL_SEPARATOR` is in itself "`::`", the name is mangled in
#   a strategy similar to C++, see below.

#   There are also long identifiers to present internal linkage to the
#   outside. The rules are similar as for those with external linkage,
#   only that things are added to the list of prefixes, such that the
#   long identifier becomes obfuscated. You may use these identifiers
#   only with their short or local form.

# A unique choice of prefixes for the TU within the same project
# guaranties that such an identifier can never clash with another one
# in the same project.

# As mentionned, the separator that will be used for joining external
# name components is `SHNELL_SEPARATOR`. In the case of the special
# token `::` the output names are also mangled. Mangling is done
# according to the `mangle` shnell-module. In particular, the prefix
# `manglePrefix` is used from there. There are special conventions:

#    - `::`  C++ mangling

#    - `_`   snail_case_identifiers

#    - `C`   camelCaseIdentifiers

#    - `P`   PascalCaseIdentifiers

# Evidently you'd have to be careful that your identifiers fit with
# the convention you are using. For `C` and `P` only components that
# start with a lower case letter and do not contain an underscore are
# transformed. In particular, components that start with an underscore
# are left alone.

# This defaults to "`::`", but can
# be set by the corresponding environment variable.
export SHNELL_SEPARATOR="${SHNELL_SEPARATOR:-::}"

### `main` is special

# As for traditional C, the entry point `main` can be treated
# specially. If you name a function `stdc::main` (which you will
# probably do if you also use `implicit`) the following strategy is
# applied:

# - Your function is still compiled as `main` (without prefix) such
#   that the compiler may apply the special treatment that is reserved
#   for that. E.g not returning a value from the function is not an
#   error.

# - The function symbol with the local name `main` is made "weak" such
#   that it does not clash with similar functions from other TU that
#   may be linked together.

# - A public symbol with the long name for `main` is aliased to your
#   function.

# - The header that is produced for the TU has an entry for that long
#   name.

# All of this allows you to have one entry point per TU without
# creating conflicts. This is particularly useful to implement a test
# program for your TU in place.

## Header files

# To determine the identifiers that are defined in the program, a
# header file is produced and stored in a directory named by the
# `SHNELL_INCLUDE` environment variable, if any:

# - Objects or functions that are declared or defined `static` are
#   suppressed, unless they are `static` `inline` functions.

# - Function bodies of functions that are not `inline` are removed and
#   replaced by a `;` that terminates the declaration.

# - Function bodies of functions that are `inline` are kept, so they
#   appear in the header *and* in the output. In the output the
#   `inline` keyword is removed such that when compiling the output,
#   the function is instantiated.

# - Initializers for objects are removed, such that only the
#   declaration remains.

# - All function and object declarations that are not `static` are
#   prefixed with `extern`. Whereas this would be the default for
#   functions anyhow, for objects this would otherwise create
#   "tentative definitions", that could provoque linkage problems.

# - All defined identifiers (functions, objects, macros, type tags,
#   typenames, enumeration constants) are renamed to their long form
#   in the whole header.

# - If the code defines a `stdc::main` function, the declaration of it
#   is removed and replaced by an equivalent one using the long name
#   for `main`.

# - The header is protected by an include guard.

# - The header file is only replaced if it had changes.

## Examples:

### Without mangling and a simple universal prefix

# `SHNELL_SEPARATOR` equal to "`_`" and
#pragma CMOD amend export : string : b

# This defines a TU where the naming convention is independent of the
# filename.  All global, non-`static`, variable and function names are
# externally visible to have a `string_` prefix, if they don't have
# one, yet. If in addition, we have three identifiers (`EMPTY`,
# `INIT`, and `GET`) that are forced to be public (e.g by using
# `string::EMPTY`, `string::INIT`, and `string::GET` in their
# definition) they have external names (`string_EMPTY`, `string_INIT`,
# and `string_GET`) but within this TU the local names (`EMPTY`,
# `INIT`, and `GET`) or short names may be used just the same.

# In addition to all statically defined identifiers there is the
# identifier `b` that is forced to be internal. Again, within this TU
# it can be accessed as `string::b` or `b`, but externally it has a
# name that is something weird, hidden by name obfuscation.

# One additional identifier is special, `string` itself. With the
# setting as described here, it is left alone. This identifier should
# generally be reserved for the principal feature of this TU, such as
# the central data type or function that is defined in the TU. It is
# always external.

### Without mangling and a short and long prefix

# `SHNELL_SEPARATOR` equal to "`_`" and
#pragma CMOD amend export string : strong type : b

# Again, this defines a TU where the naming convention is independent
# of the filename.  All global, non-`static`, variable and function
# names are augmented to interally have a `strong::type::` prefix and
# externally to have a `strong_type_` prefix, if they don't have one,
# yet. The three identifiers (`EMPTY`, `INIT`, and `GET`) as above,
# are forced to have external names, so `strong_type_EMPTY`,
# `strong_type_INIT`, and `strong_type_GET`, but within this TU the
# long names (`strong::type::EMPTY`, `strong::type::INIT`, and
# `strong::type::GET`), short names (`string::EMPTY`, `string::INIT`,
# and `string::GET`) and local names (`EMPTY`, `INIT`, and `GET`) may
# be used just the same.

# As above, in addition to all statically defined identifiers there is
# the identifier `b` that is forced to be internal. Again, within this
# TU it can be accessed as `string::b` or `b`, but externally it has a
# name that is something weird, but distinguishable from the other
# weird form that `b` would have in the previous setting.

# Again, `string` itself is special. Within the TU, it is left alone,
# but to the outside it is visible as `strong::type`, and that name
# can also be used internally just as `string`.

# This naming scheme can also be made dependent on the source
# filename. If that would be `strong-type.c`, the `strong type` part
# above could be omitted.

### With mangling

# If `SHNELL_SEPARATOR` is equal to "`::`" the internal names are
# exactly the same as above. The outside visible forms are mangled by
# using the `PREFIXx` components. For the `strong type` example this
# would result in something like `_ZN2_C6strong4type5EMPTYE`,
# `_ZN2_C6strong4type4INITE`, and `_ZN2_C6strong4type3GETE`, but you
# should not care much. Within this TU the short, long and local names
# (`EMPTY`, `INIT`, and `GET`) may be used just the same as above.

## Implementation considerations

# We use sorting to have unique lists of symbols. Therefore we must
# ensure that the collating sequence of the locale is ignored.
export LC_COLLATE=C
SORT="${SORT:-sort}"
SORTUNIQ="${SORTUNIQ:--u}"

#

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import arguments
import tmpd
import tokenize
import match
import echo
import mangle
import join
import hash
import header
import keywords

# &nbsp;

endPreamble $*

#
### check if the identifier components are conforming
#
checkNames () {
    ret=
    while [ "$*" ] ; do
        if match "_[A-Z_]*" "$1" ; then
            echo "name $1 starting with underscore and capital letter or other underscore is not valid, aborting" >&2
            ret=1
        fi
        if match "${keywordsSH}" "${$1}" ; then
            complain "name '${1}' is a keyword, aborting"
            ret=1
        fi
        shift
    done
    [ "${ret}" ] && exit 1
}

addRegxps () {
    report "starting addRegxps: $*"
    local syms="$1"
    local sedext="$2"
    local state="$3"
    local restriction="$4"
    local misep="$5"
    local mesep="$6"
    join " " $(LC_ALL=C "${SORT}" "${SORTUNIQ}" "${syms}")
    echo "${joinRet}" > "${syms}"
    syms="${joinRet}"
    if [ "private" = "${restriction}" ] ; then
        obfusc="_Intern${misep}"
    else
        obfusc=
    fi
    for sym in ${syms} ; do
        report "symbol: ${sym}"
        match "*[]\$\{\}\(\)\;\,\.+\*\<\>=/#:\?\|\&\^-]*|*\[*|*-*" "${sym}" && continue
        if [ "${sym}" = "${stdcmain}" ] ; then
            echo "#line 1 \"<${prefix} entry point main>\""
            echo "#pragma weak main"
            checkMangle "${misep}" "${mesep}" "${prefix}${misep}main"
            echo "#pragma weak ${mangleRet}=main"
            hashin "${state}" "main" 1
        else
            # Only mangle if this is not a reserved name and if it is not
            # yet mangled.
            if ! match "_[[:upper:]]*" "${sym}" ; then
                report "local: ${sym}"
                case "${sym}" in
                    (${mname}${tisep}*)
                        sym="${sym#${mname}${tisep}}"
                        report "found short ${sym}"
                        ;;
                    (${prefix}${tisep}*)
                        sym="${sym#${prefix}${tisep}}"
                        report "found long ${sym}"
                        ;;
                esac
                if ! match "*${tisep}*" "${sym}" ; then
                    report "still local: ${sym}"
                    hashout ans "${state}" "${sym}" ""
                    if ! [ "${ans}" ] ; then
                        if match "${keywordsSH}" "${sym}" ; then
                            [ "${restriction}" = "public" ] && complain "Identifier '${sym}' is a keyword."
                            continue
                        fi
                        append "${restriction}" "${sym}"
                        hashin "${state}" "${sym}" 1
                        checkMangle "${misep}" "${mesep}" "${obfusc}${prefix}${misep}${sym}"
                        [ "${sym}" != "${mangleRet}" ] && Echo "s!\(${boundary}\)${sym}\(${boundary}\)!\1${mangleRet}\2!g ; t RESTART" >> "${sedext}"
                        [ "${mname}${tisep}${sym}" != "${mangleRet}" ] && Echo "s!\(${boundary}\)${mname}${tisep}${sym}\(${boundary}\)!\1${mangleRet}\2!g ; t RESTART" >> "${sedext}"
                    fi
                fi
            else
                if [ "${restriction}" = "public" ] ; then
                    complain "Warning: found foreign symbol ${sym}, making it weak."
                    echo "/* found foreign symbol ${sym}, making it weak */"
                    echo "#pragma weak ${sym}"
                fi
            fi
        fi
    done

}

# A class to detect word boundaries. Within this class the ']'
# character must be first and the '-' character must be last.
boundary='[]+*/%^|&<>=,.;:?(){}\[[:blank:]-]'

### The actual work of this shnell-module is done here.
# (It can't have the name `export` because that is a shell keyword.)
#
compile () {
    # the external separator that is used for exporting symbols of
    # this TU
    mesep="${SHNELL_SEPARATOR}"

    report "export seeing $*"

    checkNames $*

    # the internal separator that is used in this TU
    misep="::"
    tisep="${markc}"

    stats="${TMPD}/_Stats.txt"
    types="${TMPD}/_Types.txt"
    sname="${TMPD}/_Sname.txt"

    # the short name of the TU, in case of ambiguities this can be
    # used to prefix local names
    if [ "$1" -a ":" != "$1" ] ; then
        mname="$1"
        shift
    fi
    [ ":" = "$1" ] && shift

    # but we already have a name, that overwrites this.
    if [ -r "${sname}" ] ; then
        read -r mname < "${sname}"
    else
        [ "${mname}" ] && echo "${mname}" > "${sname}"
    fi

    report "export for '${mname}' '${misep}' '${mesep}' '${prefix}'"

    ###########################################################################
    # Start processing the remaining arguments.
    all="$*"
    ppath="${all%%:*}"
    join "${misep}" ${ppath}
    prefix="${joinRet}"
    join "-" ${ppath}
    bname="${joinRet}"

    if [ "${prefix}" = "${misep}" ] ; then
        if [ "${CMOD_SOURCE}" ] ; then
            bname="${CMOD_SOURCE##*/}"
            bname="${bname%.c}"
            sj "-" "${misep}" ${bname}
            prefix="${joinRet}"
        else
            prefix="${mname}"
            bname="${mname}"
        fi
    fi

    # The local name defaults to the last component of the prefix, if
    # any.
    if ! [ "${mname}" ] ; then
        mname="${bname##*-}"
        echo "${mname}" > "${sname}"
    fi

    if match "${keywordsSH}" "${mname}" ; then
        complain "******* Short name '${mname}' for this TU is a keyword."
        complain "******* Consider using the 'alias' directive to assign another short name."
        complain "******* Aborting."
        exit 1
    fi
    report "short name is ${mname}"

    # we need some temporary files
    newTmp syms txt
    newTmp defs txt
    newTmp sedext txt
    newTmp sfile
    newTmp cfile
    newTmp heads h

    tokenize <<EOF >"${sfile}"
/* bogus comment to circumvent a bug in the header generation */
#line 1 "<${prefix} static definitions of strings>"
//#pragma weak __UNIT__
static char const __UNIT__[] = "${prefix}";
static char const __SEPARATOR__[] = "${mesep}";
EOF

    # Dump stdin to a file. This will be read several times.
    ${CAT} >> "${sfile}"

    ###########################################################################
    # Find all names that are prefixed with the local TU name
    "${SED}" "s!\(${boundary}\)\(${mname}${tisep}\)!\1\n\2!g" "${sfile}" \
        | "${SED}" "-n" "/^${mname}${tisep}/{ s!^${mname}${tisep}\([[:alnum:]_]*\).*!\1! ; p }" >> "${syms}"

    echo -n >>${stats}
    echo -n >>${syms}

    # Global identifiers are extracted while producing the header.
    header "${defs}" "${syms}" "${stats}" "${types}" < "${sfile}" >"${heads}"

    # Find all macros, tags and similar
    "${SED}" "
# This must be the definition of a tag name.
s!\(struct\|union\|enum\)[[:blank:]-]\{1,\}\([[:alnum:]_]\{1,\}\)\([[:blank:]-]*{\)!\n__TAGNAME__ \2\n\3!g
" "${sfile}" \
        | "${SED}" "-n" "
# capture macro definitions, and print the name
/^[[:blank:]-]*\#[[:blank:]-]*define[[:blank:]-]\{1,\}\([[:alnum:]_]\{1,\}\)/{
        s!^[[:blank:]-]*\#[[:blank:]-]*define[[:blank:]-]\{1,\}\([[:alnum:]_]\{1,\}\).*!\1!g
        p
        d
}
# print tag names from before
/^__TAGNAME__ \([[:alnum:]_]\{1,\}\)/{
        s!^__TAGNAME__ !!
        p
        d
}
" >> "${defs}"

    ###########################################################################
    # Start to create the file with the regexps for sed.
    Echo "
# don't replace inside #include or #line directives
/^[[:blank:]]*\#[[:blank:]]*\(include\|line\)/{ p ; d }
:RESTART
" > "${sedext}"

    # Now write replacement expresssion to the sed skript, and, at the
    # same time keep track of identifiers that are `private` and
    # `public`.
    private=
    public=
    seen=

    # add a replacement rule for the short name
    checkMangle "${misep}" "${mesep}" "${prefix}"
    [ "${mname}" != "${mangleRet}" ] && Echo "s!\(${boundary}\)${mname}\(${boundary}\)!\1${mangleRet}\2!g ; t RESTART" >> "${sedext}"

    # The short prefix is special and will always be external.
    hashin seen "${mname}" 1

    # Compute the mangled name for main
    checkMangle "${misep}" "${mesep}" "stdc${misep}main"
    stdcmain="${mangleRet}"

    # Create regular expressions for all identifiers with internal
    # linkage that we found. These have higher priority than
    # identifiers with external linkage.
    addRegxps "${stats}" "${sedext}" "seen" "private" "${misep}" "${mesep}"

    # Create regular expressions for all identifiers with external
    # linkage that we found. These only take effect for identifiers
    # that have not been tagged explicitly with internal linkage, yet.
    addRegxps "${syms}" "${sedext}" "seen" "public" "${misep}" "${mesep}"

    # Create regular expressions for all other local identifiers that
    # we found and make them internal.
    addRegxps "${defs}" "${sedext}" "seen" "private" "${misep}" "${mesep}"

    newTmp hheads

    if [ -s "${types}" ] ; then
        echo "#line 1 \"<${prefix} forward declarations of types>\""  >>"${hheads}"
        ${SED} -e "
/\(^struct\|^union\)/{
        s!\(^struct\|^union\) \(.*\)!${markl} typedef ${markr} ${markl} \1 ${markr} ${markl} \2  ${markr} ${markl} \2 ${markr}${markl} ; ${markr}!
        p
}
d
" "${types}" | ${SED} -f "${sedext}" >>"${hheads}"
        ${CAT} "${hheads}"
        join " " $(${SED} -e "s!.*! & ! ; s! \(enum\|struct\|union\) ! !g" -f "${sedext}" "${types}")
        types="${joinRet}"
    fi

    # do the replacement and dump it to stdout
    "${SED}" -f "${sedext}" -e "s! inline ! !g" "${sfile}"

    # also apply the replacement to the header and untokenize it
    "${SED}" -f "${sedext}" "${heads}" >>"${hheads}"

    checkMangle "${misep}" "${mesep}" "${prefix}${misep}__guard"
    gname="${mangleRet}"
    checkMangle "${misep}" "${mesep}" "${prefix}${misep}main"
    lmain="${mangleRet}"
    ${CAT} <<EOF >"${heads}"
// START EXPORTED HEADER ${prefix}
#ifndef ${gname}
#define ${gname} 1
#line 1 "<pre ${prefix} header>"
EOF

    "${SED}" -i -e "
/^[[:blank:]]*\#.*${stdcmain}/d
s!${stdcmain}!${lmain}!g" -f "${sedunmark}" "${hheads}"

    if [ "${STYLE}" ] ; then
        eval "${STYLE}" <"${hheads}" >>"${heads}"
    else
        ${CAT} "${hheads}" >>"${heads}"
    fi

    ${CAT} <<EOF >>"${heads}"
#line 1 "<post ${prefix} header>"
#endif // ${gname}
// END EXPORTED HEADER ${prefix}
EOF

    ###########################################################################
    # sort the list of private symbols
    join "\n" ${private}
    join " " $(echo ${joinRet} | LC_ALL=C "${SORT}" "${SORTUNIQ}")
    private="${joinRet}"

    # Also dump strings containing the external symbols and similar
    # information. Therefore we need some exportable names.
    checkMangle "${misep}" "${mesep}" "${prefix}${misep}__private"
    iname="${mangleRet}"
    checkMangle "${misep}" "${mesep}" "${prefix}${misep}__public"
    ename="${mangleRet}"
    checkMangle "${misep}" "${mesep}" "${prefix}${misep}__separator"
    sname="${mangleRet}"
    checkMangle "${misep}" "${mesep}" "${prefix}${misep}__unit"
    pname="${mangleRet}"
    checkMangle "${misep}" "${mesep}" "${prefix}${misep}__header"
    hname="${mangleRet}"
    checkMangle "${misep}" "${mesep}" "${prefix}${misep}__main"
    mname="${mangleRet}"
    checkMangle "${misep}" "${mesep}" "${prefix}${misep}__types"
    tname="${mangleRet}"
    checkMangle "${misep}" "${mesep}" "${obfusc}${prefix}${misep}__UNIT__"
    uname="${mangleRet}"
    checkMangle "${misep}" "${mesep}" "${obfusc}${prefix}${misep}__SEPARATOR__"
    Sname="${mangleRet}"
    echo "
#line 1 \"<export ${prefix}>\"
char const ${iname}[] = \"private: ${private}\";
char const ${ename}[] = \"public: ${public}\";
char const ${sname}[] = \"separator: ${mesep}\";
char const ${pname}[] = \"unit: ${prefix}\";
char const ${tname}[] = \"types: ${types}\";
char const ${hname}[] = \"// don't remove this empty line${marksb}n\"
\"${marksb}n\"
" | tokenize
    # Now stringify the whole header and append it to the generated
    # code.
    "${SED}" "
s!${markbb}!${markbb}${markbb}!g
s!${marksb}\|[\\]!${markbb}!g
s!${marksm}\|\"!${marksb}${marksm}!g
s!\"\|${marksl}\|${marksr}!${marksb}${marksm}!g
# eat spaces at beginning and end of the line
s!\(${markib}\|${markit}\|[[:blank:]]\)\{1,\}\$!!
s!^\(${markib}\|${markit}\|[[:blank:]]\)\{1,\}!!
s!.*!${marksl}&${marksb}n${marksr}!
" "${heads}"
    echo ";"

    # Generate a potential `main` that can be used to extract the
    # export information.
    ${CAT} <<EOF
// Make these weak symbols such that we have directly accessibly
// names, but no conflict occurs between different TU when they are linked
// together.
#pragma weak _EXPORTMAIN=${mname}
int ${mname}(int argc, char* argv[argc+1]) {
    int ret = 1;
    extern int puts(const char*);
    extern int strcmp(const char*, const char*);
    // Ensure that the compile does not warn about these.
    (void)${uname};
    (void)${Sname};
    //
    for (int i = 1; i < argc; ++i) {
EOF
    fields="
            header
            unit
            private
            public
            separator
            types
"
    for n in ${fields} ; do
    checkMangle "${misep}" "${mesep}" "${prefix}${misep}__${n}"
    ${CAT} <<EOF
    if (strcmp(argv[i], "$n")) {
       puts(${mangleRet});
       ret = 0;
    }
EOF
    done
    ${CAT} <<EOF
    }
    return ret;
}
EOF

    # Only copy the header file if there were changes.
    if [ -d "${SHNELL_INCLUDE}" ] ; then
        hheads="${SHNELL_INCLUDE}/${bname}.h"
        if [ ! -r "${hheads}" -o "${hheads}" -ot "${CMOD_SOURCE}" ]         \
               || ! cmp -s "${heads}" "${hheads}" ; then
            cp "${heads}" "${hheads}"
        fi
    fi


}

arguments argv

compile ${argv}
