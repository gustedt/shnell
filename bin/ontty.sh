#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## Trick a program into assuming that it is run in a terminal.

### Usage:

#import ontty
#...
#ontty program arguments ...

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import echo

MKNOD=${MKNOD:=mknod}

# Found the solution of intercepting the `isatty` call here:
# https://unix.stackexchange.com/a/273412/11934

# It consists in compiling a dummy function and creating a shared library.
#echo "int isatty(int fd) { return 1; }" | c99 -O2 -fpic -shared -ldl -o isatty.so -xc -

# This should usually be built via the `Makefile`.

endPreamble $*

ontty () {
    shnlpathRaw isatty.so

    if [ ! -x "${shnlpathRet}" ] ; then
        complain "can't find isatty.so, aborting"
        exit 1
    fi

    TERM=xterm LD_PRELOAD="${shnlpathRet}" $*
}
