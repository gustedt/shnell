#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## A directive to put all depending code on one line

### Usage:

#pragma CMOD amend oneline

# The primary use case for this is to compose macro definitions such
# as in

#pragma CMOD amend oneline
#\#define ARRAY
#(double){
#pragma CMOD amend do I = 3
#[${I}] = ${I},
#pragma CMOD done
#}
#pragma CMOD done

# This should be rewritten to something like

#\#define ARRAY (double){ [0] = 0, [1] = 1, [2] = 2, }

# Preprocessor directives within the text that remain after previous
# expansions are treated specially. A line directive on the first line
# is just transmitted, all other preprocessor directives are removed.

# Just as in normal macro definitions, the subject code must not have
# C++ style comments.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import match

endPreamble $*

${SED} "
       /[[:blank:]]*\#[[:blank:]]*line/n
       :AGAIN
       \$,\$ n
       N
       s!\n[[:blank:]]*\#.*!!
       s!\n! !
       b AGAIN
"
