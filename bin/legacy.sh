#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## Use other TU in legacy mode

# This is intended to be used with the `implicit` directive.

# A list of legacy pseudo-TU is kept for which it is assumed that they
# use top level identifiers for their symbols. Use this feature only
# if you must, notably for legacy interfaces that you can't
# change.

# The `stdc` prefix for the standard C library is always included in
# that list. Identifiers found in such a legacy realm are translated
# back to that toplevel form by a set of macros that is put in front
# of the source code. You may add legacy realms to the list by using
# the `legacy` directive in a scan of the source that preceeds the
# scan for `implicit`.

# For such a realm you then have to provide a header file similar to
# the `stdc.h` file that is included with `shnell`. Suppose e.g we use
# a fictive legacy project `POSIX` that has a type `struct stat` in
# our TU `test::export`. Then the following line establishes that
# realm:

#pragma TRADE legacy POSIX

# and later the type can be used as

#struct POSIX::stat filestat;


# At the very end, this is translated back into a direct use of the
# plain `struct`

#struct stat test::export::filestat;

# and implicitly an `#include` line similar to the following is
# automatically generated:

#`#include "POSIX.h"`

# That resolved identifier `stat` will never be in conflict with
# identifiers from the TU hat uses it; local identifier `stat` would
# always be mangled as private or public identifier before
# `POSIX::stat` is translated back. So we could even have used `stat`
# instead of `filestat`

#struct POSIX::stat stat;

# And the result would have been

#struct stat test::export::stat;

# respectively the mangled form of all of this.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import arguments
import echo
import tmpd
import join

endPreamble $*

legfunc () {
    lname="${TMPD}/_Legacy.txt"
    echo "stdc" >> "${lname}"
    join "\n" $*
    echo "${joinRet}" >> "${lname}"
}

arguments argv
legfunc ${argv}

