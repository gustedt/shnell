#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2020

## Implement generic selection for C and C++
#
# Usage:
##pragma CMOD amend generic_selection EXPR NAME PAR1 [ PAR2 ... ]
#typeA: resultA
#typeB: resultB
#...
##pragma CMOD done
#
# where `EXPR` is an expression that uses the parameter names, all
# types must not be pairwise compatible and not array or function
# types, and where one type may be the keyword `default` to indicate
# the default choice. Each choice has to be on a single line, extend
# lines with a backslash if necessary.

# Any later occurence of `NAME(ARG1, ARG2, ...)` is then evaluated at
# compile time for the *type* of `EXPR` as formed from `ARG1, ARG2,
# ...` and the result expression corresponding to the type is then
# inserted in place. The effect is as if the chosen result had been
# put in place from the start, and the overall result has all the
# properties, of being a constant expression, for example, if the
# original one is a constant expression.

# The result is a snippet that is `#ifdef`'d w.r.t `__cplusplus` to
# allow that this feature can be uniformly used from C or C++.

# The C part is just a macro with a `_Generic` expression.

# The C++ part is a series of overloaded template functions (which are
# `constexpr`) that are used to dispatch the expression according to
# the type. Here care must be taken that the macro parameters are not
# evaluated if they don't need to. We try to automatically detect, if
# a macro parameter is used in one of the `resultX` cases. If, not,
# the macro argument is guaranteed not to be evaluated.


SRC="$_" . "${0%%/${0##*/}}/import.sh"

import echo
import match
import arguments
import tmpd
import join

endPreamble $*

selection () {
    local expr="$1"
    local name="$2"
    shift
    shift
    local n="1"
    local paras
    local parat
    local parap
    local parar
    local sedrep
    for p in $* ; do
        if [ -n "${paras}" ] ; then
            paras="${paras}, "
            parat="${parat}, "
            parap="${parap}, "
            parar="${parar}\\|"
        fi
        paras="${paras}_T${n} ${p}"
        parat="${parat}typename _T${n}"
        parar="${parar}${p}"
        parap="${parap}(_CORE_PARAM_${p})"
        case "${p}" in
            (_NOEVAL*)
                true
                ;;
            (*)
                sedrep="${sedrep}
s!\<${p}\>!\n_CORE_PARAM_${p}\n!g"
                ;;
        esac
        n="$((${n} + 1))"
    done
    #
    join ", " $*
    local paral="${joinRet}"
    local numb=
    local cases="0"
    local sour=
    newTmp cpp .cc
    newTmp c .c
    newTmp rep .sed
    echo -n "#define ${name}(${paral}) \
_Generic((${expr})"  >>"${c}"
    while read -r line ; do
        case "${numb}" in
            (*[0-9]*)
                numb="$((${numb} + 1))"
                lineno="#line ${numb} ${sour}"
                ;;
        esac
        case "${line}" in
            (\#*)
                startline="${line}"
                echo "${line}" >>"${cpp}"
                case "${line}" in
                    (\#line*)
                        numb="${line###line }"
                        numb="${numb%%\"*}"
                        sour="${line###line ${numb}}"
                        report "found line number ${numb} and file ${sour}"
                    ;;
                esac
                ;;
            (*)
                cases="$((${cases}+1))"
                local type="${line%%:*}"
                local what="${line#*:}"
                echo ",\\"             >>"${c}"
                echo -n "\t${line}" >>"${c}"
                # Check if the choosable contains one of the parameters
                echo "${what}"                          \
                    | ${SED} -n "
                # replace all occurences by tokens on their own line
                /${parar}/{
                  ${sedrep}
                  p
                }"                                      \
                    | ${SED} -n "
               # now ignore all other lines and create a substitution regexp for it
               /^_CORE_PARAM_/{
                  s!^_CORE_PARAM_\(.*\)!s@&@\\1@g!
                  p
               }" >> "${rep}"

                case "${type}" in
                    (*default*)
                        ${CAT}<<EOF >>"${cpp}"
${lineno}
template< typename _T0, ${parat} >
#line ${numb} ${sour}
constexpr auto ${name}(_T0, ${paras}) {
#line ${numb} ${sour}
           return ${what};
}
EOF
                        ;;
                    (*)
                        ${CAT}<<EOF  >>"${cpp}"
template< ${parat} >
#line ${numb} ${sour}
constexpr auto ${name}(generic_type((${type}){}), ${paras}) {
#line ${numb} ${sour}
           return ${what};
}
EOF
                        ;;
                esac
        esac
    done
    echo "s@_CORE_PARAM_\\([_[:alnum:]]*\\)@(generic_type(\\\\1)){}@g" >> "$rep"
    #complain "------------------------------------\nreplacement for ${name}:"
    evaluated=$(${SED} "
/generic_type/d
s!.*@\([_[:alnum:]]*\)@g!\1!" "${rep}" | sort -u)
    found=
    for var in ${evaluated} ; do
        append found "${var}"
    done
    if [ -n "${found}" ] ; then
        evaluated="Other then that, the following arguments may be evaluated for the result: ${found}"
    else
        evaluated="None of the arguments is evaluated to compute the result, either."
    fi
    # Create the interface macro and protect parameters against unnecessary evaluation
    ${CAT}<<EOF | ${SED} -f "${rep}" >>"${cpp}"
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< ${parat} >
${startline}
constexpr auto ${name}(${paras}) {
${startline}
           return ${name}((generic_type(${expr})){}, ${parap});
}
/**
 ** The second is the principal user interface and is implemented as macro ${name}.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** ${evaluated}
 **/
${startline}
#define ${name}(${paral}) ${name}((generic_type(${expr})){}, ${parap})
EOF
    echo "#ifdef __cplusplus"
    echo "// A series of ${cases} overloaded interfaces with ${n} parameters that "
    echo "// trigger generic selection by the type of expression '${expr}'"
    echo "// passed as their first parameter, plus two dispatch interfaces of"
    echo "// the same name with $((${n}-1)) parameters, that is without that first parameter."
    ${CAT} "${cpp}"
    echo "#else"
    if [ -n "${PHASE2}" ] ; then
        echo "${startline}"
    fi
    ${CAT} "${c}"
    echo ")"
    echo "#endif"
}

arguments args
selection ${args}
