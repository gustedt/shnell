#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2018

## rudimentary command line processing

# this detects
# - `-o` for the target
# - `-c`, `-E`, `-M`, `-S` for the type of job to do
# - `-x`, usually the programming language
# - `-L` for libraries
# - `-static`

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import list

endPreamble $*

cmdline () {
    argv=""
    target=""
    source=""
    extensions="$1"
    shift
    while [ $# -gt 0 ]; do
        case $1 in
            -[cEMS])
                if [ -n "${job}" ]; then
                    echo "Warning: $1 set, but job is already ${job}, ignoring" >&2
                else
                    job="$1"
                fi
                ;;
            -o)
                shift
                target="$1"
                ! match '-*' "${target}" \
                    || die "Error: ${target} is not a valid argument to -o\nAborting compilation"
                ;;
            -o*)
                target="${1#-o}"
                ! match '-*' "${target}" \
                    || die "Error: ${target} is not a valid argument to -o\nAborting compilation"
                ;;
            -L)
                library="$(realpath ${2})"
                append lpath "${library}"
                append argv "-L${library}"
                argv="${argv##[ ]}"
                report "asking for ld library ${library}: ${argv}"
                shift
                ;;
            -L*)
                library="$(realpath ${1#-L})"
                append lpath "${library}"
                append argv "-L${library}"
                argv="${argv##[ ]}"
                report "asking for ld library ${library}: ${argv}"
                ;;
            -x)
                append argv "-x $2"
                argv="${argv##[ ]}"
                shift
                ;;
            -static)
                append argv "$1"
                argv="${argv##[ ]}"
                static="1"
                ;;
            -*)
                append argv "$1"
                argv="${argv##[ ]}"
                ;;
            *[.]${extensions})
                append source "$1"
                source="${source##[ ]}"
                ;;
            *)
                append argv "$1"
                argv="${argv##[ ]}"
                ;;
        esac
        shift
    done
}
