#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## Echo the arguments to this directive as C comments, but ignore it
## otherwise.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

endPreamble $*

ech () {
    echo "// $@"
}

ech ${CMOD_AMEND_ARGUMENTS}
