# Normalize the pragma directives
s!^[[:blank:][:cntrl:]]*#[[:blank:][:cntrl:]]*pragma!#pragma!
/^#pragma/{
        s![[:blank:][:cntrl:]]*::[[:blank:][:cntrl:]]*!::!g
        #s![[:blank:][:cntrl:]]\{1,\}! !g
        #s![[:cntrl:]]\{1,\}!!g
}

