#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## A semi-toy example that evaluates integer constant expressions that
## are based on decimal values.

# This directive spans the whole text that it amends for integer
# constant expresssion consisting of decimal integers and the
# operators `+`, `-`, `*`, `/`, `%`, `<<`, and `>>`.

# To make this really useful, we would have to know how to handle `(`
# and `)`, which we don't, yet.

SRC="$_" . "${0%%/${0##*/}}/import.sh"
import join
import tokenize
import list
import tmpd

endPreamble $*

compute () {
    expression=
    last=
    while read -r line ; do
        for w in ${line} ; do
            if [ -z "${expression}" ] ; then
                case "${w}" in
                    ([\!])
                        # A prefix negation operator may start an
                        # expression
                        expression="${w}"
                        last="${w}"
                        ;;
                    # Any word that has a non-digit is just echoed to
                    # stdout
                    (*[![:digit:]]*)
                        echo -n "${w} "
                        ;;
                    (*)
                        # The first decimal number starts a new
                        # sequence
                        expression="${w}"
                        last="${w}"
                        ;;
                esac
            else
                case "${w}" in
                    # Markers from the tokenizer are just ignored
                    (${markl}|${markr})
                        true
                        ;;
                    (${markr}${markl})
                        true
                        ;;
                    # Identify our operators
                    (\<\<|\>\>|\&\&|\|\||[-+!*/%^\|\&])
                        append expression "${w}"
                        last="${w}"
                        ;;
                    # Any word that has a non-digit is the end of the sequence.
                    (*[![:digit:]]*)
                        # If the new word "${w}" is an identifier or
                        # similar, it shortcuts the evaluation of the
                        # expression. We have to remove the last
                        # operator, if any.
                        case "${last}" in
                            ([-+*/%!^\|\&])
                                expression="${expression%[-+*/%!^|&]}"
                                echo -n "$((${expression})) "
                                echo -n "${last} "
                                ;;
                            (\<\<|\>\>|\&\&|\|\|)
                                expression="${expression%${last}}"
                                echo -n "$((${expression})) "
                                echo -n "${last} "
                                ;;
                            (*)
                                echo -n "$((${expression})) "
                                ;;
                        esac
                        # empty it
                        expression=
                        last=
                        # "${w}" itself is just echoed to stdout
                        echo -n "${w} "
                        ;;
                    # The remaining are decimal integers
                    (*)
                        append expression "${w}"
                        last="${w}"
                        ;;
                esac
            fi
        done
        # Echo a newline character at the end of the current input
        # line, but only if there is not an open expression
        [ -z "${expression}" ] && echo
    done
    if [ -n "${expression}" ] ; then
        # Don't forget to output the value of a trailing
        # expression. (But in a real C program, this can't happen.)
        echo "$((${expression}))"
    fi
}

compute
