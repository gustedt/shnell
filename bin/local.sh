#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## Make a set of identifiers locally unique

### Usage:

#pragma CMOD amend local NAME0 ...

# Replaces the the indentifiers `NAMEx` by a unique identifier,
# each. This is to prevent that some auxiliarry variable names clash
# because multiple copies of the same code is used.


#

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import arguments
import tmpd
import echo
import mangle

# &nbsp;

endPreamble $*

#

localfunc () {
    count="${TMPD}/_Local"
    newTmp regexp .sed
    if [ -r "${count}" ] ; then
        read -r c < "${count}"
    else
        c=0
    fi
    obfusc="_local${c}"
    echo "$((${c} + 1))" >"${count}"
    echo -n "#pragma CMOD insert private"
    for f in $* ; do
        F="__${f}${obfusc}"
        echo "s! ${f} ! ${F} !g" >>"${regexp}"
        echo -n " ${F}"
    done
    echo
    #${CAT} "${regexp}" >&2
    ${SED} -f "${regexp}"
}

arguments argv

localfunc ${argv}
