#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2017

## Implement an import function.

#endPreamble $*

# This needs an include guard. Later sources that are used with
# "import" don't need such include guards.
if [ -z "${IMPORT_PATH}" ] ; then
    WE="${WE:-${0##*/}}"
    export EXEDIR="${EXEDIR:-${0%%/${WE}}}"
    WEUP=$(echo "${WE}" | sed "y/abcdefghijklmnopqrstuvwxyz/ABCDEFGHIJKLMNOPQRSTUVWXYZ/")
    #echo "starting exedir for ${WE} is ${EXEDIR}" >&2
    IMPORT_PATH="${IMPORT_PATH:-${WE}}"

    Source () {
        _="$1" . $1
    }


    rp () {
        case "$1" in
            (/*)
                fn="$1"
                ;;
            (*)
                fn=`pwd`"/$1"
                ;;
        esac
        echo -n "${fn}" | sed "
s!/\([.]/\)\{1,\}!/!g
:restart
s!/\{2,\}!/!g
/[.][.]/{
        s!/[^/]\{1,\}/[.][.]!!g
        b restart
}
"
    }

    rl () {
        sans=`rp $*`
        with=`readlink -f ${sans}`
        echo "${with}"
    }

    exedir=`rp ${EXEDIR}`
    rexedir=`rl ${EXEDIR}/${WE}`
    rexedir=`dirname ${rexedir}`
    export EXEDIR="${rexedir}"

    if [ "${rexedir}" != "${exedir}" ] ; then
        shnldirs="${rexedir}:${exedir}"
    else
        shnldirs="${rexedir}"
    fi

    #echo "exedir for ${WE} is ${shnldirs}" >&2

    if [ -z "${SHNLPATH}" ] ; then
        export SHNLPATH="${shnldirs}"
    else
        case "${SHNLPATH}" in
            (${shnldirs})
                true
                ;;
            (${shnldirs}:*)
                true
                ;;
            (*)
                export SHNLPATH="${shnldirs}:${SHNLPATH}"
                ;;
        esac
    fi

    shnlpathRaw () {
        local IFS=":"
        for d in ${SHNLPATH} ; do
            for e in "" ".sh" ".sed" ; do
                if [ -r "${d}/$1$e" ] ; then
                    shnlpathRet="${d}/$1$e"
                    return 0
                fi
            done
        done
        return 1
    }

    shnlpath () {
        shnlpathRaw "$*" && echo "${shnlpathRet}"
    }

    shnlpathDo () {
        doname="$1"
        shift
        shnlpathRaw "${doname}" && "${shnlpathRet}" $*
    }

    shnlpathExec () {
        doname="$1"
        shift
        shnlpathRaw "${doname}" && exec "${shnlpathRet}" $*
    }

    #! Import sh skript all arguments exactly once
    import () {
        for i in $* ; do
            eval "import_known=\${IMPORT_${1}}"
            if [ -z ${import_known} ] ; then
                local IMPORT_SRC="${i}"
                local IMPORT_PATH="${IMPORT_PATH}:${i}"
                if shnlpathRaw "${i}" ; then
                    Source ${shnlpathRet}
                    eval "IMPORT_${i}=1"
                else
                    echo "could not find ${i} in ${SHNLPATH}" >&2
                    exit 1
                fi
            fi
        done
    }

endPreamble () {
    if [ "${IMPORT_PATH}" = "${0##*/}" ] ; then
        if [ -n "${endPreambleCalled}" ] ; then
            echo "endPreamble called from ${IMPORT_PATH} more than once, aborting" >&2
            exit 1
        fi
        readonly endPreambleCalled=1
        todo=
        while [ "$1" ] ; do
            case "$1" in
                (--help)
                    todo=getHelp
                    ;;
                (--doxy)
                    todo=getDoxy
                    ;;
                (--html)
                    todo=getHtml
                    ;;
                (--markdown)
                    todo=getMarkdown
                    ;;
                (*)
                    sfile="$1"
                    ;;
            esac
            shift
        done
        [ "${todo}" ] && "${todo}"
    fi
}

getPreamble() {
    if ! [ "${endPreambleCalled}" ] ; then
        echo "getPreamble needs a preceding call of 'endPreamble', aborting" >&2
        exit 1
    fi
    if [ "${importHeader}" ] ; then
        echo "#!"
        sed -n "
1,/^[[:blank:]\#]*endPreamble/{
        /^[[:blank:]\#]*endPreamble/{
                d
        }
        p
        d
}"      "${sfile}"
    else
        sed -n "
1,/^[[:blank:]\#]*endPreamble/{
        /^[[:blank:]\#]*endPreamble/{
                d
        }
        p
        d
}
/^\#/{
        :docu
        N
        /\n[[:blank:]]*[[:alpha:]][[:alnum:]]*[[:blank:]]*()[[:blank:]]*[{]/{
                s!\(\#*\)\(.*\)\n[[:blank:]]*\([[:alpha:]][[:alnum:]]*\)[[:blank:]]*()[[:blank:]]*[{]!\1 <a id=\"\3\"><code>\3</code></a>: \2!
                s!\n!MIST!g
                s!__DOCU_LINE__!\n#!g
                i __DOCU__START__
                p
                d
       }
       /\n\#/{
                s!\n#!__DOCU_LINE__!
                b docu
       }
       d
}
" $0    \
            | sed "
        1,/^__DOCU__START__\$/{
            s!__DOCU__START__!\#\#\#\# Declared functions!
        }
        /^__DOCU__START__\$/d
"
        echo "#"
    fi
}

pager () {
    exec 2>/dev/null
    ${PAGER}
}

getHelp() {
    getPreamble | sed3 3<<'EOF' | pager
s!^#!!
s!@\([[:alnum:]]\{1,\}\)!\1:!g
EOF
    exit 0
}

getDoxy() {
    getPreamble | sed3 3<<'EOF' | pager
/^#!/d
/^# Cmod/{
  s!^# \(.*\)!/// @addtogroup tools Tools for working with Modular C\n\n/// @defgroup _Cmod \1\n/// @ingroup tools\n///!
  p
  d
}
/^#/!{
  #/[^ ]/s!.*!<code>&</code><br>!
  /./!{
    i ///
    d
  }
  i /// @code
  :REPEAT
  $ {
    p
    i /// @endcode
    q
  }
  s!^!/// !
  p
  N
  s!.*\n!!
  /^#/!b REPEAT
  i /// @endcode
}
s!^#!!
s!^!/// !
EOF
    exit 0
}

getMarkdown() {
    if [ "${importHeader}" ] ; then
        WE="${sfile##*/}"
        echo "seeing: ${sfile} ${WE}" >&2
    fi
    getPreamble | sed "
/^\#!/{
     s!.*!!g
     :HEADER
     N
     s!\n!!
     /./!b HEADER
     i ---
     i title: ${WE%.sh}
     i css: \"./shnell-style.css?v=1\"
     i self-contained: yes
     i ---
     i
     i <div id=\"content\"  class=\"outline-2\">
}
:RESTART
/^\/\*/d
/^\*\//d
s!^\#\#\#\# Declared functions!\#\#\# Declared functions!
/^\#\#/{
  :CONTINUE
  N
  /\n\# /{
    s!\n\# ! !g
    b CONTINUE
  }
  i
  p
  i
  N
  s!.*\n!!
  b RESTART
}
s!^#[\\]\#!\#!g
/^\#[[:space:]\$]/{
  s!^\# \([0-9]\{1,\}\)!- \1!
  s!^\# \{,1\}!!
  # find the author
  s!\(©\|copyright\|\&copy;\)[[:space:]]*\([^[:digit:]]*[[:alpha:],]\)\(.*\)!\n\&copy; *\2* \3\n\n!
  #
  /^\${WEUP}≡/{
        /^\${WEUP}≡${WEUP}/!{
                d
        }
        s!^\${WEUP}≡[[:alnum:]]*!!
        s!^[[:space:]]*!!
  }
  /^\${WEUP}≠/{
        /^\${WEUP}≠${WEUP}/{
                d
        }
        s!^\${WEUP}≠[[:alnum:]]*!!
        s!^[[:space:]]*!!
  }
  # Use the script name instead of $0
  s!\${WEUP}!${WEUP}!g
  s!\$0!${WE}!g
  #
  p
  N
  s!.*\n!!
  b RESTART
}
/^\#\$/{
  i
  N
  s!.*\n!!
  b RESTART
}
/^\#./{
  i \`\`\`cpp
  :LOOP
  s!^#[\\]\#!\#!g
  /^\#\(pragma\|if\|else\|elif\|ifn\|endif\|error\|define\|undef\)/!s!^#!!
  p
  N
  s!.*\n!!
  /^\#/{
     b LOOP
  }
  i \`\`\`
  b RESTART
}
/^#/!{
  /./!{
    i
    d
  }
  i
  /^import/{
        i #### Imports
        i The following <code>sh</code>-modules are imported:
        i
        :imports
        s!import \([[:alnum:]]*\)!- \`\1\`!
        p
        N
        s!.*\n!!
        /\(^import\)\|\(^[[:blank:]]*\$\)/b imports
       i
       i ### Details
       b RESTART
  }
  /\$_\|import.sh/{
      i
      i ### Coding and configuration
      i The following code is needed to enable the sh-module framework.</p>
      i \`\`\`shell
      :CONFIG
      p
      N
      s!.*\n!!
      /\$_\|import.sh/b CONFIG
      i \`\`\`
      i
      b RESTART
  }
  i
  i \`\`\`shell
  :REPEAT
  $ {
    p
    i \`\`\`
    i </div>
    q
  }
  p
  N
  s!.*\n!!
  /^#/!b REPEAT
  i \`\`\`
  i
  b RESTART
}
"
    exit 0
}

getHtml() {
    if [ "${importHeader}" ] ; then
        WE="${sfile##*/}"
        echo "seeing: ${sfile} ${WE}" >&2
    fi
    getPreamble | sed "
/^\#!/{
 s!.*!!g
 :HEADER
 N
 s!\n!!
 /./!b HEADER
 i <html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">
 i <header>
 i <meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />
 i <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />
 i <title>${WE%.sh}</title>
 i <link rel=\"stylesheet\" type=\"text/css\" href=\"./shnell-style.css?v=1\" media=\"(min-width: 960px)\"/>
 i </header>
 i <body>
 i <div id=\"content\"  class=\"outline-2\">
 i <h1>${WE%.sh}</h1>
 i <p>
}
:RESTART
/^\/\*/d
/^\*\//d
/^\#pragma/{
    i </p>
    i <div class=\"org-src-container\">
    i <pre class=\"src src-c\">
    p
    i </pre>
    i </div>
    i <p>
    s!.*!!
    b RESTART
}
/^\#/{
  s!^\#!!
  # find the author
  s!\(©\|copyright\|\&copy;\)[[:space:]]*\([^[:digit:]]*[[:alpha:]]\)\(.*\)! </p>\n<p>\&copy <em>\2</em>\3<p>\n<p>!
  #
  /^\${WEUP}≡/{
        /^\${WEUP}≡${WEUP}/!{
                d
        }
        s!^\${WEUP}≡[[:alnum:]]*! !
  }
  /^\${WEUP}≠/{
        /^\${WEUP}≠${WEUP}/{
                d
        }
        s!^\${WEUP}≠[[:alnum:]]*! !
  }
  # Use the script name instead of $0
  s!\${WEUP}!${WEUP}!g
  s!\$0!${WE}!g
  #
  s!^[[:space:]]*-\{3,\}[[:space:]]*!<hr/>!
  s!---!\&mdash;!g
  s!--!\&ndash;!g
  # replace code fragments
  s!\`\([^\`]\{1,\}\)\`!<code>\1</code>!g
  # replace simple emphasis
  s!^[*]\([^*]\{1,\}\)[*]\$!<em>\1</em>!
  s!^\([*][^*]\{1,\}[*]\)\([^*[:alnum:]]\)!<em>\1</em>\2!
  s!\([^*[:alnum:]]\)\([*][^*]\{1,\}[*]\)\$!\1<em>\2</em>!
  s!\([^*[:alnum:]]\)[*]\([^*]\{1,\}\)[*]\([^*[:alnum:]]\)!\1<em>\2</em>\3!g
  # replace strong emphasis
  s!^[*][*]\([^*]\{1,\}\)[*][*]\$!<strong>\1</strong>!
  s!^\([*][*][^*]\{1,\}[*][*]\)\([^*[:alnum:]]\)!<strong>\1</strong>\2!
  s!\([^*[:alnum:]]\)\([*][*][^*]\{1,\}[*][*]\)\$!\1<strong>\2</strong>!
  s!\([^*[:alnum:]]\)[*][*]\([^*]\{1,\}\)[*][*]\([^*[:alnum:]]\)!\1<strong>\2</strong>\3!g
  /^\#/{
        i </p>
        /^\#[[:blank:]]/{
                i <h2>
                :h2
                s!^\#*[[:blank:]]\{1,\}!!
                p
                N
                s!.*\n!!
                # replace code fragments
                s!\`\([^\`]\{1,\}\)\`!<code>\1</code>!g
                /^\#\#[[:blank:]]/b h2
                i </h2>
                b RESTART
        }
        /^\#\#[[:blank:]]/{
                i <h3>
                :h3
                s!^\#*[[:blank:]]\{1,\}!!
                p
                N
                s!.*\n!!
                # replace code fragments
                s!\`\([^\`]\{1,\}\)\`!<code>\1</code>!g
                /^\#\#\#[[:blank:]]/b h3
                i </h3>
                b RESTART
        }
        /^\#\#\#[[:blank:]]/{
                i <h4>
                :h4
                s!^\#*[[:blank:]]\{1,\}!!
                p
                N
                s!.*\n!!
                # replace code fragments
                s!\`\([^\`]\{1,\}\)\`!<code>\1</code>!g
                /^\#\#\#[[:blank:]]/b h4
                i </h4>
                b RESTART
        }
        /^#pragma/!{
                p
                i <p>
                d
       }
  }
  /^[^[:blank:]]/{
    i </p>
    i <div class=\"org-src-container\">
    i <pre class=\"src\">
    s![\\]#!#!g
    :PRE
    $ {
      p
      i </pre>
      i </div>
      i <p>
      d
    }
    p
    N
    s!.*\n!!
    /^#pragma/{
       b PRE
    }
    /^#[^[:blank:]]/{
       s!^#!!
       b PRE
    }
    i </pre>
    i </div>
    i <p>
    $!b RESTART

  }
  $!{
    p
    N
    s!.*\n!!
    b RESTART
  }
  $ {
    p
    i </p>
    q
  }
}
/^#/!{
  /./!{
    i </p>
    i <p>
    d
  }
  i </p>
  /^import/{
        i <h4>Imports</h4>
        i <p>The following <code>sh</code>-modules are imported:</p>
        i <ul>
        :imports
        s!import \([[:alnum:]]*\)!<li><code>\1</code></li>!
        p
        N
        s!.*\n!!
        /\(^import\)\|\(^[[:blank:]]*\$\)/b imports
       i </ul>
       i <h3>Details</h3>
       b RESTART
  }
  /\$_\|import.sh/{
      i </p>
      i <h3>Coding and configuration</h3>
      i <p>The following code is needed to enable the sh-module framework.</p>
      i <div class=\"org-src-container\">
      i <pre  class=\"src\">
      :CONFIG
      p
      N
      s!.*\n!!
      /\$_\|import.sh/b CONFIG
      i </pre>
      i </div>
      i <p>
      b RESTART
  }
  i </p>
  i <div class=\"org-src-container\">
  i <pre class=\"src\">
  :REPEAT
  $ {
    p
    i </pre>
    i </div>
    i <p>
    q
  }
  p
  N
  s!.*\n!!
  /^#/!b REPEAT
  i </pre>
  i </div>
  i <p>
  b RESTART
}
"
    echo "</div>"
    echo "</body>"
    echo "</html>"
    exit 0
}

fi

_bn="${SRC##*/}"
_bn="${_bn%.sh}"
shnlpathRaw "${_bn}"
if [ -n "${_bn}" ] ; then
    case "$_bn" in
        (*[^[:alnum:]_]*)
            echo "failure: shnlpath for ${_bn} (${SRC}) is ${shnlpathRet}"
            ;;
        (*)
            #echo "success: shnlpath for ${_bn} (${SRC}) is ${shnlpathRet}"
            eval "export IMPORT_SOURCE_${_bn}=${shnlpathRet}"
            ;;
    esac
else
    endPreamble $*
fi
unset _bn
