#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## A toy example that splits all decimal integers into their prime
## factors.

# This directive spans the whole text that it amends for decimal
# integers, factors these, and replaces them by the product of the
# factors.

# It uses the Unix&trade; `factor` utility under the hood, which might
# not be present on all POSIX systems.
SRC="$_" . "${0%%/${0##*/}}/import.sh"
import join
import tokenize

endPreamble $*

factorize () {
    while read -r line ; do
        join " " ${line}
        case "${joinRet}" in
            (\#*line*)
                echo "${line}"
                ;;
            (*)
                for w in ${line} ; do
                    case "${w}" in
                        # Any word that has a non-digit is just echoed to
                        # stdout, as are 0 and 1, since factor does not know
                        # how to deal with them
                        (*[^[:digit:]]* | 1 | 0)
                            echo -n "${w} "
                            ;;
                        (*)
                            # Use the Unix utility factor to do the work
                            res=$(factor ${w})
                            # "factor" prefixes each output line with the
                            # number and a colon. The "#${w}:" part below says
                            # to remove a leading string from variable res
                            # that starts with "${w}" and is followed by a
                            # colon.
                            res="${res#${w}:}"
                            # Join the result with * operators. Calling "join"
                            # like this also gets rid of the terminating
                            # newline that is still hidden in "${res}"
                            join " ${markr}${markl} * ${markr}${markl} " ${res}
                            # Echo the return of "join" to stdout without
                            # adding a newline character.
                            echo -n "${joinRet} "
                            ;;
                    esac
                done
                # Echo a newline character at the end
                echo
                ;;
        esac
    done
}

# To be able to apply this correctly to each number, we have to
# separate the input stream into tokens that can then be
# recognised. After the operation we have to "untokenize" that is to
# remove the markers that we added.
#tokenize | factorize | untokenize

factorize
