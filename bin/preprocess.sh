#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2020

## A directive to run all depending code through the C preprocessor

### Usage:

#pragma CMOD amend preprocess

# Uses the environment variables `COMPILER` and `PREPROCESS` for the
# name and argument to use. They default to `cc` and `-E`,
# respectively. Additional arguments can be provided in the
# `COMPILERARGS` environment variable.
#
# The `shneller` comandline tool sets these variables appropriately,
# so if you use this directive from there you should not worry.


SRC="$_" . "${0%%/${0##*/}}/import.sh"

import tokenize

endPreamble $*

# the preprocessor might confused by the control symbols that we added
untokenize | ${COMPILER:-cc} ${PREPROCESS:--E} ${COMPILERARGS} - | tokenize
