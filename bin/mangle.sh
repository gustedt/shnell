#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2017

## Mangle names according to a common C++ strategy

# We use mangling of the form _ZN#1s1#2s2...E where #1 is the length
# of identifier s1 etc. All names are prefixed with a specific prefix
# that avoids conflicts with mangled identifiers such as produced by
# C++. By default this is `_C`.
manglePrefix=${manglePrefix:-_C}

SRC="$_" . "${0%%/${0##*/}}/import.sh"
import join
import match
import echo
import list

endPreamble $*

## This is the core of the mangle algorithm.
# A list of identifiers is transformed to a string that precedes each
# of the identifiers by its character length.
#
mangleRaw () {
    mangleRawRet=""
    while [ "$*" ] ; do
        mangleRawRet="${mangleRawRet}${#1}${1}"
        shift
    done
}

if [ "${manglePrefix}" ] ; then
    mangleRaw ${manglePrefix}
    mangleStart="_ZN${mangleRawRet}"
else
    mangleStart="_ZN"
fi
mangleEnd="E"

compsep="QQQQQQ"

compStart="I"
compEnd="E"

## mangle a sequence of identifiers as it where a template with
## arguments
#
mangleComp () {
    mangleRaw $1
    mangleCompRet="${mangleRawRet}"
    shift
    if [ "$*" ] ; then
        mangleRaw $*
        mangleCompRet="${mangleCompRet}${compStart}${mangleRawRet}${compEnd}"
    fi
}

## Split along a long separator that may consist of several characters
# The return value is stored in the variable `splitlongRet`.
splitlong () {
    splitlongRet=""
    w="$1"
    while true ; do
        case "${w}" in
            (*${compsep}*)
                true
                ;;
            (*)
                break
                ;;
        esac
        append splitlongRet "${w%%${compsep}*}"
        w="${w#*${compsep}}"
    done
    append splitlongRet "${w}"
}

mangleSpec () {
    mangleIDRet=
    while [ "$*" ] ; do
        if [ "$1" ] ; then
            case "$1" in
                (*${compsep}*)
                    splitlong "$1"
                    mangleComp ${splitlongRet}
                    mangleIDRet="${mangleIDRet}${mangleCompRet}"
                    ;;
                (*)
                    mangleRaw "$1"
                    mangleIDRet="${mangleIDRet}${mangleRawRet}"
                    ;;
            esac
        fi
        shift
    done
}

## partially mangle a list of identifiers separated by '#'
#
mangleIDlocal () {
    local IFS=" 	#"
    case "$*" in
        (*${compsep}*)
            mangleSpec $*
            ;;
        (*)
            mangleRaw $*
            mangleIDRet="${mangleRawRet}"
            ;;
    esac
}

mangleID () {
    mangleIDlocal $*
    echo -n "${mangleIDRet}"
}

## completely mangle a composed identifier
# The return value is stored in `mangleRet`.
mangleLocal () {
    if [ "${1###}" = "$1" ]; then
        mangleIDlocal $*
        mangleRet="${mangleStart}${mangleIDRet}${mangleEnd}"
    else
        mangleRet="${1###}"
    fi
}

## Same as `mangleLocal` but the result is echoed to `stdout`
#
mangle () {
    mangleLocal $*
    echo "${mangleRet}"
}

camelCase () {
    camelCaseRet="$1"
    shift
    local start
    local end
    while [ "$*" ] ; do
        end="${1#[[:lower:]]}"
        start=$(echo ${1%${end}} | ${SED} y/abcdefghijklmnopqrstuvwxyz/ABCDEFGHIJKLMNOPQRSTUVWXYZ/)
        end=$(echo ${end} | ${SED} y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/)
        camelCaseRet="${camelCaseRet}${start}${end}"
        shift
    done
    echo -n ${camelCaseRet}
}

camelCaseLocal () {
    camelCaseRet="$1"
    shift
    local start
    local end
    while [ "$*" ] ; do
        case "$1" in
            (*[![:alnum:]]*)
                end=
                start="$1"
                ;;
            ([[:lower:]]*)
                end="${1#[[:lower:]]}"
                start=$(echo ${1%${end}} | ${SED} y/abcdefghijklmnopqrstuvwxyz/ABCDEFGHIJKLMNOPQRSTUVWXYZ/)
                end=$(echo ${end} | ${SED} y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/)
                ;;
            (*)
                end=
                start="$1"
                ;;
        esac
        camelCaseRet="${camelCaseRet}${start}${end}"
        shift
    done
}

camelCase () {
    camelCaseLocal $*
    echo -n ${camelCaseRet}
}

legacy () {
    res=$(echo $* | ${SED} "s!${compsep}! !g")
    case "${CMOD_LEGSEP}" in
        (C)
            res=$(split "#" ${res})
            camelCase $res
            ;;
        (P)
            res=$(split "#" ${res})
            camelCase "" $res
            ;;
        (*)
            sj "#" "${CMOD_LEGSEP}" ${res}
            echo "${joinRet}"
            ;;
    esac
}

## check if we have to mangle and return the symbol in mangleRet
#
#    - ::  C++ mangling
#    - _   snail_case_identifiers
#    - C   camelCaseIdentifiers.
#    - P   PascalCaseIdentifiers.
checkMangle () {
    # The name 'compsep' is used by `splitlong`
    local compsep="$1"
    shift
    local mesep="$1"
    shift
    report "mangle $* ('${misep}', '${mesep}')"
    splitlong $*
    case "${mesep}" in
        (::)
            mangleLocal ${splitlongRet}
            ;;
        (C)
            camelCaseLocal ${splitlongRet}
            mangleRet="${camelCaseRet}"
            ;;
        (P)
            camelCaseLocal "" ${splitlongRet}
            mangleRet="${camelCaseRet}"
            ;;
        (*)
            join "${mesep}" ${splitlongRet}
            mangleRet="${joinRet}"
            ;;
    esac
    report "mangle $* ('${misep}', '${mesep}') : ${mangleRet}"
}
