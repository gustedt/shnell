#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2017

## Utilities for shell level regexp matching.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import echo

# We provide environment variables for certain tools.

export SED=${SED:=sed}
export SEDU=${SEDU:=-u}
export CAT=${CAT:=cat}
if [ ! "${PAGER}" ] ; then
    PAGER=`whereis -b "less"`
    if [ "${PAGER}" != "less:" ] ; then
        export PAGER="less -R"
    else
        PAGER=`whereis -b "more"`
        if [ "${PAGER}" != "more:" ] ; then
            export PAGER="more"
        else
            export PAGER="${CAT}"
        fi
    fi
fi

# The matching we do requires UTF-8 capacities, so we try to set
# `LC_LANG` to a UTF-8 locale.

endPreamble $*

case "${LC_ALL:=${LANG}}" in
    (*utf8*|*utf-8*|*UTF8*|*UTF-8*)
        true
        ;;
    (*)
        locales="$(locale -a)"
        LC_ALL="${LC_ALL%.*}"
        for l in ${locales} ; do
            case "${l}" in
                (${LC_ALL}*utf8*|${LC_ALL}*utf-8*|${LC_ALL}*UTF8*|${LC_ALL}*UTF-8*)
                    LC_ALL="${l}"
                    clocale=
                    break
                    ;;
                (C*utf8*|C*utf-8*|C*UTF8*|C*UTF-8*)
                    clocale="${l}"
                    ;;
            esac
        done
        [ "${clocale}" ] && LC_ALL="${clocale}"
        export LC_ALL
        complain "LC_ALL set to ${LC_ALL}"
        warnThem=$(locale)
        ;;
esac

if [ "${WD}" = "" ] ; then
    WD="$(pwd)"
fi


#### Check if string $2 matches regexp $1
# This uses shell pattern matching and can be used more simply than
# the shell's `case` construct on which it is built. It can be used as
# simple prefix conditionals
#
#match "&ast;[a-z]&ast;" "${id}" || echo "${id} is not an identifier"
#
# or in `if`-`then`-`else` constructs
#
#if match "&ast;[a-z]&ast;" "${id}" ; then
#echo "${id} is not an identifier"
#fi
#
match () {
    eval "case '$2' in $1) return 0;; *) return 1;; esac"
    return $?
}

#### Test if a list given as an argument has more than one member
#
hasBlank () {
    [ $# -gt 1 ]
}

#### In $1, replace the occurences of string $2 by the string $3
# The modified string is returned in the variable `rplret`.
replace () {
    rplret=
    rplnex="${1}"
    while true; do
        rplpre="${rplnex%%${2}*}"
        if [ "${rplnex}" = "${rplpre}" ] ; then
            rplret="${rplret}${rplnex}"
            return
        fi
        rplnex="${rplnex#*${2}}"
        rplret="${rplret}${rplpre}${3}"
    done
}

#### A sed that reads the commands from fd 3
# As usual this reads the text to be processed from `stdin`.
sed3 () {
    command=$(${CAT} <&3)
    ${SED} -e "${command}" $*
}

#### A sed that reads the commands from $1 and the text from the remaining arguments
#
sed2 () {
    command="$1"
    shift
    ${SED} -e "${command}" <<EOF
$*
EOF
}

#### Shortcut "./" and "../" elements in pathnames.
#
realpath () {
    path=$1
    if match "${path}" "./*" ; then
        path=${WD}${path#\./}
        path="${path}/"
    elif [ "${path##/}" = "${path}" ] ; then
        path=${WD}"/"${path}
    fi
    sed3 3<<'EOF' <<PATH
      s![^/]*/[.][.]/!!g
      s!/[.]/!/!g
      s!/[.]$!!g
EOF
${path}
PATH
}

