#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2020

## A directive to put all depending code on a logical line, joined by terminating backslashes

### Usage:

#pragma CMOD amend logicalline

# The primary use case for this is to compose macro definitions for
# code that just should be preprocessed  by shnell, but that should remain readable.
# Example:

#pragma CMOD amend oneline
#\#define ARRAY
#(double){
#pragma CMOD amend do I = 3
#[${I}] = ${I},
#pragma CMOD done
#}
#pragma CMOD done

# This should be rewritten to something like

#\#define ARRAY \
#(double){      \
#[0] = 0,       \
#[1] = 1,       \
#[2] = 2,       \
#}

# Line directives that are mingled within are removed.

# Just as in normal macro definitions, the subject code must not have
# C++ style comments.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import match
import tmpd
import tokenize

endPreamble $*

newTmp tmpf

maxline=0

count () {
    while read -r line ; do
        [ "${#line}" -gt "${maxline}" ] && maxline="${#line}"
    done
}

fill () {
    while read -r line ; do
        printf "%-${maxline}s\n" "${line}"
    done
}

untokenize            \
    | eval "${STYLE}" \
    | "${SED}" '
         # remove line directives
         /[[:blank:]]*\#[[:blank:]]*line/d
         # protect all space characters
         s! !@!g'     >"${tmpf}"

count <"${tmpf}"

fill <"${tmpf}"       \
    | "${SED}" '
       # unprotect all space characters
       s!@! !g
       # The last line receives special treatment
       $,$ {
           s![ ]*$!!
           n
       }
       # all other lines receive a continuation mark
       s!$! \\!'
