#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## A tool to process ballanced parenthesis.

# The functions here read some parenthized code on `stdin` and by
# default dump it to `stdout` until the first open parenthesis is
# closed. Recognized parenthesis are `Paren` ~ `()`, `Bracket` ~ `[]`,
# `Brace` ~ `{}` and `Attr` ~ `[[]]`.

# Instead of directly dumping to `stdout`, for each type of
# parenthesis there can be a callback that filters the the code that
# is received. Such a callback is defined by the variables
# `callbackParen`, `callbackBracket`, `callbackBrace` and
# `callbackAttr`, respectively.

# The whole procedure uses (and reserves!) two variables, `top` for
# the current syntax element and `line` for the remainder of the
# current line.

# For all of this to work, the parenthesis must be visible as
# individual words in the input. Therefore the input should have been
# pretreated by `tokenize`.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import echo
import match

endPreamble $*

popRaw () {
    top="$1"
    [ "$*" ] && shift
    line="$*"
}

getLine () {
    lineno="$(( ${lineno:-0} + 1 ))"
    read -r line || return
    popRaw ${line}
}

#### Delete the current word and advance one word forward.
#
# Newline characters are printed to `stdout` each time that the
# current line in `line` became empty.
deleteWord () {
    popRaw ${line}
    while ! [ "${top}" ] ; do
        # we finished the previous line
        echo
        # get the new line
        lineno="$(( ${lineno:-0} + 1 ))"
        read -r line || return 1
        popRaw ${line}
    done
}

firstWord () {
    getLine || return 1
    while ! [ "${top}" ] ; do
        # we finished the previous line
        echo
        # get the new line
        getLine || return 1
    done
}

#### Write out the current word and advance one word forward.
#
# This is the main tool to move through the source stream.
#
# Newline characters are printed to `stdout` each time that the
# current line in `line` became empty.
popWord () {
    echo -n " ${top}"
    deleteWord
}

skipCntrl () {
    while match "[[:cntrl:]]*" "${top}" ; do
        popWord
    done
}

deleteSkipCntrl () {
    deleteWord
    while match "[[:cntrl:]]*" "${top}" ; do
        deleteWord
    done
}

popToken () {
    popWord
    while match "[[:cntrl:]]*" "${top}" ; do
        popWord
    done
}

popLine () {
    echo -n "${top}" "${line}"
    line=
    deleteWord
}

popCntrl () {
    popWord
    while match "[\#[:cntrl:]]*" "${top}" ; do
        case "${top}" in
            (\#*)
                popLine
                ;;
            (*)
                popWord
                ;;
        esac
    done

}

#### Callback to ignore all `stdin`
ignoreCallback () {
    return
}

#### Callback to print empty lines
linesCallback () {
    ${SED} "s!.*!!"
}

#### Process one single ballanced expression
callBack () {
    local tmpf
    newTmp tmpf
    case "${top}" in
        (\[\[)
            if [ "${callbackAttr}" ] ; then
                ballancedAttr > "${tmpf}"
                "${callbackAttr}" < "${tmpf}"
            else
                ballancedAttr
            fi
            ;;
        (\()
            if [ "${callbackParen}" ] ; then
                ballancedParen > "${tmpf}"
                "${callbackParen}" < "${tmpf}"
            else
                ballancedParen
            fi
            ;;
        (\{)
            if [ "${callbackBrace}" ] ; then
                ballancedBrace > "${tmpf}"
                "${callbackBrace}" < "${tmpf}"
            else
                ballancedBrace
            fi
            ;;
        (\[)
            if [ "${callbackBracket}" ] ; then
                ballancedBracket > "${tmpf}"
                "${callbackBracket}" < "${tmpf}"
            else
                ballancedBracket
            fi
            ;;
    esac
}

#### Process an expression that is surrounded by `()`.
ballancedParen () {
    local depth=0
    while [ "${top}" ] ; do
        case "${top}" in
            (\()
                depth="$((${depth} + 1))"
                ;;
            ([\[\{]|\[\[)
                callBack
                continue
                ;;
            (\))
                depth="$((${depth} - 1))"
                ;;
            ([\]\}]|\]\])
                echo -n "/*(${top} mismatch, level ${depth}*/"
                ;;
        esac
        popWord
        [ "${depth}" -le 0 ] && return
    done
    echo -n "/*) missing*/"
}

#### Process an expression that is surrounded by `[]`.
ballancedBracket () {
    local depth=0
    while [ "${top}" ] ; do
        case "${top}" in
            (\[)
                depth="$((${depth} + 1))"
                ;;
            ([\(\{]|\[\[)
                callBack
                continue
                ;;
            (\])
                depth="$((${depth} - 1))"
                ;;
            (\]\])
                # sometimes two ] may be glued together
                depth="$((${depth} - 2))"
                ;;
            ([\)\}])
                echo -n "/*[${top} mismatch, level ${depth}*/"
                ;;
        esac
        popWord
        [ "${depth}" -le 0 ] && return
    done
    echo -n "/*] missing*/"
}

#### Process an expression that is surrounded by `[[]]`.
ballancedAttr () {
    local depth=0
    while [ "${top}" ] ; do
        case "${top}" in
            (\[\[)
                depth="$((${depth} + 2))"
                popWord
                continue
                ;;
            (\]\])
                depth="$((${depth} - 2))"
                popWord
                [ "${depth}" -ne 0 ] && echo -n "/*attr mismatch, level ${depth}*/"
                return
                ;;
            (\])
                depth="$((${depth} - 1))"
                popWord
                [ "${depth}" -eq 0 ] && return
                [ "${depth}" -eq 1 ] && continue
                echo -n "/*[[${top} mismatch, level ${depth}*/"
                ;;
            ([\[\(\{])
                callBack
                ;;
            ([\)\}])
                echo -n "/*[[${top} mismatch, level ${depth}*/"
                ;;
        esac
        popWord
    done
    echo -n "/*]] missing*/"
}

#### Process an expression that is surrounded by `{}`.
ballancedBrace () {
    local depth=0
    while [ "${top}" ] ; do
        case "${top}" in
            (\{)
                depth="$((${depth} + 1))"
                ;;
            ([\[\(]|\[\[)
                callBack
                continue
                ;;
            (\})
                depth="$((${depth} - 1))"
                ;;
            ([\]\)])
                echo -n "/*{${top} mismatch, level ${depth}*/"
                ;;
        esac
        popWord
        [ "${depth}" -le 0 ] && return
    done
    echo -n "/*} missing*/"
}

ballanced () {
    while [ "${top}" ] ; do
        case "${top}" in
            ([\[\(\{]|\[\[)
                callBack
                ;;
            ([\]\)\}]|\]\])
                echo -n "/*mismatch*/"
                ;;
        esac
        popWord
    done
}
