#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import join
import echo
import list

endPreamble $*

MODSED="${MODSED:-${TMPD}/modsed.sed}"

getnames () {
    prefix="$1"
    shift
    for n in $* ; do
        report "seeing identifier $n for ${prefix}"
        echo "s! $n ! ${prefix}::$n !g" >> "${MODSED}"
    done
}
