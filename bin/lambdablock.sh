#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2020

## Emulate lambda expressions for just one block

# This is the inner part of the `lambda` directive.



SRC="$_" . "${0%%/${0##*/}}/import.sh"

import echo
import tokenize
import tmpd
import ballanced

endPreamble $*

captures() {
    count=0
    local top=
    local line=
    local lineno=0
    newTmp expr
    local structs="$1"
    local inits="$2"
    local prefix="$3"
    local basename="$4"
    local types="$5"
    local xpres="$6"
    local parms="$7"
    echo -n > "${prefix}"
    echo -n > "${inits}"
    echo -n > "${structs}"
    echo -n > "${types}"
    echo -n > "${xpres}"
    echo -n > "${parms}"

    deleteSkipCntrl >/dev/null
    deleteSkipCntrl >/dev/null
    deleteSkipCntrl >/dev/null
    capture="_LambdaState_${basename}"
    while [ "XXX${top}" != "XXX]" ] ; do
        var="${top}"
        deleteSkipCntrl >/dev/null
        case "${var}" in
            (\&)
                case "${top}" in
                    ([[:alnum:]_]*)
                        var="${top}"
                        deleteSkipCntrl >/dev/null
                        ;;
                    (*)
                        var=
                        ;;
                esac
                if [ "XXX${top}" = "XXX," ] ; then
                    deleteSkipCntrl >/dev/null
                elif [ "XXX${top}" != "XXX]" ] ; then
                    complain "// garbage after lvalue capture ${count}: ${top}"
                fi
                if [ "${var}" ] ; then
                    echo "\t// lvalue capture ${var}"  >>"${structs}"
                else
                    echo "\t// default lvalue capture" >>"${structs}"
                fi
                ;;
            ([=])
                echo "\t/* default value capture cannot be properly handled */" >>"${structs}"
                ;;
            (*)
                case "${top}" in
                    (,)
                        echo "\ttypedef __typeof__ (${var}) _LambdaType_${basename}_${var};"           >>"${types}"
                        echo "\t_LambdaType_${basename}_${var} ${var};"                   >>"${structs}"
                        echo -n "\t.${var} = ${var},"                          >>"${inits}"
                        echo -n ", (${var})"                                   >>"${xpres}"
                        echo -n ", _LambdaType_${basename}_${var} ${var}"      >>"${parms}"
                        echo "\t_LambdaType_${basename}_${var} const ${var} = ${capture}.${var};" >>"${prefix}"
                        deleteSkipCntrl >/dev/null
                        ;;
                    (\])
                        echo "\ttypedef __typeof__ (${var}) _LambdaType_${basename}_${var};"           >>"${types}"
                        echo "\t_LambdaType_${basename}_${var} ${var};"                   >>"${structs}"
                        echo -n "\t.${var} = ${var},"                          >>"${inits}"
                        echo -n ", (${var})"                                   >>"${xpres}"
                        echo -n ", _LambdaType_${basename}_${var} ${var}"      >>"${parms}"
                        echo "\t_LambdaType_${basename}_${var} const ${var} = ${capture}.${var};" >>"${prefix}"
                        ;;
                    (=)
                        deleteSkipCntrl >/dev/null
                        echo -n > "${expr}"
                        echo "\t_LambdaType_${basename}_${var} ${var};"                   >>"${structs}"
                        while [ "XXX${top}" != "XXX," -a "XXX${top}" != "XXX]" ] ; do
                            case "${top}" in
                                ([\[\(\{]|\[\[)
                                    callBack >>"${expr}"
                                    ;;
                                (*)
                                    popToken >>"${expr}"
                                    ;;
                            esac
                        done
                        #${CAT} "${expr}"                                       >>"${structs}"
                        #echo -n ")  ${var}; "                                  >>"${structs}"
                        echo "\ttypedef __typeof__ ($(${CAT} ${expr})) _LambdaType_${basename}_${var};"           >>"${types}"
                        echo -n "\t.${var} = ${var},"                          >>"${inits}"
                        echo -n ", ($(${CAT} ${expr}))"                        >>"${xpres}"
                        echo -n ", _LambdaType_${basename}_${var} ${var}"      >>"${parms}"
                        echo "\t_LambdaType_${basename}_${var} const ${var} = ${capture}.${var};" >>"${prefix}"
                        ;;
                    (*)
                        expr="what??"
                        ;;
                esac
                ;;
        esac
        count="$((${count} + 1))"
    done
    if [ "${count}" -eq 0 ] ; then
        echo "\n/* no captures, this is a function literal */" >>"${structs}"
    elif [ ! -s "${prefix}" ] ; then
        echo "\n/* beware: this has lvalue captures */"        >>"${structs}"
    else
        newTmp str
        echo "\n#line 1 \"<_LambdaType_${basename}>\""                         > "${str}"
        echo "typedef struct _LambdaType_${basename} _LambdaType_${basename};" >>"${str}"
        ${CAT} "${types}"                                                      >>"${str}"
        echo "struct _LambdaType_${basename} {"                                >>"${str}"
        ${CAT} "${structs}"                                                    >>"${str}"
        echo "};"                                                              >>"${str}"
        echo "_LambdaType_${basename} _LambdaState_${basename};"               >>"${str}"
        echo "\t/* this is a closure */"                                       >>"${str}"
        ${CAT} <"${str}" >"${structs}"
    fi
}

lambdaInner () {
    local decls="$1"
    newTmp captures
    newTmp parameters
    newTmp parms
    newTmp body
    newTmp source
    newTmp structs
    newTmp inits
    newTmp types
    newTmp xpres
    newTmp prefix
    newTmp rettype
    echo -n >"${decls}"
    local top=
    local line=
    firstWord
    while [ "${top}" ] ; do
        case "${top}" in
            ("[")
                export NUMB="$(( ${NUMB:-0} + 1 ))"
                echo -n >"${rettype}"
                ballancedBracket > "${captures}"
                skipCntrl        >>/dev/null
                if [ "${top}" = "(" ] ; then
                    ballancedParen    > "${parameters}"
                    skipCntrl         >>/dev/null
                fi
                echo -n > "${body}"
                if [ "${top}" = "->" ] ; then
                    deleteWord
                    while [ "${top}" != "{" ] ; do
                        if [ "${top}" = "#" ] ; then
                            popLine >>"${body}"
                        else
                            popToken >>"${rettype}"
                        fi
                    done
                fi
                if [ "${top}" != "{" ] ; then
                    report "found [ that does not start a lambda, rolling back"
                    ${CAT} "${captures}" "${rettype}" "${body}"
                else
                    # set defauts for omitted components
                    if [ ! -s "${parameters}" ] ; then
                        echo -n "(void) "  > "${parameters}"
                    fi
                    if [ ! -s "${rettype}" ] ; then
                        echo -n "auto " >>"${rettype}"
                    fi
                    lambdaname="${REC:-0}_${NUMB:-0}_${lineno}"
                    local funcname="_Lambda_${lambdaname}"
                    local lambdatype="_LambdaType_${lambdaname}"
                    local lambdaget="_LambdaGet_${lambdaname}"
                    local lambdastate="_LambdaState_${lambdaname}"
                    ballancedBrace                                                >>"${body}"
                    captures "${structs}" "${inits}" "${prefix}" "${lambdaname}" "${types}" "${xpres}" "${parms}" < "${captures}" >/dev/null
                    if [ -s "${structs}" ] ; then
                        ${CAT} "${structs}"                                       >>"${decls}"
                    fi
                    # print the function
                    echo "#line 1 \"<${funcname}>\""                              >>"${decls}"
                    if [ "${REC:-0}" -eq "0" ] ; then
                        echo "static "                                            >>"${decls}"
                    fi
                    ${CAT} "${rettype}"                                           >>"${decls}"
                    echo -n " ${funcname}"                                        >>"${decls}"
                    ${CAT} "${parameters}"                                        >>"${decls}"
                    if [ -s "${prefix}" ] ; then
                        echo "{"                                                  >>"${decls}"
                        echo "/* initialization of captures */"                   >>"${decls}"
                        ${SED} '/^$/ d' "${prefix}"                               >>"${decls}"
                        ${SED} -i '
1,1 s!{!!
1,/./ { /^[[:blank:]]*$/d }
' "${body}"
                    fi
                    ${CAT} "${body}"                                              >>"${decls}"
                    echo "/* end for ${funcname} */"                              >>"${decls}"
                    if [ -s "${inits}" ] ; then
                        if [ "${REC:-0}" -eq "0" ] ; then
                            echo "static "                                        >>"${decls}"
                        fi
                        echo "#line 1 \"<${lambdaget}>\""                         >>"${decls}"
                        echo -n " __typeof__(&${funcname}) ${lambdaget}"          >>"${decls}"
                        par="`${CAT} ${parms}`"
                        par="${par#, }"
                        echo "(${par}) {"          >>"${decls}"
                        echo "${lambdastate} = (${lambdatype}){ " `${CAT} "${inits}"` " };"          >>"${decls}"
                        echo "return &${funcname};"          >>"${decls}"
                        echo "}"          >>"${decls}"
                        xpr="`${CAT} ${xpres}`"
                        xpr="${xpr#, }"
                        echo -n "${lambdaget}(${xpr}) " | tokenize
                    else
                        echo -n "${funcname}" | tokenize
                    fi
                    # watch that following preprocessor lines are not glued without newline
                    skipCntrl
                fi
                ;;
            (\#*)
                popLine
                ;;
            (*)
                popToken
                ;;
        esac
    done
}

lambdaBlock () {
    newTmp decls
    newTmp rest
    lambdaInner "${decls}" > "${rest}"
    ${CAT} "${decls}" | tokenize
    ${CAT} "${rest}"
    echo -n > "${decls}"
    echo -n > "${rest}"
}
