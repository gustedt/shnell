#!/bin/sh -f

## Specialize a code snippet for a set of ranges.

### Usage:
#pragma CMOD amend range NAME [bound1 ... ]

# Here bounds should be numeric and admissible for a `<` operator, or
# the special string `else`. The bounds should be sorted with respect
# to that ordering.

# A copy of the code snippet enclosed in its own block is created for
# each bound, plus a fallback version if the bound `else` is in the
# list. Within each of these copies the string `${NAME}` is replaced by
# the actual bound within that copy. In the fallback version `${NAME}`
# is replaced by `NAME`, so not much is gained, there.

# If `else` is not in the list, and thus there is no fallback version,
# you can add a fallback just after the closing pragma:

#pragma CMOD done
#else {
#// code for the general case, greater or equal to the last bound
#}



SRC="$_" . "${0%%/${0##*/}}/import.sh"
import tmpd
import echo
import arguments

endPreamble $*

source="${tmpd}/range$$.txt"
cat > "${source}"

range () {

    name="$1"
    shift

    [ -n "${PHASE2}" ] && echo "#line 1 \"<range $1>\""
    for val in $* ; do
        if [ "${val}" = "else" ] ; then
            havelse=1
        else
            echo "${eif}if (${name} < ${val}) {"
            if [ -n "${prev}" ] ; then
                echo "static_assert(${prev} < ${val}, \"${prev} and ${val} in wrong order\");"
            fi
            # set the next values
            eif="else "
            prev="${val}"

            echo "#pragma CMOD amend bind ${name}=${val}"
            cat "${source}"
            echo "#pragma CMOD done"
            [ -n "${PHASE2}" ] && echo "#line 1 \"<range end ${val}>\""
            echo -n "}"
        fi
    done
    if [ -n "${havelse}" ] ; then
        echo "else {"
        echo "#pragma CMOD amend bind ${name}=${name}"
        cat "${source}"
        echo "#pragma CMOD done"
        [ -n "${PHASE2}" ] && echo "#line 1 \"<range end>\""
        echo -n "}"
    fi
    echo
}

arguments args

range $args | shnlpathDo expand
