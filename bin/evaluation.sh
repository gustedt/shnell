#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2017

## Evaluate the depending code fragment with protected directives
## several times.

#   This allows to use constructs such as `${N}` also as arguments to
#   subsequent directives.

#   In it simplest form, something like

#pragma CMOD amend eval HERE

# will first replace the `HERE` in occurences such as

#pragma HERE amend ...

# with `CMOD` and then call the expansion procedure with input the
# modified program part, again.

# Several identifiers in the line for `eval` have the code fragment
# processed as often as there are such "labels", last is inner most.


# Per default the following directives have shortcuts for amend:
CMOD_AMENDS="${CMOD_AMEND:=
bind
do
env
eval
foreach
let
}"

# So the following line, where a variable `N` currently has the value `4`,

#pragma HERE do I = ${N}

# is then replaced by

#pragma CMOD amend do I = 4

# The directives that can appear instead of the above can be changed
# by defining the environment variable `CMOD_AMEND` to a list of
# directives that should have shortcuts. If an element in the list is
# of the form `NEW=OLD` the

#pragma HERE NEW

# directive resolves to

#pragma CMOD amend OLD

# in the depending source snippet.

# If only `NEW` is given,

#pragma HERE NEW

# resolves to

#pragma CMOD amend NEW.

# A particular directive can be switched off by leaving `OLD` empty,
# that is by only giving `NEW=`. After that

#pragma HERE NEW

# is ivalid. Thus, by addition of `amend=` and/or `insert=` to the
# list all other directives can be disabled for `HERE`.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import match
import arguments
import join
import echo

endPreamble $*

#### Evaluate the source code for the tags received on the commandline
evaluation () {
    pipe=
    for name in $* ; do
        filename="${TMPD}/eval_$$_${name}"
        echo "
/^[[:blank:]]*\#[[:blank:]]*pragma[[:blank:]]\{1,\}${name}[[:blank:]]\{1,\}/ {
         s!^[[:blank:]]*\#[[:blank:]]*pragma[[:blank:]]\{1,\}${name}[[:blank:]]\{1,\}!\#pragma ${name} !
" > "${filename}"
        todo="amend"
        for word in ${CMOD_AMENDS} ; do
            case "${word}" in
                (*=)
                 word="${word%%=}"
                 report "// ${name}: '${word}' disabled"
                 echo "
        s!^\#pragma ${name} \(amend\|insert\) ${word}!\#pragma CMOD invalid \1 ${word}!
        s!^\#pragma ${name} ${word} !\#pragma CMOD invalid ${word} !
        s!^\#pragma ${name} ${word}\$!\#pragma CMOD invalid ${word}!
" >>"${filename}"
                 ;;
                (*=*)
                    old="${word%%=*}"
                    new="${word##*=}"
                    report "// ${name}: '${old}' -> '${new}' for ${todo}"
                    echo "
        s!^\#pragma ${name} ${old} !\#pragma CMOD ${todo} ${new} !
        s!^\#pragma ${name} ${old}\$!\#pragma CMOD ${todo} ${new}!
" >>"${filename}"
                    ;;
                (:)
                    todo="insert"
                    ;;
                (*)
                    report "// ${name}: '${word}' enabled for ${todo}"
                    echo "
        s!^\#pragma ${name} ${word} !\#pragma CMOD ${todo} ${word} !
        s!^\#pragma ${name} ${word}\$!\#pragma CMOD ${todo} ${word}!
" >>"${filename}"
                    ;;
            esac
        done

        echo "
        s!\#pragma ${name} !\#pragma CMOD !
}
" >>"${filename}"
        shnlpathRaw expand                                              \
            && pipe="${pipe}| ${SED} -f ${filename} | ${shnlpathRet}"
    done
    pipe="${pipe#|}"
    exec sh -c "${pipe}"
}

arguments args
args="${args:-EVAL}"
evaluation ${args}
