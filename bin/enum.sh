#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## generate an enumeration type together with some support values and functions

SRC="$_" . "${0%%/${0##*/}}/import.sh"
import match
import arguments
import echo
import join
import mangle
import list
import tokenize
import tmpd

endPreamble $*

enum () {
    ltname="$1"
    shift
    report "${ltname}: $*"
    ltnames="$*"
    compsep="::" splitlong ${ltname}
    tname="${splitlongRet#* }"
    report "local name of '${ltname}' is '${tname}'"
    enames=
    for nam in $* ; do
        symb="${nam%=*}"
        append enames "${symb}"
    done

    newTmp fout c

    if [ "${PHASE2}" ] ; then
        cat <<EOF >>"${fout}"
#line 1 "<enum generation ${tname}>"
#define _SHNELL_STRINGIFY0(...) #__VA_ARGS__
#define _SHNELL_STRINGIFY1(...) _SHNELL_STRINGIFY0(__VA_ARGS__)
#define _SHNELL_STRINGIFY(...) _SHNELL_STRINGIFY1(__VA_ARGS__)
EOF
    fi
    cat <<EOF >>"${fout}"
// @brief An enumeration type that has been generated with an enum directive.
//
// Additional tools are available for this type:
// - ${tname}_min and ${tname}_max are the minimum and maximum values, respectively.
// - ${tname}_inv() returns the smallest int value that is not covered by the type.
// - ${tname}_names() returns a string with the name of the value
// - ${tname}_pos() and ${tname}_elem give the mapping between positions in the declaration order
//   and enumeration values
enum ${ltname} {
EOF
    joinEcho ",\t\n" ${ltnames}  >>"${fout}"
    cat <<EOF >>"${fout}"

};
typedef enum ${ltname} ${ltname};

enum {
            ${ltname}_number = $#,
            // Since we now know that we have two's complement,
            // specify INT_MAX and INT_MIN like this.
            ${ltname}_typemax = -1U/2,
            ${ltname}_typemin = -${tname}_typemax-1,
            ${tname}_nmin = ${tname}_typemin+$#,
EOF
    i=0
    for NAME in ${enames} ; do
        echo "${tname}_val${i} = ${NAME}," >>"${fout}"
        i="$(($i + 1))"
    done
    cat <<EOF >>"${fout}"
            ${tname}_min0 = ${tname}_val0,
            ${tname}_max0 = ${tname}_val0,
EOF

    # This computes the min and max values line by line at compile
    # time.
    i=1
    while true ; do
        echo "         ${tname}_max${i} = ( ${tname}_val${i} < ${tname}_max$((${i}-1)) ? ${tname}_max$((${i}-1)) : ${tname}_val${i} )," >>"${fout}"
        echo "         ${tname}_min${i} = ( ${tname}_val${i} > ${tname}_min$((${i}-1)) ? ${tname}_min$((${i}-1)) : ${tname}_val${i} )," >>"${fout}"
        if [ "$((${i}+1))" -eq $# ] ; then
            cat <<EOF >>"${fout}"
            ${ltname}_min = ${tname}_min${i},
            ${ltname}_max = ${tname}_max${i},
EOF
            break;
        else
            i="$((${i}+1))"
        fi
    done


    cat <<EOF >>"${fout}"
};

// @brief This is a pure function that returns that smallest \`int\`
// value that is not in the enumeration ${tname}.
inline int ${tname}_inv(void) {
       _Bool ${tname}_arr[$#+1] = { 0 };

       /* The acrobatic in the index expressions ensures that the
        * compiler does not falsely report an arithmetic overflow.*/
EOF

    i=0
    while [ "${i}" -lt $# ] ; do
        echo "         if (${tname}_val${i} < ${tname}_nmin) ${tname}_arr[(unsigned)${tname}_val${i} - ${tname}_typemax -1u] = 1;" >>"${fout}"
        i="$((${i}+1))"
    done
    #Echo "for (unsigned i = 0; i < $#; ++i) { printf(\"arr[%u] = %d\", i, ${tname}_arr[i]); puts(\"\"); }"
    i=0
    while [ "${i}" -lt $# ] ; do
        echo "         if (!${tname}_arr[${i}]) return ${tname}_typemin + ${i};" >>"${fout}"
        i="$((${i}+1))"
    done

    cat <<EOF >>"${fout}"
    return ${tname}_nmin;
}

// @brief A pure function that returns the px' value of ${tname} in order of
// declaration.
inline ${tname} ${tname}_elem(unsigned px) {
       switch (px) {
EOF
    i=0
    for NAME in ${enames} ; do
        echo "case ${i}: return ${NAME};" >>"${fout}"
        i="$(($i + 1))"
    done
    cat <<EOF >>"${fout}"
                  default: return ${tname}_inv();
        }
};

// @brief Return a string with the name of each ${tname} value.
//
// If the value is not a valid value for ${tname} but the specific
// value returned by ${tname}_inv() this returns "<${tname} invalid>"
// and otherwise "<${tname} unknown>".
inline char const* ${tname}_names(${tname} x) {
       if (x == ${tname}_inv()) return "<${tname} invalid>";
       switch (x) {
EOF
    i=0
    for NAME in ${enames} ; do
        echo "case ${NAME}: return _SHNELL_STRINGIFY(${NAME});" >>"${fout}"
        i="$(($i + 1))"
    done
    cat <<EOF >>"${fout}"
        default: return "<${tname} unknown>";
    }
}

// @brief Return the position in the declaration of each ${tname}
// value.
inline unsigned ${tname}_pos(${tname} x) {
       switch (x) {
EOF
    i=0
    for NAME in ${enames} ; do
        echo "case ${NAME}: return ${i};" >>"${fout}"
        i="$(($i + 1))"
    done
    cat <<EOF >>"${fout}"
             default: return -1;
    }
}
EOF
    tokenize < "${fout}"
    #tokenize < "${fout}" >&2
    #cat "${fout}" >&2
}

arguments args

enum ${args}

