#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## establish `atexit` handlers

### Usage:
#pragma CMOD insert atexit FUNCTION0 ...

# where `FUNCTIONx` are functions that have the signature

#void FUNCTIONx(void);

# This is intended to be used with the `implicit` directive.

# The `atexit` directive establishes atexit handlers that are intended
# to be run after any thread but the last of an execution has
# finished. It is implemented by means of the `atexit` C library call
# and is compatible with explicit calls to that function.

# The order of the execution of these handlers from different TU is
# the inverse of the "used-by" relation as detected by the `implicit`
# directive. That is, if such a handler is called, we know that all
# handlers from all TU that use the current TU have been executed.

# See `startup` for an analogous directive hat establishes handlers
# that are run at the start of an execution, and how the order of
# invocation is established.


SRC="$_" . "${0%%/${0##*/}}/import.sh"

import arguments
import echo
import tmpd
import join

endPreamble $*

aefunc () {
    aename="${TMPD}/_Atexit.txt"
    join "\n" $*
    echo "${joinRet}" >> "${aename}"
}

arguments argv
aefunc ${argv}
