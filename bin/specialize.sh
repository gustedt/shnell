#!/bin/sh -f

## Specialize a code snippet for a set of values.

### Usage:
# For an existing C variable NAME use this as
#pragma CMOD amend specialize NAME [value1 ... ]

# Here values should be numeric and admissible for a `==` operator, or
# the special string `else`. Thus this construct is more powerful than
# a switch statement, because in addition to integers, floating point
# values can also be used as values.

# A copy of the code snippet enclosed in its own block is created for
# each value, plus a fallback version if the value `else` is in the
# list. Within each of these copies the string `${NAME}` is replaced by
# the actual value within that copy. In the fallback version `${NAME}`
# is replaced by `NAME`, so not much is gained, there.

# If `else` is not in the list, and thus there is no fallback version,
# you can add a fallback just after the closing pragma:

#pragma CMOD done
#else {
#// code for the general case
#}

### Example:

# The combination of this directive with the stringification feature
# of meta-variables allows to generate output that does not depend on
# dynamic interpretation of format strings

#pragma CMOD amend specialize x = false true
#puts("x is " #${x})
#pragma CMOD done


SRC="$_" . "${0%%/${0##*/}}/import.sh"
import tmpd
import echo
import arguments
import tokenize

endPreamble $*

source="${tmpd}/specialize$$.txt"
untokenize > "${source}"

specialize () {

    name="$1"
    shift

    [ -n "${PHASE2}" ] && echo "#line 1 \"<specialize $1>\""
    for val in $* ; do
        if [ "${val}" = "else" ] ; then
            havelse=1
        else
            echo "${eif}if (${name} == ${val}) {"
            echo "#pragma CMOD amend bind ${name}=${val}"
            cat "${source}"
            echo "#pragma CMOD done"
            [ -n "${PHASE2}" ] && echo "#line 1 \"<specialize end ${val}>\""
            echo -n "}"
            eif="else "
        fi
    done
    if [ -n "${havelse}" ] ; then
        echo "else {"
        echo "#pragma CMOD amend bind ${name}=${name}"
        cat "${source}"
        echo "#pragma CMOD done"
        [ -n "${PHASE2}" ] && echo "#line 1 \"<specialize end>\""
        echo -n "}"
    fi
    echo
}

arguments args

specialize $args | tokenize | shnlpathDo expand
