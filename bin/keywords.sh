#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## Produce a regexp for C keyword tokens

# The result of this code snippet are variables `keywordsSED` and
# `keywordsSH` for `sed` and `sh` regular expression to detect C's
# keywords, respectively.

# Additionally detected are the future keywords that will replace
# underscore-capital keywords, `false`, `true`, `nullptr` and
# `defined`.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import join
import echo

### separate keywords according to the maximal crunch rule

# This is protected by an "include" guard.

if [ -z "${keywords}" ] ; then

    keyw="
_Alignas
_Alignof
_Atomic
_Bool
_Complex
_Decimal128
_Decimal32
_Decimal64
_Generic
_Imaginary
_Noreturn
_Static_assert
_Thread_local
alignas
alignof
auto
bool
break
case
char
complex
const
continue
default
defined
do
double
else
enum
extern
false
float
for
goto
if
imaginary
inline
int
long
nullptr
register
restrict
return
short
signed
sizeof
static
static_assert
struct
switch
thread_local
true
typedef
union
unsigned
void
volatile
while
"

    join '\|' ${keyw}
    export keywordsSed="${joinRet}"

    join '|' ${keyw}
    export keywordsSH="${joinRet}"

fi

endPreamble $*
