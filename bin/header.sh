#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## A tool to extract a header from a C file

### Usage: header [IDS] [EXT] [STAT]

# Where the C source file is read from `stdin` and the header is
# dumped to `stdout`.  The intent is to have the same line structure
# in the header than in the source, such that error indications of
# compilers may be tracked to the orginal source.

# `IDS`, `EXT` and `STAT` are optional file names to which lists of
# the declared idenfiers are written:

# -- `IDS` are all identifiers without linkage,

# - `typedef` names

# - tag names of declared `struct`, `union` and `enum` types,

# - enumeration constants

# - macro names

# -- `EXT` are all identifiers with external linkage, that is global
#    objects and functions that have not been declared `static`

# -- `STAT` are all identifiers with internal linkage, that is global
#    objects and functions that have been declared `static`

### Procedure

# We will be treating declarations (including definitions) as we go by
# collecting data about them in `category`, `storage` and `id`.

# The `category` is only used when we have met a definition with
# `{}`. It will be `struct`, `union` or `enum` when *defining* such a
# construct, and "function" if we are defining a function. For
# `struct`, `union` or `enum` the definition will be emitted, for
# functions (unless they are `inline`, see below) the function body is
# suppressed.

# If the category is `enum` the definition is in addition scanned for
# enumeration constants that are then classified to have no predefined
# linkage.
category=

# `storage` will hold the storage specifiers (in a broad sense) that
# we encountered. If it contains `static` we assume that this defines
# an identifier with internal linkage. If it has `typedef` it has no
# predefined linkage. If it contains `inline`, the function body is
# emitted.

# Note that in C, parameter lists may also may contain the keyword
# `static`, but this use is detected by the nesting computation, see
# below.
storage=

# `id` holds all the magic of C declarations. It is used to cumulate
# information of the type in its first word. For basic integer or
# floating types this first word will cumulate a comma separated list
# of keywords such as `long,int` or `_Complex,double` (no
# spaces!). For all other types it will hold the `typedef` or tag
# name, e.g `size_t` or `string` if the declaration had `struct string`.
# When collecting this information, all qualifiers are simply ignored.

# For continuation declarations such as variables that are defined
# together with a `struct` or `union` definitions, or comma-separated
# declarations the first word will just be `continuation`.

# The second word of `id` is then the next identifier in the
# declaration that is found after this. In C this is always the
# declared identifier, regardless of the punctuation structure
# surrounding it.
id=

# We have to distinguish two different types of commas in a
# declaration, commas that separate function arguments and commas
# that are continuation declarations. The first are always found
# inside parenthesis, so we count the level of nestedness to
# distinguish these two situations.

# This also helps to decide if the keyword `static` is a storage
# specifier or decorates an array parameter of a function.
nest=0

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import echo
import tokenize
import tmpd
import ballanced

endPreamble $*

#### Ignore initializers for the header
# This dumps all the source to `/dev/null` until we find a terminating
# semicolon or comma.
processInit () {
    local callbackParen=linesCallback
    local callbackBrace=linesCallback
    local callbackBracket=linesCallback
    local callbackAttr=linesCallback
    while [ "${top}" ] ; do
        case "${top}" in
            ([\;,])
                return
                ;;
            (\(|\{|\[|\[\[)
                callBack
                ;;
            (*)
                deleteWord
                ;;
        esac
    done
}

dumpOut () {
    while match "[\#[:cntrl:]]*" "${top}" ; do
        #report "dumpOut: ${top} ${line}"
        case "${top}" in
            (\#*)
                echo -n "${top} ${line}"
                top=
                line=
                deleteWord
                ;;
            (*)
                popWord
                ;;
        esac
    done
}

#### Classify an identifier, if any, into one of the three linkage classes.
# $1 is the `storage`, $2, $3, $4 are the three file names, $5 is the
# type, and $6 is the identifier, if any.
getId () {
    if [ $# -gt 5 ] ; then
        case "$1" in
            (*typedef*)
                echo "$6" >> "$2"
                ;;
            (*static*)
                echo "$6" >> "$4"
                ;;
            (*)
                echo "$6" >> "$3"
                ;;
        esac
    fi
}

header () {
    local ids
    if [ $# -gt 0 ] ; then
        ids="$1"
    else
        newTmp ids txt
    fi
    if [ $# -gt 1 ] ; then
        syms="$2"
    else
        newTmp syms txt
    fi
    if [ $# -gt 2 ] ; then
        stats="$3"
    else
        newTmp stats txt
    fi
    if [ $# -gt 3 ] ; then
        types="$4"
    else
        newTmp types txt
    fi
    popWord
    newTmp head
    newTmp body
    newTmp fine
    echo -n >"${fine}"
    category=
    storage=
    nest=0

    while [ "${top}" ] ; do
        report "seeing: '${top}' '${id}'"
        case "${top}" in
            (\[\[)
                ballancedAttr >>"${head}"
                popWord >>"${head}"
                continue
                ;;
            (\#*)
                #report "seeing: '${top}' '${id}'"
                echo -n "${top} ${line}" >>"${head}"
                top=
                line=
                deleteWord >>"${head}"
                continue
                ;;
            ([\{])
                # This must start a type or function definition. An
                # initializer would have been preceeded by an = sign,
                # and thus already have been eaten.
                id="${id#[,]}"
                if hasBlank ${id} ; then
                    # is a function
                    category="function"
                    getId "${storage}" "${ids}" "${syms}" "${stats}" ${id}
                    case "${storage}" in
                        (*inline*|*extern*|*static*)
                            true
                            ;;
                        (*typedef*)
                            echo ${id} >> "${types}"
                            ;;
                        (*)
                            storage="extern"
                            echo -n "extern "
                            ;;
                    esac
                else
                    case "${category}" in
                        (struct|union|enum)
                            echo "${id}" >> "${ids}"
                            echo "${category}" "${id}" >> "${types}"
                            ;;
                        (*)
                            echo -n "/*weired category ${category}*/"
                            ;;
                    esac
                fi
                if ! match "*static*" "${storage}" || match "*inline*" "${storage}" ; then
                    ${CAT} "${head}"
                fi
                echo -n > "${head}"

                # Collect the definition
                ballancedBrace > "${body}"
                if match "enum|struct|union" "${category}" ; then
                    ${CAT} "${body}"
                    # Enumeration constants are also global
                    # identifiers.
                    if [ "enum" = "${category}" ] ; then
                        "${SED}" "
:collect
\$,\$!{
        N
        # remove preprocessor directives
        s!\n[[:blank:]]*\#.*\$!!
        b collect
}
s![{]! !g
s![}]!,!g
# remove comments
s!${markCL}[^ ]*${markCR}! !g
s!,[[:blank:]-]*,!,!g
s!\n! !g
s![[:blank:]-]*\([^,=[:blank:]-]\{1,\}\)[^,]*,!\1\n!g
" "${body}" >> "${ids}"
                        #complain "ids after enum"
                        #cat "${ids}" >&2
                    fi
                else
                    category="function"
                    if match "*inline*" "${storage}" ; then
                        ${CAT} "${body}"
                    else
                        # Only provide the header for such a function.
                        if ! match "*static*" "${storage}" ; then
                            echo ";"
                        fi
                        # Produce as many empty lines as are in the body.
                        "${SED}" "1,1 d ; s!.*!! " "${body}"
                    fi
                fi
                echo -n > "${body}"
                # The closing parenthesis of a function definition
                # terminates the definition, where as for tag types
                # there still could be dependent definitions
                # comming. But the latter don't need intervention to
                # add "extern" or similar.
                if [ "${category}" = "function" ] ; then
                    id=
                    storage=
                    category=
                    dumpOut
                    continue
                else
                    id=continuation
                fi
                while match "[-]*" "${top}" ; do
                    popWord
                done
                category=
                #report "top: ${top}"
                ;;
            (\()
                nest="$((${nest} + 1))"
                popWord >> "${head}"
                ;;
            (\))
                nest="$((${nest} - 1))"
                popWord >> "${head}"
                ;;
            (\,)
                if [ "${nest}" -eq 0 ] ; then
                    # We are at the end of this declaration and seeing
                    # a continuation.
                    if ! match "*static*" "${storage}" ; then
                        ${CAT} "${head}"
                    fi
                    echo -n > "${head}"

                    # Retrieve the identifier.
                    id="${id#[ ,]}"
                    getId "${storage}" "${ids}" "${syms}" "${stats}" ${id}

                    if match "*typedef*" "${storage}" && hasBlank ${id} ; then
                        echo "${id#* }" >>"${types}"
                    fi

                    id=continuation
                fi
                if match "*static*" "${storage}" ; then
                    deleteWord
                else
                    popWord >> "${head}"
                fi
                ;;
            ([\;=])
                # We are at the end of this declaration.
                if ! match "*extern*|*static*|*typedef*" "${storage}" && hasBlank ${id} ; then
                    storage="extern"
                    echo -n "extern "
                fi
                # We are using "match" because this could have
                # thread_local in addition to static.
                if ! match "*static*" "${storage}" ; then
                    ${CAT} "${head}"
                fi
                echo -n > "${head}"

                # Retrieve the identifier.
                id="${id#[ ,]}"
                getId "${storage}" "${ids}" "${syms}" "${stats}" ${id}

                if match "*typedef*" "${storage}" && hasBlank ${id} ; then
                    echo "${id#* }" >>"${types}"
                fi

                case "${top}" in
                    ([=])
                        processInit
                        if [ "${top}" = "," ] ; then
                            id=continuation
                            if match "*static*" "${storage}" ; then
                                deleteWord
                            else
                                popWord
                            fi
                            continue
                        fi
                        ;;
                esac
                case "${top}" in
                    (\;)
                        # Most likely we are at the end of a line.
                        if ! match "*[[:alnum:]_[:punct:]]*" "${line}" ; then
                            # Only controls remain. Output them and skip
                            # to the next line.
                            if ! match "*static*" "${storage}" ; then
                                echo -n "${top} ${line}"
                                top=
                                line=
                            fi
                        fi
                        ;;
                esac
                if match "*static*" "${storage}" ; then
                    deleteWord
                else
                    popWord
                fi
                category=
                storage=
                id=
                dumpOut
                ;;
            (enum|struct|union)
                category="${top}"
                popWord >> "${head}"
                ;;
            (${marksl}*|${markCL}*|[\#\"]*|*${markib}*|\<*\>)
                popWord >> "${head}"
                ;;
            (static)
                # `static` may also appear within parameter lists
                if [ "${nest}" -eq 0 ] ; then
                    append storage "${top}"
                fi
                popWord >> "${head}"
                ;;
            (typedef|inline|extern|_Thread_local|thread_local|_Noreturn|noreturn)
                append storage "${top}"
                popWord >> "${head}"
                ;;
            (const|volatile|restrict|_Atomic)
                popWord >> "${head}"
                ;;
            (int|long|double|float|unsigned|signed|complex|_Complex|_Decimal*)
                id="${id#[,]}"
                if ! hasBlank ${id} ; then
                    # we have not yet seen the id
                    id="${id},${top}"
                fi
                popWord >> "${head}"
                ;;
            # Be sure to capture identifiers that contain an
            # underscore or an ASCII alpha.
            (*[[:alpha:]_]*)
                report "identifier ${top}"
                if ! hasBlank ${id} ; then
                    # we have not yet seen the id
                    append id "${top}"
                fi
                popWord >> "${head}"
                ;;
            (*[[:cntrl:][:punct:]]*)
                report "dumping ${top}"
                popWord >> "${head}"
                ;;
            # This should only match identifiers that don't contain an
            # ASCII character.
            (*)
                report "identifier ${top}"
                if ! hasBlank ${id} ; then
                    # we have not yet seen the id
                    append id "${top}"
                fi
                popWord >> "${head}"
                ;;
        esac
    done
    if [ -s "${head}" ] ; then
        complain "suspicious contents at end of file, flushing"
        ${CAT} "${head}"
        echo -n "${head}"
    fi
}
