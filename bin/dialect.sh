#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## Implement a pragma dialect

# This expects arguments in the form

#DIALECTNAME AMEND0[=[VALUE0]] AMEND1[=[VALUE1]] ... [: INSERT0[=[VALEUR0]] INSERT1[=[VALEUR1]] ...]

# Without special characters, and where the ":" character separates
# the lists for amendments and insertions.

# This replaces all occurrences of

#pragma DIALECTNAME AMENDx

# or

#pragma DIALECTNAME INSERTx

# by

#pragma CMOD amend VALUEx

# or

#pragma CMOD insert VALEURx

### Defaults

# If `=` and `VALUEx` are omitted, `VALUEx` defaults to `AMENDx`.  If
# `=` and `VALEURx` are omitted, `VALEURx` defaults to `INSERTx`.

# If the `VALUEx` or `VALEURx` part of a pair is omitted, but an `=`
# sign is still present, `AMENDx` or `INSERTx` is disabled for this
# dialect.

# In particular, `amend=` will switch off all other directives for
# amendments, and `insert=` will do the same for insertions.


SRC="$_" . "${0%%/${0##*/}}/import.sh"

import arguments
import join

endPreamble $*

dialect () {
    name="$1"
    shift
    shnlpathRaw evaluation                                                      \
        && CMOD_AMEND="$@" CMOD_AMEND_ARGUMENTS="${name}" exec "${shnlpathRet}"
    return 1
}

arguments argv
dialect ${argv}
