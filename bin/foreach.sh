#!/bin/sh -f

## Instantiate the amended code multiple times by successively binding
## values to a variable

#pragma CMOD foreach NAME[:INDEX] [=] TOKEN0 TOKEN1 ...

# This instantiates the depending code snippet as often as their are
# tokens. For each of these instantiations the meta-variable `${NAME}`
# is bound to the corresponding token `TOKENx`. See the description of
# `regVar` how these tokens can be manipulated.

# If `:INDEX` is also present, the meta-variable `${INDEX}` is bound
# to the number of the current instance, instance counts starting at
# `0`.

### Example

#typdef enum coucou {
##pragma CMOD amend foreach COUCOU = ka ke ki kuku
#${COUCOU},
##pragma CMOD amend done
#} coucou;

# Will generate four copies of the depending source where the
# appearance of the string `${COUCOU}` will be replaced by the each of
# the words in the list, that is

#typedef enum coucou {
#ka,
#ke,
#ki,
#kuku,
# } coucou;

# The following declares an array of names of these enumeration
# constants.

#char const*coucouNames[] = {
##pragma CMOD amend foreach COUCOU:I = ka ke ki kuku
#[${I}] = #${COUCOU},
##pragma CMOD amend done
#}

# is expanded to

#char const*coucouNames[] = {
#[ 0 ] = "ka",
#[ 1 ] = "ke",
#[ 2 ] = "ki",
#[ 3 ] = "kuku",
#}

# Note that the `#` in `#${COUCOU}` forces it to be replaced by a string
# containing the value and that `${I}` is replaced by the position of
# the token in the list.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import tmpd
import arguments
import regVar
import echo
import tokenize

### See also:
### `bind`
### `do`
### `env`
### `let`

endPreamble $*

source="${tmpd}/foreach$$.txt"

cat > "${source}"

foreach () {
    fename="$1"
    shift
    case "${fename}" in
        (*:*)
            feI="0"
            feindex="${fename##*:}"
            fename="${fename%%:*}"
            report "seeing ${fename} and index ${feindex}"
            for val in $* ; do
                "${SED}" "
$(regVar ${fename} ${val})
$(regVar ${feindex} ${feI})
" < "${source}"
                feI="$((${feI} + 1))"
            done
            ;;
        (*)
            for val in $* ; do
                "${SED}" "$(regVar ${fename} ${val})" < "${source}"
            done
            ;;
    esac
}

arguments name args

foreach "${name}" ${args}
