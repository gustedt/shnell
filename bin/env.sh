#!/bin/sh

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2018

## Evaluate the argument list as the names of environment variables
## and substitue with the value

### Usage:

#`pragma CMOD amend env [NAME0=]VARIABLE0[:DEFAULT0] [NAME1=]VARIABLE1[:DEFAULT] ...`

# Replace meta-variable `NAMEx` (`${NAME0}`, `${NAME1}` ...) by the
# values of environment variables `VARIABLEx`.

# If `NAMEx=` is omitted `NAMEx` defaults to `VARIABLEx`.

# Depending on the status of `VARIABLEx`, `DEFAULTx` can be used to
# control the returned value. The syntax is borrowed from the shell
# for parameter expansion. Therefore `DEFAULTx` must start with one of
# the characters `-`, `+` or `?`, but other than for the shell `=` is
# not allowed:

# `-VALUEx` sets `VALUEx` as default value if the environment variable
# is unset or empty.

# `+VALUEx` provides an alternate value `VALUEx`, but only if the
# environment variable is set and not empty. Otherwise the result is
# empty.

# `?VALUEx` is similar to the previous, but if the environment
# variable is unset or empty, the compilation is aborted.

### Effect on the code that depends on this directive

# All occurrences of `${NAMEx}` etc (with the dollar and the braces)
# in the submitted text are replaced by the obtained value. All
# occurences of `#${NAME0}` (with the hash, the dollar and the braces)
# are replaced by the value put inside a C string.

### Example:

#pragma CMOD amend env SH=SHELL PATH HOME:?1 ANS=ANSWER:-42
#char shell[] = #${SH};
#char path[] = #${PATH};
#int const hasHome = ${HOME};
#int const everything = ${ANS};

# This should usually be replaced by something similar to

#char shell[] = "/bin/bash";
#char path[] = "/usr/bin:/bin";
#int const hasHome = 1;
#int const everything = 42;

# unless the environment variable `ANSWER` is set to some different
# value (that value is then used for `everything`), or unless the
# environment variable `HOME` is unset or empty (the compilation would
# abort).


SRC="$_" . "${0%%/${0##*/}}/import.sh"

import match
import list
import join
import echo
import arguments

### See also:
### `bind`
### `do`
### `foreach`
### `let`
### `POSIX`

endPreamble $*

mexp=

arguments args

for variable in ${args} ; do
    case "${variable}" in
        (*=*=*)
            complain "attempt to set an environment variable, aborting"
            exit 1
            ;;
        (*=*)
            alias="${variable%=*}"
            variable="${variable#*=}"
            ;;
        (*)
            alias="${variable%:*}"
            ;;
    esac
    case "${variable}" in
        (*:[-+\?]*)
            name="${variable%:[-+\?]*}"
            default="${variable#${name}}"
            variable="${name}"
            report "looking for ${variable} with replacement ${default}"
            ;;
        (*)
            default=
            ;;
    esac
    case "${variable}" in
        (*[[:lower:]]*)
            complain "${variable} has lower case letters, aborting"
            exit 1
            ;;
        (*)
            if [ -n "${default}" ] ; then
                case "${default}" in
                    (:\?*)
                        error="__error$$__"
                        eval "value=\${${variable}:+${error}}"
                        if [ "${error}" != "${value}" ] ; then
                            complain "replacement of ${variable} by ${default} failed, aborting"
                            exit 1
                        fi
                        ;;
                esac
                eval "value=\${${variable}${default}}"
            else
                eval "value=\${${variable}}"
            fi
            if [ -n "${value}" ] ; then
                join "\ " ${value}
                append mexp "${alias}=${joinRet}"
            else
                if [ -n "${value:+true}" ] ; then
                    append mexp "${alias}="
                else
                    report "${variable} ignored"
                fi
            fi
            ;;
    esac
done

report "have: ${mexp}"

shnlpathRaw bind                                                        \
            && CMOD_AMEND_ARGUMENTS="${mexp}" exec "${shnlpathRet}"

return 1
