#!/bin/sh -f

## Obtain an argument list of a directive and separate it into first
## word and rest.

### Example:
# This can be used as in the following

#arguments argv
#dialect ${argv}

# Here, the first line stores the arguments of the directive in the
# variable `argv`. The second then would split these up into "words"
# and passes these words to the function `dialect` as arguments.
# Note, that this removes an optional `=` sign between the first word
# and the rest of the arguments.

# The environment list `CMOD_AMEND_ARGUMENTS` (which holds the
# arguments of the directive on startup) is cleared.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

endPreamble $*

arguments () {
    while [ $# -gt 1 ] ; do
        eval "$1='${CMOD_AMEND_ARGUMENTS%%[[:blank:]=]*}'"
        CMOD_AMEND_ARGUMENTS="${CMOD_AMEND_ARGUMENTS#*[[:blank:]=]}"
        # remove an optional = sign
        CMOD_AMEND_ARGUMENTS="${CMOD_AMEND_ARGUMENTS#*=}"
        shift
    done
    eval "$1='${CMOD_AMEND_ARGUMENTS}'"
    CMOD_AMEND_ARGUMENTS=""
    unset CMOD_AMEND_ARGUMENTS
}
