#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## introduce short names for other TU

### Usage:
#pragma CMOD amend alias NAME0[=LNNAME0] NAME1[=LNAME1] ...

# Replace each short name `NAMEx` (without `::`) with the full name
# `LNAMEx` of a TU (which may or may not have `::`). This replaces the
# unsuffixed identifier and also when it is used as a prefix.

# The short names `NAMEx` should not be used for a different purpose
# in the current TU. The long names `LNAMEx` can be used several times
# with different aliases.

# If `LNAMEx` is omitted, `NAMEx` is used as shortname for the current
# TU.

# Since this directive scans the whole code, it should only appear
# multiple times in a TU if it is bounded by a `done` directive.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import tmpd
import join
import arguments
import echo
import tokenize

endPreamble $*

arguments argv

argv=$(echo ${argv} | ${SED} "s!::!${markc}!g")

newTmp regexp sed
echo -n > "${regexp}"

for arg in ${argv} ; do
    case "${arg}" in
        (*=*)
            al="${arg%=*}"
            rep="${arg#*=}"
            echo "s! ${al} ! ${rep} !g"               >>"${regexp}"
            echo "s! ${al}${markc}! ${rep}${markc}!g" >>"${regexp}"
            ;;
        (*)
            report "setting short name to ${arg}"
            sname="${TMPD}/_Sname.txt"
            echo "${arg}" > "${sname}"
            ;;
    esac
done

${SED} -f "${regexp}"
