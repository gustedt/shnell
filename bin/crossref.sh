#!/bin/sh

#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## Crossref the html files in a list

### Usage:
#$0 NAME0.html NAME1.html ...

# All html files that are given on the command line are searched for
# markers such as <code>&lt;code&gt;NAMEx&lt;code&gt;</code> and if
# <code>NAMEx.html</code> is in the list, an anchor for crossreference
# to that file is added.

# The html files are modified in-place.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import list
import join
import echo
import match

### See also:
### `import`


endPreamble $*

names=

for f in $* ; do
    append names "${f%.html}"
done

join "\|" ${names}

sedreg="s!<\([[:alnum:]]\{1,\}\)>\(${joinRet}\)</\1>!<a href=\"\2.html\">&</a>!g"

report "regexp: ${sedreg}"

for f in $* ; do
    complain "crossreferencing in place: $f"
    "${SED}" -i "
s!<span class=\"dt\">\(static\|_Generic\|typeof\|auto\|register\|const\|double\|float\)</span>!<span class=\"kw\">\1</span>!g
${sedreg}
s!href=\"${f}\"!!g
s!<a \([^>]*><a *\)\{1,\}!<a !g
s!\(</a>\)\{1,\}!</a>!g
" "${f}"
done
