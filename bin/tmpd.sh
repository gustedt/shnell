#!/bin/sh

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2017

## Temporary files and garbage collection

# If not otherwise specified temporary files are created in a specific
# directory below /tmp

# If this module is used by recursive scripts, the temporary
# directories for them will be nested according to the call
# dependency.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import list
import echo
import match

endPreamble $*


TMP=${TMP:-/tmp}
RM=${RM:-rm}
MKTEMP=${MKTEMP:-mktemp}

garbage () {
    append garbageBin $*
}

#### Program exit
# This function is called at the end of execution to clean up
# temporary files. It is installed on all exits that we might be able
# to capture.
cleanup () {
    # close stdout and stderr such that the filter can terminate
    exec 1>&-
    exec 2>&-
    # wait for any filter
    wait
    ${RM} -rf ${garbageBin}
}

#### Signal handling
# This function is called in addition, if a signal has been caught. It
# prints the number of the signal and then runs `cleanup`.
killup () {
    sig="$?"
    complain "exiting on signal (${sig}), cleaning up"
    cleanup
}

trap cleanup EXIT
for sig in  HUP INT QUIT ILL ABRT BUS FPE SEGV PIPE TERM IO SYS ; do
    trap killup ${sig} || true
done

# we need some temporary
tmpd=$(${MKTEMP} -d "${TMP}/cmod-tmpd.XXXXXXXXXXXXXXXX")
if [ -z "${CMOD_KEEP}" ] ; then
    garbage "${tmpd}"
fi

if ! [ "${jobid}" ] ; then
    export jobid="${tmpd#*.[[:alnum:]][[:alnum:]][[:alnum:]][[:alnum:]][[:alnum:]][[:alnum:]][[:alnum:]][[:alnum:]]}"
fi

# have recursive calls use the new directory
export TMP="${tmpd}"
# give recursive calls access to the top directory
if [ -z "${TMPD}" ] ; then
    export TMPD="${tmpd}"
    export JOBID="${jobid}"
fi


tmpdCount=0

#### Get a new temporary file name.
# Call this as
#
#`newTmp VARIABLE [EXTENSION]`
#
# where `EXTENSION` is set to `c` if omitted. After the call, variable
# `VARIABLE` will be set to the result.
newTmp () {
    local ext="${2:-c}"
    eval "$1='${tmpd}/tmp-$1-No-${tmpdCount}.${ext}'"
    tmpdCount="$((${tmpdCount}+1))"
}

#### Get a new number that is unique for the same jobid
# Call this as
#
#`newNumber VARIABLE`
#
# After the call, variable `VARIABLE` will be set to the result. All
# places that use the same name `VARIABLE` will receive a different
# value during processing for the same job, even if they end up being
# in different sub-processes.
newNumber () {
    # A file that will contain the actual number of the holdback.
    # This technique allows to have different capture names for all
    # holdbacks such that they don't step on each others feet.
    numberFile="${TMPD}/tmpd-backup-$1.txt"
    # create it, if it doesn't exist, yet, and read the current value
    echo -n >>"${numberFile}"
    read -r numberHere < "${numberFile}"
    numberHere=$(( ${numberHere:-0} + 1 ))
    echo "${numberHere}" >"${numberFile}"
    eval "$1=${numberHere}"
}
