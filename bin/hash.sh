#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2017

### A simple hash table for shell programming

# The shell has no builtin hash function that would be easy to
# use. Here, we provide some simple minded approach to close that
# gap. It has the important restriction that the hashed values must
# not contain special characters.

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import match

# The implementation uses naming conventions and the shell's `eval`
# feature. All of this is relatively fragile. The name of the hash
# must be alphanumeric ASCII. The key value can be any string.

endPreamble $*

## Transform a string into hexvalues prefixed with "U"
# This is then used as unique name component for the shell identifier
# that emulates the hash entry.
hexify () {
    arg="$*"
    # Eat one character at a time. The hexvalues are then prefixed
    # with "U" to ensure that utf8 characters with different coding
    # length cannot clash.
    while [ "${arg}" ] ; do
        short="${arg#?}"
        printf "U%X" \'${arg%${short}}
        arg="${short}"
    done
}

hexname () {
    hexName="HASH_$1_$(hexify $2)_HSAH"
}

## Store the value of key $3 of hash $2 in variable $1
# value $4 is the default if that entry does not exist
hashout () {
    # local var=$1
    # local hash="$2"
    # local key="$3"
    # local def="$4"
    hexname "$2" "$3"
    eval "$1=\"\${${hexName}:-$4}\""
}

## Store value $3 into key $2 of hash $1
#
hashin () {
    # local hash="$1"
    # local key="$2"
    # local val="$3"
    hexname "$1" "$2"
    eval "${hexName}='$3'"
}

## Echo the value of key $2 of hash $1.
# value $3 is the default if that entry does not exist
hashEcho () {
    hashout hashEchoRet $*
    echo "${hashEchoRet}"
}

## Add value $3 to the list of key $2 of hash $1
#
# This collects data in a hash position as a ;-separated list
hashadd () {
    local hash="$1"
    local key="$2"
    local val="$3"
    local prev
    hexname "${hash}" "${key}"
    eval "prev=\"\${${hexName}}\""
    if [ -n "${prev}" -a "${prev}" != "${val}" ]; then
        val="${prev};${val}"
    fi
    eval "${hexName}='${val}'"
}
