#!/bin/sh

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2018

## Evaluate the argument list as the names of POSIX' getconf configuration
## variables and substitue with the value

### Usage:

#pragma CMOD amend getconf [NAME0=]VARIABLE0 [NAME1=]VARIABLE1 ...

# Replace meta-variable `NAMEx` (`${NAME0}`, `${NAME1}` ...) by the
# values of POSIX' getconf configuration variables `VARIABLEx`.  A leading
# underscore in such a POSIX configuration variable can be omitted.
# E.g the POSIX variable `_REGEX_VERSION` can be refered to as
# `REGEX_VERSION`.

# If `NAMEx=` is omitted `NAMEx` defaults to `VARIABLEx`.

# Two POSIX variables are always caught from the environment,
# `POSIX_VERSION` and `POSIX2_VERSION`. Thus a portable method to
# ensure that a platform is POSIX is to add

#pragma CMOD amend getconf
#static_assert(${POSIX_VERSION} > 0}, "we depend on POSIX interfaces")

# On a POSIX platform this should expand to something as

#static_assert(200809L > 0}, "we depend on POSIX interfaces");

# On other platforms this should be

#static_assert(-1L > 0}, "we depend on POSIX interfaces");

# POSIX variables could be obtained with the C APIs `sysconf` and
# `confstr`. The first type of variable results in an integer value
# that is of type `long`. We add the `L` integer suffix where we
# detect an integer value. For variables that are not provided by the
# platform, the return value is `-1`.

# The second type of variable results in a string. These should not be
# used with the `${NAMEx}` notation but only with `#${NAMEx}`.

### Example:

#pragma CMOD amend getconf FALLBACK_PATH=PATH INT_MAX
#char fallback[] = #${FALLBACK_PATH};
#long int_max[] = ${INT_MAX};

# This should be replaced by something similar to

#char fallback[] = "/bin:/usr/bin";
#long int_max = 2147483647L;



SRC="$_" . "${0%%/${0##*/}}/import.sh"

import match
import list
import echo
import join
import arguments

### See also:
### `bind`
### `do`
### `foreach`
### `let`

endPreamble $*

mexp=

arguments args

for variable in "POSIX_VERSION" "POSIX2_VERSION" ${args} ; do
    case "${variable}" in
        (*=*)
            alias="${variable%=*}"
            variable="${variable#*=}"
            ;;
        (*)
            alias="${variable}"
            ;;
    esac
    value=$(LANG=C getconf "${variable}" 2>/dev/null || LANG=C getconf "_${variable}" 2>/dev/null || echo "-1")
    report "${alias} (${variable}) -> ${value}"
    for v in ${value} ; do
        case "${v}" in
            (*\.*)
                value="${v}"
                ;;
            ([0-9]*)
                value="${v}L"
                ;;
            ([-][0-9]*)
                value="${v}L"
                ;;
        esac
    done
    if [ -n "${value}" ] ; then
        join "\ " ${value}
    else
        joinRet=
    fi
    append mexp "${alias}=${joinRet}"
done

shnlpathRaw bind                                                        \
            && CMOD_AMEND_ARGUMENTS="${mexp}" exec "${shnlpathRet}"

return 1
