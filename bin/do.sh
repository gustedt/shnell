#!/bin/sh -f

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## A directive to repeat code snippets

### Usage:

#pragma CMOD amend do NAME[:INDEX] [=] [START] END [OPERATION]

# This repeats the depending code a number of times where it binds the
# variable `${NAME}` to specific values as computed by
# `seq.sh`. `OPERATION` can be any of `+`, `-`, `*` or `/` followed by
# a number. If `OPERATION` is omitted it is `+1`, if the operator is
# submitted it defaults to `+`.  If `START` is also omitted it is `0`.

# The produced sequence of numbers is checked against `END` with `<`
# if the sequences increases (that is for `+` and `*`) and with `>` if
# it decreases (that is for `-` and `/`).

# If additionally prefixed with a `#` as in `#${NAME}`, the resulting
# value is put inside a C string literal. See `regVar` for more on how
# tokens can be manipulated and separated for this replacement
# procedure.

# As for `foreach` an optional `INDEX` meta-variable may also be used
# to track the current iteration number.

### Examples:

#double A[2][3] = {
#pragma CMOD amend do I = 2
#[${I}] = {
#pragma CMOD amend do J = 3
#[${J}] = ${I} + ${J},
#pragma CMOD done
#},
#pragma CMOD done
#};


# will result in the following replacement:

#double A[2][3] = {
#[0] = {
#[0] = 0 + 0,
#[1] = 0 + 1,
#[2] = 0 + 2,
#},
#[1] = {
#[0] = 1 + 0,
#[1] = 1 + 1,
#[2] = 1 + 2,
#},
#};

SRC="$_" . "${0%%/${0##*/}}/import.sh"

import arguments
import seq

### See also:
### `bind`
### `env`
### `foreach`
### `let`

endPreamble $*

arguments name vals

shnlpathRaw foreach                                                                     \
    && CMOD_AMEND_ARGUMENTS="${name} = "$(sequence ${vals}) exec "${shnlpathRet}"

# Since we exec, we should never come here.
return 1
