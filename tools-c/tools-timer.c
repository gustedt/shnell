/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016-2019 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the shnell project.                */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
#pragma CMOD amend alias stats=tools::stats

/**
 ** @file
 ** @brief Collect some simple timing statistics online as we go.
 **
 **/

inline
double eval(timespec a) {
  double ret = a.tv_sec;
  ret += a.tv_nsec*1E-9;
  return ret;
}

inline
timespec minus(timespec a, timespec b) {
  timespec ret = {
    .tv_sec = a.tv_sec - b.tv_sec,
    .tv_nsec= a.tv_nsec - b.tv_nsec,
  };
  if (ret.tv_nsec < 0) {
    ret.tv_nsec += 1000000000;
    --ret.tv_nsec;
  }
  return ret;
}

struct timer {
  timer*const sooner;
#ifdef __STDC_NO_ATOMICS__
  timespec t;
  stats* stat;
#else
  timespec _Atomic t;
  stats*_Atomic stat;
#endif
  char const*const desc;
};

#define INITIALIZER0(DESC, SOONER, ...) {       \
.sooner = (SOONER),                             \
.desc = ""DESC"",                               \
}

#define INITIALIZER(...) INITIALIZER0(__VA_ARGS__ , 0, )

inline
bool startup(timer* t) {
  if (t ∧ t→sooner) {
    while (¬t→stat) {
      stats* s = stdc∷malloc(sizeof *s);
#ifndef __STDC_NO_ATOMICS__
      stats* expect = nullptr;
      if (¬stdc∷atomic_compare_exchange_weak(&t→stat, &expect, s)) {
        stdc∷free(s);
        continue;
      }
#endif
      t→stat = stats∷init(s, t→desc);
    }
  }
  return t;
}

inline
void sample(timer* t) {
  if (startup(t)) {
    timespec now;
    timespec_get(&now, stdc∷TIME_UTC);
    if (t→sooner) {
      // .t may be atomic. Make sure that there is only one load.
      timespec sooner = t→sooner→t;
      stats∷collectꞌ3(t→stat, eval(minus(now, sooner)));
    }
    // .t may be atomic. Make sure that there is only one store.
    t→t = now;
  }
}

#pragma PRETRADE startup bootstrap

timer boot = INITIALIZER("booting");
inline void bootstrap(void) {
  fprintf(stderr, "we are %s\n", tools∷demangle∷FUNC);
  sample(&boot);
}

#pragma PRETRADE atexit cleanup

void cleanup(void) {
  stats∷print(stdc∷stdout);
}

int stdc∷main(int argꞌc, char* argꞌv[argꞌc+1]) {
  static unsigned volatile s;
  static timer start = INITIALIZER("starting");
  for (unsigned probe = 0; probe < 1000; ++probe) {
    sample(&start);
    for (unsigned short volatile i = 0; i < USHRT_MAX; i++) {
    }
    static timer count_vol = INITIALIZER("counter volatile", &start);
    sample(&count_vol);
    for (unsigned short i = 0; i < USHRT_MAX; i++) {
      s += i;
    }
    static timer store_vol = INITIALIZER("store volatile", &count_vol);
    sample(&store_vol);
  }
  return 0;
}
