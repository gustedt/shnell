// This may look like garbage but is actually -*- C -*-

/* From: George Marsaglia (geo@stat.fsu.edu)    */
/* Subject: Re: good C random number generator  */
/* Newsgroups: comp.lang.c                      */
/* Date: 2003-05-13 08:55:05 PST                */

/* Here is an example with k=5, period about 2^160, one of the fastest
   long period RNGs, returns more than 120 million random 32-bit
   integers/second (1.8MHz CPU), seems to pass all tests: */

enum  { seedꞌ160ꞌlen = 6 };
typedef stdc∷uint32_t seedꞌ160[seedꞌ160ꞌlen];

#pragma PRETRADE private bitpack

/**
 ** @brief The internal state that the PRG for ::rand and
 ** ::drand uses.
 **
 ** @remark Usually you don't have to deal with this type other than
 ** the cases described for ::get.
 **
 ** This is currently a set of 12 values of type @c stdc∷uint32_t, so 384
 ** bits. Any reasonable initialization with true random bits of such
 ** a state variable should give you a different PRG of cycle length
 ** 2^160. (Reasonable here means that initializations with 160
 ** consecutive 0 bits or more should be avoided.)
 **
 ** We initialize a different state for each thread that uses the
 ** corresponding functions. The setup of that state for each thread
 ** may incur a cost of about 16 to 20 times the call to
 ** ::rand. The initialization only produces one state out of
 ** 2^160 possibilities, so the state space of 384 is not used
 ** optimally.
 **
 ** Please let me know if you have an idea of using other portable
 ** sources of randomness for the initialization, currently we use the
 ** wall clock time, an atomic counter, the stack address of the
 ** thread and an address inside the mapped executable code. This
 ** guarantees that the states are different for all threads that run
 ** concurrently (they will not have the same stack address) or that
 ** are scheduled with a suitable time difference that exceeds the
 ** clock granularity (they will not use the same time stamp).
 **
 ** All of this is only a heuristic and can perhaps be spoofed; it might
 ** give the same initial state for threads that are scheduled in short
 ** sequence if:
 **  - the first thread terminates before the second is started
 **  - the second obtains the same time stamp as the first
 **  - the second obtains the same counter value as the first
 **  - the second is scheduled with the same stack address as the first
 **
 ** Under non-hostile circumstance this should only occur on a
 ** combination of platform and code that quickly launches a lot of
 ** threads (at least 2**32 ~ 4E9) within the granularity of its clock
 ** and has no good address randomization.
 **/
typedef seedꞌ160 seed[2];

inline
stdc∷uint32_t xorꞌshift(seedꞌ160 * s) {
  /* Use the 6th element as the running index, and compute the
     necessary derived indices mod 5 */
  ++(*s)[5];
  register stdc∷uint32_t const p0 = ((*s)[5]) % 5;
  register stdc∷uint32_t const p4 = p0 ≡ 0 ? 4 : p0-1;
  register stdc∷uint32_t const p2 = (p0 ≡ 4) ? 1 : ((p0 ≡ 3) ? 0 : p0+2);

  /* Load the part of the state variable that we need. */
  register stdc∷uint32_t const x = (*s)[p0];
  register stdc∷uint32_t const y = (*s)[p2];
  register stdc∷uint32_t const v = (*s)[p4];

  register stdc∷uint32_t t = (x^(x>>7));
  t = (v^(v<<6))^(t^(t<<13));

  /* Change just one place in the state variables. */
  (*s)[p4] = t;

  /* Multiply with an odd number */
  return (2*y + 1) * t;
}

inline
stdc∷uint32_t bitpack(void const* p) {
  stdc∷uintptr_t u = (stdc∷uintptr_t)p;
#if UINTPTR_MAX ≡ UINT32_MAX
  return u;
#else
  stdc∷uint32_t r = 0;
  do {
    r ^= u;
    u >>= 32;
  } while (u);
  return r;
#endif
}

struct state {
  seed s;
  bool isꞌinit;
};

static _Thread_local state local;
static _Atomic stdc∷uint32_t tick;

void init(seed* s) {
  /* local to this initialization call, stack address */
  stdc∷uint32_t p0 = bitpack(&s);
  /* unique to this invocation */
  stdc∷uint32_t p1 = ++tick;
  stdc∷timespec ts;
  stdc∷timespec_get(&ts, stdc∷TIME_UTC);
  stdc∷uint32_t p2 = ts.tv_sec;
  stdc∷uint32_t p3 = ts.tv_nsec;
  /* take advantage of address randomization, if any */
  stdc∷uint32_t p4 = bitpack(__func__);
  /* index, unique to this thread */
  stdc∷uint32_t ind = p0 ^ p1 ^ p2 ^ p3 ^ p4;
  seedꞌ160 st = {
    p0,
    p1,
    p2,
    p3,
    p4,
    ind,
  };
  /* mix things up a bit */
  for (unsigned i = 0; i < 32; ++i) xorꞌshift(&st);
  /* Now create two different state vectors that are de-phased. We
     can be in any state of the xorꞌshift generator, so there are
     2^160 different initializations. */
  for (unsigned j = 0; j < 2; ++j) {
    for (unsigned i = 0; i < seedꞌ160ꞌlen; ++i)
      (*s)[j][i] = xorꞌshift(&st);
  }
}

/**
 ** @brief Access the seed state for this particular thread.
 **
 ** The main purpose in user code of this function is to be able to
 ** store a pointer to the seed state at the beginning of the
 ** execution an then re-use that pointer on calls to ::rand or
 ** ::drand.
 **
 ** @code
 ** seed * seed = get();
 **
 **
 ** for (C¯size i = 0; i < largeNumber; ++i) {
 **   A[i] = rand(seed);
 ** }
 ** @endcode
 **
 ** This avoids repetitive invocations of calls to ::get that
 ** usually can't be optimized away by the compiler.
 **
 ** It may also serve to establish a reproducible state of the random
 ** generator. You'd have to use this to access the state and pump any
 ** bit pattern of your favor over that state.
 **
 ** @code
 ** seed * seed = get();
 ** memcpy(seed, (char [sizeof *seed]){ "some long string" }, sizeof *seed);
 ** @endcode
 **
 ** But beware that you'd have to apply that strategy in @b all
 ** threads and to a different value for each.
 **/
seed * get(void) __attribute__((__returns_nonnull__, __const__));
seed * get(void) {
  state* l = &local;
  if (¬l->isꞌinit) {
    init(&l->s);
    l->isꞌinit = true;
  }
  return &l->s;
}
