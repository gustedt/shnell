/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2019 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the shnell project.                */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */

static
char* demangle5(stdc∷size_t tlen, char target[tlen], char const source[],
                   char const sep[], stdc∷size_t skip);

// Determine the length of a mangle prefix.
enum {
      preflen =
      (sizeof tools∷STRINGIFY(stdc∷str∷str) - 1)
      - (sizeof "4stdc" - 1)
      - (sizeof "3str"  - 1)
      - (sizeof "3str"  - 1)
      -1 // end of mangle
};

/**
 ** @brief Demangle a mangled composed identifier into its components.
 **
 ** Similar to tools::demangle but with finer control of the buffer to
 ** which the demangled string is written.
 **
 ** @return If all goes well returns @a target, otherwise @a source.
 **/
char const* demangle4(stdc∷size_t tlen, char target[tlen], char const source[], char const* sep) {
  char const* ret = source;
  /* If no separator is provided, use the default. */
  if (¬sep) sep = "∷";
  if (stdc∷strlen(source) > preflen ∧ ¬stdc∷memcmp(source, __func__, preflen)) {
    demangle5(tlen, target, source + preflen, sep, 0);
    ret = target;
  }
  return ret;
}

static
char* demangle5(stdc∷size_t tlen, char target[tlen], char const source[],
                   char const sep[], stdc∷size_t skip) {
  char*const stop = target+tlen-1;
  char* t = target;
  for (char const*p = source; t < stop; ) {
    char* q = 0;
    stdc∷size_t len = stdc∷strtoull(p, &q, 10);
    if (q) p = q;
    else break;
    p += skip;
    len -= skip;
    if (t+len ≥ stop)
      len = stop-t;
    stdc∷memcpy(t, p, len);
    t += len;
    p += len;
    if (t ≥ stop)
      break;
    if (¬stdc∷isdigit(*p)) {
      t[0] = 0;
      return t;
    } else {
      for (char const*s = sep; t < stop ∧ *s; ++s, ++t)
        t[0] = s[0];
      t[0] = 0;
    }
  }
  t[0] = 0;
  return t;
}



#define demangle3(SOUR, SEP, …) demangle4(256, (char[256]){ 0 }, (SOUR), (SEP))

/**
 ** @brief Demangle a mangled composed identifier into its components.
 **
 ** This returns a string that has the components of its first
 ** argument separated by a separator. This separator can either be
 ** given as the second argument, or, if that is @c 0 or omitted, is
 ** chosen from a per process default value that can be set with
 ** ::C¯mod¯set.
 **
 ** To ensure that setting the separator doesn't result in a race
 ** condition, this function uses a lock to protect that separator. If
 ** this lock can't be acquired, this function returns the source
 ** argument.
 **
 ** If all goes well, this returns the string in a temporary buffer
 ** that is valid in the scope where the call to this macro is found.
 **
 ** @see demangle4 if you need finer control of the target buffer
 **/
#define demangle(…) demangle3(__VA_ARGS__, nullptr,)

/**
 ** @brief The pretty printed name of the current function.
 **/
#define demangle∷FUNC demangle4(2*sizeof(__func__)+10, (char[2*sizeof(__func__)+10]){ 0 }, __func__, nullptr)

#pragma PRETRADE private printer α show

#define show(X) \
  printf("we are '%s' with name '%s' (%s), mangled %s\n", FUNC, demangle(tools∷STRINGIFY(X)), __UNIT__, tools∷STRINGIFY(X))

void printer(void) {
  show(printer);
}

void \u03B1(void) {
  show(\u03B1);
}

int stdc∷main(void) {
  printf("length of prefix %d, %s\n", preflen, &tools∷STRINGIFY(demangle)[preflen]);
  show(demangle);
  printer();
  \u03B1();
}
