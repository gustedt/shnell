// -*- C -*-
// This needs two extra levels of evaluation
#pragma CMOD amend eval INNER
#pragma CMOD amend eval OUTER
// This is not "-32" but the shell jargon ":-" for variable default
// replacement.
#pragma CMOD amend env N=COMMA_MAX:-32

/**
 ** @brief This is the maximum number of arguments that `comma` can
 ** handle.
 **
 ** The environment variable `COMMA_MAX` can be used to control this
 ** number.
 **/
#define comma::max ${N}

#pragma OUTER amend let N2 = ${N} + 2
#pragma OUTER amend let N1 = ${N} + 1

// This creates a descending sequence of integers, separated by comma
// and each but the first preceeded by an underscore
#pragma INNER amend let ASCENDING = -join ,\ _ -seq 0 ${N2}
// With an additional underscore for the first, these serve as the
// parameter declarations of `POS`, which selects the last of them,
// the N1st.
#define POS(_##${ASCENDING}, …) _##${N1}
#pragma INNER done

// This creates a descending sequence of integers, separated by comma
// These serve as the return value of `comma`.
#pragma INNER amend let DESCENDING = -join ,\  -seq ${N} -1 -1
/**
 ** @brief Count the number of commas in the list.
 **/
#define comma(…) POS(__VA_ARGS__, ${DESCENDING}, GARBAGE)
#pragma INNER done

#pragma OUTER done
#pragma OUTER done

#pragma CMOD done
#pragma CMOD done
#pragma CMOD done

// The remaining is for testing.

#define EXAMPLE(…) printf("%d for '" #__VA_ARGS__ "'\n", comma(__VA_ARGS__));

int stdc::main(void) {
  puts("The result for the empty list should be zero.");
  EXAMPLE();
  puts("The result for the others is the number of comma in the list.");
  EXAMPLE(a);
  EXAMPLE(a, b);
  EXAMPLE(a, c, d);
  EXAMPLE(a, c, d, e);
}
