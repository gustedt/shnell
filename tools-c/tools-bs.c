/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/**
 ** @brief handle bit sets
 **/
typedef uint64_t bs;

#define IDENTITY(X) (X)
#define UNION2(X, Y) ((X)∪(Y))
#define INTERSECT2(X, Y) ((X)∩(Y))
#define PARITY2(X, Y) ((X)^(Y))

#define SET1(X) (UINT64_C(1)<<(X))

#pragma CMOD amend eval ALL
#pragma CMOD amend foreach LB:I = \  F FF FFF FFFF FFFFF FFFFFF FFFFFFF FFFFFFFF FFFFFFFFF FFFFFFFFFF FFFFFFFFFFF FFFFFFFFFFFF FFFFFFFFFFFFF FFFFFFFFFFFFFF FFFFFFFFFFFFFFF
#pragma CMOD amend foreach HB:J = 1 3 7 F
#pragma ALL amend let N = ${I} -mul 4 + ${J} + 1
#define bs∷all##${N} 0x##${HB}##${LB}
#pragma ALL done
#pragma CMOD done
#pragma CMOD done
#pragma CMOD done

#define bs∷empty UINT64_C(0)
#define bs∷all0 empty
#define bs∷all all64

#pragma CMOD amend eval ALL
#pragma CMOD amend foreach LB:I = \  0 00 000 0000 00000 000000 0000000 00000000 000000000 0000000000 00000000000 000000000000 0000000000000 00000000000000 000000000000000
#pragma CMOD amend foreach HB:J = 1 2 4 8
#pragma ALL amend let N = ${I} -mul 4 + ${J}
#define bs∷singleton##${N} 0x##${HB}##${LB}
#pragma ALL done
#pragma CMOD done
#pragma CMOD done
#pragma CMOD done

#pragma CMOD amend eval ALL
#pragma CMOD amend foreach LB:I = \  0 00 000 0000 00000 000000 0000000 00000000 000000000 0000000000 00000000000 000000000000 0000000000000 00000000000000 000000000000000
#pragma CMOD amend foreach HB:J = 3 6 C 18
#pragma ALL amend let N = ${I} -mul 4 + ${J}
#if ${N} < 63
#define bs∷duo##${N} 0x##${HB}##${LB}
#endif
#pragma ALL done
#pragma CMOD done
#pragma CMOD done
#pragma CMOD done

#pragma CMOD amend eval ALL
#pragma CMOD amend foreach LB:I = \  0 00 000 0000 00000 000000 0000000 00000000 000000000 0000000000 00000000000 000000000000 0000000000000 00000000000000 000000000000000
#pragma CMOD amend foreach HB:J = 7 E 1C 38
#pragma ALL amend let N = ${I} -mul 4 + ${J}
#if ${N} < 62
#define bs∷trio##${N} 0x##${HB}##${LB}
#endif
#pragma ALL done
#pragma CMOD done
#pragma CMOD done
#pragma CMOD done

#pragma CMOD amend eval ALL
#pragma CMOD amend foreach LB:I = \  0 00 000 0000 00000 000000 0000000 00000000 000000000 0000000000 00000000000 000000000000 0000000000000 00000000000000 000000000000000
#pragma CMOD amend foreach HB:J = F 1E 3C 78
#pragma ALL amend let N = ${I} -mul 4 + ${J}
#if ${N} < 61
#define bs∷quartett##${N} 0x##${HB}##${LB}
#endif
#pragma ALL done
#pragma CMOD done
#pragma CMOD done
#pragma CMOD done

#pragma CMOD amend do L = 1 8
// The subset of L elements, starting at position P. P defaults to 0.
#define bs::subSet##${L}(X, P) ((X) ∩ (all##${L}⌫(P)))
#define bs::subSET##${L}(X, P, …) subSet##${L}(X, P)
#define bs::subset##${L}(…) subSET##${L}(__VA_ARGS__, 0,)

// The subset of L elements, starting at position P, reindexed at 0
#define bs::downset##${L}(X, P) ((X)⌦(P) ∩ all##${L})
#pragma CMOD done

#define bs∷ALL(N) (all64⌦(64-(N)))

/**
 ** @brief Construct a bitset from the arguments
 **
 ** The arguments are assumed to be bit numbers, starting from 0 up to
 ** 63, that are to be set in the result.
 **
 ** The possible number of arguments is restricted, usually to 32.
 **
 ** If the arguments are ICE, so is the result.
 **/
#define bs(…) tools∷map(SET1, UNION2, __VA_ARGS__)

/**
 ** @brief Form the set union of all the bitsets given as arguments.
 **
 ** The arguments are assumed to be bitsets or converted to them.
 **
 ** The possible number of arguments is restricted, usually to 32.
 **
 ** If the arguments are ICE, so is the result.
 **/
#define bs∷Union(…) tools∷map(IDENTITY, UNION2, __VA_ARGS__)

/**
 ** @brief Form the set intersection of all the bitsets given as arguments.
 **
 ** The arguments are assumed to be bitsets or converted to them.
 **
 ** The possible number of arguments is restricted, usually to 32.
 **
 ** If the arguments are ICE, so is the result.
 **/
#define bs∷intersect(…) tools∷map(IDENTITY, INTERSECT2, __VA_ARGS__)

/**
 ** @brief Form the bit parity of all the bitsets given as arguments.
 **
 ** The arguments are assumed to be bitsets or converted to them.
 **
 ** The possible number of arguments is restricted, usually to 32.
 **
 ** If the arguments are ICE, so is the result.
 **/
#define bs∷parity(…) tools∷map(IDENTITY, PARITY2, __VA_ARGS__)

#define bs∷ARRAY(T, …) ((T const[]){ __VA_ARGS__ })
#define bs∷ALEN(T, …) (sizeof(ARRAY(T, __VA_ARGS__))/sizeof(T))

/**
 ** @brief Construct a bitset from the arguments
 **
 ** The arguments are assumed to be bit numbers, starting from 0 up to
 ** 63, that are to be set in the result.
 **
 ** The possible number of arguments is only restricted by the stack memory.
 **
 ** This results in a function call, so the result is never an
 ** ICE. Use the "constructor" `bs` if you need an ICE.
 **/
#define bs∷SET(…) set(ALEN(unsigned, __VA_ARGS__), ARRAY(unsigned, __VA_ARGS__))

#define bs∷isin(X, S) intersect(bs(X), S)

#define bs∷complement(X) (∁(X))

#define bs∷minus(X, Y) intersect(X, complement(Y))

inline
unsigned min(size_t len, unsigned const a[static restrict len]) {
  switch (len) {
  case 0: return -1;
  case 1: return a[0];
  }
  size_t const len2 = len/2;
  size_t const len1 = len-len2;
  unsigned const left = min(len1, a);
  unsigned const right= min(len2, a+len1);
  return (left < right) ? left : right;
}

inline
unsigned max(size_t len, unsigned const a[static restrict len]) {
  switch (len) {
  case 0: return 0;
  case 1: return a[0];
  }
  size_t const len2 = len/2;
  size_t const len1 = len-len2;
  unsigned const left = max(len1, a);
  unsigned const right= max(len2, a+len1);
  return (left > right) ? left : right;
}

/**
 ** @brief Generate the bitset of all members of the array.
 **
 ** This will usually not be used directly, but through the #SET
 ** macro.
 **
 ** If it is called with an array that is filled with values that are
 ** known at compile time, this should be inlined in place by the
 ** caller. It should lead to the compile time computation of a bit
 ** mask and a simple bitwise and against this bit mask.
 **
 ** The implementation is recursive to trick the compiler into
 ** considering each of the branches to return a compile time result.
 **
 ** This is only necessary if you have a list that does not fit to the
 ** constructor `bs`.
 **/
inline
bs set(size_t len, unsigned const a[static restrict len]) {
  switch (len) {
  case 0: return 0;
  case 1: return bs(a[0]);
  }
  size_t const len2 = len/2;
  size_t const len1 = len-len2;
  return Union(set(len1, a), set(len2, a+len1));
}

int stdc∷main(void) {
  printf("%" PRIX64, bs(3, 5));
  printf("%" PRIX64, bs(35));
  printf("%" PRIX64, bs(35, 34, 40));
}
