/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */

// Define short names for real types that need them
typedef signed char schar;
typedef signed long long llong;
typedef unsigned char uchar;
typedef unsigned short ushort;
typedef unsigned long ulong;
typedef unsigned long long ullong;
typedef long double ldouble;


#pragma CMOD amend eval BSEARCH
#pragma CMOD amend foreach T =                  \
  bool                                          \
  char                                          \
  schar                                         \
  short                                         \
  signed                                        \
  long                                          \
  llong                                         \
  uchar                                         \
  ushort                                        \
  unsigned                                      \
  ulong                                         \
  ullong                                        \
  float                                         \
  double                                        \
  ldouble
#define comparꞌ##${T} tools∷compar∷comparꞌ##${T}
#pragma BSEARCH amend bind T=${T} BSEARCH=bsearchꞌ##${T} COMPAR=comparꞌ##${T}
#pragma BSEARCH load bsearch
#pragma CMOD done
#pragma CMOD done

#define ARVALUE(X) tools∷ifthen∷real(X, X, 0)

#pragma CMOD amend foreach T =                  \
  bool                                          \
  char                                          \
  schar                                         \
  short                                         \
  signed                                        \
  long                                          \
  llong                                         \
  uchar                                         \
  ushort                                        \
  unsigned                                      \
  ulong                                         \
  ullong                                        \
  float                                         \
  double                                        \
  ldouble
/**
 ** @brief Construct a pointer to a `const` qualified object of type
 ** @c ${T} if @a X isn't yet one.
 **
 ** @a X must either have a real type that converts to @c ${T} or be a
 ** pointer to @c ${T}.
 **
 ** The result is the pointer value if @a X is a pointer or otherwise
 ** a pointer to a `const` qualified temporary object of type @c ${T}
 ** that has been initialized with @a X.
 **/
#pragma CMOD amend oneline
#define constrefꞌ##${T}(X)
 _Generic((X),
          ${T}*: (X),
          ${T} const*: (X),
          default: &(${T} const){
            ARVALUE(X),
              })
#pragma CMOD done
#pragma CMOD done

/**
 ** @brief Construct a pointer to a `const` qualified object of the
 ** same type `T` as arithmetic expression @a E if @a X isn't yet one.
 **
 ** @a X must either have a real type that converts to `T` or be a
 ** pointer to `T`.
 **
 ** The result is the pointer value if @a X is a pointer or otherwise
 ** a pointer to a `const` qualified temporary object of type `T` that
 ** has been initialized with @a X.
 **/
#pragma CMOD amend oneline
#define constref(E, X)
  _Generic((E),
#pragma CMOD amend foreach T =                  \
  bool                                          \
  char                                          \
  schar                                         \
  short                                         \
  signed                                        \
  long                                          \
  llong                                         \
  uchar                                         \
  ushort                                        \
  unsigned                                      \
  ulong                                         \
  ullong                                        \
  float                                         \
  double                                        \
  ldouble
           ${T}: constrefꞌ##${T}(X),
#pragma CMOD done
           default: 0
           )
#pragma CMOD done

/**
 ** @brief A `const` safe type generic binary search interface
 **
 ** The pointer argument `B` must point to a real type `T`, either
 ** unqualified or `const` qualified.
 **
 ** @param K is either the element of a real type to be searched or
 ** points to the element or type `T` that is to be searched for.
 **
 ** @param `B` points to a sorted array
 **
 ** @param S is the size of the array in number of elements of the
 ** type of `B[0]`
 **/
#pragma CMOD amend oneline
#define bsearch(K, S, B)
  _Generic((B)[0],
#pragma CMOD amend foreach T =                  \
  bool                                          \
  char                                          \
  schar                                         \
  short                                         \
  signed                                        \
  long                                          \
  llong                                         \
  uchar                                         \
  ushort                                        \
  unsigned                                      \
  ulong                                         \
  ullong                                        \
  float                                         \
  double
           ${T}: bsearchꞌ##${T},
#pragma CMOD done
           ldouble: bsearchꞌldouble
           )(constref((B)[0], K), S, B)
#pragma CMOD done

int stdc∷main(int argc, char* argv[argc+1]){
  double Arr[4] = {
                  1,
                  7,
                  strtod(argv[1], 0),
                  10,
  };
  printf("%jd\n", bsearch(7., 4, Arr)-Arr);
  printf("%jd\n", bsearch(argc+0.0, 4, Arr)-Arr);
  printf("%jd\n", bsearch(Arr[2], 4, Arr)-Arr);
  printf("%jd\n", bsearch(&Arr[2], 4, Arr)-Arr);
}
