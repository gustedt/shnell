/* Use this file to test the include guard deactivation */
#define MAKES_A_DIFFERENCE 1
/* This should be detected as useless */
#ifndef DOESNT_CHANGE_ANYTHING

#endif

/* This should be detected as needed and not deactivated */
#ifndef MAKES_A_DIFFERENCE
inline
void should_never_appear(void) {

}
#endif

/* This should be detected as useless */
#if !DOESNT_CHANGE_ANYTHING

#endif

/* This should be detected as needed and not deactivated */
#if !MAKES_A_DIFFERENCE
inline
void should_never_appear_either(void) {

}
#endif

enum tools::sizes {
  /**
   ** @brief The buffer size.
   **/
  tools::bsize = 1024,
};

#define STRINGIFY0(...) #__VA_ARGS__
#define STRINGIFY1(...) STRINGIFY0(__VA_ARGS__)
#define tools∷STRINGIFY(...) STRINGIFY1(__VA_ARGS__)
