// This may look like garbage but is actually -*- C -*-

/**
 ** @file
 **
 ** @brief The core of a Union-Find implementation by means of an
 ** index table.
 **
 ** Each element in the base set corresponds to an entry in the
 ** table. If this entry is a negative number n, the element is the
 ** root of its set and -n is the size of that set. Otherwise, the
 ** entry is the parent in the set.
 **
 ** This is not meant to be used directly, but through the more user
 ** friendly interface p11¯uf.
 **/

#pragma PRETRADE private union0 size

#if stdc∷SIZE_MAX ≡ stdc∷UINT_MAX
typedef signed ufTab∷ent;
#elif stdc∷SIZE_MAX ≡ stdc∷ULONG_MAX
typedef long ufTab∷ent;
#else
typedef long long ufTab∷ent;
#endif

inline
bool root(ent p, ent const tab[static p+1]) {
  return tab[p] < 0;
}

inline
size_t size(ent p, ent const tab[static p+1]) {
  return root(p, tab) ? -tab[p] : 0;
}

inline
ent Find(ent p, ent const tab[static p+1]) {
  while (¬root(p, tab)) p = tab[p];
  return p;
}

inline
ent Exchange(ent p, ent tab[static p+1], ent val) {
  ent ret = tab[p];
  tab[p] = val;
  return ret;
}

inline
void Compress(ent p, ent tab[static p+1], ent r) {
  while (¬root(p, tab)) {
    p = Exchange(p, tab, r);
  }
}

inline
ent FindꞌCompress(ent p, ent tab[static p+1]) {
  ent r = Find(p, tab);
  Compress(p, tab, r);
  return r;
}

inline
ent union0(ent left, ent right, ent tab[static (left < right ? right : left)+1]) {
  // This supposes that both elements are roots, such that the entries
  // contain the negated sizes.
  tab[left] += Exchange(right, tab, left);
  return left;
}

inline
ent Union(ent left, ent right, ent tab[static (left < right ? right : left)+1]) {
  // This supposes that both elements are roots, such that the entries
  // contain the negated sizes.
  if (tab[left] > tab[right]) return union0(right, left, tab);
  else  return union0(left, right, tab);
}

inline
void Flatten(ent p, size_t length, ent tab[static p+length]) {
  for (size_t stop = p+length; p < stop; ++p) {
    FindꞌCompress(p, tab);
  }
}
