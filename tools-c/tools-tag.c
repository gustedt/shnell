#pragma CMOD amend alias                        \
  bs=tools::bs                                  \
  keyw=tools::keyword                           \
  flex=tools::flex                              \
  fnct=tools::functions                         \
  grm=tools::grammar


/**
 ** @file
 **
 ** This implements a simple augmentation of C code by adding @c typedef
 ** to tagged type definitions. For a structure type
 ** @code
 ** struct toto {
 **   struct toto* next;
 **   double val;
 ** };
 ** @endcode
 ** this would just look like
 ** @code
 ** typedef struct toto toto; struct toto {
 **   struct toto* next;
 **   double val;
 ** };
 ** @endcode
 **
 ** that is the structure definition is prefixed by a typedef with the
 ** same identifier as the tag name. When using this, you can then
 ** systematically drop the use of the @c struct keyword in all other
 ** places than the definition.
 ** @code
 ** typedef struct toto toto; struct toto {
 **   toto* next;
 **   double val;
 ** };
 ** @endcode
 **
 ** The rules for @c union types are completely analogous.
 **
 ** For @c enum there is a difference: the @c typedef comes *after*
 ** the definition:
 ** @code
 ** enum twins {
 **   romulus,
 **   remus,
 ** }; typedef enum twins twins;
 **
 ** The reason that we put it on the same line, is that we don't want
 ** to mess up the line structure of the code such that compiler
 ** diagnosis can always point us to the correct line if there are
 ** problems.
 **/

int stdc::main(int argc, char* argv[static restrict argc+1]) {
  FILE* out = stdout;
  char const*const filename = (argc > 1)
    ? argv[1]
    :"unknown file";
  flex* il = flex::printf(0,
                         "#ifdef __CMOD_INTERNAL_COMPILATION\n"
                         "/* Collected instantiations for inline functions. */\n"
                         "#line 1 \"instantiation in %s\"\n",
                         filename);
  size_t const li = flex::actual(il);
  flex const* fl = fnct::readin(stdin);
  if (¬fl) return EXIT_FAILURE;
  char const*const restrict f  = flex::data(fl);
  size_t line = 1;
  size_t pos = 0;
  keyw::properties prop = 0;
  int len = 0;
  char const* enumname = 0;
  for (;f[pos];++pos) {
    {
      size_t len = fnct::skipat(f+pos);
      line += grm::lines(len, f+pos);
      fnct::writeout(out, len, f+pos);
      pos += len;
    }
    size_t s = pos, e = s + grm::decl(f + s), ie = e, b = e;
    if (¬f[e]) break;
    if (f[e] ≡ '#' ) {
      if (s < e) {
        line += grm::lines(e-s, f+s);
        fnct::writeout(out, e-s, f+s);
      }
      size_t l = grm::prepro(f+e, &line);
      fnct::writeout(out, l, f+e);
      pos = e+l-1;
      continue;
    }
    if (¬prop) prop = grm::props(e-s, f+s, 0);
    if (f[e] ≡ '=') {
      ie = grm::init(f+e+1) + e + 1;
    }
    if (f[ie] ≠ '{') {
      pos = ie;
    } else {
      b = ie + grm::block(f+ie);
      pos = b;
    }
    if (b < ie) {
      fnct::writeout(out, ie-s, f+s);
      line += grm::lines(ie-s, f+s);
    } else {
      if (¬keyw::internal(prop)) {
        if (keyw::typename(prop)) {
          keyw kw = -1;
          char const* needle = 0;
          switch(keyw::typename(prop)){
#pragma CMOD amend foreach X _struct _union _enum
          case bs(keyw::${X}):
            kw = keyw::${X};
            len = sizeof #${X} -1 -1; // extra -1 for the _
            needle = &#${X}[1];       // extra +1 for the _
            break;
#pragma CMOD done
          }
          if (needle) {
            char const*start = strstr(f+s, needle);
            if (start) {
              start += len;
              start += fnct::skip(start);
              len = fnct::word(start);
              if (len) {
                if (kw ≠ keyw::_enum) fprintf(out, "typedef %s %.*s %.*s; ", needle, len, start, len, start);
                else enumname = start;
              }
            }
          }
        } else if (bs::isin(keyw::_inline, prop)) {
          il = flex::cat(il, "extern ");
          il = flex::cat(il, e-s, f+s);
          il = flex::cat(il, ";\n");
        }
      }
      fnct::writeout(out, b-s, f+s);
      line += grm::lines(b-s, f+s);
    }
    switch (f[pos]) {
    case ';' :;
      fputc(f[pos], out);
      if (enumname) fprintf(out, " typedef enum %.*s %.*s;", len, enumname, len, enumname);
      enumname = 0;
      break;
    case ',' :;
      fputc(f[pos], out);
      break;
    default: --pos;
    }
    if (f[b] ≡ '\n') ++line;
    // A typedef always expects a terminating ";"
    if (bs::isin(keyw::_typedef, prop)) {
      if (f[pos] ≡ ';') prop = 0;
    } else {
      // If it is not a typedef but a tag type definition,
      // continuations are forbidden, see the introduction.
      //
      // So the only possible continuation is that it is not a tag
      // type definition and thus a static symbol. A static function
      // never has a continuation and thus also never has a comma
      // following it. Now, the last case is a static object, and
      // that can only have a continuation, if it is followed by a
      // comma.
      if (f[pos] ≠ ',') prop = 0;
    }
  }
  if (flex::actual(il) > li) {
    il = flex::cat(il, "#endif\n");
    flex::puts(il);
  }
  flex::free(il);
  flex::free(fl);
  return EXIT_SUCCESS;
}
