#pragma CMOD amend alias                        \
  bs=tools::bs                                  \
  keyw=tools::keyword                           \
  flex=tools::flex                              \
  fnct=tools::functions                         \
  grm=tools::grammar

/**
 ** @file
 **
 ** This implements a rudimentary parser that reads a C file and spits
 ** out an annotated list of all global identifiers.
 **
 **/

void printenumeration(FILE*restrict out, size_t slen, char const f[static restrict slen]) {
  for (size_t pos = 0; pos < slen; ++pos) {
  RETRY:
    pos += fnct::skipat(f+pos);
    if (f[pos] ≡ '#' ) {
      pos += grm::prepro(f+pos, &(size_t){ 0 });
      goto RETRY;
    }
    size_t s = pos, e = s + grm::decl(f + s), ie = e;
    if (f[e] ≡ '=') {
      ie = grm::init(f+e+1) + e + 1;
    }
    size_t positions[2] = { -1, -1, };
    grm::props(e-s, f+s, positions);
    if (positions[0] < -1) {
      fputs("enumconstant: ", out);
      fnct::wordout(out, f+s+positions[0]);
      fputc('\n', out);
      pos = ie + 1;
    } else {
      break;
    }
  }
}


int stdc::main(int argc, char* argv[static restrict argc+1]) {
  FILE*restrict out = stdout;
  flex const* fl = fnct::readin(stdin);
  if (¬fl) return EXIT_FAILURE;
  char const*const restrict f  = flex::data(fl);
  size_t pos = 0;
  keyw::properties prop = 0;
  size_t len = fnct::skipat(f+pos);
  if (memchr(f+pos, '@', len)) {
    fnct::lineout(out, len, f+pos);
    pos += len;
  }
  for (;f[pos];++pos) {
    {
      size_t len = fnct::skipat(f+pos);
      if (len) {
        fputs("// skipped |", out);
        fnct::lineout(out, len, f+pos);
        fputs("|\n", out);
        pos += len;
        //fprintf(out, "// found %.6s\n", f+pos);
      }
    }
    size_t s = pos, e = s + grm::decl(f + s), ie = e, b = e;
    size_t positions[2] = { -1, -1, };
    prop |= grm::props(e-s, f+s, positions);
    if (¬f[e]) break;
    if (f[e] ≡ '#' ) {
      size_t l = grm::prepro(f+e, &(size_t){ 0 });
      size_t first = fnct::skip(f+e+1);
      size_t wl = fnct::word(f+e+1+first);
      keyw::properties kw = grm::prop(wl, f+e+1+first);
      if (bs::isin(keyw::_define, kw)) {
        first += wl;
        first += fnct::skip(f+e+1+first);
        size_t l = fnct::word(f+e+1+first);
        if (f[e+1+first+l] ≡ '(')
          fputs("function macro: ", out);
        else
          fputs("object macro: ", out);
        fnct::lineout(out, l, f+e+1+first);
        fputc('\n', out);
      }
      pos = e+l-1;
      continue;
    } else {
      if (f[e] ≡ '=') {
        ie = grm::init(f+e+1) + e + 1;
      }
      if (f[ie] ≠ '{') {
        pos = ie;
      } else {
        b = ie + grm::block(f+ie);
        pos = b;
      }
    }
    if (¬keyw::internal(prop) ∧ ¬grm::tagtype(e-s, f+s)) {
      if (¬keyw::noextern(prop)) {
        prop |= bs(keyw::_extern);
      }
      if ((positions[0] < -1) ∧ (keyw::basetype(prop) ∨ (positions[1] < -1))) {
        if (positions[1] < -1) {
          fputs("two ids: ", out);
          fnct::wordout(out, f+s+positions[1]);
        } else {
          fputs("basetype: ", out);
          fnct::wordout(out, f+s+positions[0]);
        }
      } else {
        // a symbol
        if (positions[0] < -1) {
          keyw::print(out, prop);
          fputs(" symbol: ", out);
          fnct::wordout(out, f+s+positions[0]);
        } else {
          fputs("// empty ", out);
        }
      }
      switch (f[ie]) {
      case ',' :
      case '}' :
        break;
      case '{' : ;
      default  : ;
      case ';' : ;
        prop = 0;
        break;
      }
      fputc('\n', out);
    } else {
      if (¬bs::isin(keyw::_static_assert, prop)∧¬bs::isin(keyw::__Static_assert, prop)) {
        if (bs::isin(keyw::_typedef, prop)) {
          if (f[pos] ≡ ';' ∨ f[pos] ≡ ',') {
            fputs("typedef: ", out);
            if (positions[1] < -1) {
              fnct::wordout(out, f+s+positions[1]);
            } else {
              fnct::wordout(out, f+s+positions[0]);
            }
            fputc('\n', out);
            if (f[pos] ≡ ';')
              prop = 0;
            pos = ie;
            continue;
          }
        }
        keyw::properties typename = keyw::typename(prop);
        keyw kw = 0;
        if (typename) {
          switch(typename){
          case keyw::_struct: kw = keyw::_struct; pos = b-1;     prop = bs::minus(prop, bs(keyw::_struct)); break;
          case keyw::_union: kw = keyw::_union;  pos = b-1;     prop = bs::minus(prop, bs(keyw::_union));  break;
          case keyw::_enum: kw = keyw::_enum;
            if (positions[0] < -1) {
              fprintf(out, "%s: ", keyw::words[kw]);
              fnct::wordout(out, f+s+positions[0]);
            } else {
              fprintf(out, "// anonymous %s type ", keyw::words[kw]);
            }
            fputc('\n', out);
            printenumeration(out, b-e, f+e+1);
            prop = bs::minus(prop, bs(keyw::_enum));
            pos = b-1;
            continue;
          }
          if (positions[0] < -1) {
            fprintf(out, "%s: ", keyw::words[kw]);
            fnct::wordout(out, f+s+positions[0]);
          } else {
            fprintf(out, "// anonymous %s type ", keyw::words[kw]);
          }
          fputc('\n', out);
          continue;
        } else if ((positions[0] < -1) ∧ (keyw::basetype(prop) ∨ (positions[1] < -1))) {
          size_t pp = (positions[1] < -1) ? positions[1] : positions[0];
          fputs("symbol: ", out);
          fnct::wordout(out, f+s+pp);
          fputc('\n', out);
        } else {
          keyw::print(out, prop);
          fputs(" others: ", out);
          fnct::lineout(out, e-s, f+s);
          fputc('\n', out);
        }
      }
      if (f[pos] ≠ ',') {
        prop = 0;
      }
    }
  }
  flex::free(fl);
  return EXIT_SUCCESS;
}
