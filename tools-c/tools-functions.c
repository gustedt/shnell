/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */

#pragma PRETRADE alias bs=tools::bs flex=tools::flex

/**
 ** @file
 **
 ** @brief An adhoc utility to hide characters, strings and comments.
 **
 ** All of these are transformed into sequences of the form
 ** @code
 ** @ X nm nm nm nm nm nm .... @
 ** @endcode
 **
 ** Where @c nm are two character hex numbers for character code and
 ** @c X is one character with the following significance:
 **
 ** - @c C C comment
 ** - @c L @c "wide string literal"
 ** - @c M @c 'wide character'
 ** - @c P C++ comment
 ** - @c S @c "normal string literal"
 ** - @c T @c 'normal character'
 ** - @c U @c U"bla"
 ** - @c V @c U'bla'
 ** - @c u @c u"bla"
 ** - @c u8 @c u8"bla"
 ** - @c v @c u'bla'
 **
 ** Multiline comments are split at the end of each line to ensure
 ** that the line number count remains correct.
 **/

inline
char* chunk(FILE*restrict in, size_t size, char buffer[static restrict size]) {
  char* ret = 0;
  if (¬feof(in))
    ret = fgets(buffer, size, in);
  return ret;
}

/**
 ** @brief Return the hex character for the lower half byte @a c.
 **/
inline
char xdigit(unsigned c) {
  c &= 0xF;
  // this works for ASCII and EBDIC, since for both the characters
  // 'A'...'F' are consecutive.
  signed const A10 = 'A' - 10;
  return c + (c < 10 ? '0' : A10);
}

inline
unsigned udigit(unsigned c) {
  switch (c) {
  default : return -1;
  case '0': return 0x0;
  case '1': return 0x1;
  case '2': return 0x2;
  case '3': return 0x3;
  case '4': return 0x4;
  case '5': return 0x5;
  case '6': return 0x6;
  case '7': return 0x7;
  case '8': return 0x8;
  case '9': return 0x9;
  case 'a': return 0xa;
  case 'b': return 0xb;
  case 'c': return 0xc;
  case 'd': return 0xd;
  case 'e': return 0xe;
  case 'f': return 0xf;
  case 'A': return 0XA;
  case 'B': return 0XB;
  case 'C': return 0XC;
  case 'D': return 0XD;
  case 'E': return 0XE;
  case 'F': return 0XF;
  }
}

inline
size_t skip(char const buf[static 1]) {
  return strspn(buf, " \t\n");
}

inline
bs setbit(unsigned x, unsigned level) {
  unsigned l = x / 64;
  return (l ≡ level) ? bs(x%64) : 0;
}

inline
bs idchar(unsigned L) {
  return (setbit('a', L)
          ∪ setbit('b', L)
          ∪ setbit('c', L)
          ∪ setbit('d', L)
          ∪ setbit('e', L)
          ∪ setbit('f', L)
          ∪ setbit('g', L)
          ∪ setbit('h', L)
          ∪ setbit('i', L)
          ∪ setbit('j', L)
          ∪ setbit('k', L)
          ∪ setbit('l', L)
          ∪ setbit('m', L)
          ∪ setbit('n', L)
          ∪ setbit('o', L)
          ∪ setbit('p', L)
          ∪ setbit('q', L)
          ∪ setbit('r', L)
          ∪ setbit('s', L)
          ∪ setbit('t', L)
          ∪ setbit('u', L)
          ∪ setbit('v', L)
          ∪ setbit('w', L)
          ∪ setbit('x', L)
          ∪ setbit('y', L)
          ∪ setbit('z', L)
          ∪ setbit('A', L)
          ∪ setbit('B', L)
          ∪ setbit('C', L)
          ∪ setbit('D', L)
          ∪ setbit('E', L)
          ∪ setbit('F', L)
          ∪ setbit('G', L)
          ∪ setbit('H', L)
          ∪ setbit('I', L)
          ∪ setbit('J', L)
          ∪ setbit('K', L)
          ∪ setbit('L', L)
          ∪ setbit('M', L)
          ∪ setbit('N', L)
          ∪ setbit('O', L)
          ∪ setbit('P', L)
          ∪ setbit('Q', L)
          ∪ setbit('R', L)
          ∪ setbit('S', L)
          ∪ setbit('T', L)
          ∪ setbit('U', L)
          ∪ setbit('V', L)
          ∪ setbit('W', L)
          ∪ setbit('X', L)
          ∪ setbit('Y', L)
          ∪ setbit('Z', L)
          ∪ setbit('_', L)
          ∪ setbit('0', L)
          ∪ setbit('1', L)
          ∪ setbit('2', L)
          ∪ setbit('3', L)
          ∪ setbit('4', L)
          ∪ setbit('5', L)
          ∪ setbit('6', L)
          ∪ setbit('7', L)
          ∪ setbit('8', L)
          ∪ setbit('9', L));
}

inline
bool isid(unsigned char c) {
  unsigned const l = c/64U;
  unsigned const m = c%64U;
  return bs∷isin(m, idchar(l));
}

 inline
size_t word(char const buf[static 1]) {
  for (size_t ret = 0;;++ret) {
    if (¬isid(buf[ret]))
      return ret;
  }
}

inline
size_t skipat2(char const buf[static 1]) {
  size_t ret = 0;
  if (buf[0] ≡ '@') {
    {
      char const* at = strchr(&buf[1], '@');
      if (at) {
        ret = (at-&buf[1]) + 2;
      }
    }
  }
  return ret;
}

char const* decode(FILE*restrict out, FILE*restrict in, char const*p, size_t size, char buffer[static restrict size], bool show) {
  while (*p ≠ '@') {
    if (¬p[0]) goto RELOAD;
    if (*p ≡ '\n') {
      putc('\n', out);
      fflush(out);
      ++p;
    }
    if (¬p[0]) goto RELOAD;
    if (¬p[1]) {
      ungetc(p[0], in);
      goto RELOAD;
    }
    if (show) {
      unsigned c = (udigit(p[0])<<(CHAR_BIT/2)) ∪ udigit(p[1]);
      putc(c, out);
    }
    p += 2;
    continue;
RELOAD:
    p = chunk(in, size, buffer);
    if (¬p) return 0;
  }
  return p;
}

flex const* readin(FILE*restrict in) {
  size_t len = 1024;
  flex* ret = flex∷resize(0, len);
  while (flex∷fgets(ret, in)) {
    if (flex∷full(ret)) {
      size_t nlen = 2*flex∷total(ret);
      ret = flex∷resize(ret, nlen);
      // we are not able to allocate, so better stop the loop.
      if (ret→Total ≠ nlen) break;
    }
  }
  return ret;
}

void writeout(FILE *restrict out, size_t len, char const buf[static restrict len]) {
  errno = 0;
  while (len) {
    int l = len > INT_MAX ? INT_MAX : len;
    int k = fprintf(out, "%.*s", l, buf);
    if (k < 0) {
      perror("writeout");
      return;
    }
    len -= k;
    buf += k;
  }
}

void lineout(FILE *restrict out, size_t len, char const buf[static restrict len]) {
  errno = 0;
  for (char const* end = buf + len; buf < end; ++buf) {
    if (buf[0] ≠ '\n')
      putc(buf[0], out);
    else
      putc(' ', out);
  }
}

inline
void wordout(FILE*restrict out, char const buf[static restrict 1]) {
  for (; isid(*buf); ++buf)
    putc(*buf, out);
}

size_t skipat(char const buf[static 1]) {
  size_t pos = 0, len;
 RESTART:
  len = skip(buf+pos);
  len += skipat2(buf+pos+len);
  pos += len;
  if (len) goto RESTART;
  return pos;
}

size_t count(size_t len, char const buf[static len], char needle) {
  size_t ret = 0;
  while (len) {
    char const* nbuf = memchr(buf, needle, len);
    if (¬nbuf) break;
    ++ret;
    ++nbuf;
    len -= (nbuf-buf);
    buf = nbuf;
  }
  return ret;
}

flex* uniq(char const* name, char const* suffix){
  flex* ret = 0;
  size_t len = strlen(name);
  char buf[2*len + 1];
  for (size_t i = 0; i < len; ++i) {
    snprintf(buf+2*i, 3, "%.2X", name[i]);
  }
  ret = flex∷cpy(ret, "CMOD_");
  ret = flex∷cat(ret, buf);
  ret = flex∷cat(ret, suffix);
  return ret;
}

flex* hbtob(flex* ret, size_t len, char const code[len]) {
  ret = flex∷resize(ret, len/2 + 1);
  char* d = flex∷data(ret);
  for (size_t i = 0; i < len; i += 2) {
    d[i/2] = (udigit(code[i]) << CHAR_BIT/2) ∪ udigit(code[i+1]);
  }
  d[len/2] = 0;
  ret→Actual = len;
  return ret;
}

flex* get_linenumber(flex* currentf, char const b[static 1], size_t line[static 1]) {
  char* end = 0;
  size_t nl = strtoull(b, &end, 10);
  if (end ≠ b) {
    char const* sfile = end + skip(end);
    switch (sfile[0]) {
    case '@': ;
    case '"': ;
      line[0] = nl;
      end = strchr(sfile+1, sfile[0]);
      size_t eol = end-sfile;
      switch (sfile[0]) {
      case '@':
        eol -= 2;
        sfile += 2;
        return hbtob(currentf, eol, sfile);
      case '"':
        eol -= 1;
        sfile += 1;
        return flex∷cpy(currentf, eol, sfile);
      }
    }
  }
  return 0;
}
