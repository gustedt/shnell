/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */

// Define short names for all 9 real types that we are interested in
typedef signed s;
typedef signed long sl;
typedef signed long long sll;

#define MINꞌs INT_MIN
#define MINꞌsl LONG_MIN
#define MINꞌsll LLONG_MIN

typedef unsigned u;
typedef unsigned long ul;
typedef unsigned long long ull;

#define MINꞌu 0
#define MINꞌul 0
#define MINꞌull 0

typedef float f;
typedef double d;
typedef long double l;

// Used for erroneous invocations
typedef struct _InvType min∷_floatingTypeTooNarrow;
extern min∷_floatingTypeTooNarrow floatingTypeTooNarrow;

#pragma CMOD amend eval LATER

// Construct width macros for all the types that we handle.  For the
// float types we have to compute it from the radix and the mantissa
// digits.
#if FLT_RADIX ≡ 2
#define FBITS 1
#elif FLT_RADIX ≡ 4
#define FBITS 2
#elif FLT_RADIX ≡ 8
#define FBITS 3
#elif FLT_RADIX ≡ 16
#define FBITS 4
#endif

#define WIDTHꞌf (FBITS*FLT_MANT_DIG)
#define WIDTHꞌd (FBITS*DBL_MANT_DIG)
#define WIDTHꞌl (FBITS*LDBL_MANT_DIG)

// For integer types, if not already provided by a newer C version, we
// have to deduce them from the MAX macros.
#if ¬ defined(min::UINT_WIDTH) ∨ ¬ defined(min::ULONG_WIDTH) ∨ ¬ defined(min::ULLONG_WIDTH)
#pragma CMOD amend foreach P UINT ULONG ULLONG
#pragma CMOD amend foreach M:I 0xFFFF 0xFFFFFFFF 0xFFFFFFFFFFFF 0xFFFFFFFFFFFFFFFF
#pragma LATER amend let W 16 -mul ( ${I} + 1 )

#if ${P}##_MAX ≡ ${M}
#define ${P}##_WIDTH ${W}
#else
#pragma LATER done
#pragma CMOD done
#pragma CMOD amend do I = 4
#endif
#pragma CMOD done

#pragma CMOD done
#endif

#define WIDTHꞌu UINT_WIDTH
#define WIDTHꞌul ULONG_WIDTH
#define WIDTHꞌull ULLONG_WIDTH

#define WIDTHꞌs UINT_WIDTH
#define WIDTHꞌsl ULONG_WIDTH
#define WIDTHꞌsll ULLONG_WIDTH

#define RANKꞌu 3
#define RANKꞌul 4
#define RANKꞌull 5

#define RANKꞌs 3
#define RANKꞌsl 4
#define RANKꞌsll 5

// Create lists of floating, signed and unsigned types
#pragma CMOD amend bind             \
  FLIST=f d l                       \
  SLIST=s sl sll                    \
  ULIST=u ul ull

#pragma LATER amend foreach T = ${ULIST} ${SLIST} ${FLIST}
/**
 ** @brief compute the minimum value of the arguments
 **
 ** Usually you use the @c min type generic macro, because these
 ** macros here may not behave well under argument conversion.
 **/
inline
${T} minꞌ##${T}(${T} a, ${T} b) {
  return (a < b) ? a : b;
}
#pragma LATER done

// Mixing different signed types just should work fine.
//
// The special case of two signed types with different rank but with
// the same representation results in the type with the lesser rank.
#pragma LATER amend foreach T = ${SLIST}
#pragma LATER amend foreach S = ${SLIST}
#if MINꞌ##${T} < MINꞌ##${S} ∨ ((MINꞌ##${T} ≡ MINꞌ##${S}) ∧ (RANKꞌ##${T} <= RANKꞌ##${S}))
# define minꞌ##${T}##ꞌ##${S} minꞌ##${T}
#else
# define minꞌ##${T}##ꞌ##${S} minꞌ##${S}
#endif
#pragma LATER done
#pragma LATER done


// Mixing different unsigned types does the comparison in the wider
// type and then reduces to the narrower onė
#pragma LATER amend foreach T = ${ULIST}
#pragma LATER amend foreach S = ${ULIST}

#if RANKꞌ##${T} < RANKꞌ##${S}
inline
${T} minꞌ##${T}##ꞌ##${S}(${S} a, ${S} b) {
  if (a < b) return a;
  else return b;
}
#elif RANKꞌ##${T} ≡ RANKꞌ##${S}
# define minꞌ##${T}##ꞌ##${S} minꞌ##${T}
#else
inline
${S} minꞌ##${T}##ꞌ##${S}(${T} a, ${T} b) {
  if (b < a) return b;
  else return a;
}
#endif
#pragma LATER done
#pragma LATER done


// Mixing different signed and unsigned types does the special
// comparison and then converts to the signed type.
//
// The special comparison itself first does check if the signed
// argument is negative. If so, it must be the minimum. If not,
// comparison between is done with the default promotion rules. These
// warrant that comparison of non-negative values are consistent with
// the abstract value comparison.
#pragma LATER amend foreach U = ${ULIST}
#pragma LATER amend foreach S = ${SLIST}
inline
${S} minꞌ##${U}##ꞌ##${S}(${U} a, ${S} b) {
  if ((b < 0) ∨ (b < a)) return b;
  else return a;
}
inline
${S} minꞌ##${S}##ꞌ##${U}(${S} a, ${U} b) {
  if ((a < 0) ∨ (a < b)) return a;
  else return b;
}
#pragma LATER done
#pragma LATER done


// Perform a case analysis to disallow lack of precision when mixing
// integer and floating point arguments.
#pragma CMOD amend foreach F = f d l
#pragma CMOD amend foreach S = \  l ll
#if WIDTHꞌ##${F} < WIDTHu##${S}-1
#define minꞌ##${F}##ꞌs##${S} floatingTypeTooNarrow
#else
#define minꞌ##${F}##ꞌs##${S} minꞌ##${F}
#endif
#pragma CMOD done
#pragma CMOD amend foreach S = \  l ll
#if WIDTHꞌ##${F} < WIDTHu##${S}
#define minꞌ##${F}##ꞌu##${S} floatingTypeTooNarrow
#else
#define minꞌ##${F}##ꞌu##${S} minꞌ##${F}
#endif
#pragma CMOD done
#pragma CMOD done

#pragma LATER amend foreach TA = ${ULIST} ${SLIST}
#pragma LATER amend oneline
#define caseꞌ##${TA}(B)
_Generic(+(B),
#pragma LATER amend foreach TB = ${ULIST} ${SLIST}
         ${TB}: minꞌ##${TA}##ꞌ##${TB},
#pragma LATER done
         f: minꞌfꞌ##${TA}, d: minꞌdꞌ##${TA}, l: minꞌlꞌ##${TA})
#pragma LATER done
#pragma LATER done

#define caseꞌf(B) _Generic(+(B), u: minꞌfꞌu, ul: minꞌfꞌul, ull: minꞌfꞌull, s: minꞌfꞌs, sl: minꞌfꞌsl, sll: minꞌfꞌsll, f: minꞌf, d: minꞌd, l: minꞌl)
#define caseꞌd(B) _Generic(+(B), u: minꞌdꞌu, ul: minꞌdꞌul, ull: minꞌdꞌull, s: minꞌdꞌs, sl: minꞌdꞌsl, sll: minꞌdꞌsll, f: minꞌd, d: minꞌd, l: minꞌl)
#define caseꞌl(B) _Generic(+(B), u: minꞌlꞌu, ul: minꞌlꞌul, ull: minꞌlꞌull, s: minꞌlꞌs, sl: minꞌlꞌsl, sll: minꞌlꞌsll, f: minꞌl, d: minꞌl, l: minꞌl)

/**
 ** @brief Compute the minimum of the two arguments.
 **
 ** If one of the arguments has floating point, the usual arithmetic
 ** conversions are performed.
 **
 ** If both promoted arguments have integer type, the return type is
 ** the narrower of the promoted argument types that can hold the
 ** minimum value of both of the types. Such a type always exist and
 ** no arithmetic oddities can occur:
 **
 ** - If both promoted arguments have a signed type the result is the
 **   wider of the two types.
 **
 ** - If both promoted arguments have an unsigned type the result is
 **   the narrower of the two types.
 **
 ** - If one is signed and the other is unsigned the result is the
 **   signed type.
 **
 ** If one argument has integer type and the other has a floating
 ** type, the result may loose precision. This is e.g commonly the
 ** case if one argument is `uint64_t` and the other is `float` or
 ** `double`.
 **/
#pragma LATER amend oneline
#define min(A, B)
   _Generic(+(A),
#pragma LATER amend foreach TA = ${ULIST} ${SLIST} ${FLIST}
            ${TA}: caseꞌ##${TA}(B),
#pragma LATER done
            default: minꞌd)((A), (B))
#pragma LATER done

#pragma CMOD done

#define print(X, Y) printf("min of %.30Lg, %.30Lg is %.30Lg\n", (l)X, (l)Y, (l)min(X, Y))

int stdc∷main(void) {

  print(2, -3);
  print(3, -2);
  print(-2, -3);
  print(-3, -2);
  print(2L, -3);
  print(3L, -2);
  print(-2L, -3);
  print(-3L, -2);
  //
  print(2u, 3u);
  print(3u, 2u);
  print(-2u, 3u);
  print(-3u, 2u);
  print(2uL, 3u);
  print(3uL, 2u);
  print(-2uL, 3u);
  print(-3uL, 2u);
  //
  print(2, 3u);
  print(3, 2u);
  print(-2, 3u);
  print(-3, 2u);
  print(2L, 3u);
  print(3L, 2u);
  print(-2L, 3u);
  print(-3L, 2u);
  //
  print(3u, 2);
  print(2u, 3);
  print(3u, -2);
  print(2u, -3);
  print(3u, 2L);
  print(2u, 3L);
  print(3u, -2L);
  print(2u, -3L);
  puts("The following should not loose precision");
  print(2U, -(s)((-1U)/2));
  print(2UL, -(s)((-1U)/2));
  print(2ULL, -(s)((-1U)/2));
  print(2U, -(sl)((-1UL)/2));
  print(2UL, -(sl)((-1UL)/2));
  print(2ULL, -(sl)((-1UL)/2));
  print(2U, -(sll)((-1ULL)/2));
  print(2UL, -(sll)((-1ULL)/2));
  print(2ULL, -(sll)((-1ULL)/2));
  puts("Depending on the precision of the types, one or several of the following may loose precision");
  /* print(2.F, -(s)((-1U)/2)); */
  print(2., -(s)((-1U)/2));
  print(2.L, -(s)((-1U)/2));
  /* print(2.F, -(sl)((-1UL)/2)); */
  /* print(2., -(sl)((-1UL)/2)); */
  print(2.L, -(sl)((-1UL)/2));
  /* print(2.F, -(sll)((-1ULL)/2)); */
  /* print(2., -(sll)((-1ULL)/2)); */
  print(2.L, -(sll)((-1ULL)/2));
  puts("Investigating all combinations of unsigned and floating types");
#pragma CMOD amend foreach F = f d l
#pragma CMOD amend foreach S = \  l ll
  {
    if ( WIDTHꞌ##${F} < WIDTHꞌu##${S}-1) puts("s" #${S} " does not fit into " #${F});
  }
#pragma CMOD done
#pragma CMOD done

}


