/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */

/**
 ** @file
 **
 ** @brief An adhoc utility to transform code points of the form
 ** <code>\uXXXX</code> or <code>\UXXXXXXX</code> into UTF-8.
 **/

#pragma CMOD amend alias bs=tools::bs fnct=tools::functions

/**
 ** @ an auxiliary DT to pump bytes into integers
 **
 ** This avoid dealing with endianess.
 **/
union over {
  uint32_t i;
  char o[4];
};

int stdc∷main(int argc, char* argv[argc+1]) {
  FILE* fout = stdout;
  FILE* in = stdin;
  size_t f = 1;
  // Loop over all command line arguments and interpret them as
  // filenames. If there is none or the first argument is a single -,
  // use stdin.
  if (argc < 2 ∨ ¬strcmp("-", argv[1]))
    goto ONCE;
  for (; f < argc; ++f) {
    if (¬freopen(argv[f], "r", in)) goto FREOPEN;
ONCE:;
    char buffer[tools∷bsize];
    while (fnct∷chunk(in, tools∷bsize, buffer)) {
      char const* p = buffer;
      while (*p) {
        // Position ourselves at the next backslash.
        char const* next = strchr(p, '\\');
        if (¬next) {
          fputs(p, fout);
          break;
        } else {
          fnct∷writeout(fout, next-p, p);
          p = next + 1;
        }
        // Here we know that we have seen a backslash.
        int len = 4;
        switch (*p) {
          // We were not lucky, and the backslash was at the end of
          // our current input buffer.
        case 0: ungetc('\\', in); goto RETRY;
        default: fnct∷writeout(fout, 2, p-1); ++p; break;
        case 'U': len = 8;
        case 'u': {
          char const* zero = memchr(p, 0, len+1);
          // If the buffer ends too early, unwind it and retry.
          if (zero) {
            while (zero ≠ p) ungetc(*--zero, in);
            ungetc('\\', in);
            goto RETRY;
          }
          // Now we know that there are enough characters in the
          // buffer. Obtain an estimate on how long the output may be.
          size_t const olen_max
            = (len ≡ 8
               // Nothing particular can be known about `U`
               // encodings.
               ? 4
               // In a `u` encoding a leading zero means that the
               // output is at most 2 bytes long.
               : ((p[1] ≠ '0')
                  ? 3
                  // If it has even two leading zeros, the output may
                  // even only be one character.
                  : ((p[2] ≡ '0') ∧ ('0' ≤ p[3]) ∧ (p[3] ≤ '7')) ? 1 : 2));
          // With all this information captured, specialize according
          // to the four possible cases.
#pragma CMOD amend specialize olen_max 1 2 3 4
          // Marking may be useful for debugging.
          __asm__("# specialize " #${olen_max});
          // `val` will be the Unicode code point.
#if ${olen_max} ≤ 3
          // If in any of the following the argument is not in the
          // range of udigit, the output will overflow in the high
          // bits and the problem will be detected later.
          register uint32_t val = fnct∷udigit(p[3])⌫4 ∪ fnct∷udigit(p[4]);
# if ${olen_max} > 1
          val ∪= fnct∷udigit(p[2])⌫8;
#  if ${olen_max} > 2
          val ∪= fnct∷udigit(p[1])⌫12;
#  endif
# endif
#else
          register uint32_t val = 0;
          for (unsigned i = 0; i < len/4; ++i) {
            uint16_t scn;
            int ret = sscanf(p+1+(i*4), "%4" SCNx16, &scn);
            val = (val << 16) + scn;
            if (ret < 1) {
              fprintf(stderr, "invalid hex for Unicode point: %.4s\n", p+1);
              // sscanf failed. Just print the input pattern.
              fnct∷writeout(fout, 1, p-1);
              len = 0;
              goto FORWARD;
            }
          }
#endif
          if (val ≥ 0x110000) {
            fprintf(stderr, "invalid hex for Unicode point: %.4s\n", p+1);
            // The code is not valid. Just print the input pattern.
            fnct∷writeout(fout, 1, p-1);
            len = 0;
            goto FORWARD;
          }
          // Strip the bits of that down into chunks, basically
          // sixpacks.
          over data = {
            .o = {
#if ${olen_max} ≡ 4
              // four byte encoding, 3, 6, 6 and 6 bit = 21 bit in 32 bit
              [0] = bs∷downset3(val, 3×6),
              [1] = bs∷downset6(val, 2×6),
              [2] = bs∷downset6(val, 1×6),
              [3] = bs∷downset6(val, 0×6),
#elif ${olen_max} ≡ 3
              // three byte encoding, 4, 6 and 6 bit = 16 bit in 24 bit
              [1] = bs∷downset4(val, 2×6),
              [2] = bs∷downset6(val, 1×6),
              [3] = bs∷downset6(val, 0×6),
#elif ${olen_max} ≡ 2
              // two byte encoding, 5 and 6 bit = 11 bit in 16 bit
              [2] = bs∷downset5(val, 1×6),
              [3] = bs∷downset6(val, 0×6),
#else
              // one byte ASCII encoding, 7 bit in 8 bit
              [3] = val,
#endif
            },
          };
          int olen = 1;
          if ((${olen_max} ≡ 1) ∨ (val ≤ bs∷all7)) {
            // one byte ASCII encoding, 7 bit in 8 bit

            // This should only trigger for real if we had a `U`
            // encoding that had only the lowest byte set. For a `u`
            // encoding we detected this already and the following
            // assignment is redundant and should be removed by the
            // optimizer.
            data.o[3] = val;
          } else {
            if ((${olen_max} ≡ 2) ∨ (val ≤ bs∷all11)) {
              // two byte encoding, 5 and 6 bit = 11 bit in 16 bit
              olen=2;
              // Here (and the two following cases) we are really
              // interested in `marker.i`. Since all components are
              // ICE, the compiler should be able to construct an ICE
              // also for this, so this and the following bit
              // instruction should result in exactly one assembler
              // instruction with an immediate.
              over marker = {
                .o = {
                  [2] = bs∷duo6,
                  [3] = bs∷singleton7,
                },
              };
              data.i ∪= marker.i;
            } else {
              if ((${olen_max} ≡ 3) ∨ (val ≤ bs∷all16)) {
                // three byte encoding, 4, 6 and 6 bit = 16 bit in 24 bit
                olen=3;
                over marker = {
                  .o = {
                    [1] = bs∷trio5,
                    [2] = bs∷singleton7,
                    [3] = bs∷singleton7,
                  },
                };
                data.i ∪= marker.i;
              } else {
                // four byte encoding, 3, 6, 6 and 6 bit = 21 bit in 32 bit
                olen=4;
                over marker = {
                  .o = {
                    [0] = bs∷quartett4,
                    [1] = bs∷singleton7,
                    [2] = bs∷singleton7,
                    [3] = bs∷singleton7,
                  },
                };
                data.i ∪= marker.i;
              }
            }
          }
          over out = { .i = data.i };
          fnct∷writeout(fout, olen, out.o+(4-olen));
#pragma CMOD done
          FORWARD:
          p += len+1;
        }
        }
      }
RETRY:;
    }
  }
  return EXIT_SUCCESS;
FREOPEN:
  perror(argv[0]);
  if (argc > 1)
    fprintf(stderr, "\twhen trying to open \"%s\"\n", argv[f]);
  return EXIT_FAILURE;
}
