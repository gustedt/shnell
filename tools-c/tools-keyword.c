/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */

#pragma PRETRADE alias bs=tools::bs
/**
 ** @file
 **
 ** @brief Tools to handle the keywords that are needed for
 ** header/body classification.
 **/

/**
 ** @brief handle sets of keywords
 **/
typedef bs keyword∷properties;

/**
 ** @brief a list of keywords that we need in three different places
 **
 ** @remark this list must remain sorted
 **/
#pragma CMOD amend bind                         \
  KEYWORDS=CMOD                                 \
  _Alignas                                      \
  _Atomic                                       \
  _Bool                                         \
  _Comma                                        \
  _Complex                                      \
  _Imaginary                                    \
  _Noreturn                                     \
  _Static_assert                                \
  _Thread_local                                 \
  alignas                                       \
  auto                                          \
  bool                                          \
  char                                          \
  complex                                       \
  const                                         \
  declaration                                   \
  define                                        \
  definition                                    \
  double                                        \
  elif                                          \
  else                                          \
  endif                                         \
  enum                                          \
  extern                                        \
  float                                         \
  if                                            \
  ifdef                                         \
  ifndef                                        \
  imaginary                                     \
  inline                                        \
  int                                           \
  line                                          \
  long                                          \
  noreturn                                      \
  pragma                                        \
  register                                      \
  restrict                                      \
  short                                         \
  signed                                        \
  static                                        \
  static_assert                                 \
  struct                                        \
  thread_local                                  \
  typedef                                       \
  union                                         \
  unsigned                                      \
  void                                          \
  volatile

#pragma TRADE foreach X ${KEYWORDS}
extern int _ ## ${X};
#pragma TRADE done


/**
 ** @brief a lexicographic list of all keywords that we use
 **
 ** These numbers will be used as bit numbers to summarize properties
 ** of blobs of code.
 **
 ** Added at the end are some of the combinations that we use
 ** hereafter.
 **/
#pragma TRADE foreach X:I ${KEYWORDS}
#define _ ##${X} ${I}
#pragma TRADE done

enum keyword {
  keyword∷_max = 0
#pragma TRADE foreach X ${KEYWORDS}
  +1
#pragma TRADE done
};
typedef enum keyword keyword;

// bug
//static_assert(_max ≤ 64, "too many elements in enum keyword");

union maxsize {
#pragma TRADE foreach X ${KEYWORDS}
    char intern_##${X}[(sizeof #${X}) - 1];
#pragma TRADE done
};

/* determine the longest keyword */
enum {
  /**
   ** @brief the size of the longest keyword string
   **/
  keyword∷size = sizeof(maxsize),

};

/**
 ** @brief Used internally to hold identifiers that could be keywords.
 **/
typedef char keyword∷string[size];

/**
 ** @brief a lexicographic list of all keywords as strings
 **/
string const words[] = {
#pragma TRADE foreach X ${KEYWORDS}
  [_##${X}] = #${X},
#pragma TRADE done
};

// bug
//static_assert(_max ≡ (sizeof words / sizeof words[0]), "max and number of elements don't coincide");

#define keyword∷controls bs(_if, _elif, _else, _ifdef, _ifndef, _endif, _pragma)
#define keyword∷internals bs(_typedef, _static, _static_assert, __Static_assert)
#define keyword∷typenames bs(_enum, _struct, _union)
#define keyword∷wides bs(_int, _unsigned, _signed, _long)
#define keyword∷narrows bs(_short, _char, _bool, __Bool)
#define keyword∷floatings bs(_double, _float)
#define keyword∷integers bs∷Union(wides, narrows)
#define keyword∷basetypes bs∷Union(integers, floatings, bs(_void))
#define keyword∷cmods bs(_pragma, _CMOD, _declaration, _definition)
#define keyword∷noexterns bs(_extern, _inline)

#pragma CMOD amend foreach N = control internal typename wide narrow floating integer basetype cmod noextern
#define keyword::${N}(X) bs∷intersect(X, ${N}##s)
#pragma CMOD done

inline
string const* get(keyword k) {
  return (k < _max) ? &words[k] : 0;
}

inline
int compar(void const* a, void const* b) {
  string const* A = a;
  string const* B = b;
  return strcmp(*A, *B);
}

inline
string const* bsearch(string const s[static 1],
                            size_t nmemb,
                            string const arr[static nmemb]) {
  return stdc∷bsearch(s, arr, nmemb, sizeof arr[0], compar);
}

inline
keyword find(string const w[static 1]) {
  string const* ret = bsearch(w, _max, words);
  return ret ? ret - &words[0] : -1;
}

void print(FILE* restrict out, properties prop) {
  char const*restrict format = "%s";
  for (unsigned kw = 0; prop; ++kw, prop ⌦=1) {
    if (prop % 2) {
      fprintf(out, format, words[kw]);
      format = ", %s";
    }
  }
}
