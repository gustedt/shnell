/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */
// This lists all includes explicitly, such that this later on also
// compiles as Modular C.
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tools.h"
#include "tools-functions.h"

/**
 ** @file
 **
 ** @brief An adhoc utility to hide characters, strings and comments.
 **
 ** All of these are transformed into sequences of the form
 ** @code
 ** @ X nm nm nm nm nm nm .... @
 ** @endcode
 **
 ** Where @c nm are two character hex numbers for character code and
 ** @c X is one character with the following significance:
 **
 ** - @c C C comment
 ** - @c L @c "wide string literal"
 ** - @c M @c 'wide character'
 ** - @c P C++ comment
 ** - @c S @c "normal string literal"
 ** - @c T @c 'normal character'
 ** - @c U @c U"bla"
 ** - @c V @c U'bla'
 ** - @c u @c u"bla"
 ** - @c u8 @c u8"bla"
 ** - @c v @c u'bla'
 **
 ** Multiline comments are split at the end of each line to ensure
 ** that the line number count remains correct.
 **/

int stdc::main(int argc, char* argv[argc+1]) {
  FILE* out = stdout;
  FILE* in = stdin;
  size_t f = 1;
  // Whether or not to remove comments
  bool remove = getenv("CMOD_REMOVE_COMMENTS");
  // Loop over all command line arguments and interpret them as
  // filenames. If there is none or the first argument is a single -,
  // use stdin.
  if (argc < 2 ∨ ¬strcmp("-", argv[1]))
    goto ONCE;
  for (; f < argc; ++f) {
    if (¬freopen(argv[f], "r", in)) goto FREOPEN;
ONCE:;
    char buffer[tools::bsize];
    char const* p = 0;
    char const* e = 0;
    while (tools::functions::chunk(in, tools::bsize, buffer)) {
      for (p = buffer; *p; ++p) {
        bool show = true;
RESTART:
        if (*p ≠ '@') {
          fputc(*p, out);
          continue;
        } else {
          ++p;
          switch (*p) {
          case 0  : ungetc('@', in);                  goto RETRY;
          case 'C':
            ++p;
            if (remove) {
              show = false;
            } else {
              fputs("/*", out);
            }
            e = "*/";
            break;
          case 'P':
            ++p;
            if (remove) {
              show = false;
            } else {
              fputs("//", out);
            }
            e = "";
            break;
          case 'L': ++p; fputs("L\"", out); e = "\""; break;
          case 'M': ++p; fputs("L'", out);  e = "'";  break;
          case 'S': ++p; fputs("\"", out);  e = "\""; break;
          case 'T': ++p; fputs("'", out);   e = "'";  break;
          case 'U': ++p; fputs("U\"", out); e = "\""; break;
          case 'V': ++p; fputs("U'", out);  e = "'";  break;
          case 'u': ++p; fputs("u\"", out); e = "\""; break;
          case 'v': ++p; fputs("u'", out);  e = "'";  break;
          case '8': ++p; fputs("u8\"", out); e = "\""; break;
          case ' ': ++p;                                 goto RESTART;
          default: fprintf(stderr, "unkown token @%c in program, stopping: %s\n", *p, buffer); return EXIT_FAILURE;
          }
          p = tools::functions::decode(out, in, p, tools::bsize, buffer, show);
          if (show) fputs(e, out);
        }
      }
RETRY:;
    }
  }
  return EXIT_SUCCESS;
FREOPEN:
  perror(argv[0]);
  if (argc > 1)
    fprintf(stderr, "\twhen trying to open \"%s\"\n", argv[f]);
  return EXIT_FAILURE;
}
