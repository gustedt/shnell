/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */

// Define short names for real types that need them
typedef signed char schar;
typedef signed long long llong;
typedef unsigned char uchar;
typedef unsigned short ushort;
typedef unsigned long ulong;
typedef unsigned long long ullong;
typedef long double ldouble;

#pragma CMOD amend eval COMPAR
#pragma CMOD amend foreach T =                  \
  bool                                          \
  char                                          \
  schar                                         \
  short                                         \
  signed                                        \
  long                                          \
  llong                                         \
  uchar                                         \
  ushort                                        \
  unsigned                                      \
  ulong                                         \
  ullong                                        \
  float                                         \
  double                                        \
  ldouble

#pragma COMPAR amend bind T=${T} COMPAR=comparꞌ${T}
#pragma CMOD load compar

#pragma CMOD done
#pragma COMPAR done
#pragma CMOD done
#pragma CMOD done

