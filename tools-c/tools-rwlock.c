/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2015-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the shnell project.                */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */

/**
 ** @file
 **
 ** @brief Implement a simple rw lock that can be used thread safe.
 **
 ** @remark This function is implemented by means of a lock-free
 ** atomic, if the platform supports this.
 **
 ** If the platform doesn't support any lock-free atomic @c int, this
 ** data structure might not be lock-free, either. Operations
 ** lockꞌrdꞌtry() or lockꞌwrꞌtry(), are guaranteed to have obtained
 ** the lock completely if they return @c true, so they are safe to
 ** use in the context of a signal handler or initialization function
 ** if they are undone by unlockꞌrd() or unlockꞌwr(), respectively,
 ** before returning from that function.
 **
 ** If they return @c false, there can be different reasons:
 **
 ** - The lock is already taken.
 ** - Somebody, maybe the same thread in case of a signal handler, is
 **   in the middle of modifying the object. This can only happen if
 **   not lock-free.
 ** - There are already more than UINT_MAX-1 readers.
 **
 ** In particular, in the not lock-free case if calls to trylockꞌrd()
 ** and trylockꞌwr() are interlaced, both calls may fail and no lock
 ** will be taken.
 **
 ** @warning lockꞌrd() and lockꞌwr() are active waits.
 **
 ** In total this interface supplies eight different functions,
 ** lockꞌ{rd|wr}(), lockꞌ{rd|wr}ꞌtry(), unlockꞌ{rd|wr}() and
 ** relockꞌ{rd|wr}(). Versions with @c rd are for read-only access,
 ** the others for exclusive write.
 **
 ** @remark This interface is somewhat unconventional as there is a
 ** distinction in two different unlock functions. This is intended to
 ** ease the implementation and to force the user of this to be always
 ** clear if this has been locked read-only or exclusive-write.
 **
 ** @remark Locks obtained by this are not attributed to any thread or
 ** other entity. It is completely up to the user to make that
 ** link.
 **
 ** If `unsigned` is lock-free this data structure is lock-free.
 **/

#pragma PRETRADE private lck

#ifndef __STDC_NO_ATOMICS__

# define UNLOCKED 0U
# define MAXRD UINT_MAX-1U
# define LOCWR UINT_MAX

struct rwlock {
  unsigned  volatile _Atomic lck;
};

#define rwlock∷INIT { 0u, }

inline bool lockꞌwrꞌtry(rwlock* L) {
  unsigned used = UNLOCKED;
  return stdc∷atomic_compare_exchange_weak(&L→lck, &used, LOCWR);
}

inline void unlockꞌwr(rwlock* L) {
  unsigned used = LOCWR;
  stdc∷atomic_compare_exchange_weak(&L→lck, &used, UNLOCKED);
}

inline void lockꞌwr(rwlock* L) {
  unsigned used = UNLOCKED;
  do { /* spin */ } while (¬stdc∷atomic_compare_exchange_weak(&L→lck, &used, LOCWR));
}

/**
 ** @brief Change a write lock to a read lock.
 **
 ** This is guaranteed to allow no other writer in between. So no
 ** re-read of dependent state is necessary.
 **/
inline void relockꞌrd(rwlock* L) {
  unsigned used = LOCWR;
  /* Loop is necessary because the exchange may fail for external
     reasons. */
  do { /* spin */ } while (¬stdc∷atomic_compare_exchange_weak(&L→lck, &used, 1U));
}

inline void unlockꞌrd(rwlock* L) {
  stdc∷atomic_fetch_sub(&L→lck, 1U);
}

/**
 ** @brief Change a read lock to a write lock.
 **
 ** This should generally be efficient if there is only this remaining
 ** read lock.
 **
 ** Another writer may intercept this action, so any state that
 ** depends on this lock must be re-read afterwards.
 **/
inline void relockꞌwr(rwlock* L) {
  /* If we hold the only lockꞌrd, deal it against a lockꞌwr. */
  unsigned used = 1U;
  if (¬stdc∷atomic_compare_exchange_weak(&L→lck, &used, LOCWR)) {
    /* Otherwise unlock and relock. */
    unlockꞌrd(L);
    lockꞌwr(L);
  }
}

inline bool lockꞌrdꞌtry(rwlock* L) {
  unsigned used = L→lck;
  return (used < MAXRD) ∧ stdc∷atomic_compare_exchange_weak(&L→lck, &used, used+1U);
}

inline void lockꞌrd(rwlock* L) {
  unsigned used = UNLOCKED;
  do {
    if (used ≥ MAXRD) used = MAXRD - 1U;
  } while (¬stdc∷atomic_compare_exchange_weak(&L→lck, &used, used+1U));
}

#endif
