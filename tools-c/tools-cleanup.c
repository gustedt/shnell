// This may look like garbage but is actually -*- C -*-

/**
 ** @file
 **
 ** @brief A module to cleanup dynamic allocations at the end of the
 ** process' lifetime.
 **
 ** As such this feature is not so important, it only ensures that a
 ** cleanup of malloc'ed space is done at the end of the
 ** execution. The primary goal is to get these allocations from a
 ** list of memory leaks that tools as valgrind provide.
 **
 **/


#pragma PRETRADE atexit final

/* A lock that protects the following two variables. */
static tools∷rwlock lock = tools∷rwlock∷INIT;
static void const**volatile base = 0;
static size_t volatile len = 0;

void const* cleanup(void const* p) {
  // This is used to draw a unique slot number to register `p`.
  static uint64_t _Atomic max_pos = 0;
  uint64_t pos = max_pos++;
  tools∷rwlock∷lockꞌrd(&lock);
  /* max_pos may have been incremented several times before we arrive
     here. */
  if (pos < len) {
    base[pos] = p;
    tools∷rwlock∷unlockꞌrd(&lock);
  } else {
    /* switch to slow mode */
    tools∷rwlock∷relockꞌwr(&lock);
    /* only enlarge if nobody else has done it, yet */
    if (pos ≥ len) {
      // ensure that we always have enough place for pos.
      size_t nlen = 2*pos + 16;
      void const** pb = stdc∷realloc(base, sizeof(void**[nlen]));
      if (pb) {
        for (size_t i = len; i < nlen; ++i)
          pb[i] = nullptr;
        len = nlen;
        base = pb;
        pb[pos] = p;
      }
    } else {
      base[pos] = p;
    }
    tools∷rwlock∷unlockꞌwr(&lock);
  }
  return p;
}

static void final(void) {
  size_t l = len;
  void const** tmp = base;
  base = nullptr;
  len = 0;
  for (size_t i = 0; i < l; ++i) {
    stdc∷free((void*)tmp[i]);
  }
  stdc∷free((void*)tmp);
}
