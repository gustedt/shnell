/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2018 Jens Gustedt, INRIA, France                               */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */

/**
 ** @file
 **
 ** @brief A simple flexible string type à la C.
 **
 ** This is a type that extends usual C string properties (null
 ** termination) with the possibility to have instant access to the
 ** length and the end of the string, and to have such strings grow
 ** dynamically if needed.
 **
 ** They are used through pointers, in fact it is almost never a good
 ** idea to have a <code>flex</code> allocated on the stack or even as static.
 **
 ** Most functions working on <code>flex*</code> return also such a
 ** pointer. The idea is that this gives the function the possibility
 ** to reallocate the object at a different place in memory if its
 ** needs to, and to return the new position.
 **
 ** All <code>flex</code> functions can safely receive a null pointer
 ** as their <code>flex*</code> argument. They will then simply
 ** allocate space and return the newly created object.
 **/

struct flex {
  size_t Actual;
  size_t Total;
  char Data[];
};

#define flex∷next(B) _Generic((B), flex*: next_mutable, default: next_const)(B)
#define flex∷data(B) _Generic((B), flex*: data_mutable, default: data_const)(B)

#define cat3(_0, _1, _2, ...)                   \
  _Generic((_1),                                \
           char*: strcat_char3,                 \
           char const*: strcat_char3,           \
           flex*: strcat_flex3,                 \
           flex const*: strcat_flex3,           \
  default: _Generic((_2),                       \
                    char*: strncat_char,        \
                    char const*: strncat_char,  \
           default: strncat_flex                \
                    ))((_0), (_1), (_2))

#define flex∷cat(...) cat3(__VA_ARGS__, 0, 0)

#define cpy3(_0, _1, _2, ...)                   \
  _Generic((_1),                                \
           char*: strcpy_char3,                 \
           char const*: strcpy_char3,           \
           flex*: strcpy_flex3,                 \
           flex const*: strcpy_flex3,           \
  default: _Generic((_2),                       \
                    char*: strncpy_char,        \
                    char const*: strncpy_char,  \
           default: strncpy_flex                \
                    ))((_0), (_1), (_2))

#define flex∷cpy(...) cpy3(__VA_ARGS__, 0, 0)

#define flex∷gets(B) fgets((B), stdin)
#define flex∷puts(B) fputs((B), stdout)

inline size_t actual(flex const* buf) {
  return (buf ? buf→Actual : 0);
}

inline size_t total(flex const* buf) {
  return (buf ? buf→Total : 0);
}

inline char* data_mutable(flex* buf) {
  return (buf ? buf→Data : 0);
}

inline char const* data_const(flex const* buf) {
  return (buf ? buf→Data : 0);
}

inline size_t remaining(flex const* buf) {
  return buf ? (total(buf)-actual(buf)-1) : 0;
}

inline char* next_mutable(flex* buf) {
  return remaining(buf)
    ? buf→Data + buf→Actual
    : 0;
}

inline char const* next_const(flex const* buf) {
  return remaining(buf)
    ? buf→Data + buf→Actual
    : 0;
}

inline flex* cut(flex* buf, size_t s) {
  if (buf) {
    if (s < buf→Actual) {
      buf→Actual = s;
      buf→Data[s] = 0;
    }
  }
  return buf;
}

/**
 ** @brief Enlarge or shrink the flex to maximum size @a s, including
 ** the @c 0 termination.
 **
 ** This doesn't provide data if @a s is bigger than the actual size,
 ** and so the contents of the string will remain the same.
 **
 ** If on the other hand @a s is smaller, the string will be cut at
 ** that character, so the actual string length will be
 ** <code>s-1</code>.
 **
 ** As as special case, @a s of @c 0 will completely deallocate the
 ** whole string and return a null pointer.
 **/
inline flex* resize(flex* buf, size_t s) {
  if (¬s) {
    stdc∷free(buf);
    return 0;
  }
  flex* nbuf = realloc(buf, offsetof(flex, Data) + s);
  if (¬nbuf) return buf;
  if (buf) {
    cut(nbuf, s-1);
  } else {
    memset(nbuf, 0, offsetof(flex, Data)+1);
  }
  nbuf→Total = s;
  return nbuf;
}

inline void free(flex const* buf) {
  stdc∷free((void*)buf);
}

inline flex* strncat_char_inner(flex*restrict buf, size_t len, char const txt[static restrict len]) {
  size_t needed = actual(buf) + len + 1;
  if (needed > total(buf)) {
    buf = resize(buf, 2× needed);
  }
  if (needed ≤ total(buf)) {
    memcpy(next(buf), txt, len);
    buf→Actual += len;
    buf→Data[buf→Actual] = 0;
  }
  return buf;
}

inline flex* strncat_char(flex*restrict buf, size_t len, char const txt[static restrict len]) {
  char const* etxt = memchr(txt, 0, len);
  if (etxt) len = etxt-txt;
  return strncat_char_inner(buf, len, txt);
}

inline flex* strncat_flex(flex*restrict buf, size_t len,  flex const*restrict src) {
  if (¬src) return buf;
  if (len > actual(src)) len = actual(src);
  return strncat_char_inner(buf, len, data(src));
}

inline flex* strncpy_char(flex*restrict buf, size_t len, char const txt[static restrict len]) {
  cut(buf, 0);
  return strncat_char(buf, len, txt);
}

inline flex* strncpy_flex(flex*restrict buf, size_t len,  flex const*restrict src) {
  cut(buf, 0);
  return strncat_flex(buf, len, src);
}

inline flex* strcat_char(flex*restrict buf, char const txt[static restrict 1]) {
  size_t len = strlen(txt);
  return strncat_char_inner(buf, len, txt);
}

inline flex* strcat_char3(flex*restrict buf, char const txt[static restrict 1], int _ignore) {
  return strcat_char(buf, txt);
}

inline flex* strcat_flex(flex*restrict buf, flex const*restrict src) {
  if (¬src) return buf;
  size_t len = actual(src);
  return strncat_char_inner(buf, len, data(src));
}

inline flex* strcat_flex3(flex*restrict buf, flex const*restrict src, int _ignore) {
  return strcat_flex(buf, src);
}

inline flex* strcpy_char(flex*restrict buf, char const txt[static restrict 1]) {
  cut(buf, 0);
  return strcat_char(buf, txt);
}

inline flex* strcpy_flex(flex*restrict buf, flex const*restrict src) {
  cut(buf, 0);
  return strcat_flex(buf, src);
}

inline flex* strcpy_char3(flex*restrict buf, char const txt[static restrict 1], int _ignore) {
  return strcpy_char(buf, txt);
}

inline flex* strcpy_flex3(flex*restrict buf, flex const*restrict src, int _ignore) {
  return strcpy_flex(buf, src);
}

inline int fputs(flex const*restrict buf, FILE* out) {
  return buf
    ? stdc∷fputs(buf→Data, out)
    : EOF;
}

inline bool full(flex const* buf) {
  return actual(buf)+1 ≥ total(buf);
}

size_t fgets(flex*restrict buf, FILE* in) {
  if (¬stdc∷fgets(next(buf), remaining(buf)+1, in))
    return 0;
  size_t found = strlen(buf→Data+(buf→Actual));
  buf→Actual += found;
  return found;
}

flex* printf(flex*restrict buf, char const format[static restrict 1], ...) {
  int avail  = total(buf)-actual(buf);
  va_list ap;
  va_start(ap, format);
  int size = vsnprintf(data(buf), avail, format, ap);
  va_end(ap);

  if (size > avail) {
    size_t needed = actual(buf) + size + 1;
    if (needed < 2× total(buf)) needed = 2× total(buf);
    buf = resize(buf, needed);
    if (buf) {
      va_start(ap, format);
      size = vsnprintf(data(buf), size + 1, format, ap);
      va_end(ap);
    }
  }
  if (buf ∧ size > 0) {
    buf→Actual += size;
  }
  return buf;
}
