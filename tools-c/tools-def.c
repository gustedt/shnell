#pragma CMOD amend alias                        \
  bs=tools::bs                                  \
  keyw=tools::keyword                           \
  flex=tools::flex                              \
  fnct=tools::functions                         \
  grm=tools::grammar

/**
 ** @file
 **
 ** This implements a rudimentary parser that reads a C file and spits
 ** out a header file with all declarations of symbols with external
 ** linkage.
 **
 ** This has an important restriction that comes with Modular
 ** C. Definitions that contain a type *and* a symbol are not allowed
 ** in a definition section, unless they are just pointers to the
 ** type.  This is because types are not external by default, and
 ** symbols that use this type cannot be external because they can't
 ** be declared.
 **
 ** For these symbols the same rule applies as for symbols that use a
 ** hidden type: you'd have to declare it "static". But you did that
 ** anyhow, didn't you?
 **
 ** (For declaration sections see below.)
 **
 ** This is not a standalone tool, but relies on the other tools found
 ** in this directory. Namely it assumes that all comments are hidden
 ** and can easily be skipped over.
 **
 ** The idea for the implementation of this utility is simple. In file
 ** scope there are only a few types of blobs:
 **
 ** - preprocessor directives
 ** - static assertions
 ** - declarations that may also happen to be definitions
 **
 ** A declaration is also a definition, if
 **
 ** - it is an object declaration that is followed by a "=" token and
 **   an initializer.
 **
 ** - it is a function, struct, union or enum declaration that is also followed
 **   by a {} block.
 **
 ** So we can decide that full declaration ends when we meet a "=",
 ** ";", "," or "{" character that is fully exposed, that is not
 ** protected by () or [] parenthesis.
 **
 ** (The comma operator cannot appear in an top level initialization.)
 **
 ** Once we have a declaration, we can decide if it has external
 ** linkage. All but typedef, static, struct, union, and enum have
 ** external linkage.
 **
 ** Internal definitions and static assertions are skipped, unless
 ** they are found in a declaration section, see below, but for struct
 ** or union types with a tagname, a forward declaration as a typedef
 ** is produced.
 **
 ** Preprocessor conditionals, line numbers and pragmas are kept.
 **
 ** External definitions have their initialization or block removed,
 ** to form a declaration, only. Only inline functions that are not
 ** static keep their block.
 **
 ** Such external declarations are then prefixed with "extern", unless
 ** they are already marked extern or inline.
 **
 ** CMOD declaration and definition pragmas play a special role. Lines
 ** that are found after a declaration directive and before any other
 ** definition directive are mostly passed through as they are. The
 ** only addition are struct, union or enum typedef, that introduce
 ** the tagname as an identifier.
 **
 ** Some effort is made to keep a count of the line numbers. All
 ** identified particles are annotated with #line directives such that
 ** compilers that error out have chances to give the line number
 ** where the error occured.
 **/

int main(int argc, char* argv[static restrict argc+1]) {
  FILE* out = stdout;
  flex const* formatpre = 0;
  flex const* formatsym = 0;
  flex const* nameuniq = 0;
  flex* currentf = 0;
  if (argc > 1) {
    formatpre = flex::printf(0, "#line %%zu \"%%s\" // preprocessor\n");
    formatsym = flex::printf(0, "#line %%zu \"%%s\" // symbol\n");
    nameuniq = fnct::uniq(argv[1], "_HEADER");
    currentf = flex::cpy(0, argv[1]);
  } else {
    fprintf(stderr, "%s: we need the source file name as first argument", argv[0]);
    return EXIT_FAILURE;
  }
  flex const* fl = fnct::readin(stdin);
  if (¬fl) return EXIT_FAILURE;
  char const*const restrict f  = flex::data(fl);
  size_t const flen  = flex::actual(fl);
  // The actual position in the buffer.
  size_t pos = 0;
  // The collected state of the current declaration or definition. May
  // be accumulated over several iterations.
  keyw::properties prop = 0;
  // skip initial comment in the file
  size_t len = fnct::skipat(f+pos);
  // the current line number
  size_t line = 1 + grm::lines(len, f+pos);
  if (memchr(f+pos, '@', len)) {
    fnct::writeout(out, len, f+pos);
    pos += len;
  }
  // Output appropriate in instantiation macro definition and include the .h file.
  {
    int arglen = strlen(argv[1]);
    if (argv[1][arglen-2] ≠ '.') {
      fprintf(stderr, "%s: missing '.' in extension '%s' of source file name %s\n",
              argv[0], argv[1]+arglen-2, argv[1]);
      return EXIT_FAILURE;
    }
    fprintf(out, flex::data(formatpre), 0, flex::data(currentf));
    fprintf(out, "#define %s_INSTANTIATE\n", flex::data(nameuniq));
    fprintf(out, flex::data(formatpre), 0, flex::data(currentf));
    fprintf(out, "#include \"%.*s.h\"\n", arglen-2, argv[1]);
    fprintf(out, flex::data(formatpre), line, flex::data(currentf));
  }
  for (;pos < flen;++pos) {
    // Skip initial white space, but only if it doesn't contain
    // comments.
    {
      size_t len = fnct::skipat(f+pos);
      size_t nl = grm::lines(len, f+pos);
      if (memchr(f+pos, '@', len)) {
        fnct::writeout(out, len, f+pos);
      } else if (nl) {
        fputc('\n', out);
      }
      line += nl;
      pos += len;
      if (¬f[pos]) break;
    }
    size_t s = pos,                  // start of the declaration
      e = s + grm::decl(f + s),   // end of the declaration, proper
      ie = e,                        // end of an initializer
      b = e;                         // end of an associated { } block
    if (¬f[e]) break;
    // Start a new declaration, eventually.
    if (¬prop) {
      prop = grm::props(e-s, f+s, 0);
    }
    if (f[e] ≡ '#' ) {
      if (s < e) {
        line += grm::lines(e-s, f+s);
        fnct::writeout(out, e-s, f+s);
      }
      size_t before = line;
      bool declaration = false;
      // find the keyword after the #
      size_t l = grm::prepro(f+e, &line);
      size_t first = fnct::skip(f+e+1);
      size_t w = fnct::word(f+e+1+first);
      keyw::properties kw = grm::prop(w, f+e+1+first);

      if (bs::isin(keyw::_pragma, kw)) {
        kw = grm::props(l, f+e, 0);
        switch (keyw::cmod(kw)) {
        case bs(keyw::_declaration)|bs(keyw::_pragma)|bs(_CMOD):
          declaration = true;
          fprintf(out, flex::data(formatpre), before, flex::data(currentf));
          fputs("/* #pragma CMOD declaration */\n", out);
          break;
        case bs(keyw::_definition)|bs(keyw::_pragma)|bs(_CMOD):
          fprintf(out, flex::data(formatpre), before, flex::data(currentf));
          fputs("/* #pragma CMOD definition */\n", out);
          pos = e+l-1;
          continue;
        }
        // If we detected the start of a declaration section we
        // process it separately.
        if (declaration) {
          pos = e+l-1;
          pos += grm::skip(0, f+pos, &line);
          continue;
        }
      } else {
        if (bs::isin(keyw::_line, kw) ∨ isdigit(f[e+1+first])) {
          if (isdigit(f[e+1+first])) w = 0;
          flex* tmp = fnct::get_linenumber(currentf, f+e+1+first+w, &line);
          if (tmp) {
            currentf = tmp;
            fprintf(out, "#line %zu \"%s\" // detected\n", line, flex::data(currentf));
          }
        } else {
          fprintf(out, flex::data(formatpre), before, flex::data(currentf));
          fnct::writeout(out, l, f+e);
        }
      }
      pos = e+l-1;
      continue;
    } else {
      // Now we know we have identified the next declaration or
      // definition.
      if (f[e] ≡ '=') {
        ie = grm::init(f+e+1) + e + 1;
      }
      if (f[ie] ≠ '{') {
        pos = ie;
      } else {
        b = ie + grm::block(f+ie);
        pos = b;
      }
    }
    if ((b > e) ∧ bs::isin(keyw::_inline, prop)) {
      // Replace an inline definition by an declaration such
      // that the emission of the corresponding symbol may be forced.
      fprintf(out, flex::data(formatsym), line, flex::data(currentf));
      if (¬bs::isin(keyw::_static, prop) ∧ ¬bs::isin(keyw::_extern, prop))
        fputs("extern ", out);
      // Retract while we see spaces.
      while (isspace(f[ie-1])) --ie;
      fnct::writeout(out, ie-s, f+s);
      fputc(';', out);
      prop = 0;
    } else {
      fprintf(out, flex::data(formatsym), line, flex::data(currentf));
      if (ie < b) fnct::writeout(out, b-s, f+s);
      else fnct::writeout(out, ie-s, f+s);
    }
    fputc(f[pos], out);
    if (f[ie] ≡ ';') {
      fputc('\n', out);
      prop = 0;
    }
    if (b < ie) line += grm::lines(ie-s, f+s);
    else {
      line += grm::lines(b-s, f+s);
      prop = 0;
    }
    if (f[pos] ≡ '\n') {
      ++line;
    }
    fputc('\n', out);
  }
  flex::free(fl);
  flex::free(formatsym);
  flex::free(formatpre);
  flex::free(nameuniq);
  return EXIT_SUCCESS;
}
