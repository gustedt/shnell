// -*- C -*-
#define _MAP0(_1, _2, X) _1(X)

// Create a recursive set of macros that apply operation _2 from left
// to right and apply _1 on each of the arguments
#pragma CMOD amend eval INNER
#pragma CMOD amend do I = 1 32
#pragma INNER amend let I1 = ${I} - 1
#define _MAP${I}(_1, _2, X, ...) _2(_1(X), _MAP##${I1}(_1, _2, __VA_ARGS__))
#pragma INNER done
#pragma CMOD done
#pragma CMOD done

#define MAP__(N, ...) _MAP ## N(__VA_ARGS__)
#define MAP_(...) MAP__(__VA_ARGS__)

// Apply _1(X) to each element in the list and join the results by _2(X, Y)
#define map(_1, _2, ...) MAP_(tools::comma(__VA_ARGS__), _1, _2, __VA_ARGS__)
