/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016-2017 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the Modular C project.             */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */

/**
 ** @file
 **
 ** @brief An adhoc utility to transform UTF-8 encoded source code
 ** into ASCII encoded where the unicode code points are represented
 ** in the form <code>"\uXXXX"</code> or <code>"\UXXXXXXXX"</code>, respectively.
 **/

#pragma CMOD amend alias bs=tools::bs

#define xdigit tools::functions::xdigit

static
bool dump(size_t len, void* buff, FILE* out) {
  return fwrite(buff, 1, len, out) < len;
}

int stdc::main(int argc, char* argv[argc+1]) {
  FILE*const out = stdout;
  FILE*const in = stdin;
  alignas(16) char loc2[6] = { '\\', 'u', '0', };
  alignas(16) char loc3[6] = { '\\', 'u', };
  alignas(16) char loc4[10] = { '\\', 'U', '0', '0', };
  // The last four bytes of this buffer will never be written, again,
  // but maybe accessed when a lookout for a 4 byte utf8 character
  // sequences happens at the end of the currently read chunk.
  alignas(16) char buffer[tools::bsize+4] = { 0 };
  size_t f = 1;
  // Loop over all command line arguments and interpret them as
  // filenames. If there is none or the first argument is a single -,
  // use stdin.
  if (argc < 2 ∨ ¬strcmp("-", argv[1]))
    goto ONCE;
  for (; f < argc; ++f) {
    if (¬freopen(argv[f], "r", in)) goto FREOPEN;
ONCE:;
    unsigned remnant = 0;
    while (tools::functions::chunk(in, tools::bsize-remnant, buffer+remnant)) {
      /* This is an unfortunate mix of types. For the IO functions we
         need to use char. For the computations with the values,
         here we must assume that the bytes are all unsigned values. */
      unsigned char* p = (unsigned char*)buffer;
      remnant = 0;
      while (p[0]) {
        // The upper quad of the first byte has a special form for
        // surrogates. It is 1110 for three byte sequences and 1111
        // for four byte sequences. Two byte sequences can be 110x,
        // where this last bit enters the computation of the code
        // point. All others have either the form 10xx (and thus are
        // not valid as a start sequence) or the form 0xxx when they
        // represent an ASCII character.
        switch (p[0] & 0xF0) {
        default: {
          // Write maximal sequences of ASCII directly.
          unsigned char* q = p+1;
          while (*(signed char*)q > 0)
            ++q;
          if (dump(q-p, p, out)) goto FWRITE;
          p = q;
          continue;
        }
        case 0xC0:;
        case 0xD0: {
          if (¬p[1]) {
            goto FINISH;
          }
          // collect the 11 bit that compose a two byte utf8 character sequence
          // 110abc|de 10fg|hijk
          unsigned high = bs::subset5(p[0]); // 000abc|de
          unsigned low  = bs::subset6(p[1]); // 00fg|hijk
          unsigned code = high<<6 | low;
          // 0000|0abc|de00|0000
          // 0000|0000|00fg|hijk
          p += 2;
          loc2[3] = '0' + (code>>8);
          loc2[4] = xdigit(code>>4);
          loc2[5] = xdigit(code>>0);
          if (dump(sizeof loc2, loc2, out)) goto FWRITE;
          continue;
        }
        case 0xE0: {
          if (¬p[1] ∨ ¬p[2]) {
            goto FINISH;
          }
          // collect the 16 bit that compose a three byte utf8 character sequence
          // 1110|abcd 10|efgh|ij 10kl|mnop
          unsigned high = bs∷subset4(p[0]); // 0000|abcd
          unsigned mid  = bs∷subset6(p[1]); // 00|efgh|ij
          unsigned low  = bs∷subset6(p[2]); // 00kl|mnop
          unsigned code = mid<<6 | low;
          // 0000|efgh|ij00|0000
          // 0000|0000|00kl|mnop
          p += 3;
          loc3[2] = xdigit(high);
          loc3[3] = xdigit(code>>8);
          loc3[4] = xdigit(code>>4);
          loc3[5] = xdigit(low);
          if (dump(sizeof loc3, loc3, out)) goto FWRITE;
          continue;
        }
        case 0xF0: {
          if (¬p[1] ∨ ¬p[2] ∨ ¬p[3]) {
            goto FINISH;
          }
          // collect the 21 bit that compose a four byte utf8 character sequence
          // 11110a|bc 10de|fghi 10|jklm|no 10pq|rstu
          unsigned long high = bs∷subset3(p[0]);  // 00|000a|bc
          unsigned long mid  = bs∷subset6(p[1]);  // |00de|fghi
          unsigned long low  = bs∷subset6(p[2]);  // 00|jklm|no
          unsigned long last = bs∷subset6(p[3]);  // |00pq|rstu
          unsigned long code = high<<18 | mid<<12 | low<<6 | last;
          // 0000|0000|000a|bc00|0000|0000|0000|0000
          // 0000|0000|0000|00de|fghi|0000|0000|0000
          // 0000|0000|0000|0000|0000|jklm|no00|0000
          // 0000|0000|0000|0000|0000|0000|00pq|rstu
          p += 4;
          loc4[4] = '0' + (code>>20);
          loc4[5] = xdigit(code>>16);
          loc4[6] = xdigit(code>>12);
          loc4[7] = xdigit(code>>8);
          loc4[8] = xdigit(code>>4);
          loc4[9] = xdigit(code>>0);
          if (dump(sizeof loc4, loc4, out)) goto FWRITE;
          continue;
        }
        }
      }
      if (false) {
        // we have a remnant of some bytes, move them to the front of
        // the buffer.
FINISH:
        fprintf(stderr, "bytes remaining: %s\n", buffer);
        char* q = buffer;
        while (p[0]) {
          q[0] = p[0];
          ++q;
          ++p;
        }
        remnant = q-buffer;
        fprintf(stderr, "found %u\n", remnant);
      }
    }
  }
  return EXIT_SUCCESS;
FREOPEN:
  perror(argv[0]);
  if (argc > 1)
    fprintf(stderr, "\twhen trying to open \"%s\"\n", argv[f]);
  return EXIT_FAILURE;
FWRITE:
  perror(argv[0]);
  if (argc > 1)
    fprintf(stderr, "\twhen processing \"%s\"\n", argv[f]);
  return EXIT_FAILURE;
}
