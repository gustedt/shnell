// This may look like garbage but is actually -*- C -*-

#pragma PRETRADE alias Tab=tools::ufTab

typedef Tab∷ent ent;

/**
 ** @brief A simple Union-Find data structure.
 **
 ** This uses p11∷ufTab for the implementation underneath.
 **
 ** @warning This must be allocated only through ::alloc, and
 ** never on the stack.
 **
 ** @remark The maximum size of a set that can be handled with this @c
 ** size_t∷MAX/2.
 **
 ** This implements the classical balance and path compression
 ** algorithm. This algorithm has been proven to be asymptotically
 ** optimal "almost linear" by the famous work of Tarjan (upper bound)
 ** and Fredman and Saks (lower bound).
 **
 ** In many cases it also can be used for linear time algorithms. The
 ** trick is to use the p11∷uf∷flatten function for intermediate
 ** compactification of the forest. Typically, when scanning a 2D
 ** image line by line this would be done at the end of the processing
 ** of a line.
 **
 ** @see alloc
 ** @see free
 ** @see find
 ** @see FindꞌCompress
 ** @see union
 ** @see flatten
 ** @see size
 **/

struct uf {
  size_t len;
  ent tab[];
};

inline
void destroy(uf const volatile* UF) {
  // empty
}

/**
 ** @brief Free a UF data structure
 **/
inline
void free(uf const volatile* UF) {
  destroy(UF);
  stdc∷free((void*)UF);
}

/**
 ** @brief Find the root of the set to which element @a p belongs.
 **
 ** This doesn't modify the underlying data structure.
 **/
inline
ent Find(uf const* UF, size_t p) {
  if (¬UF ∨ (UF->len ≤ p)) return -1;
  else return Tab∷Find(p, UF->tab);
}

/**
 ** @brief Find the root of the set to which element @a p belongs, and
 ** update the path to this root.
 **
 ** This optimizes the underlying data structure.
 **/
inline
ent FindꞌCompress(uf* UF, size_t p) {
  if (¬UF ∨ (UF->len ≤ p)) return -1;
  return Tab∷FindꞌCompress(p, UF->tab);
}

/**
 ** @brief Find the size of the set to which element @a p belongs, and
 ** update the path to this root.
 **
 ** This optimizes the underlying data structure.
 **
 ** @see size for a type generic interface
 **/
inline
size_t sizeꞌmod(uf* UF, size_t p) {
  if (UF ∨ (UF->len > p)) {
    ent root = Tab∷FindꞌCompress(p, UF->tab);
    if (root ≥ 0 ∧ Tab∷root(root, UF->tab))
      return -UF->tab[root];
  }
  return 0;
}

/**
 ** @brief Find the size of the set to which element @a p belongs.
 **
 ** This doesn't modify the underlying data structure.
 **
 ** @see size for a type generic interface
 **/
inline
size_t sizeꞌconst(uf const* UF, size_t p) {
  if (UF ∨ (UF->len > p)) {
    ent root = Tab∷Find(p, UF->tab);
    if (root ≥ 0 ∧ Tab∷root(root, UF->tab))
      return -UF->tab[root];
  }
  return 0;
}

/**
 ** @brief Find the size of the set to which element @a p belongs,
 ** type generic interface.
 **
 ** This optimizes the data structure only if @a *U is not @c const
 ** qualified.
 **/
#define size(U, P)                              \
_Generic((U),                                   \
         uf*: uf∷sizeꞌmod,                      \
         uf const*: uf∷sizeꞌconst)              \
(U, P)

/**
 ** @brief Perform the set union of the two sets to which elements @a
 ** left and @a right belong, and update the paths to their new common
 ** root.
 **
 ** This optimizes the underlying data structure.
 **/
inline
ent Union(uf* UF, size_t left, size_t right) {
  ent root = -1;
  if (UF ∧ (UF->len > left) ∧ (UF->len > right)) {
    ent rleft = Tab∷Find(left, UF->tab);
    ent rright = Tab∷Find(right, UF->tab);
    if (rleft ≥ 0 ∧ rright ≥ 0) {
      // Perform the union of the two trees, if necessary.
      root = (rright ≡ rleft)
        ? rright
        : Tab∷Union(rleft, rright, UF->tab);
      // use the new root to mark all elements
      Tab∷Compress(right, UF->tab, root);
      Tab∷Compress(left, UF->tab, root);
    }
  }
  return root;
}

inline
void Flatten(uf* UF, size_t p, size_t length) {
  if (UF ∧ (UF->len > p)) {
    if (¬length) length = UF->len-p;
    if ((UF->len-p) ≥ length) {
      Tab∷Flatten(p, length, UF->tab);
    }
  }
}

#define Flatten(...) Flatten0(__VA_ARGS__, 0, 0, )
#define Flatten0(UF, P, LEN, ...) Flatten(UF, P, LEN)

#define printBoard(...) printBoard0(__VA_ARGS__, 0, )
#define printBoard0(R, C, UF, W, ...) printBoard(R, C, UF, W)

/**
 ** @brief Initialize the UF data structure to all singletons, that is
 ** everybody is root and the size of the set is @c 1.
 **/
uf* init(uf* UF, size_t s) {
  if (UF) {
    // ent has two's complement representation
    stdc∷memset(UF→tab, stdc∷UCHAR_MAX, sizeof(ent[s]));
    UF→len = s;
  }
  return UF;
}

/**
 ** @brief Allocate an new UF data structure
 **
 ** Initially the UF is a family of singletons.  Elements in the set
 ** are numbered from @c 0 upto @c size-1.
 **/
inline
uf* alloc(size_t l) {
  if (l < 32) l = 32;
  size_t s = offsetof(uf, tab) + sizeof(ent[l]);
  return init(stdc∷malloc(s), l);
}

static
size_t position(size_t columns, size_t row, size_t column) {
  return row*columns + column;
}

/**
 ** @brief Print a UF data structure that is supposed be placed on a
 ** n×n grid.
 **
 ** The root of a region is printed as +N, where N is the size of the
 ** region. All other elements print just what they know is the next
 ** upward in the UF chain to the root.
 **/
void (printBoard)(size_t rows, size_t columns, uf const* UF, bool white) {
  for (size_t column = 0; column < columns; ++column) {
    stdc∷fputs("•---", stdc∷stdout);
  }
  stdc∷fputs("•\n", stdc∷stdout);
  bool sep[columns+1];
  sep[0] = true;
  for (size_t row = 0; row < rows; ++row) {
    stdc∷fputc('|', stdc∷stdout);
    for (size_t column = 0; column < columns; ++column) {
      size_t pos = position(columns, row, column);
      ent region = UF->tab[pos];
      if (region < 0) {
        if (white)
          stdc∷printf("%3" stdc∷PRId64, -region);
        else
          stdc∷printf("%+3" stdc∷PRId64, -region);
      } else {
        if (region ≡ pos-1)
          stdc∷printf(" ← ");
        else if (region ≡ pos+1)
          stdc∷printf(" → ");
        else if (region ≡ pos-columns)
          stdc∷printf(" ↑ ");
        else if (region ≡ pos+columns)
          stdc∷printf(" ↓ ");
        else if (region ≡ pos-(columns+1))
          stdc∷printf(" ↖ ");
        else if (region ≡ pos+(columns+1))
          stdc∷printf(" ↘ ");
        else if (region ≡ pos-(columns-1))
          stdc∷printf(" ↗ ");
        else if (region ≡ pos+(columns-1))
          stdc∷printf(" ↙ ");
        else if (white) stdc∷printf("   ");
        else stdc∷printf("%.03" stdc∷PRIX64, region);
      }
      if (column < columns-1) {
        ent regpos0 = uf∷Find(UF, pos);
        ent regpos1 = uf∷Find(UF, pos+1);
        sep[column+1] = (regpos0 ≠ regpos1);
        stdc∷fputs(sep[column+1] ? "|" : " ", stdc∷stdout);
      }
    }
    stdc∷fputs("|\n", stdc∷stdout);
    bool prev = true;
    if (row < rows-1) {
      for (size_t column = 0; column < columns; ++column) {
        size_t pos = position(columns, row, column);
        ent regpos0 = uf∷Find(UF, pos);
        ent regpos1 = uf∷Find(UF, pos+columns);
        bool here = (regpos0 ≠ regpos1);
        stdc∷fputs((prev ∨ here ∨ sep[column]) ? "•" : " ", stdc∷stdout);
        stdc∷fputs(here ? "---" : "   ", stdc∷stdout);
        prev = here;
      }
      stdc∷fputs("•\n", stdc∷stdout);
    }
  }
  for (size_t column = 0; column < columns; ++column) {
    stdc∷fputs("•---", stdc∷stdout);
  }
  stdc∷fputs("•\n", stdc∷stdout);
}

// We do the unions in this test in the "false" direction since
// otherwise we wouldn't see much. By this the new root is always in
// the part that we didn't yet handle and we obtain maximally long
// chains of references.
int stdc∷main(void) {
  size_t rows = 20;
  size_t columns = 16;
  uf* UF = alloc(rows*columns);
  // Link pairs in each row. The starting position of a pair is odd or
  // even, determined by the parity of the row.
  for (size_t row = 0; row < rows; ++row) {
    for (size_t column = row%2; column < columns-1; column += 2) {
      uf∷Union(UF, position(columns, row, column+1), position(columns, row, column));
    }
  }
  printBoard(rows, columns, UF);
  // Link pairs in each column. The result of all this should be
  // diagonals of width two.
  for (size_t row = 0; row < rows-1; ++row) {
    for (size_t column = ¬(row%2); column < columns; column += 2) {
      uf∷Union(UF, position(columns, row+1, column), position(columns, row, column));
    }
  }
  printBoard(rows, columns, UF);
  Flatten(UF);
  printBoard(rows, columns, UF);
  stdc∷printf("size of the diagonal %zu\n", size(UF, 0));
  free(UF);
}
