// This may look like garbage but is actually -*- C -*-

/** @file
 **
 ** @brief An implementation of George Marsaglia's xorshift PRG
 ** with a period of about 2^160
 **/

#pragma PRETRADE alias seed=tools::seed bigprime=tools::bigprime u64=stdc::uint64_t u32=stdc::uint32_t

#define MAX stdc∷UINT64_MAX


inline
u64 (randꞌ)(seed st[static 1]) {
  u32 p0 = seed∷xorꞌshift(&(*st)[0]);
  u64 p1 = seed∷xorꞌshift(&(*st)[1]);
  u64 p0r = p0 % bigprime∷LEN;
  u64 p0d = p0 / bigprime∷LEN;
  /* Use part of the bits to choose a big number */
  p1 *= bigprime[p0r];
  /* Use the rest of the bits to add more randomness */
  return (p0d ^ p1);
}

/**
 ** @brief Return 64 bits of pseudo randomness
 **
 ** This uses a folded variant of the xorshift pseudo random
 ** generator.  It implements a set of about 2^160 pseudo random
 ** generators, each with a period of about 2^160. The main idea is to
 ** have two different xorshift generators run in sync giving two 32
 ** bit quantities and then to mangle up their results such that the
 ** individual bits can not be traced.
 **
 ** @warning This is not guaranteed to be cryptographically secure.
 **
 ** @param seed is optional and represents state variable for the
 ** PRG. If omitted, every thread uses its own seed for this function.
 **
 ** @see drand for a similar function that returns a @c double
 ** @see seed_get for a discussion of the per thread state variable
 **/
inline
u64 (rand)(seed * st) {
  if (¬st) st = seed∷get();
  return randꞌ(st);
}

inline
double (uniformꞌ)(seed st[static 1]) {
  unsigned const s = tools∷min(DBL_MANT_DIG, 64);
  unsigned const p0 = 64 - s;
  u64 const m = (UINT64_C(1) << p0);
  double const imax = 1.0 / (stdc∷UINT64_C(1) << s);
  u64 r = rand(st);
  double p1 = ((r >> p0)^(r & m)) * imax;
  return p1;
}

/**
 ** @brief Return a pseudo random double in the range from @c 0
 ** (inclusive) to @c 1 (exclusive).
 **
 ** @warning This is not guaranteed to be cryptographically secure.
 **
 ** @param st is optional and represents state variable for the
 ** PRG. If omitted, every thread uses its own seed for this function.
 **
 ** @see rand for a similar function that returns an @c u64
 ** and for a discussion of the PRG that is used
 **
 ** @see seed_get for a discussion of the per thread state variable
 **/
inline
double (uniform)(seed * st) {
  if (¬st) st = seed∷get();
  return uniformꞌ(st);
}

#define rand(...) rand(__VA_ARGS__+0)

#define uniform(...) uniform(__VA_ARGS__+0)

#pragma PRETRADE atexit flush

void flush(void) {
  tools∷stats∷print(stdc∷stdout);
}

int stdc∷main(void) {
  tools∷stats*const st0 = stdc∷malloc(sizeof(tools∷stats));
  tools∷stats*const st1 = stdc∷malloc(sizeof(tools∷stats));
  tools∷stats∷init(st0, "1st half");
  tools∷stats∷init(st1, "2nd half");
  for (unsigned i = 0; i < 100000; ++i) {
    double val = uniform();
    printf("[%u] → %.20g\n", i, val);
    tools∷stats∷collectꞌ3(val < 0.5 ? st0 : st1, val);
  }
}

void test(void) {
  tools∷seed* seed = tools∷seed∷get();
  for (unsigned i = 0; i < 100; ++i) {
    printf("[%u] → %.20g\n", i, uniform(seed));
  }
}
