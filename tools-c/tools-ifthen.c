/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */

#pragma CMOD insert private ARITHMETICFLOAT

#pragma CMOD amend oneline
#define REALFLOAT(X)
  float: (X),
  double: (X),
  long double: (X)
#pragma CMOD done

#if __STDC_NO_COMPLEX__
#define ARITHMETICFLOAT(X, Y) REALFLOAT(X)
#else
#pragma CMOD amend oneline
#define ARITHMETICFLOAT(X, Y)
    REALFLOAT(X),
    _Complex float: (Y),
    _Complex double: (Y),
    _Complex long double: (Y)
#pragma CMOD done
#endif

#pragma CMOD amend oneline
#define CLASSIFY(X, VP, VF, VC, T, F)
 _Generic(
          /* Either a null pointer constant of type void* (if the
             other is a zero valued integer) or a null pointer of a
             complete type type (if the other is such a pointer).*/
          (1
           ? (void*)0
           :
           /* Either an integer of value 0 (if X is arithmetic) or a
              null pointer of a complete type (if X is of any pointer
              type).*/
           (1
            ? 0
            : _Generic((X),
                       /* void pointers resolve to a null pointer to int */
                       void*: VP,
                       /* All real floating types resolve to VF, all
                          complex floating types to VC */
                       ARITHMETICFLOAT(VF, VC),
                       /* remaining are either integer, pointer to
                          arithmetic type or pointer to qualified void
                          and resolve to themselves */
                       default: (X)))),
          void*: /* X has arithmetic type */ (T),
          default: /* X has other scalar type, so it is pointer
                      type */ (F))
#pragma CMOD done

/**
 ** @brief For scalar expression @a X, evalutate to @a T if @a X is of
 ** arithmetic type and to @a F otherwise.
 **/
#define ifthen∷arithmetic(X, T, F) CLASSIFY(X, (int*)0, 0, 0, T, F)

/**
 ** @brief For scalar expression @a X, evalutate to @a T if @a X is of
 ** real type and to @a F otherwise.
 **/
#define ifthen∷real(X, T, F) CLASSIFY(X, (int*)0, 0, (int*)0, T, F)

/**
 ** @brief For scalar expression @a X, evalutate to @a T if @a X is of
 ** integer type and to @a F otherwise.
 **
 ** This should also detect non-standard integer types, if the
 ** implementation has them.
 **/
#define ifthen∷integer(X, T, F) CLASSIFY(X, (int*)0, (int*)0, (int*)0, T, F)

/**
 ** @brief For scalar expression @a X, evalutate to @a T if @a X is of
 ** pointer type and to @a F otherwise.
 **/
#define ifthen∷pointer(X, T, F) ifthen∷arithmetic(X, F, T)

/**
 ** @brief For scalar expression @a X, evalutate to @a T if @a X is of
 ** non-void pointer type and to @a F otherwise.
 **/
#define ifthen∷nonvoid(X, T, F) CLASSIFY(X, 0, 0, 0, F, T)

/**
 ** @brief For expression @a X, evalutate to @a T if @a X is of real
 ** floating type and to @a F otherwise.
 **/
#define ifthen∷realfloat(X, T, F) _Generic((X), REALFLOAT(T), default: (F))

/**
 ** @brief For expression @a X, evalutate to @a T if @a X is of
 ** complex floating type and to @a F otherwise.
 **/
#define ifthen∷complexfloat(X, T, F) _Generic((X), ARITHMETICFLOAT(F, T), default: (F))

/**
 ** @brief For expression @a X, evalutate to @a T if @a X is of
 ** floating type and to @a F otherwise.
 **/
#define ifthen∷floating(X, T, F) _Generic((X), ARITHMETICFLOAT(T, T), default: (F))


