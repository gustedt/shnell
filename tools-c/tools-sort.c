/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */

// Define short names for real types that need them
typedef signed char schar;
typedef signed long long llong;
typedef unsigned char uchar;
typedef unsigned short ushort;
typedef unsigned long ulong;
typedef unsigned long long ullong;
typedef long double ldouble;

#pragma CMOD amend eval SORT
#pragma CMOD amend eval EVAL
#pragma CMOD amend foreach T =                  \
  bool                                          \
  char                                          \
  schar                                         \
  short                                         \
  signed                                        \
  long                                          \
  llong                                         \
  uchar                                         \
  ushort                                        \
  unsigned                                      \
  ulong                                         \
  ullong                                        \
  float                                         \
  double                                        \
  ldouble
#pragma EVAL amend bind T=${T} SORT=sortꞌ##${T}
#pragma EVAL load sort
#pragma CMOD done
#pragma CMOD done
#pragma CMOD done

/**
 ** @brief A `const` safe type generic binary search interface
 **
 ** The pointer argument `B` must point to a real type `T`, either
 ** unqualified or `const` qualified.
 **
 ** @param K is either the element of a real type to be searched or
 ** points to the element or type `T` that is to be searched for.
 **
 ** @param `B` points to a sorted array
 **
 ** @param S is the size of the array in number of elements of the
 ** type of `B[0]`
 **/
#pragma CMOD amend oneline
#define sort(S, B)
  _Generic((B)[0],
#pragma CMOD amend foreach T =                  \
  bool                                          \
  char                                          \
  schar                                         \
  short                                         \
  signed                                        \
  long                                          \
  llong                                         \
  uchar                                         \
  ushort                                        \
  unsigned                                      \
  ulong                                         \
  ullong                                        \
  float                                         \
  double
           ${T}: sortꞌ##${T},
#pragma CMOD done
           ldouble: sortꞌldouble
           )(S, B)
#pragma CMOD done

int stdc∷main(int argc, char* argv[argc+1]){
    double Arr[argc+10];
    for (int i = 1; i < argc; ++i) {
      printf("Reading [%d] ← %s, \n", i-1, argv[i]);
      Arr[i-1] = strtod(argv[i], 0);
    }
    puts("-------- before sorting -----------------------");
    for (int i = 0; i < argc-1; ++i)
      printf("[%d] = %g, \n", i, Arr[i]);
    sort(argc-1, Arr);
    puts("-------- after sorting -----------------------");
    for (int i = 0; i < argc-1; ++i)
      printf("[%d] = %g, \n", i, Arr[i]);
}
