/* This may look like nonsense, but it really is -*- mode: C; coding: utf-8 -*- */
/*                                                                              */
/* Except for parts copied from previous work and as explicitly stated below,   */
/* the author and copyright holder for this work is                             */
/* Copyright (c) 2016-2019 Jens Gustedt, INRIA, France                          */
/*                                                                              */
/* This file is free software; it is part of the shnell project.                */
/*                                                                              */
/* You can redistribute it and/or modify it under the terms of the BSD          */
/* License as given in the file LICENSE.txt. It is distributed without          */
/* any warranty; without even the implied warranty of merchantability or        */
/* fitness for a particular purpose.                                            */
/*                                                                              */

/**
 ** @file
 ** @brief Collect some simple statistics online as we go.
 **
 ** This uses a generalization of Welford's trick to compute running
 ** mean and variance. See
 **
 ** Philippe Pébay. Formulas for robust, one-pass parallel computation
 ** of covariances and arbitrary-order statistical moments. Technical
 ** Report SAND2008-6212, SANDIA, 2008. URL
 ** http://prod.sandia.gov/techlib/access-control.cgi/2008/086212.pdf.
 **/

/**
 ** @brief A simple data structure to collect the 0th to 3rd moment of
 ** a statistic.
 **
 ** @warning Since this also uses a @c double for the number of
 ** samples, the validity of all this is restricted to about
 ** @f$2^{50}@f$ samples.
 **/
struct stats {
  double moment[4];
  stats* next;
  char const* description;
};


/**
 ** @brief Return the number of samples that had been entered into the
 ** statistic @a c.
 **/
inline
double samples(stats* c) {
  return c→moment[0];
}

/**
 ** @brief Return the mean value of the samples that had been entered
 ** into the statistic @a c.
 **/
inline
double mean(stats* c) {
  return c→moment[1];
}

/**
 ** @brief Return the variance of the samples that had been entered
 ** into the statistic @a c.
 **/
inline
double var(stats* c) {
  return c→moment[2]/samples(c);
}

/**
 ** @brief Return the standard deviation of the samples that had been
 ** entered into the statistic @a c.
 **/
inline
double sdev(stats* c) {
  return stdc∷sqrt(var(c));
}

/**
 ** @brief Return the relative standard deviation of the samples that
 ** had been entered into the statistic @a c.
 **/
inline
double rsdev(stats* c) {
  return stdc∷sqrt(var(c))/mean(c);
}

/**
 ** @brief Return the normalized skew of the samples that had been
 ** entered into the statistic @a c.
 **/
inline
double skew(stats* c) {
  double v = var(c);
  return (c→moment[3]/stdc∷pow(v, 1.5))/samples(c);
}

/**
 ** @brief Return the unbiased variance of the samples that had been
 ** entered into the statistic @a c.
 **
 ** Use Bessel's correction to have an estimation of the unbiased
 ** variance of the overall population.
 **/
inline
double varꞌunbiased(stats* c) {
  return c→moment[2]/(samples(c)-1);
}

/**
 ** @brief Return the unbiased standard deviation of the samples that
 ** had been entered into the statistic @a c.
 **
 ** Use Bessel's correction to have an less biased estimation of the
 ** variance of the overall population.
 **/
inline
double sdevꞌunbiased(stats* c) {
  return stdc∷sqrt(varꞌunbiased(c));
}

/**
 ** @brief Return the unbiased relative standard deviation of the
 ** samples that had been entered into the statistic @a c.
 **/
inline
double rsdevꞌunbiased(stats* c) {
  return rsdev(c)*(1+1/(4*samples(c)));
}

/**
 ** @brief Add value @a val to the statistic @a c.
 **/
inline
void collect(stats* c, double val, unsigned moments) {
  double n  = samples(c);
  double n0 = n-1;
  double n1 = n+1;
  double delta0 = 1;
  double delta  = val - mean(c);
  double delta1 = delta/n1;
  double delta2 = delta1*delta*n;
  switch (moments) {
  default:
    c→moment[3] += (delta2*n0 - 3*c→moment[2])*delta1;
  case 2:
    c→moment[2] += delta2;
  case 1:
    c→moment[1] += delta1;
  case 0:
    c→moment[0] += delta0;
  }
}

inline
void collectꞌ0(stats* c, double val) {
  collect(c, val, 0);
}

inline
void collectꞌ1(stats* c, double val) {
  collect(c, val, 1);
}

inline
void collectꞌ2(stats* c, double val) {
  collect(c, val, 2);
}

inline
void collectꞌ3(stats* c, double val) {
  collect(c, val, 3);
}


#ifdef __STDC_NO_ATOMICS__

static stats* list;

stats* insert(stats* s) {
  if (s ∧ ¬s→next) {
    s→next = list;
    list = s;
  }
  return s;
}

#else

static stats*_Atomic list;

stats* insert(stats* s) {
  if (s ∧ ¬s→next) {
    while (¬stdc∷atomic_compare_exchange_weak(&list, &s→next, s)) {
      // empty
    }
  }
  return s;
}

#endif

void print(stdc∷FILE* out) {
  int mlen = stdc∷strlen("description");
  stdc∷size_t number = 0;
  stdc∷size_t maxn = 10;
  stats** tab = stdc∷malloc(sizeof(stats*[maxn]));
  for (stats* p = list; p; p = p→next) {
    if (samples(p)) {
      char const* pretty = tools∷demangle(p→description);
      stdc∷size_t len = stdc∷mbstowcs(nullptr, pretty, 0);
      mlen = tools∷max(mlen, len);
      //stdc∷fprintf(stdc∷stderr, "%zu (%d) %s\n", len, mlen, pretty);
      tab[number] = p;
      number++;
      if (number > maxn) {
        maxn *= 2;
        tab = stdc∷realloc(tab, sizeof(stats*[maxn]));
      }
    }
  }
  stdc∷fprintf(out, "#%*s %12s%12s%12s%12s\n",
                mlen-1, "description", "samples", "mean",
                "rsdev", "skew");
  for (stdc∷size_t i = number-1; i < number; --i) {
    char const* pretty = tools∷demangle(tab[i]→description);
    int len = stdc∷mbstowcs(nullptr, pretty, 0);
    stdc∷fprintf(out, "%*s%s %12g%12g%12.4e%12.4e\n",
                  mlen-len, "", pretty, samples(tab[i]), mean(tab[i]),
                  rsdev(tab[i]), skew(tab[i]));
  }
  stdc∷free(tab);
}

#pragma PRETRADE atexit cleanup

void cleanup(void) {
  stats* p;
#ifdef __STDC_NO_ATOMICS__
  p = list;
  list = nullptr;
#else
  p = stdc∷atomic_exchange(&list, nullptr);
#endif
  list = nullptr;
  while (p) {
    stats* prev = p→next;
    stdc∷free(p);
    p = prev;
  }
}

#pragma PRETRADE startup bootstrap

inline void bootstrap(void) {
  fprintf(stderr, "we are %s\n", tools∷demangle∷FUNC);
}
stats* init(stats* s, char const* desc) {
  if (s) {
    *s = (stats) { .description = desc, };
    insert(s);
  }
  return s;
}
