<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <a ><title>tokenize</title></a>
  <style type="text/css">
      code{white-space: pre-wrap;}
      span.smallcaps{font-variant: small-caps;}
      span.underline{text-decoration: underline;}
      div.column{display: inline-block; vertical-align: top; width: 50%;}
  </style>
  <link rel="stylesheet" href="./shnell-style.css?v=1" />
</head>
<body>
<header id="title-block-header">
<h1 class="title">tokenize</h1>
</header>
<div id="content"  class="outline-2">
<p>part of shnell – a source to source compiler enhancement tool</p>
<p>© <em>Jens Gustedt,</em> 2019</p>
<h2 id="tokenize-the-input-stream">Tokenize the input stream</h2>
<p>This script provides means to tokenize a byte stream according to C’s rules about different kinds of tokens:</p>
<ul>
<li><p>number: Beware that number tokens are not always proper numbers, but only a superset which is sufficiently distinct from the identifiers. We also recognize the <code>i</code> suffix of complex floating point literals and rewrite this to a portable form.</p></li>
<li><p>identifier: All words starting with an alphabetic character or underscore, continuing the same or decimal digits.</p></li>
<li><p>punctuator: We recognize all C punctuators, plus the bang-bang operator <code>!!</code> and opening and closing double-brackets <code>[[</code> and <code>]]</code> for attributes. See <a href="punctuators.html"><code>punctuators</code></a> and the Unicode section below for details.</p></li>
<li><p>string and character literals: These are encoded and escaped specially such that no blank character is visible inside such a token, and such that there is no direct contact between alphanumeric characters and punctuaton.</p></li>
<li><p>comment tokens: These are encoded in a similar way, such that no parsing will match these.</p></li>
</ul>
<p>After this tokenization such tokens are surrounded by spaces and a lot of markers (composed of control characters, see below) such that other skripts then easily can treat them on a word base. This is done in a way that the spacing of the source can mostly be reconstructed after parsing.</p>
<h3 id="interfaces">Interfaces</h3>
<ul>
<li><p><a ><code>tokenize</code></a> and <code>untokenize</code> which are functions that do the job of doing the tokenization on the input stream and removing the markers, respectively.</p></li>
<li><p><code>${sedmark}</code> and <code>${sedunmark}</code> which are temporary files that contain regular expressions for sed to do the job</p></li>
</ul>
<h3 id="unicode">Unicode</h3>
<p>Tokenization of Unicode code points is delegated to <code>sed</code>, so all character classification your <code>sed</code> knows about should work. Usually this should work well for the “basic multilanguage plane” (BMP). Most modern scripts for existing languages fall into that category. Beware though, that C has restrictions which Unicode code points are permitted for identifiers, since it only allows alphanumeric characters. E.g a Greek alpha, α, is fine, whereas an infinity symbol, ∞, is not permitted.</p>
<p>We also recognize certain Unicode punctuators that represent mathematical operators. There use should make your source easier readable and also avoid posible ambiguities (for the human reader) in cases where several punctuators are adjacent or where the same punctuator (such as <code>*</code>) can have very different meanings according to the context. The following are the <code>sed</code> replacement patterns that are used.</p>
<pre class="shell"><code>opUtf8=&quot;
 #### comparisons
 s@ ≤ @ &lt;= @g
 s@ ≥ @ &gt;= @g
 s@ ≟ @ == @g
 s@ ≡ @ == @g
 s@ ≠ @ != @g
 #### Boolean logic
 # Beware, &amp; on RHS is special for sed
 s@ ¬ @ ! @g
 s@ ‼ @ !! @g
 s@ ∨ @ || @g
 s@ ∧ @ \\&amp;\\&amp; @g
 #### set operations
 # Beware, &amp; on RHS is special for sed
 s@ ∪ @ | @g
 s@ ∩ @ \\&amp; @g
 s@ ∁ @ ~ @g
 s@ ⌫ @ &lt;&lt; @g
 s@ ⌦ @ &gt;&gt; @g
 #### arithmetic
 # The second line for division is in fact the Unicode point x2215,
 # which happens to have the same glyph as the usual division character
 # x2F.
 s@ × @ * @g
 s@ ÷ @ / @g
 s@ ∕ @ / @g
 s@ − @ - @g
 #### assignment ops
 # Beware, &amp; on RHS is special for sed
 s@ ∪= @ |= @g
 s@ ∩= @ \\&amp;= @g
 s@ ×= @ *= @g
 s@ ÷= @ /= @g
 s@ ∕= @ /= @g
 s@ ⌫= @ &lt;&lt;= @g
 s@ ⌦= @ &gt;&gt;= @g
 #### syntax specials
 s@ … @ ... @g
 s@ → @ -&gt; @g
 #### attributes
 s@ ⟦ @ [[ @g
 s@ ⟧ @ ]] @g&quot;
 </code></pre>
<h3 id="coding-and-configuration">Coding and configuration</h3>
The following code is needed to enable the sh-module framework.
</p>
<pre class="shell"><code>SRC=&quot;$_&quot; . &quot;${0%%/${0##*/}}/import.sh&quot;</code></pre>
<h4 id="imports">Imports</h4>
<p>The following <code>sh</code>-modules are imported:</p>
<ul>
<li><a href="echo.html"><code>echo</code></a></li>
<li><a href="tmpd.html"><code>tmpd</code></a></li>
<li><a href="match.html"><code>match</code></a></li>
<li><a href="punctuators.html"><code>punctuators</code></a></li>
</ul>
<h3 id="details">Details</h3>
<p>All of this can be tuned by assigning different values to the markers.</p>
<p>For procedural reasons the following must be a single control character, each:</p>
<ul>
<li>Markers for inserted spaces surrounding tokens, left or right</li>
</ul>
<pre class="shell"><code>export markl=&quot;${markl:-}&quot;
export markr=&quot;${markr:-}&quot;
</code></pre>
<ul>
<li>for character and string prefixes</li>
</ul>
<pre class="shell"><code>export markpr=&quot;${markpr:-}&quot;
</code></pre>
<ul>
<li>A marker for a double colon. If you leave this allone, the only effect is that spaces arround :: (as in [[ gnu :: deprecated]]) will be removed</li>
</ul>
<pre class="shell"><code>export markc=&quot;${markc:-}&quot;
</code></pre>
<ul>
<li>for internal blanks</li>
</ul>
<pre class="shell"><code>export markib=&quot;${markib:-}&quot;
</code></pre>
<ul>
<li>for internal tabs</li>
</ul>
<pre class="shell"><code>export markit=&quot;${markit:-}&quot;
</code></pre>
<p>The following now can be encoded as two control characters, they don’t need to appear in character classes or IFS.</p>
<ul>
<li>Transient markers for periods, plus and minus within a number. These are removed at the end of tokenization, so you probably don’t want to mess with this.</li>
</ul>
<pre class="shell"><code>export markp=&quot;${markp:-}&quot;
export markplus=&quot;${markplus:-}&quot;
export markminus=&quot;${markminus:-}&quot;
</code></pre>
<ul>
<li>string literal markers, mid, left and right</li>
</ul>
<pre class="shell"><code>export marksm=&quot;${marksm:-}&quot;
export marksl=&quot;${marksl:-}&quot;
export marksr=&quot;${marksr:-}&quot;
</code></pre>
<ul>
<li>character literal markers, mid, left and right</li>
</ul>
<pre class="shell"><code>export markcm=&quot;${markcm:-}&quot;
export markcl=&quot;${markcl:-}&quot;
export markcr=&quot;${markcr:-}&quot;
</code></pre>
<ul>
<li>for double backslash</li>
</ul>
<pre class="shell"><code>export markbb=&quot;${markbb:-}&quot;
</code></pre>
<ul>
<li>for single backslash</li>
</ul>
<pre class="shell"><code>export marksb=&quot;${marksb:-}&quot;
</code></pre>
<ul>
<li>for a backtick</li>
</ul>
<pre class="shell"><code>export markbt=&quot;${markbt:-}&quot;
</code></pre>
<ul>
<li>for a dollar</li>
</ul>
<pre class="shell"><code>export markdo=&quot;${markdo:-}&quot;
</code></pre>
<ul>
<li>for a hash</li>
</ul>
<pre class="shell"><code>export markha=&quot;${markha:-}&quot;
</code></pre>
<ul>
<li>for an underscore</li>
</ul>
<p>These are part of identifiers, but might also be separators inside numbers in the future. Therefore we use hex characters to replace them.</p>
<pre class="shell"><code>export markun=&quot;${markun:-AbCDeF$$fEdCBa}&quot;
</code></pre>
<ul>
<li>comment markers left, right, //-encoding</li>
</ul>
<pre class="shell"><code>export markCL=&quot;${markCL:-}&quot;
export markCR=&quot;${markCR:-}&quot;
export markCMP=&quot;${markCMP:-}&quot;

</code></pre>
<h1 id="section"></h1>
</body>
</html>
