// -*- c++ -*-

#include "stdintwide.hh"

#include <cstdio>
#include <cinttypes>


const long long ll = -48309320483_W;

unsigned u = 05672434111_w;

signed s = 22 + 0567567_w;

long l = 67l * 0567_w;

__uint128_t u128             = 0XFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF_W;
const __uint128_t u128c = ~0_W;
const __uint128_t u128m1 = - - ~0_W;
__uint128_t u128p1 = 0XFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE_W + 1_W;

const __int128_t i128 = 0X7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF_W;
const __int128_t i128c = ~0_W;
const __int128_t i128m1 = - - ~0_W;

uint64_t u64 = 0XFFFFFFFFFFFFFFFF_W;
uint64_t u64c = ~0_W;

const int64_t i64  = 0X7FFFFFFFFFFFFFFF_W;
const int64_t i64m = -0X8000000000000000_W;
const int64_t i64c = ~0_W;

const unsigned a = 0xFFFF_w * 0x1000_w;

const unsigned b = 0xFFFF_w * 0x10001_w;

const signed c = 97_w  % 10_w;

const signed d = -97_w  / 10_w;

const signed e = -97_w  % 10_w;

const unsigned f = (~0_w << 8);

const size_t g = 8_w;

const unsigned h = 0xa1_w ^ 0xb03_w;

const signed i = ~(~0x1_w << 3) & 0xb03_w;

void print128(__uint128_t x) {
    char buf[32_w+2_w];
    auto res = std::to_chars(buf, buf+sizeof(buf), x, 16);
    if (!static_cast<int>(res.ec)) {
        res.ptr[0] = 0;
        printf("%s\n", buf);
    } else {
        printf("invalid string (%d)\n", static_cast<int>(res.ec));
    }
}

void print128(__int128_t x) {
    __uint128_t y = x;
    if (x < 0) {
        putc('-', stdout);
        print128(-y);
    } else {
        putc('+', stdout);
        print128(y);
    }
}

#define PRINTWIDE(F, ...) do { char buf[128_w+3_w]; std::to_chars(buf, buf+sizeof(buf), __VA_ARGS__); printf(F, buf); } while (false)


int main(int argc, char* argv[]) {
    print128(std::numeric_limits<__uint128_t>::max());
    print128(std::numeric_limits<__int128_t>::max());
    print128(std::numeric_limits<__int128_t>::min());
    print128((__uint128_t)std::numeric_limits<intwide_t>::max());
    print128((__uint128_t)(std::numeric_limits<intwide_t>::min()+1_w));
    printf("%lld\n", ll);
    print128(u128);
    print128(u128c);
    print128(u128m1);
    print128(i128);
    print128(i128c);
    print128(i128m1);
    printf("%d\n", u128 == u128m1);
    printf("%d\n", u128 == u128c);
    printf("%d\n", u128c == u128m1);
    printf("%ld\n", i64);
    printf("%ld\n", i64m);
    printf("%ld\n", i64c);
    printf("%x\n", a);
    printf("%x\n", b);
    printf("%d\n", c);
    printf("%d\n", d);
    printf("%d\n", e);
    printf("%#x\n", f);
    printf("%zu\n", g);
    printf("%#x\n", h);
    printf("%d\n", i);
    (intwide_t::k\u0259n\u02C8v\u025Dt("0x100000000000000000000000000000000")).printerror("result of conversion");
    (intwide_t::k\u0259n\u02C8v\u025Dt("0xffffffffffffffffffffffffffffffff") + 1_w).printerror("result of conversion");
    (-1_w - intwide_t::k\u0259n\u02C8v\u025Dt("0xffffffffffffffffffffffffffffffff")).printerror("result of conversion");
    (intwide_t::k\u0259n\u02C8v\u025Dt("0xffffffffffffffffffffffffffffffff") * 1_w).printerror("result of conversion");
    (intwide_t::k\u0259n\u02C8v\u025Dt("0xffffffffffffffffffffffffffffffff") * 2_w).printerror("result of conversion");
    (intwide_t::k\u0259n\u02C8v\u025Dt("0xffffffffffffffffffffffffffffffff") / 0_w).printerror("result of conversion");
    (intwide_t::k\u0259n\u02C8v\u025Dt("0xffffffffffffffffffffffffffffffff") % 0_w).printerror("result of conversion");
    (intwide_t::k\u0259n\u02C8v\u025Dt("0xffffffffffffffffffffffffffffffff") << 1_w).printerror("result of conversion");
    (intwide_t::k\u0259n\u02C8v\u025Dt("0xffffffffffffffffffffffffffffffff") >> 1_w).printerror("result of conversion");
    ((-intwide_t::k\u0259n\u02C8v\u025Dt("0xffffffffffffffffffffffffffffffff")) << 1_w).printerror("result of conversion");
    ((-intwide_t::k\u0259n\u02C8v\u025Dt("0xffffffffffffffffffffffffffffffff")) >> 1_w).printerror("result of conversion");
    long long pn = (int64_t)intwide_t::k\u0259n\u02C8v\u025Dt("0x7fffffffffffffff");
    printf("%lld\n", pn);
    long long pn0 = (int64_t)intwide_t::k\u0259n\u02C8v\u025Dt("0x8000000000000000");
    printf("%lld\n", pn0);
    long long pn1 = (int64_t)intwide_t::k\u0259n\u02C8v\u025Dt("0x8000000000000001");
    printf("%lld\n", pn1);
    long long nn = (int64_t)-intwide_t::k\u0259n\u02C8v\u025Dt("0x7fffffffffffffff");
    printf("%lld\n", nn);
    long long nn0 = (int64_t)-intwide_t::k\u0259n\u02C8v\u025Dt("0x8000000000000000");
    printf("%lld\n", nn0);
    long long nn1 = (int64_t)-intwide_t::k\u0259n\u02C8v\u025Dt("0x8000000000000001");
    printf("%lld\n", nn1);

    constexpr __uint128_t up = (((1_w << 127_w)-1_w)<<1_w) + 1_w;
    constexpr __uint128_t mask = ~(~0_w << 8_w) ^ (~(~0_w << 16_w))<<48_w;
    print128(up);
    print128(mask);
// This should be optimized to a simple comparison to 2
    if (argc > 2_w) {
        printf("%d > 2w\n", argc);
    } else {
        printf("%d <= 2w\n", argc);
    }
// This should produce an error because of the negation of a FP
// value, and then more for doing shift and other bitwise operation.
    __uint128_t mask\u2080 = ~(~0.0_w << 8_w) ^ (~(~0_w << 16_w))<<48_w;
// This should be optimized to an unconditional printf.
    if (argc < std::numeric_limits<intwide_t>::max()) {
        printf("%d < std::numeric_limits<intwide_t>::max(), good\n", argc);
    } else {
// This should never trigger
        printf("%d >= std::numeric_limits<intwide_t>::max(), bad\n", argc);
    }
    PRINTWIDE("%s\n", std::numeric_limits<intwide_t>::max(), 2);
    PRINTWIDE("%s\n", std::numeric_limits<intwide_t>::min(), 2);
    PRINTWIDE("%s\n", -0x80000000000000000000000000000000_w, 2);
    PRINTWIDE("%s\n", -0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF_w, 2);
    PRINTWIDE("%s\n", std::numeric_limits<intwide_t>::max(), 8);
    PRINTWIDE("%s\n", std::numeric_limits<intwide_t>::min(), 8);
    PRINTWIDE("%s\n", -0x80000000000000000000000000000000_w, 8);
    PRINTWIDE("%s\n", -0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF_w, 8);
    PRINTWIDE("%s\n", std::numeric_limits<intwide_t>::max(), 10);
    PRINTWIDE("%s\n", std::numeric_limits<intwide_t>::min(), 10);
    PRINTWIDE("%s\n", -0x80000000000000000000000000000000_w, 10);
    PRINTWIDE("%s\n", -0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF_w, 10);
    PRINTWIDE("%s\n", std::numeric_limits<intwide_t>::max(), 16);
    PRINTWIDE("%s\n", std::numeric_limits<intwide_t>::min(), 16);
    PRINTWIDE("%s\n", -0x80000000000000000000000000000000_w, 16);
    PRINTWIDE("%s\n", -0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF_w, 16);
    PRINTWIDE("%s\n", std::numeric_limits<intwide_t>::max(), 32);
    PRINTWIDE("%s\n", std::numeric_limits<intwide_t>::min(), 32);
    PRINTWIDE("%s\n", -0x80000000000000000000000000000000_w, 32);
    PRINTWIDE("%s\n", -0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF_w, 32);
    PRINTWIDE("%s\n", 0x314_w, 32);
    PRINTWIDE("%s\n", 0x314_w*0x20_w, 32);
    PRINTWIDE("Ki %s\n", 0x314_w*0x400_w, 32);
    PRINTWIDE("Mi %s\n", 0x314_w*0x100000_w, 32);
    PRINTWIDE("Gi %s\n", 0x314_w*0x40000000_w, 32);
    PRINTWIDE("Ti %s\n", 0x314_w*0x10000000000_w, 32);
// Use for array indexing
    double A[10_w] = { 34, 23, 37 };
    printf("A[0] %g %g\n", A[0_w], *(A+2_w));
// Test comparison to floating point
    typedef long double ldouble;
    constexpr float xfloat = (1_w << std::numeric_limits<float>::digits) ;//+ 1_w;
    printf("%La " "-1_w" "\t" "==" "\tx" "float" ":\t%d\n", (ldouble)xfloat, ((1_w << std::numeric_limits<float>::digits) -1_w) == xfloat);
    printf("x" "float" "\t" "==" "\t%La " "-1_w" ":\t%d\n", (ldouble)xfloat, xfloat == ((1_w << std::numeric_limits<float>::digits) -1_w));
    printf("%La " "+0_w" "\t" "==" "\tx" "float" ":\t%d\n", (ldouble)xfloat, ((1_w << std::numeric_limits<float>::digits) +0_w) == xfloat);
    printf("x" "float" "\t" "==" "\t%La " "+0_w" ":\t%d\n", (ldouble)xfloat, xfloat == ((1_w << std::numeric_limits<float>::digits) +0_w));
    printf("%La " "+1_w" "\t" "==" "\tx" "float" ":\t%d\n", (ldouble)xfloat, ((1_w << std::numeric_limits<float>::digits) +1_w) == xfloat);
    printf("x" "float" "\t" "==" "\t%La " "+1_w" ":\t%d\n", (ldouble)xfloat, xfloat == ((1_w << std::numeric_limits<float>::digits) +1_w));
    printf("%La " "-1_w" "\t" "!=" "\tx" "float" ":\t%d\n", (ldouble)xfloat, ((1_w << std::numeric_limits<float>::digits) -1_w) != xfloat);
    printf("x" "float" "\t" "!=" "\t%La " "-1_w" ":\t%d\n", (ldouble)xfloat, xfloat != ((1_w << std::numeric_limits<float>::digits) -1_w));
    printf("%La " "+0_w" "\t" "!=" "\tx" "float" ":\t%d\n", (ldouble)xfloat, ((1_w << std::numeric_limits<float>::digits) +0_w) != xfloat);
    printf("x" "float" "\t" "!=" "\t%La " "+0_w" ":\t%d\n", (ldouble)xfloat, xfloat != ((1_w << std::numeric_limits<float>::digits) +0_w));
    printf("%La " "+1_w" "\t" "!=" "\tx" "float" ":\t%d\n", (ldouble)xfloat, ((1_w << std::numeric_limits<float>::digits) +1_w) != xfloat);
    printf("x" "float" "\t" "!=" "\t%La " "+1_w" ":\t%d\n", (ldouble)xfloat, xfloat != ((1_w << std::numeric_limits<float>::digits) +1_w));
    printf("%La " "-1_w" "\t" "<" "\tx" "float" ":\t%d\n", (ldouble)xfloat, ((1_w << std::numeric_limits<float>::digits) -1_w) < xfloat);
    printf("x" "float" "\t" "<" "\t%La " "-1_w" ":\t%d\n", (ldouble)xfloat, xfloat < ((1_w << std::numeric_limits<float>::digits) -1_w));
    printf("%La " "+0_w" "\t" "<" "\tx" "float" ":\t%d\n", (ldouble)xfloat, ((1_w << std::numeric_limits<float>::digits) +0_w) < xfloat);
    printf("x" "float" "\t" "<" "\t%La " "+0_w" ":\t%d\n", (ldouble)xfloat, xfloat < ((1_w << std::numeric_limits<float>::digits) +0_w));
    printf("%La " "+1_w" "\t" "<" "\tx" "float" ":\t%d\n", (ldouble)xfloat, ((1_w << std::numeric_limits<float>::digits) +1_w) < xfloat);
    printf("x" "float" "\t" "<" "\t%La " "+1_w" ":\t%d\n", (ldouble)xfloat, xfloat < ((1_w << std::numeric_limits<float>::digits) +1_w));
    printf("%La " "-1_w" "\t" ">" "\tx" "float" ":\t%d\n", (ldouble)xfloat, ((1_w << std::numeric_limits<float>::digits) -1_w) > xfloat);
    printf("x" "float" "\t" ">" "\t%La " "-1_w" ":\t%d\n", (ldouble)xfloat, xfloat > ((1_w << std::numeric_limits<float>::digits) -1_w));
    printf("%La " "+0_w" "\t" ">" "\tx" "float" ":\t%d\n", (ldouble)xfloat, ((1_w << std::numeric_limits<float>::digits) +0_w) > xfloat);
    printf("x" "float" "\t" ">" "\t%La " "+0_w" ":\t%d\n", (ldouble)xfloat, xfloat > ((1_w << std::numeric_limits<float>::digits) +0_w));
    printf("%La " "+1_w" "\t" ">" "\tx" "float" ":\t%d\n", (ldouble)xfloat, ((1_w << std::numeric_limits<float>::digits) +1_w) > xfloat);
    printf("x" "float" "\t" ">" "\t%La " "+1_w" ":\t%d\n", (ldouble)xfloat, xfloat > ((1_w << std::numeric_limits<float>::digits) +1_w));
    constexpr double xdouble = (1_w << std::numeric_limits<double>::digits) ;//+ 1_w;
    printf("%La " "-1_w" "\t" "==" "\tx" "double" ":\t%d\n", (ldouble)xdouble, ((1_w << std::numeric_limits<double>::digits) -1_w) == xdouble);
    printf("x" "double" "\t" "==" "\t%La " "-1_w" ":\t%d\n", (ldouble)xdouble, xdouble == ((1_w << std::numeric_limits<double>::digits) -1_w));
    printf("%La " "+0_w" "\t" "==" "\tx" "double" ":\t%d\n", (ldouble)xdouble, ((1_w << std::numeric_limits<double>::digits) +0_w) == xdouble);
    printf("x" "double" "\t" "==" "\t%La " "+0_w" ":\t%d\n", (ldouble)xdouble, xdouble == ((1_w << std::numeric_limits<double>::digits) +0_w));
    printf("%La " "+1_w" "\t" "==" "\tx" "double" ":\t%d\n", (ldouble)xdouble, ((1_w << std::numeric_limits<double>::digits) +1_w) == xdouble);
    printf("x" "double" "\t" "==" "\t%La " "+1_w" ":\t%d\n", (ldouble)xdouble, xdouble == ((1_w << std::numeric_limits<double>::digits) +1_w));
    printf("%La " "-1_w" "\t" "!=" "\tx" "double" ":\t%d\n", (ldouble)xdouble, ((1_w << std::numeric_limits<double>::digits) -1_w) != xdouble);
    printf("x" "double" "\t" "!=" "\t%La " "-1_w" ":\t%d\n", (ldouble)xdouble, xdouble != ((1_w << std::numeric_limits<double>::digits) -1_w));
    printf("%La " "+0_w" "\t" "!=" "\tx" "double" ":\t%d\n", (ldouble)xdouble, ((1_w << std::numeric_limits<double>::digits) +0_w) != xdouble);
    printf("x" "double" "\t" "!=" "\t%La " "+0_w" ":\t%d\n", (ldouble)xdouble, xdouble != ((1_w << std::numeric_limits<double>::digits) +0_w));
    printf("%La " "+1_w" "\t" "!=" "\tx" "double" ":\t%d\n", (ldouble)xdouble, ((1_w << std::numeric_limits<double>::digits) +1_w) != xdouble);
    printf("x" "double" "\t" "!=" "\t%La " "+1_w" ":\t%d\n", (ldouble)xdouble, xdouble != ((1_w << std::numeric_limits<double>::digits) +1_w));
    printf("%La " "-1_w" "\t" "<" "\tx" "double" ":\t%d\n", (ldouble)xdouble, ((1_w << std::numeric_limits<double>::digits) -1_w) < xdouble);
    printf("x" "double" "\t" "<" "\t%La " "-1_w" ":\t%d\n", (ldouble)xdouble, xdouble < ((1_w << std::numeric_limits<double>::digits) -1_w));
    printf("%La " "+0_w" "\t" "<" "\tx" "double" ":\t%d\n", (ldouble)xdouble, ((1_w << std::numeric_limits<double>::digits) +0_w) < xdouble);
    printf("x" "double" "\t" "<" "\t%La " "+0_w" ":\t%d\n", (ldouble)xdouble, xdouble < ((1_w << std::numeric_limits<double>::digits) +0_w));
    printf("%La " "+1_w" "\t" "<" "\tx" "double" ":\t%d\n", (ldouble)xdouble, ((1_w << std::numeric_limits<double>::digits) +1_w) < xdouble);
    printf("x" "double" "\t" "<" "\t%La " "+1_w" ":\t%d\n", (ldouble)xdouble, xdouble < ((1_w << std::numeric_limits<double>::digits) +1_w));
    printf("%La " "-1_w" "\t" ">" "\tx" "double" ":\t%d\n", (ldouble)xdouble, ((1_w << std::numeric_limits<double>::digits) -1_w) > xdouble);
    printf("x" "double" "\t" ">" "\t%La " "-1_w" ":\t%d\n", (ldouble)xdouble, xdouble > ((1_w << std::numeric_limits<double>::digits) -1_w));
    printf("%La " "+0_w" "\t" ">" "\tx" "double" ":\t%d\n", (ldouble)xdouble, ((1_w << std::numeric_limits<double>::digits) +0_w) > xdouble);
    printf("x" "double" "\t" ">" "\t%La " "+0_w" ":\t%d\n", (ldouble)xdouble, xdouble > ((1_w << std::numeric_limits<double>::digits) +0_w));
    printf("%La " "+1_w" "\t" ">" "\tx" "double" ":\t%d\n", (ldouble)xdouble, ((1_w << std::numeric_limits<double>::digits) +1_w) > xdouble);
    printf("x" "double" "\t" ">" "\t%La " "+1_w" ":\t%d\n", (ldouble)xdouble, xdouble > ((1_w << std::numeric_limits<double>::digits) +1_w));
    constexpr ldouble xldouble = (1_w << std::numeric_limits<ldouble>::digits) ;//+ 1_w;
    printf("%La " "-1_w" "\t" "==" "\tx" "ldouble" ":\t%d\n", (ldouble)xldouble, ((1_w << std::numeric_limits<ldouble>::digits) -1_w) == xldouble);
    printf("x" "ldouble" "\t" "==" "\t%La " "-1_w" ":\t%d\n", (ldouble)xldouble, xldouble == ((1_w << std::numeric_limits<ldouble>::digits) -1_w));
    printf("%La " "+0_w" "\t" "==" "\tx" "ldouble" ":\t%d\n", (ldouble)xldouble, ((1_w << std::numeric_limits<ldouble>::digits) +0_w) == xldouble);
    printf("x" "ldouble" "\t" "==" "\t%La " "+0_w" ":\t%d\n", (ldouble)xldouble, xldouble == ((1_w << std::numeric_limits<ldouble>::digits) +0_w));
    printf("%La " "+1_w" "\t" "==" "\tx" "ldouble" ":\t%d\n", (ldouble)xldouble, ((1_w << std::numeric_limits<ldouble>::digits) +1_w) == xldouble);
    printf("x" "ldouble" "\t" "==" "\t%La " "+1_w" ":\t%d\n", (ldouble)xldouble, xldouble == ((1_w << std::numeric_limits<ldouble>::digits) +1_w));
    printf("%La " "-1_w" "\t" "!=" "\tx" "ldouble" ":\t%d\n", (ldouble)xldouble, ((1_w << std::numeric_limits<ldouble>::digits) -1_w) != xldouble);
    printf("x" "ldouble" "\t" "!=" "\t%La " "-1_w" ":\t%d\n", (ldouble)xldouble, xldouble != ((1_w << std::numeric_limits<ldouble>::digits) -1_w));
    printf("%La " "+0_w" "\t" "!=" "\tx" "ldouble" ":\t%d\n", (ldouble)xldouble, ((1_w << std::numeric_limits<ldouble>::digits) +0_w) != xldouble);
    printf("x" "ldouble" "\t" "!=" "\t%La " "+0_w" ":\t%d\n", (ldouble)xldouble, xldouble != ((1_w << std::numeric_limits<ldouble>::digits) +0_w));
    printf("%La " "+1_w" "\t" "!=" "\tx" "ldouble" ":\t%d\n", (ldouble)xldouble, ((1_w << std::numeric_limits<ldouble>::digits) +1_w) != xldouble);
    printf("x" "ldouble" "\t" "!=" "\t%La " "+1_w" ":\t%d\n", (ldouble)xldouble, xldouble != ((1_w << std::numeric_limits<ldouble>::digits) +1_w));
    printf("%La " "-1_w" "\t" "<" "\tx" "ldouble" ":\t%d\n", (ldouble)xldouble, ((1_w << std::numeric_limits<ldouble>::digits) -1_w) < xldouble);
    printf("x" "ldouble" "\t" "<" "\t%La " "-1_w" ":\t%d\n", (ldouble)xldouble, xldouble < ((1_w << std::numeric_limits<ldouble>::digits) -1_w));
    printf("%La " "+0_w" "\t" "<" "\tx" "ldouble" ":\t%d\n", (ldouble)xldouble, ((1_w << std::numeric_limits<ldouble>::digits) +0_w) < xldouble);
    printf("x" "ldouble" "\t" "<" "\t%La " "+0_w" ":\t%d\n", (ldouble)xldouble, xldouble < ((1_w << std::numeric_limits<ldouble>::digits) +0_w));
    printf("%La " "+1_w" "\t" "<" "\tx" "ldouble" ":\t%d\n", (ldouble)xldouble, ((1_w << std::numeric_limits<ldouble>::digits) +1_w) < xldouble);
    printf("x" "ldouble" "\t" "<" "\t%La " "+1_w" ":\t%d\n", (ldouble)xldouble, xldouble < ((1_w << std::numeric_limits<ldouble>::digits) +1_w));
    printf("%La " "-1_w" "\t" ">" "\tx" "ldouble" ":\t%d\n", (ldouble)xldouble, ((1_w << std::numeric_limits<ldouble>::digits) -1_w) > xldouble);
    printf("x" "ldouble" "\t" ">" "\t%La " "-1_w" ":\t%d\n", (ldouble)xldouble, xldouble > ((1_w << std::numeric_limits<ldouble>::digits) -1_w));
    printf("%La " "+0_w" "\t" ">" "\tx" "ldouble" ":\t%d\n", (ldouble)xldouble, ((1_w << std::numeric_limits<ldouble>::digits) +0_w) > xldouble);
    printf("x" "ldouble" "\t" ">" "\t%La " "+0_w" ":\t%d\n", (ldouble)xldouble, xldouble > ((1_w << std::numeric_limits<ldouble>::digits) +0_w));
    printf("%La " "+1_w" "\t" ">" "\tx" "ldouble" ":\t%d\n", (ldouble)xldouble, ((1_w << std::numeric_limits<ldouble>::digits) +1_w) > xldouble);
    printf("x" "ldouble" "\t" ">" "\t%La " "+1_w" ":\t%d\n", (ldouble)xldouble, xldouble > ((1_w << std::numeric_limits<ldouble>::digits) +1_w));

    constexpr float yfloat = (1_w << std::numeric_limits<float>::digits);// + 1_w;
    constexpr double ydouble = (1_w << std::numeric_limits<double>::digits);// + 1_w;
    constexpr ldouble yldouble = (1_w << std::numeric_limits<ldouble>::digits);// + 1_w;
    const float h0 = 0x8.0p+21_w;
    printf("hex, %.30g %.30g\n", h0, 0x8.0p+21f);
    const float h1 = 0x8.0000000p+21_w;
    printf("hex, %.30g %.30g\n", h1, 0x8.0000000p+21f);
    const float h2 = intwide_t::k\u0259n\u02C8v\u025Dt("0x8.000008p+21");
    printf("hex, %.30g %.30g\n", h2, 0x8.000008p+21);
    const float hn0 = 0x8.0p-21_w;
    printf("hex-, %.30g %.30g\n", hn0, 0x8.0p-21f);
    const float hn1 = 0x8.0000000p-21_w;
    printf("hex-, %.30g %.30g\n", hn1, 0x8.0000000p-21f);
    const float hn2 = intwide_t::k\u0259n\u02C8v\u025Dt("0x8.000008p21");
    printf("hex-, %.30g %.30g\n", hn2, 0x8.000008p-21);
    const float d0 = 16777216.E0_w;
    printf("dec, %.30g %.30g\n", d0, 16777216.E0f);
    const float d1 = 16777216.000000E0_w;
    printf("dec, %.30g %.30g\n", d1, 16777216.000000E0f);
    const float d2 = intwide_t::k\u0259n\u02C8v\u025Dt("16777217.000000E0");
    printf("dec, %.30g %.30g\n", d2, 16777217.000000E0);
    const float b0 = intwide_t::k\u0259n\u02C8v\u025Dt("0b1000000000000000000000000.P0");
    printf("bin, %.30g %.30g\n", b0, 16777216.E0f);
    const float b1 = intwide_t::k\u0259n\u02C8v\u025Dt("0b1000000000000000000000000.000000P0");
    printf("bin, %.30g %.30g\n", b1, 16777216.000000E0f);
    const float b2 = intwide_t::k\u0259n\u02C8v\u025Dt("0b1000000000000000000000001P0");
    printf("bin, %.30g %.30g\n", b2, 16777217.000000E0);

    unsigned long long c0 = (uint64_t)intwide_t::k\u0259n\u02C8v\u025Dt("0x1.0000000000000000p+64");
    printf("conv64, %llu %.30g\n", c0, 0x1.0000000000000000p+64);
    unsigned long long c1 = (uint64_t)intwide_t::k\u0259n\u02C8v\u025Dt("0x0.FFFFFFFFFFFFFFFFp+63");
    printf("conv64, %llu %.30g\n", c1, 0x0.FFFFFFFFFFFFFFFFp+63);
    unsigned long long c2 = (uint64_t)intwide_t::k\u0259n\u02C8v\u025Dt("0x0.FFFFFFFFFFFFFFFFp+64");
    printf("conv64, %llu (%llu) %.30g\n", c2, 0xFFFFFFFFFFFFFFFF, 0x0.FFFFFFFFFFFFFFFFp+64);

    unsigned long long c3 = (uint64_t)intwide_t::k\u0259n\u02C8v\u025Dt("0.25E0");
    printf("conv64, %llu %.30g\n", c3, 0.25E0);
    unsigned long long c4 = (uint64_t)intwide_t::k\u0259n\u02C8v\u025Dt("0.25E1");
    printf("conv64, %llu %.30g\n", c4, 0.25E1);
    unsigned long long c5 = (uint64_t)intwide_t::k\u0259n\u02C8v\u025Dt("0.25E2");
    printf("conv64, %llu %.30g\n", c5, 0.25E2);

    constexpr float m0 = 0x11.P3_w*0x1.P-7_w;
    printf("mul, %.30g %.30Lg\n", m0, 0x11.P3L*0x1.P-7L);
    const float m1 = intwide_t::k\u0259n\u02C8v\u025Dt("0x0.FFFFFFFFFFFFp+23") * intwide_t::k\u0259n\u02C8v\u025Dt("0x0.Fp+29");
    printf("mul, %.30a %.30La\n", m1, 0x0.FFFFFFFFFFFFp+23L*0x0.Fp+23L);
    const float m2 = intwide_t::k\u0259n\u02C8v\u025Dt("0x0.1FFFFFFFFFFFFFFFFp+23") * intwide_t::k\u0259n\u02C8v\u025Dt("0x0.FFFFFFFFFFFFFFFFp+23");
    printf("mul, %.30a %.30La\n", m2, 0x0.FFFFFFFFFFFFp+23L*0x0.Fp+23L);

    const float div0 = 15.E2_w/20.E3_w;
    printf("div, %.30a %.30La\n", div0, 15.E2L/20.E3L);
    const float div1 = intwide_t::k\u0259n\u02C8v\u025Dt("0x0.1p+23") / intwide_t::k\u0259n\u02C8v\u025Dt("0x3.0P0");
    printf("div, %.30a %.30La\n", div1, 0x0.1p+23L/0x3.0P0L);
    const float div2 = intwide_t::k\u0259n\u02C8v\u025Dt("0x0.1FFFFFFFFFFFFFFFFp+23") / intwide_t::k\u0259n\u02C8v\u025Dt("0x0.FFFFFFFFFFFFFFFFp+23");
    printf("div, %.30a %.30La\n", div2, 0x0.FFFFFFFFFFFFp+23L/0x0.Fp+23L);

    (71.E-2_w) . printerror("71.E-2" " RHS");
    (71.E-2_w) . normalize() . printerror("71.E-2" " RHS, normalized");
    (0.71_w) . printerror("0.71" " LHS");
    (0.71_w) . normalize() . printerror("0.71" " LHS, normalized");
    printf("0.71" " " "==" " " "71.E-2" " : %s\n",  0.71_w == 71.E-2_w ? "true" : "false");
    printf("0.71" " " "!=" " " "71.E-2" " : %s\n",  0.71_w != 71.E-2_w ? "true" : "false");
    printf("0.71" " " "<" " " "71.E-2" " : %s\n",  0.71_w < 71.E-2_w ? "true" : "false");
    printf("0.71" " " "<=" " " "71.E-2" " : %s\n",  0.71_w <= 71.E-2_w ? "true" : "false");
    printf("0.71" " " ">" " " "71.E-2" " : %s\n",  0.71_w > 71.E-2_w ? "true" : "false");
    printf("0.71" " " ">=" " " "71.E-2" " : %s\n",  0.71_w >= 71.E-2_w ? "true" : "false");
    (0.709999_w) . printerror("0.709999" " LHS");
    (0.709999_w) . normalize() . printerror("0.709999" " LHS, normalized");
    printf("0.709999" " " "==" " " "71.E-2" " : %s\n",  0.709999_w == 71.E-2_w ? "true" : "false");
    printf("0.709999" " " "!=" " " "71.E-2" " : %s\n",  0.709999_w != 71.E-2_w ? "true" : "false");
    printf("0.709999" " " "<" " " "71.E-2" " : %s\n",  0.709999_w < 71.E-2_w ? "true" : "false");
    printf("0.709999" " " "<=" " " "71.E-2" " : %s\n",  0.709999_w <= 71.E-2_w ? "true" : "false");
    printf("0.709999" " " ">" " " "71.E-2" " : %s\n",  0.709999_w > 71.E-2_w ? "true" : "false");
    printf("0.709999" " " ">=" " " "71.E-2" " : %s\n",  0.709999_w >= 71.E-2_w ? "true" : "false");
    (0_w) . printerror("0" " LHS");
    (0_w) . normalize() . printerror("0" " LHS, normalized");
    printf("0" " " "==" " " "71.E-2" " : %s\n",  0_w == 71.E-2_w ? "true" : "false");
    printf("0" " " "!=" " " "71.E-2" " : %s\n",  0_w != 71.E-2_w ? "true" : "false");
    printf("0" " " "<" " " "71.E-2" " : %s\n",  0_w < 71.E-2_w ? "true" : "false");
    printf("0" " " "<=" " " "71.E-2" " : %s\n",  0_w <= 71.E-2_w ? "true" : "false");
    printf("0" " " ">" " " "71.E-2" " : %s\n",  0_w > 71.E-2_w ? "true" : "false");
    printf("0" " " ">=" " " "71.E-2" " : %s\n",  0_w >= 71.E-2_w ? "true" : "false");
    (1_w) . printerror("1" " LHS");
    (1_w) . normalize() . printerror("1" " LHS, normalized");
    printf("1" " " "==" " " "71.E-2" " : %s\n",  1_w == 71.E-2_w ? "true" : "false");
    printf("1" " " "!=" " " "71.E-2" " : %s\n",  1_w != 71.E-2_w ? "true" : "false");
    printf("1" " " "<" " " "71.E-2" " : %s\n",  1_w < 71.E-2_w ? "true" : "false");
    printf("1" " " "<=" " " "71.E-2" " : %s\n",  1_w <= 71.E-2_w ? "true" : "false");
    printf("1" " " ">" " " "71.E-2" " : %s\n",  1_w > 71.E-2_w ? "true" : "false");
    printf("1" " " ">=" " " "71.E-2" " : %s\n",  1_w >= 71.E-2_w ? "true" : "false");
    (1.0_w) . printerror("1.0" " LHS");
    (1.0_w) . normalize() . printerror("1.0" " LHS, normalized");
    printf("1.0" " " "==" " " "71.E-2" " : %s\n",  1.0_w == 71.E-2_w ? "true" : "false");
    printf("1.0" " " "!=" " " "71.E-2" " : %s\n",  1.0_w != 71.E-2_w ? "true" : "false");
    printf("1.0" " " "<" " " "71.E-2" " : %s\n",  1.0_w < 71.E-2_w ? "true" : "false");
    printf("1.0" " " "<=" " " "71.E-2" " : %s\n",  1.0_w <= 71.E-2_w ? "true" : "false");
    printf("1.0" " " ">" " " "71.E-2" " : %s\n",  1.0_w > 71.E-2_w ? "true" : "false");
    printf("1.0" " " ">=" " " "71.E-2" " : %s\n",  1.0_w >= 71.E-2_w ? "true" : "false");
    (-1_w) . printerror("-1" " LHS");
    (-1_w) . normalize() . printerror("-1" " LHS, normalized");
    printf("-1" " " "==" " " "71.E-2" " : %s\n",  -1_w == 71.E-2_w ? "true" : "false");
    printf("-1" " " "!=" " " "71.E-2" " : %s\n",  -1_w != 71.E-2_w ? "true" : "false");
    printf("-1" " " "<" " " "71.E-2" " : %s\n",  -1_w < 71.E-2_w ? "true" : "false");
    printf("-1" " " "<=" " " "71.E-2" " : %s\n",  -1_w <= 71.E-2_w ? "true" : "false");
    printf("-1" " " ">" " " "71.E-2" " : %s\n",  -1_w > 71.E-2_w ? "true" : "false");
    printf("-1" " " ">=" " " "71.E-2" " : %s\n",  -1_w >= 71.E-2_w ? "true" : "false");
    (-1.0_w) . printerror("-1.0" " LHS");
    (-1.0_w) . normalize() . printerror("-1.0" " LHS, normalized");
    printf("-1.0" " " "==" " " "71.E-2" " : %s\n",  -1.0_w == 71.E-2_w ? "true" : "false");
    printf("-1.0" " " "!=" " " "71.E-2" " : %s\n",  -1.0_w != 71.E-2_w ? "true" : "false");
    printf("-1.0" " " "<" " " "71.E-2" " : %s\n",  -1.0_w < 71.E-2_w ? "true" : "false");
    printf("-1.0" " " "<=" " " "71.E-2" " : %s\n",  -1.0_w <= 71.E-2_w ? "true" : "false");
    printf("-1.0" " " ">" " " "71.E-2" " : %s\n",  -1.0_w > 71.E-2_w ? "true" : "false");
    printf("-1.0" " " ">=" " " "71.E-2" " : %s\n",  -1.0_w >= 71.E-2_w ? "true" : "false");
    (-0.71_w) . printerror("-0.71" " LHS");
    (-0.71_w) . normalize() . printerror("-0.71" " LHS, normalized");
    printf("-0.71" " " "==" " " "71.E-2" " : %s\n",  -0.71_w == 71.E-2_w ? "true" : "false");
    printf("-0.71" " " "!=" " " "71.E-2" " : %s\n",  -0.71_w != 71.E-2_w ? "true" : "false");
    printf("-0.71" " " "<" " " "71.E-2" " : %s\n",  -0.71_w < 71.E-2_w ? "true" : "false");
    printf("-0.71" " " "<=" " " "71.E-2" " : %s\n",  -0.71_w <= 71.E-2_w ? "true" : "false");
    printf("-0.71" " " ">" " " "71.E-2" " : %s\n",  -0.71_w > 71.E-2_w ? "true" : "false");
    printf("-0.71" " " ">=" " " "71.E-2" " : %s\n",  -0.71_w >= 71.E-2_w ? "true" : "false");
    (-71.E-2_w) . printerror("-71.E-2" " RHS");
    (-71.E-2_w) . normalize() . printerror("-71.E-2" " RHS, normalized");
    (0.71_w) . printerror("0.71" " LHS");
    (0.71_w) . normalize() . printerror("0.71" " LHS, normalized");
    printf("0.71" " " "==" " " "-71.E-2" " : %s\n",  0.71_w == -71.E-2_w ? "true" : "false");
    printf("0.71" " " "!=" " " "-71.E-2" " : %s\n",  0.71_w != -71.E-2_w ? "true" : "false");
    printf("0.71" " " "<" " " "-71.E-2" " : %s\n",  0.71_w < -71.E-2_w ? "true" : "false");
    printf("0.71" " " "<=" " " "-71.E-2" " : %s\n",  0.71_w <= -71.E-2_w ? "true" : "false");
    printf("0.71" " " ">" " " "-71.E-2" " : %s\n",  0.71_w > -71.E-2_w ? "true" : "false");
    printf("0.71" " " ">=" " " "-71.E-2" " : %s\n",  0.71_w >= -71.E-2_w ? "true" : "false");
    (0.709999_w) . printerror("0.709999" " LHS");
    (0.709999_w) . normalize() . printerror("0.709999" " LHS, normalized");
    printf("0.709999" " " "==" " " "-71.E-2" " : %s\n",  0.709999_w == -71.E-2_w ? "true" : "false");
    printf("0.709999" " " "!=" " " "-71.E-2" " : %s\n",  0.709999_w != -71.E-2_w ? "true" : "false");
    printf("0.709999" " " "<" " " "-71.E-2" " : %s\n",  0.709999_w < -71.E-2_w ? "true" : "false");
    printf("0.709999" " " "<=" " " "-71.E-2" " : %s\n",  0.709999_w <= -71.E-2_w ? "true" : "false");
    printf("0.709999" " " ">" " " "-71.E-2" " : %s\n",  0.709999_w > -71.E-2_w ? "true" : "false");
    printf("0.709999" " " ">=" " " "-71.E-2" " : %s\n",  0.709999_w >= -71.E-2_w ? "true" : "false");
    (0_w) . printerror("0" " LHS");
    (0_w) . normalize() . printerror("0" " LHS, normalized");
    printf("0" " " "==" " " "-71.E-2" " : %s\n",  0_w == -71.E-2_w ? "true" : "false");
    printf("0" " " "!=" " " "-71.E-2" " : %s\n",  0_w != -71.E-2_w ? "true" : "false");
    printf("0" " " "<" " " "-71.E-2" " : %s\n",  0_w < -71.E-2_w ? "true" : "false");
    printf("0" " " "<=" " " "-71.E-2" " : %s\n",  0_w <= -71.E-2_w ? "true" : "false");
    printf("0" " " ">" " " "-71.E-2" " : %s\n",  0_w > -71.E-2_w ? "true" : "false");
    printf("0" " " ">=" " " "-71.E-2" " : %s\n",  0_w >= -71.E-2_w ? "true" : "false");
    (1_w) . printerror("1" " LHS");
    (1_w) . normalize() . printerror("1" " LHS, normalized");
    printf("1" " " "==" " " "-71.E-2" " : %s\n",  1_w == -71.E-2_w ? "true" : "false");
    printf("1" " " "!=" " " "-71.E-2" " : %s\n",  1_w != -71.E-2_w ? "true" : "false");
    printf("1" " " "<" " " "-71.E-2" " : %s\n",  1_w < -71.E-2_w ? "true" : "false");
    printf("1" " " "<=" " " "-71.E-2" " : %s\n",  1_w <= -71.E-2_w ? "true" : "false");
    printf("1" " " ">" " " "-71.E-2" " : %s\n",  1_w > -71.E-2_w ? "true" : "false");
    printf("1" " " ">=" " " "-71.E-2" " : %s\n",  1_w >= -71.E-2_w ? "true" : "false");
    (1.0_w) . printerror("1.0" " LHS");
    (1.0_w) . normalize() . printerror("1.0" " LHS, normalized");
    printf("1.0" " " "==" " " "-71.E-2" " : %s\n",  1.0_w == -71.E-2_w ? "true" : "false");
    printf("1.0" " " "!=" " " "-71.E-2" " : %s\n",  1.0_w != -71.E-2_w ? "true" : "false");
    printf("1.0" " " "<" " " "-71.E-2" " : %s\n",  1.0_w < -71.E-2_w ? "true" : "false");
    printf("1.0" " " "<=" " " "-71.E-2" " : %s\n",  1.0_w <= -71.E-2_w ? "true" : "false");
    printf("1.0" " " ">" " " "-71.E-2" " : %s\n",  1.0_w > -71.E-2_w ? "true" : "false");
    printf("1.0" " " ">=" " " "-71.E-2" " : %s\n",  1.0_w >= -71.E-2_w ? "true" : "false");
    (-1_w) . printerror("-1" " LHS");
    (-1_w) . normalize() . printerror("-1" " LHS, normalized");
    printf("-1" " " "==" " " "-71.E-2" " : %s\n",  -1_w == -71.E-2_w ? "true" : "false");
    printf("-1" " " "!=" " " "-71.E-2" " : %s\n",  -1_w != -71.E-2_w ? "true" : "false");
    printf("-1" " " "<" " " "-71.E-2" " : %s\n",  -1_w < -71.E-2_w ? "true" : "false");
    printf("-1" " " "<=" " " "-71.E-2" " : %s\n",  -1_w <= -71.E-2_w ? "true" : "false");
    printf("-1" " " ">" " " "-71.E-2" " : %s\n",  -1_w > -71.E-2_w ? "true" : "false");
    printf("-1" " " ">=" " " "-71.E-2" " : %s\n",  -1_w >= -71.E-2_w ? "true" : "false");
    (-1.0_w) . printerror("-1.0" " LHS");
    (-1.0_w) . normalize() . printerror("-1.0" " LHS, normalized");
    printf("-1.0" " " "==" " " "-71.E-2" " : %s\n",  -1.0_w == -71.E-2_w ? "true" : "false");
    printf("-1.0" " " "!=" " " "-71.E-2" " : %s\n",  -1.0_w != -71.E-2_w ? "true" : "false");
    printf("-1.0" " " "<" " " "-71.E-2" " : %s\n",  -1.0_w < -71.E-2_w ? "true" : "false");
    printf("-1.0" " " "<=" " " "-71.E-2" " : %s\n",  -1.0_w <= -71.E-2_w ? "true" : "false");
    printf("-1.0" " " ">" " " "-71.E-2" " : %s\n",  -1.0_w > -71.E-2_w ? "true" : "false");
    printf("-1.0" " " ">=" " " "-71.E-2" " : %s\n",  -1.0_w >= -71.E-2_w ? "true" : "false");
    (-0.71_w) . printerror("-0.71" " LHS");
    (-0.71_w) . normalize() . printerror("-0.71" " LHS, normalized");
    printf("-0.71" " " "==" " " "-71.E-2" " : %s\n",  -0.71_w == -71.E-2_w ? "true" : "false");
    printf("-0.71" " " "!=" " " "-71.E-2" " : %s\n",  -0.71_w != -71.E-2_w ? "true" : "false");
    printf("-0.71" " " "<" " " "-71.E-2" " : %s\n",  -0.71_w < -71.E-2_w ? "true" : "false");
    printf("-0.71" " " "<=" " " "-71.E-2" " : %s\n",  -0.71_w <= -71.E-2_w ? "true" : "false");
    printf("-0.71" " " ">" " " "-71.E-2" " : %s\n",  -0.71_w > -71.E-2_w ? "true" : "false");
    printf("-0.71" " " ">=" " " "-71.E-2" " : %s\n",  -0.71_w >= -71.E-2_w ? "true" : "false");

    intwide_t::epsilon< 10 >() . printerror("\u03B5\u2081\u2080");
    intwide_t::epsilon< 10 >() . normalize() . printerror("\u03B5\u2081\u2080, normalized");
    intwide_t::epsilon< 2 >() . printerror("\u03B5\u2082");
    intwide_t::epsilon< 2 >() . normalize() . printerror("\u03B5\u2082, normalized");


    (1_w + intwide_t::epsilon< 10 >()) . printerror("1+\u03B5\u2081\u2080");
    (1_w + intwide_t::epsilon< 10 >()) . normalize() . printerror("1+\u03B5\u2081\u2080, normalized");
    (1_w + intwide_t::epsilon< 2 >()) . printerror("1+\u03B5\u2082");
    (1_w + intwide_t::epsilon< 2 >()) . normalize() . printerror("1+\u03B5\u2082, normalized");

    printf("1+\u03B5\u2081\u2080 as double: %.32g, DBL_EPSILON is %.32g\n",  (double) (1_w + intwide_t::epsilon< 10 >()), std::numeric_limits< double >::epsilon());
    printf("1+\u03B5\u2082 as double: %.32g\n",  (double) (1_w + intwide_t::epsilon< 2 >()));

    (intwide_t::epsilon< 10 >()*0.1_w) . printerror("\u03B5\u2081\u2080/10, should work");
    (intwide_t::epsilon< 2 >()*0x1.0P-1_w) . printerror("\u00BD\u03B5\u2082, should work");

    (intwide_t::epsilon< 10 >()/10_w) . printerror("\u03B5\u2081\u2080/10, should work");
    (intwide_t::epsilon< 2 >()/2_w) . printerror("\u00BD\u03B5\u2082, should work");

    (10.0_w/5.0_w) . printerror("10./5., should work");

    (1_w + intwide_t::epsilon< 10 >()/10_w) . printerror("1+\u03B5\u2081\u2080/10, should fail");
    (1_w + intwide_t::epsilon< 2 >()/2_w) . printerror("1+\u00BD\u03B5\u2082, should fail");


    intwide_t::\u03C0 . printerror("\u03C0");
    printf("\u03C0"" is %La\n", trunc< ldouble >(intwide_t::\u03C0));
    printf("\u03C0"" is %.21Le\n", trunc< ldouble >(intwide_t::\u03C0));
    intwide_t::e . printerror("e");
    printf("e"" is %La\n", trunc< ldouble >(intwide_t::e));
    printf("e"" is %.21Le\n", trunc< ldouble >(intwide_t::e));
    intwide_t::\u03B3 . printerror("\u03B3");
    printf("\u03B3"" is %La\n", trunc< ldouble >(intwide_t::\u03B3));
    printf("\u03B3"" is %.21Le\n", trunc< ldouble >(intwide_t::\u03B3));
    intwide_t::\u03C6 . printerror("\u03C6");
    printf("\u03C6"" is %La\n", trunc< ldouble >(intwide_t::\u03C6));
    printf("\u03C6"" is %.21Le\n", trunc< ldouble >(intwide_t::\u03C6));
    intwide_t::Yi . printerror("Yi");
    printf("Yi"" is %La\n", trunc< ldouble >(intwide_t::Yi));
    printf("Yi"" is %.21Le\n", trunc< ldouble >(intwide_t::Yi));
    intwide_t::Zi . printerror("Zi");
    printf("Zi"" is %La\n", trunc< ldouble >(intwide_t::Zi));
    printf("Zi"" is %.21Le\n", trunc< ldouble >(intwide_t::Zi));
    intwide_t::Ei . printerror("Ei");
    printf("Ei"" is %La\n", trunc< ldouble >(intwide_t::Ei));
    printf("Ei"" is %.21Le\n", trunc< ldouble >(intwide_t::Ei));
    intwide_t::Pi . printerror("Pi");
    printf("Pi"" is %La\n", trunc< ldouble >(intwide_t::Pi));
    printf("Pi"" is %.21Le\n", trunc< ldouble >(intwide_t::Pi));
    intwide_t::Ti . printerror("Ti");
    printf("Ti"" is %La\n", trunc< ldouble >(intwide_t::Ti));
    printf("Ti"" is %.21Le\n", trunc< ldouble >(intwide_t::Ti));
    intwide_t::Gi . printerror("Gi");
    printf("Gi"" is %La\n", trunc< ldouble >(intwide_t::Gi));
    printf("Gi"" is %.21Le\n", trunc< ldouble >(intwide_t::Gi));
    intwide_t::Mi . printerror("Mi");
    printf("Mi"" is %La\n", trunc< ldouble >(intwide_t::Mi));
    printf("Mi"" is %.21Le\n", trunc< ldouble >(intwide_t::Mi));
    intwide_t::Ki . printerror("Ki");
    printf("Ki"" is %La\n", trunc< ldouble >(intwide_t::Ki));
    printf("Ki"" is %.21Le\n", trunc< ldouble >(intwide_t::Ki));
    intwide_t::Y . printerror("Y");
    printf("Y"" is %La\n", trunc< ldouble >(intwide_t::Y));
    printf("Y"" is %.21Le\n", trunc< ldouble >(intwide_t::Y));
    intwide_t::Z . printerror("Z");
    printf("Z"" is %La\n", trunc< ldouble >(intwide_t::Z));
    printf("Z"" is %.21Le\n", trunc< ldouble >(intwide_t::Z));
    intwide_t::E . printerror("E");
    printf("E"" is %La\n", trunc< ldouble >(intwide_t::E));
    printf("E"" is %.21Le\n", trunc< ldouble >(intwide_t::E));
    intwide_t::P . printerror("P");
    printf("P"" is %La\n", trunc< ldouble >(intwide_t::P));
    printf("P"" is %.21Le\n", trunc< ldouble >(intwide_t::P));
    intwide_t::T . printerror("T");
    printf("T"" is %La\n", trunc< ldouble >(intwide_t::T));
    printf("T"" is %.21Le\n", trunc< ldouble >(intwide_t::T));
    intwide_t::G . printerror("G");
    printf("G"" is %La\n", trunc< ldouble >(intwide_t::G));
    printf("G"" is %.21Le\n", trunc< ldouble >(intwide_t::G));
    intwide_t::M . printerror("M");
    printf("M"" is %La\n", trunc< ldouble >(intwide_t::M));
    printf("M"" is %.21Le\n", trunc< ldouble >(intwide_t::M));
    intwide_t::k . printerror("k");
    printf("k"" is %La\n", trunc< ldouble >(intwide_t::k));
    printf("k"" is %.21Le\n", trunc< ldouble >(intwide_t::k));
    intwide_t::h . printerror("h");
    printf("h"" is %La\n", trunc< ldouble >(intwide_t::h));
    printf("h"" is %.21Le\n", trunc< ldouble >(intwide_t::h));
    intwide_t::da . printerror("da");
    printf("da"" is %La\n", trunc< ldouble >(intwide_t::da));
    printf("da"" is %.21Le\n", trunc< ldouble >(intwide_t::da));
    intwide_t::zero . printerror("zero");
    printf("zero"" is %La\n", trunc< ldouble >(intwide_t::zero));
    printf("zero"" is %.21Le\n", trunc< ldouble >(intwide_t::zero));
    intwide_t::d . printerror("d");
    printf("d"" is %La\n", trunc< ldouble >(intwide_t::d));
    printf("d"" is %.21Le\n", trunc< ldouble >(intwide_t::d));
    intwide_t::c . printerror("c");
    printf("c"" is %La\n", trunc< ldouble >(intwide_t::c));
    printf("c"" is %.21Le\n", trunc< ldouble >(intwide_t::c));
    intwide_t::m . printerror("m");
    printf("m"" is %La\n", trunc< ldouble >(intwide_t::m));
    printf("m"" is %.21Le\n", trunc< ldouble >(intwide_t::m));
    intwide_t::\u03BC . printerror("\u03BC");
    printf("\u03BC"" is %La\n", trunc< ldouble >(intwide_t::\u03BC));
    printf("\u03BC"" is %.21Le\n", trunc< ldouble >(intwide_t::\u03BC));
    intwide_t::n . printerror("n");
    printf("n"" is %La\n", trunc< ldouble >(intwide_t::n));
    printf("n"" is %.21Le\n", trunc< ldouble >(intwide_t::n));
    intwide_t::p . printerror("p");
    printf("p"" is %La\n", trunc< ldouble >(intwide_t::p));
    printf("p"" is %.21Le\n", trunc< ldouble >(intwide_t::p));
    intwide_t::f . printerror("f");
    printf("f"" is %La\n", trunc< ldouble >(intwide_t::f));
    printf("f"" is %.21Le\n", trunc< ldouble >(intwide_t::f));
    intwide_t::a . printerror("a");
    printf("a"" is %La\n", trunc< ldouble >(intwide_t::a));
    printf("a"" is %.21Le\n", trunc< ldouble >(intwide_t::a));
    intwide_t::z . printerror("z");
    printf("z"" is %La\n", trunc< ldouble >(intwide_t::z));
    printf("z"" is %.21Le\n", trunc< ldouble >(intwide_t::z));
    intwide_t::y . printerror("y");
    printf("y"" is %La\n", trunc< ldouble >(intwide_t::y));
    printf("y"" is %.21Le\n", trunc< ldouble >(intwide_t::y));

    inverse(+2) . printerror("inverse of a power of two, " "+""2");
    (+2_w * inverse(+2)) . printerror("testing inverse of a power of two, " "+""2");
    printf("test for " "+""2" " is %.21Le\n", trunc< ldouble >((+2_w * inverse(+2))));
    inverse(-2) . printerror("inverse of a power of two, " "-""2");
    (-2_w * inverse(-2)) . printerror("testing inverse of a power of two, " "-""2");
    printf("test for " "-""2" " is %.21Le\n", trunc< ldouble >((-2_w * inverse(-2))));
    inverse(+4) . printerror("inverse of a power of two, " "+""4");
    (+4_w * inverse(+4)) . printerror("testing inverse of a power of two, " "+""4");
    printf("test for " "+""4" " is %.21Le\n", trunc< ldouble >((+4_w * inverse(+4))));
    inverse(-4) . printerror("inverse of a power of two, " "-""4");
    (-4_w * inverse(-4)) . printerror("testing inverse of a power of two, " "-""4");
    printf("test for " "-""4" " is %.21Le\n", trunc< ldouble >((-4_w * inverse(-4))));
    inverse(+8) . printerror("inverse of a power of two, " "+""8");
    (+8_w * inverse(+8)) . printerror("testing inverse of a power of two, " "+""8");
    printf("test for " "+""8" " is %.21Le\n", trunc< ldouble >((+8_w * inverse(+8))));
    inverse(-8) . printerror("inverse of a power of two, " "-""8");
    (-8_w * inverse(-8)) . printerror("testing inverse of a power of two, " "-""8");
    printf("test for " "-""8" " is %.21Le\n", trunc< ldouble >((-8_w * inverse(-8))));
    inverse(+16) . printerror("inverse of a power of two, " "+""16");
    (+16_w * inverse(+16)) . printerror("testing inverse of a power of two, " "+""16");
    printf("test for " "+""16" " is %.21Le\n", trunc< ldouble >((+16_w * inverse(+16))));
    inverse(-16) . printerror("inverse of a power of two, " "-""16");
    (-16_w * inverse(-16)) . printerror("testing inverse of a power of two, " "-""16");
    printf("test for " "-""16" " is %.21Le\n", trunc< ldouble >((-16_w * inverse(-16))));
    inverse(+32) . printerror("inverse of a power of two, " "+""32");
    (+32_w * inverse(+32)) . printerror("testing inverse of a power of two, " "+""32");
    printf("test for " "+""32" " is %.21Le\n", trunc< ldouble >((+32_w * inverse(+32))));
    inverse(-32) . printerror("inverse of a power of two, " "-""32");
    (-32_w * inverse(-32)) . printerror("testing inverse of a power of two, " "-""32");
    printf("test for " "-""32" " is %.21Le\n", trunc< ldouble >((-32_w * inverse(-32))));
    inverse(+64) . printerror("inverse of a power of two, " "+""64");
    (+64_w * inverse(+64)) . printerror("testing inverse of a power of two, " "+""64");
    printf("test for " "+""64" " is %.21Le\n", trunc< ldouble >((+64_w * inverse(+64))));
    inverse(-64) . printerror("inverse of a power of two, " "-""64");
    (-64_w * inverse(-64)) . printerror("testing inverse of a power of two, " "-""64");
    printf("test for " "-""64" " is %.21Le\n", trunc< ldouble >((-64_w * inverse(-64))));
    inverse(+128) . printerror("inverse of a power of two, " "+""128");
    (+128_w * inverse(+128)) . printerror("testing inverse of a power of two, " "+""128");
    printf("test for " "+""128" " is %.21Le\n", trunc< ldouble >((+128_w * inverse(+128))));
    inverse(-128) . printerror("inverse of a power of two, " "-""128");
    (-128_w * inverse(-128)) . printerror("testing inverse of a power of two, " "-""128");
    printf("test for " "-""128" " is %.21Le\n", trunc< ldouble >((-128_w * inverse(-128))));
    inverse(+256) . printerror("inverse of a power of two, " "+""256");
    (+256_w * inverse(+256)) . printerror("testing inverse of a power of two, " "+""256");
    printf("test for " "+""256" " is %.21Le\n", trunc< ldouble >((+256_w * inverse(+256))));
    inverse(-256) . printerror("inverse of a power of two, " "-""256");
    (-256_w * inverse(-256)) . printerror("testing inverse of a power of two, " "-""256");
    printf("test for " "-""256" " is %.21Le\n", trunc< ldouble >((-256_w * inverse(-256))));
    inverse(+512) . printerror("inverse of a power of two, " "+""512");
    (+512_w * inverse(+512)) . printerror("testing inverse of a power of two, " "+""512");
    printf("test for " "+""512" " is %.21Le\n", trunc< ldouble >((+512_w * inverse(+512))));
    inverse(-512) . printerror("inverse of a power of two, " "-""512");
    (-512_w * inverse(-512)) . printerror("testing inverse of a power of two, " "-""512");
    printf("test for " "-""512" " is %.21Le\n", trunc< ldouble >((-512_w * inverse(-512))));
    inverse(+1024) . printerror("inverse of a power of two, " "+""1024");
    (+1024_w * inverse(+1024)) . printerror("testing inverse of a power of two, " "+""1024");
    printf("test for " "+""1024" " is %.21Le\n", trunc< ldouble >((+1024_w * inverse(+1024))));
    inverse(-1024) . printerror("inverse of a power of two, " "-""1024");
    (-1024_w * inverse(-1024)) . printerror("testing inverse of a power of two, " "-""1024");
    printf("test for " "-""1024" " is %.21Le\n", trunc< ldouble >((-1024_w * inverse(-1024))));
    inverse(+2048) . printerror("inverse of a power of two, " "+""2048");
    (+2048_w * inverse(+2048)) . printerror("testing inverse of a power of two, " "+""2048");
    printf("test for " "+""2048" " is %.21Le\n", trunc< ldouble >((+2048_w * inverse(+2048))));
    inverse(-2048) . printerror("inverse of a power of two, " "-""2048");
    (-2048_w * inverse(-2048)) . printerror("testing inverse of a power of two, " "-""2048");
    printf("test for " "-""2048" " is %.21Le\n", trunc< ldouble >((-2048_w * inverse(-2048))));
    inverse(+4096) . printerror("inverse of a power of two, " "+""4096");
    (+4096_w * inverse(+4096)) . printerror("testing inverse of a power of two, " "+""4096");
    printf("test for " "+""4096" " is %.21Le\n", trunc< ldouble >((+4096_w * inverse(+4096))));
    inverse(-4096) . printerror("inverse of a power of two, " "-""4096");
    (-4096_w * inverse(-4096)) . printerror("testing inverse of a power of two, " "-""4096");
    printf("test for " "-""4096" " is %.21Le\n", trunc< ldouble >((-4096_w * inverse(-4096))));
    inverse(+8192) . printerror("inverse of a power of two, " "+""8192");
    (+8192_w * inverse(+8192)) . printerror("testing inverse of a power of two, " "+""8192");
    printf("test for " "+""8192" " is %.21Le\n", trunc< ldouble >((+8192_w * inverse(+8192))));
    inverse(-8192) . printerror("inverse of a power of two, " "-""8192");
    (-8192_w * inverse(-8192)) . printerror("testing inverse of a power of two, " "-""8192");
    printf("test for " "-""8192" " is %.21Le\n", trunc< ldouble >((-8192_w * inverse(-8192))));
    inverse(+16384) . printerror("inverse of a power of two, " "+""16384");
    (+16384_w * inverse(+16384)) . printerror("testing inverse of a power of two, " "+""16384");
    printf("test for " "+""16384" " is %.21Le\n", trunc< ldouble >((+16384_w * inverse(+16384))));
    inverse(-16384) . printerror("inverse of a power of two, " "-""16384");
    (-16384_w * inverse(-16384)) . printerror("testing inverse of a power of two, " "-""16384");
    printf("test for " "-""16384" " is %.21Le\n", trunc< ldouble >((-16384_w * inverse(-16384))));
    inverse(+32768) . printerror("inverse of a power of two, " "+""32768");
    (+32768_w * inverse(+32768)) . printerror("testing inverse of a power of two, " "+""32768");
    printf("test for " "+""32768" " is %.21Le\n", trunc< ldouble >((+32768_w * inverse(+32768))));
    inverse(-32768) . printerror("inverse of a power of two, " "-""32768");
    (-32768_w * inverse(-32768)) . printerror("testing inverse of a power of two, " "-""32768");
    printf("test for " "-""32768" " is %.21Le\n", trunc< ldouble >((-32768_w * inverse(-32768))));
    inverse(+65536) . printerror("inverse of a power of two, " "+""65536");
    (+65536_w * inverse(+65536)) . printerror("testing inverse of a power of two, " "+""65536");
    printf("test for " "+""65536" " is %.21Le\n", trunc< ldouble >((+65536_w * inverse(+65536))));
    inverse(-65536) . printerror("inverse of a power of two, " "-""65536");
    (-65536_w * inverse(-65536)) . printerror("testing inverse of a power of two, " "-""65536");
    printf("test for " "-""65536" " is %.21Le\n", trunc< ldouble >((-65536_w * inverse(-65536))));
    inverse(+131072) . printerror("inverse of a power of two, " "+""131072");
    (+131072_w * inverse(+131072)) . printerror("testing inverse of a power of two, " "+""131072");
    printf("test for " "+""131072" " is %.21Le\n", trunc< ldouble >((+131072_w * inverse(+131072))));
    inverse(-131072) . printerror("inverse of a power of two, " "-""131072");
    (-131072_w * inverse(-131072)) . printerror("testing inverse of a power of two, " "-""131072");
    printf("test for " "-""131072" " is %.21Le\n", trunc< ldouble >((-131072_w * inverse(-131072))));
    inverse(+262144) . printerror("inverse of a power of two, " "+""262144");
    (+262144_w * inverse(+262144)) . printerror("testing inverse of a power of two, " "+""262144");
    printf("test for " "+""262144" " is %.21Le\n", trunc< ldouble >((+262144_w * inverse(+262144))));
    inverse(-262144) . printerror("inverse of a power of two, " "-""262144");
    (-262144_w * inverse(-262144)) . printerror("testing inverse of a power of two, " "-""262144");
    printf("test for " "-""262144" " is %.21Le\n", trunc< ldouble >((-262144_w * inverse(-262144))));
    inverse(+524288) . printerror("inverse of a power of two, " "+""524288");
    (+524288_w * inverse(+524288)) . printerror("testing inverse of a power of two, " "+""524288");
    printf("test for " "+""524288" " is %.21Le\n", trunc< ldouble >((+524288_w * inverse(+524288))));
    inverse(-524288) . printerror("inverse of a power of two, " "-""524288");
    (-524288_w * inverse(-524288)) . printerror("testing inverse of a power of two, " "-""524288");
    printf("test for " "-""524288" " is %.21Le\n", trunc< ldouble >((-524288_w * inverse(-524288))));
    inverse(+1048576) . printerror("inverse of a power of two, " "+""1048576");
    (+1048576_w * inverse(+1048576)) . printerror("testing inverse of a power of two, " "+""1048576");
    printf("test for " "+""1048576" " is %.21Le\n", trunc< ldouble >((+1048576_w * inverse(+1048576))));
    inverse(-1048576) . printerror("inverse of a power of two, " "-""1048576");
    (-1048576_w * inverse(-1048576)) . printerror("testing inverse of a power of two, " "-""1048576");
    printf("test for " "-""1048576" " is %.21Le\n", trunc< ldouble >((-1048576_w * inverse(-1048576))));


}
