#include "core-math.h"
#include <string.h>
#include <inttypes.h>
#include <uchar.h>

constexpr char* _Core_strcpy(char* t, char const* s) {
  while (*s) {
    *t = *s;
    ++t;
    ++s;
  }
  *t = 0;
  return t;
}


enum {
#pragma CMOD amend foreach F:I = sign bin alt oct hex up array shift
      totext_##${F} = (1 ⌫ ${I}),
#pragma CMOD done
      totext_char  = totext_bin ∪ totext_hex, // 0x12
      totext_base  = totext_char∪ totext_oct, // 0x1A
      totext_exp   = totext_bin ∪ totext_hex, // 0x12
      totext_fixed = totext_oct ∪ totext_hex, // 0x18
      totext_point = 0x1F,                    // 0x1F
      // for integer values
      totext_b = totext_bin,                  // 0x02
      totext_o = totext_oct,                  // 0x08
      totext_d = totext_b ∪ totext_o,         // 0x0A
      totext_x = totext_hex,                  // 0x10
      totext_X = totext_hex ∪ totext_up,      // 0x20
      totext_c = totext_char,                 // 0x12
      totext_integer = totext_d ∪ totext_X,   // 0x3A
      // for pointer values
      totext_s = totext_char,                 // 0x12
      totext_p = totext_point,                // 0x1F
      // for floating values
      totext_a = totext_hex,                  // 0x10
      totext_e = totext_exp,                  // 0x12
      totext_f = totext_fixed,                // 0x18
      totext_g = totext_exp ∪ totext_fixed,   // 0x00
      totext_A = totext_a ∪ totext_up,        // 0x20
      totext_E = totext_e ∪ totext_up,        // 0x22
      totext_F = totext_f ∪ totext_up,        // 0x28
      totext_G = totext_g ∪ totext_up,        // 0x20
      totext_floating = totext_G,             // 0x20
};

// generic_type(x) is pointer :
// char* and no base                                         → string
// char* and base                                            → array of characters printed in that base
// integer pointer, totext_char                              → string or wide-string
// totext_point                                              → always represent address
// array of integer pointer, totext_char                     → array of string or wide-string
// array of scalar type                                      → array of scalar
// pointer                                                   → print address

#define totext_precision(X) (((X)+1u)*(unsigned)totext_shift)
#define totext_getprec(X) (signed)(((X)/totext_shift)-1)

template< size_t N, typename T > struct totext_array_struct{ T data[N]; };


#define totext_array_firstX(X, …) X
#define totext_array_first(…) totext_array_firstX(__VA_ARGS__, )
#define totext_array_size(…) (sizeof((generic_type(totext_array_first(__VA_ARGS__))[]){ __VA_ARGS__ })/sizeof(generic_type(totext_array_first(__VA_ARGS__))))

#define totext_tmp(...)  (((totext_array_struct< totext_array_size(__VA_ARGS__), generic_type(totext_array_first(__VA_ARGS__)) >){ __VA_ARGS__ }).data)

#define totext_array(N, X)  (*((generic_type(*(X))(*)[(N)])(X)))

struct ahs {
  bool vla;
  bool array;
  bool pointer;
  bool unpoint;
  size_t aize;
  size_t size;
  signed precision;
  unsigned BFP;
  char const* expr;
  char text[64];
  constexpr ahs(void) : vla(), array(), pointer(), unpoint(), aize(), size(), precision(), BFP(), expr(), text() { }

  constexpr char* setflags(char* p) {
    p[0] = '%';
    ++p;
    if (BFP ∩ totext_sign) {
      p[0] = '+';
      ++p;
    }
    if (BFP ∩ totext_alt) {
      p[0] = '#';
      ++p;
    }
    if (precision ≥ 0) {
      p[0] = '.';
      ++p;
      p[0] = '*';
      ++p;
    }
    return p;
  }

  /**
   ** @brief Fall back constructor for types that are not specialized.
   **
   ** This is used in particular for wide-characters and alike,
   ** because on some platforms these might not be captured by a
   ** specialization.
   **/
  template< typename _T0 >
  constexpr ahs(_T0 x, unsigned bfp) : vla(), array(), pointer(false), unpoint(), aize(), size(sizeof(_T0)), precision(totext_getprec(bfp)), BFP(bfp), expr(), text() {
    if (issame(x, (char8_t)0) ∨ issame(x, (wchar_t)0) ∨ issame(x, (char16_t)0) ∨ issame(x, (char32_t)0)) {
      char* p = text;
      p = setflags(p);
      if (issame(x, (wchar_t)0)) {
        p[0] = 'l';
        ++p;
        p[0] = 'c';
        ++p;
      } else if (issame(x, (char8_t)0)) {
        /* nothing */
      } else {
        if (issame(x, (char16_t)0)) {
          p = _Core_strcpy(p, PRIu16);
        } else if (issame(x, (char32_t)0)) {
          p = _Core_strcpy(p, PRIu32);
        }
        --p;
        switch (bfp & totext_integer) {
        default: p[0] = 'd';
        case 10: p[0] = 'u'; break;
        case 8: p[0] = 'o'; break;
        case 16: p[0] = 'x'; break;
        case 16 ∪ totext_up: p[0] = 'X'; break;
        }
        ++p;
      }
      p[0] = 0;
    } else {
      char* p = text;
      p = _Core_strcpy(p, "(unknown)");
      p[0] = 0;
    }
  }

  /**
   ** @brief Fall back constructor for pointed-to types that are not
   ** specialized.
   **
   ** This is used in particular for wide-strings and alike, because
   ** on some platforms these might not be captured by a
   ** specialization.
   **/
  template< typename _T0 >
  constexpr ahs(_T0* x, unsigned bfp) : vla(), array(), pointer(true), unpoint(), aize(), size(sizeof(_T0)), precision(totext_getprec(bfp)), BFP(bfp), expr(), text() {
    if (issame(x, (wchar_t*)0) ∨ issame(x, (wchar_t const*)0)) {
      char* p = text;
      p = setflags(p);
      p[0] = 'l';
      ++p;
      if ((bfp ∩ totext_array) ≡ totext_array) {
        p[0] = 'c';
        ++p;
        p[0] = '!';
      } else {
        p[0] = 's';
        ++p;
        p[0] = 0;
        unpoint = true;
      }
      ++p;
      p[0] = 0;
    } else if (issame(x, (char8_t*)0) ∨ issame(x, (char8_t const*)0)) {
      char* p = text;
      p = setflags(p);
      switch (bfp & totext_integer) {
      default: p[0] = 'c';
      case 10: p[0] = 'u'; break;
      case 8: p[0] = 'o'; break;
      case 16: p[0] = 'x'; break;
      case 16 ∪ totext_up: p[0] = 'X'; break;
      }
      ++p;
      p[0] = 0;
    } else if (issame(x, (char16_t*)0) ∨ issame(x, (char16_t const*)0)) {
      if ((¬issame((uint_least16_t){}, (char16_t*){}) ∧ ¬(bfp ∩ totext_base)) ∨ ((bfp ∩ totext_char) ≡ totext_char)) {
        unpoint = true;
      } else {
        char* p = text;
        p = setflags(p);
        p = _Core_strcpy(p, PRIu16);
        --p;
        switch (bfp & totext_integer) {
        default:;
        case 8: p[0] = 'o'; break;
        case 16: p[0] = 'x'; break;
        case 16 ∪ totext_up: p[0] = 'X'; break;
        }
        ++p;
        p[0] = 0;
      }
    } else if (issame(x, (char32_t*)0) ∨ issame(x, (char32_t const*)0)) {
      if ((¬issame((uint_least32_t){}, (char32_t){}) ∧ ¬(bfp ∩ totext_base)) ∨ ((bfp ∩ totext_char) ≡ totext_char)) {
        unpoint = true;
      } else {
        char* p = text;
        p = setflags(p);
        p = _Core_strcpy(p, PRIu32);
        --p;
        switch (bfp & totext_integer) {
        default:;
        case 8: p[0] = 'o'; break;
        case 16: p[0] = 'x'; break;
        case 16 ∪ totext_up: p[0] = 'X'; break;
        }
        ++p;
        p[0] = 0;
      }
    } else {
      _T0 y = {};
      ahs tmp(y, bfp);
      char* p = text;
      p = _Core_strcpy(p, tmp.text);
    }
  }

#pragma CMOD amend foreach T = float double long\ double
  /**
   ** @brief floating point
   **/
  constexpr ahs(${T} x, unsigned bfp) : vla(), array(), pointer(false), unpoint(), aize(), size(sizeof(${T})), precision(totext_getprec(bfp)), BFP(bfp), expr(), text() {
    char* p = text;
    p = setflags(p);
    if (issame(x, 0.0L)) {
      p[0] = 'L';
      ++p;
    }
    switch (bfp & (totext_floating)) {
    default: p[0] = 'g'; break;
    case totext_a: p[0] = 'a'; break;
    case totext_e: p[0] = 'e'; break;
    case totext_f: p[0] = 'f'; break;
    case totext_A: p[0] = 'A'; break;
    case totext_E: p[0] = 'E'; break;
    case totext_F: p[0] = 'F'; break;
    case totext_G: p[0] = 'G'; break;
    case totext_up: p[0] = 'G'; break;
    case 10 ∪ totext_up: p[0] = 'G'; break;
    }
    ++p;
    p[0] = 0;
  }

  constexpr ahs(complex_type(${T}) x, unsigned bfp) : vla(), array(), pointer(false), unpoint(), aize(), size(sizeof(${T})), precision(totext_getprec(bfp)), BFP(bfp), expr(), text() {
    ahs tmp(real_value(x), bfp);
    char const* save = expr;
    *this = tmp;
    expr = save;
    aize = sizeof(complex_type(${T}));
    BFP = bfp;
  }
#pragma CMOD done

#pragma CMOD amend foreach T =                                          \
  unsigned\ char unsigned\ short unsigned unsigned\ long unsigned\ long\ long \
  signed\ char signed\ short signed long long\ long
  /**
   ** @brief integer types
   **/
  constexpr ahs(${T} x, unsigned bfp) : vla(), array(), pointer(false), unpoint(), aize(), size(sizeof(${T})), precision(totext_getprec(bfp)), BFP(bfp), expr(), text() {
    char* p = text;
    p = setflags(p);
    switch (sizeof x) {
    case 1:
      p[0] = 'h';
      ++p;
    case sizeof(short):
      p[0] = 'h';
      ++p;
      break;
    case sizeof(long long):
      p[0] = 'l';
      ++p;
#if LONG_MAX != LLONG_MAX
    case sizeof(long):
#endif
      p[0] = 'l';
      ++p;
      break;
    default: break;
    }
    switch (bfp & totext_integer) {
    default:;
    case 10: p[0] = (issigned(x) ? 'd' : 'u'); break;
    case 8: p[0] = 'o'; break;
    case 16: p[0] = 'x'; break;
    case 16 ∪ totext_up: p[0] = 'X'; break;
    }
    ++p;
    p[0] = 0;
  }
#pragma CMOD done

  /**
   ** @brief a single character
   **
   ** If there is a base, this is printed as if it where `unsigned char`.
   **/
  constexpr ahs(char x, unsigned bfp) : vla(), array(), pointer(false), unpoint(), aize(), size(sizeof(char)), precision(totext_getprec(bfp)), BFP(bfp), expr(), text() {
    if (¬(bfp ∩ (totext_base))) {
      precision = -1;
      char* p = text;
      p[0] = '%';
      ++p;
      p[0] = 'c';
      ++p;
      p[0] = 0;
    } else {
      *this = ahs((unsigned char)x, bfp);
    }
  }

  /**
   ** @brief a single character
   **
   ** If there is a base, this is printed as if it where `unsigned char`.
   **/
  constexpr ahs(bool x, unsigned bfp) : vla(), array(), pointer(false), unpoint(), aize(), size(sizeof(bool)), precision(totext_getprec(bfp)), BFP(bfp), expr(), text() {
    if ((bfp ∩ (totext_char)) ≡ totext_char) {
      char* p = text;
      p = setflags(p);
      _Core_strcpy(p, "s");
    } else {
      *this = ahs((unsigned char)x, bfp);
    }
  }

  /**
   ** @brief A fall back for double pointers.
   **/
  template< typename _T1 >
  constexpr ahs(_T1** x, unsigned bfp) : vla(), array(), pointer(true), unpoint(), aize(), size(sizeof(_T1*)), precision(totext_getprec(bfp)), BFP(bfp), expr(), text() {
    precision = -1;
    char* p = text;
    p[0] = '%';
    ++p;
    p[0] = 'p';
    ++p;
    p[0] = 0;
  }

#pragma CMOD amend foreach T = void void\ const void\ volatile void\ const\ volatile
  /**
   ** @brief `void` pointers have lost all other information
   **/
  constexpr ahs(${T}* x, unsigned bfp) : vla(), array(), pointer(true), unpoint(), aize(), size(sizeof(void*)), precision(totext_getprec(bfp)), BFP(bfp), expr(), text() {
    precision = -1;
    char* p = text;
    p[0] = '%';
    ++p;
    p[0] = 'p';
    ++p;
    p[0] = 0;
  }
#pragma CMOD done

  template< typename _T0 >
  constexpr ahs(bool v, bool a, size_t size, _T0 x, unsigned bfp, char const* ex) : vla(), array(), pointer(), unpoint(), aize(), size(), precision(), BFP(), expr(), text() {
    ahs tmp(x, bfp & ~totext_array);
    tmp.aize = sizeof(x);
    tmp.BFP = bfp;
    tmp.expr = ex;
    *this = tmp;
    return;
  }

  /**
   ** @brief The main dispatcher for pointer values.
   **/
  template< typename _T0 >
  constexpr ahs(bool v, bool a, size_t siz, _T0* x, unsigned bfp, char const* ex) : vla(v), array(a), pointer(true), unpoint(), aize(siz), size(sizeof(x)), precision(totext_getprec(bfp)), BFP(bfp), expr(ex), text() {
    char* p = text;
    /* Force this address to be interpreted as pointer value. */
    if ((bfp ∩ totext_point) ≡ totext_point) {
      array = false;
      precision = -1;
      p[0] = '%';
      ++p;
      p[0] = 'p';
      ++p;
      p[0] = 0;
      return;
    }
    if (¬(bfp ∩ totext_base)) {
      /* first handle plain strings */
      if (issame(x, (char*)0) ∨ issame(x, (const char*)0) ∨ issame(x, (char8_t*)0) ∨ issame(x, (const char8_t*)0)) {
        array = false;
        p = setflags(p);
        p[0] = 's';
        ++p;
        p[0] = 0;
        return;
      } else if (issame(x, (char16_t*)0) ∨ issame(x, (const char16_t*)0) ∨ issame(x, (char32_t*)0) ∨ issame(x, (const char32_t*)0)) {
        return;
        /* then handle plain wide-strings */
      } else if ((bfp ∩ totext_char) ≡ totext_char) {
        array = false;
        p = setflags(p);
        p[0] = 'l';
        ++p;
        p[0] = 's';
        ++p;
        p[0] = 0;
        return;
      }
    }
    /* Now see if this is an array. */
    if (vla ∨ (siz ≠ sizeof(x)) ∨ array ∨ (bfp ∩ totext_array)) {
      ahs tmp(x, bfp);
      BFP = bfp;
      p = _Core_strcpy(p, tmp.text);
      aize = siz;
      size = sizeof(*x);
      if (text[1]) {
        array = ¬tmp.unpoint;
        return;
      }
    }
    /* Everything else is just a pointer */
    precision = -1;
    p[0] = '%';
    ++p;
    p[0] = 'p';
    ++p;
    p[0] = 0;
    return;
  }

  /**
   ** @brief The main dispatcher for pointer-to-pointer values.
   **
   ** Basically, this looks if this is an array of pointers, and if so prints these.
   **
   ** The special case is an array of pointers to strings.
   **/
  template< typename _T0 >
  constexpr ahs(bool v, bool a, size_t siz, _T0** x, unsigned bfp, char const* ex) : vla(v), array(a), pointer(true), unpoint(), aize(siz), size(sizeof(x)), precision(totext_getprec(bfp)), BFP(bfp), expr(ex), text() {
    char* p = text;
    if ((bfp ∩ totext_point) ≡ totext_point) {
      array = false;
      precision = -1;
      p[0] = '%';
      ++p;
      p[0] = 'p';
      ++p;
      p[0] = 0;
      return;
    }
    if (vla ∨ (siz ≠ sizeof(x)) ∨ array) {
      if ((bfp ∩ totext_char) ≡ totext_char) {
        if (issame(x, (char**)0) ∨ issame(x, (const char**)0) ∨ issame(x, (char8_t**)0) ∨ issame(x, (const char8_t**)0)) {
          array = true;
          p = setflags(p);
          p[0] = 's';
          ++p;
          p[0] = 0;
          return;
        } else if (issame(x, (char16_t**)0) ∨ issame(x, (const char16_t**)0)) {
          array = true;
          return;
        } else if (issame(x, (char32_t**)0) ∨ issame(x, (const char32_t**)0)) {
          array = true;
          return;
        } else  {
          array = true;
          p = setflags(p);
          p[0] = 'l';
          ++p;
          p[0] = 's';
          ++p;
          p[0] = 0;
          return;
        }
      }
      ahs tmp(x, bfp);
      BFP = bfp;
      p = _Core_strcpy(p, tmp.text);
      aize = siz;
      size = sizeof(*x);
      if (text[1]) {
        array = true;
        return;
      }
      array = ‼(bfp ∩ totext_array);
    }
    precision = -1;
    p[0] = '%';
    ++p;
    p[0] = 'p';
    ++p;
    p[0] = 0;
    return;
  }

#pragma CMOD amend foreach T = void void\ const void\ volatile void\ const\ volatile
  constexpr ahs(bool v, bool a, size_t siz, ${T}* x, unsigned bfp, char const* ex) : vla(v), array(a), pointer(true), unpoint(), aize(), size(sizeof(${T}*)), precision(totext_getprec(bfp)), BFP(bfp), expr(ex), text() {
    precision = -1;
    char* p = text;
    p[0] = '%';
    ++p;
    p[0] = 'p';
    ++p;
    p[0] = 0;
  }
#pragma CMOD done

  constexpr ahs(bool v, bool a, size_t siz, std∷nullptr_t x, unsigned bfp, char const* ex) : vla(v), array(a), pointer(true), unpoint(), aize(), size(sizeof(std∷nullptr_t)), precision(totext_getprec(bfp)), BFP(bfp), expr(ex), text() {
    precision = -1;
    char* p = text;
    p[0] = '%';
    ++p;
    p[0] = 'p';
    ++p;
    p[0] = 0;
  }

};



#define _Core_AHS(X, BFP) ahs(isvla(X), isarray(X), isvla(X) ? 0 : sizeof(X), (generic_type(X)){}, BFP, "[" #X ", " #BFP "]")

void print_ahs(size_t i, ahs x) {
     printf("%zu\t%zu\t%zu\t%#x,\t%d\t%d\t%s\t(%d)\t%s\n", i, x.size, x.aize, x.BFP, x.array, x.array, x.text, x.precision, x.expr);
}

#define print_array(X)                                          \
do {                                                            \
  for (size_t i = 0; i < sizeof((X))/sizeof((X)[0]); ++i) {     \
    print_ahs(i, (X)[i]);                                       \
  }                                                             \
 } while(false)

template< typename _T0 >
constexpr char const* _Core_bool_string(_T0 x) {
  return "<invalid>";
}

constexpr char const* _Core_bool_string(bool x) {
  return x ? "true" : "false";
}

template< typename T >
constexpr size_t _Core_binary (size_t n, char s[], T x, int prec, bool alt, bool up) { return SIZE_MAX; }

#pragma CMOD amend foreach T char short int long long\ long
constexpr size_t _Core_binary (size_t n, char s[], unsigned ${T} x, int prec, bool alt, bool up) {
  if (¬prec) prec = 1;
  constexpr int bits = (CHAR_BIT × sizeof x);
  constexpr auto half = tohighest(x)/2;
  size_t bit = bits;
  for (; bit; --bit) {
    if ((bits ≤ prec) ∨ (x > half)) break;
    x ⌫= 1;
  }
  if (alt) n -= 2;
  if (bit < n) {
    if (alt) s = _Core_strcpy(s, (up ? "0B" : "0b"));
    n = bit;
    for (; bit; --bit) {
      s[0] = (x > half) ? '1' : '0';
      ++s;
      x ⌫= 1;
    }
    s[0] = 0;
  } else {
    n = bit;
  }
  return n;
}

constexpr size_t _Core_binary (size_t n, char s[], signed ${T} x, int prec, bool alt, bool up) {
  return _Core_binary(n, s, tounsigned(x), prec, alt, up);
}
#pragma CMOD done

template< typename T >
size_t _Core_unicode (size_t n, char s[], T x, int prec) { return SIZE_MAX; }

#define _Core_u_char16_t c16rtomb
#define _Core_u_char32_t c32rtomb

#pragma CMOD amend foreach T char16_t char32_t
size_t _Core_unicode (size_t n, char s[], ${T} const* x, int prec) {
  size_t remaining = prec;
  size_t ret = 0;
  mbstate_t state = {};
  char buffer[64];
  if (¬n) {
    while (remaining) {
      size_t r = _Core_u_##${T}(buffer, *x, &state);
      // Stop when an encoding error occured ...
      if (r ≡ SIZE_MAX) break;
      // ... or when we encountered the terminating null character ...
      if (¬x[0]) {
        // check if we had to write a terminating shift state at the end
        if (r > 1) ret += (r-1);
        break;
      }
      ret += r;
      --remaining;
      ++x;
    }
  } else {
    while (remaining ∧ n) {
      size_t r = _Core_u_##${T}(buffer, x[0], &state);
      if (r ≡ SIZE_MAX) {
        s[0] = 0;
        break;
      }
      if (n ≤ r) {
        // Not enough space, emergency messures, try to write a
        // terminating shift state and null character.
        r = _Core_u_##${T}(buffer, 0, &state);
        if (n ≤ r) {
          s[0] = 0;
        } else {
          memcpy(s, buffer, r);
          if (r > 1) ret += (r-1);
        }
        break;
      } else {
        memcpy(s, buffer, r);
      }
      // We stop when we encountered the terminating null character.
      if (¬x[0]) {
        // check if we had to write a terminating shift state at the end
        if (r > 1) ret += (r-1);
        break;
      }
      // advance the output by r
      s += r;
      ret += r;
      n -= r;
      // advance the input by 1
      ++x;
      --remaining;
    }
  }
  return ret;
}
#pragma CMOD done


#pragma CMOD amend oneline
#define totext_pointers
[](size_t n, char s[], auto x, ahs spec, char const* glue)
{
  size_t ret = 0;
  memset(s, 0, n);
  if (spec.array) {
    if (¬glue) {
      if (¬spec.BFP ∨ ((spec.BFP ∩ totext_base) ≡ 10) ∨ (spec.BFP ∩ totext_alt)) {
        glue = "\t";
      }
    }
    const size_t len = spec.aize/spec.size;
    const size_t glen = glue ? (strlen)(glue) : 0;
    if (¬spec.text[0] ∧ (issame(*x, (char16_t){}) ∧ (¬issame((uint_least16_t){}, (char16_t){}) ∨ (spec.BFP ∩ totext_char)))) {
      ret += _Core_unicode(n, s, x, spec.precision);
    } else if (¬spec.text[0] ∧ (issame(*x, (char32_t){}) ∧ (¬issame((uint_least32_t){}, (char32_t){}) ∨ (spec.BFP ∩ totext_char)))) {
      ret += _Core_unicode(n, s, x, spec.precision);
    } else {
      for (size_t i = 0; i < len; ++i) {
        if (i ∧ glen) {
          if (glen ≥ n) break;
          memcpy(s, glue, glen);
          s += glen;
          n -= glen;
        }
        size_t l = 0;
        if (issame(*x, (bool)0) ∧ (spec.BFP ∩ totext_char)) {
          char const* str = _Core_bool_string(x[i]);
          if (spec.precision ≥ 0) {
            l = snprintf(s, n, spec.text, spec.precision, str);
          } else {
            l = snprintf(s, n, spec.text, str);
          }
        } else if (¬spec.text[0] ∧ ((issame(*x, (char16_t*){}) ∨ issame(*x, (char16_t const*){})) ∧ (¬issame((uint_least16_t){}, (char16_t){}) ∨ (spec.BFP ∩ totext_char)))) {
          l = _Core_unicode(n, s, x[i], spec.precision);
        } else if (¬spec.text[0] ∧ ((issame(*x, (char32_t*){}) ∨ issame(*x, (char32_t const*){})) ∧ (¬issame((uint_least32_t){}, (char32_t){}) ∨ (spec.BFP ∩ totext_char)))) {
          l = _Core_unicode(n, s, x[i], spec.precision);
        } else {
          if (spec.precision ≥ 0) {
            l = snprintf(s, n, spec.text, spec.precision, x[i]);
          } else {
            l = snprintf(s, n, spec.text, x[i]);
          }
        }
        if (n) {
          if (l ≥ n) {
            s[0] = 0;
            break;
          }
          n -= l;
          /*printf("iteration %zu address %p %p [%s]\n", i, (void*)s, (void*)&x[i], orig);*/
          s += l;
        }
        ret += l;
      }
    }
  } else if (spec.pointer) {
      if (spec.precision ≥ 0) {
        ret = snprintf(s, n, spec.text, spec.precision, (void*)x);
      } else {
        ret = snprintf(s, n, spec.text, (void*)x);
      }
  } else {
      if (spec.precision ≥ 0) {
        ret = snprintf(s, n, spec.text, spec.precision, *x);
      } else {
        ret = snprintf(s, n, spec.text, *x);
      }
  }
  return ret;
}
#pragma CMOD done

#pragma CMOD amend oneline
#define totext_single
[](size_t n, char s[], auto x, ahs spec)
{
  size_t ret = 0;
  memset(s, 0, n);
  if (issame(x, (bool)0) ∧ (spec.BFP ∩ totext_char)) {
    char const* str = x ? "true" : "false";
    if (spec.precision ≥ 0) {
      ret = snprintf(s, n, spec.text, spec.precision, str);
    } else {
      ret = snprintf(s, n, spec.text, str);
    }
  } else if (isinteger(x) ∧ (spec.BFP ∩ totext_base) ≡ 2) {
    ret = _Core_binary(n, s, x, spec.precision, (spec.BFP ∩ totext_alt), (spec.BFP ∩ totext_up));
  } else {
    if (spec.precision ≥ 0) {
      ret = snprintf(s, n, spec.text, spec.precision, x);
    } else {
      ret = snprintf(s, n, spec.text, x);
    }
  }
  return ret;
}
#pragma CMOD done

#pragma CMOD amend generic_selection _Core_tester(isnonvoidpointer(X)) totext_dispatch N S X BFP G
_Core_false: totext_single(N, S, X, BFP)
  default:  totext_pointers(N, S, X, BFP, G)
#pragma CMOD done

// totext has at least 3 arguments
#define totext(N, S, ...) totext3(N, S, __VA_ARGS__)

// These are augmented to at least 6, with zeros and an empty one.
#define totext3(N, S, ...) totext6(N, S, __VA_ARGS__, 0, 0, )

// And these 6 are then reduced to the 5 that we really need.  Here a
// zero may not be taken well as fifth argument, because we are
// expecting a pointer, here, to we transform ICE of value zero to a
// nullptr.
#define totext6(N, S, X, BFP, G, ...) totext_dispatch((N), (S), (X), _Core_AHS(X, BFP), tonullptr(G))

// should have at least one arguments
#define strlen(...) strlen1(__VA_ARGS__)

// add the two default argumenst
#define strlen1(...) strlen3(__VA_ARGS__, 0, 0, )

#define strlen3(X, BFP, G, …) totext_dispatch(0, nullptr, (X),  _Core_AHS(X, BFP), tonullptr(G))

#pragma CMOD amend oneline
#define totext_strdup3(X, bfp, G, ...)
[](auto x, ahs fl, char const* g){
  char* ret = nullptr;
  size_t n = totext_dispatch(0, nullptr, x, fl, g) + 1;
  if (n) {                    /* no error occurred */
    ret = (char*)malloc(n);
    if (ret) {
      n = totext_dispatch(n, ret, x, fl, g);
    }
  }
  return ret;
}((X), _Core_AHS((X), (bfp)), tonullptr(G))
#pragma CMOD done


// should have at least one arguments
#define strdup(...) totext_strdup1(__VA_ARGS__)

// add the two default argumenst
#define totext_strdup1(...) totext_strdup3(__VA_ARGS__, 0, 0, )


#pragma CMOD amend oneline
#define totext_strndup4(X, N, bfp, G, ...)
[](auto x, size_t n, ahs fl, char const* g){
  char* ret = nullptr;
  if (n+1) {
    ret = (char*)malloc(n+1);
    if (ret) {
      size_t m = totext_dispatch(n, ret, x, fl, g);
      if (m < n/2) {
        char* net = (char*)realloc(ret, m+1);
        if (net) ret = net;
      } else if (m ≡ SIZE_MAX) {
        free(ret);
        ret = nullptr;
      }
    }
  }
  return ret;
}((X), (N), _Core_AHS((X), (bfp)), tonullptr(G))
#pragma CMOD done


// should have at least two arguments
#define strndup(X, ...) totext_strndup2(X, __VA_ARGS__)

// add the two default argumenst
#define totext_strndup2(X, ...) totext_strndup4(X, __VA_ARGS__, 0, 0, )



#pragma CMOD amend oneline
#define totext_puts3(X, bfp, G, ...)
[](auto x, ahs fl, char const* g){
  char totext_buffer[64];
  char* buf = totext_buffer;
  size_t ret = totext_dispatch(sizeof totext_buffer, buf, x, fl, g) + 1;
  if (ret ∧ (ret > sizeof totext_buffer)) {
    buf = (char*)malloc(ret);
    ret = buf
      ? totext_dispatch(ret, buf, x, fl, g) + 1
      : 0;
  }
  if (ret) {
    puts(buf);
    if (ret > sizeof totext_buffer)
      free(buf);
  }
  return ret ? ret : EOF;
}((X), _Core_AHS((X), (bfp)), tonullptr(G))
#pragma CMOD done

// should have at least one arguments
#define puts(...) totext_puts1(__VA_ARGS__)

// add the two default argumenst
#define totext_puts1(...) totext_puts3(__VA_ARGS__, 0, 0, )

#pragma CMOD amend oneline
#define totext_fputs4(X, STREAM, bfp, G, ...)
[](auto x, FILE* stream, ahs fl, char const* g){
  char totext_buffer[64];
  char* buf = totext_buffer;
  size_t ret = totext_dispatch(sizeof totext_buffer, buf, x, fl, g) + 1;
  if (ret ∧ (ret > sizeof totext_buffer)) {
    buf = (char*)malloc(ret);
    ret = buf
      ? totext_dispatch(ret, buf, x, fl, g) + 1
      : 0;
  }
  if (ret) {
    fputs(buf, stream);
    if (ret > sizeof totext_buffer)
      free(buf);
  }
  return ret ? ret : EOF;
 }((X), (STREAM), _Core_AHS((X), (bfp)), tonullptr(G))
#pragma CMOD done

// should have at least two arguments
#define fputs(X, ...) totext_fputs2(X, __VA_ARGS__)

// add the two default argumenst
#define totext_fputs2(X, ...) totext_fputs4(X, __VA_ARGS__, 0, 0, )

int main(int argc, char* argv[]) {
  setlocale(LC_ALL, "");
  size_t fla[3] = {};
  for (int i = 0; i < argc; ++i)
    fla[i] = i;
  size_t vla[argc];
  char fs[] = "test";
  char vs[argc+5];
  strcpy(vs, fs);
  for (int i = 0; i < argc; ++i)
    vla[i] = i;
  puts("++++++++++++ arrays ++++++++++");
  puts(fla);
  puts(vla);
  puts(fs);
  puts(vs);
  puts("++++++++++++ arrays as pointers ++++++++++");
  puts(fla, totext_point);
  puts(vla, totext_point);
  puts(fs, totext_point);
  puts(vs, totext_point);
  puts("++++++++++++ arrays of pointers ++++++++++");
  puts(totext_tmp(fla, vla));
  puts(totext_tmp(fs, vs));
  puts(totext_tmp(fs, vs), totext_point);
  puts("++++++++++++ arrays of text ++++++++++");
  puts(totext_tmp(fs, vs), totext_char);
  puts(totext_tmp(L"eins", L"zwei"));
  puts(totext_tmp(L"eins", L"zwei"), totext_point);
  puts(totext_tmp(L"eins", L"zwei"), totext_char, "→");
  puts("++++++++++++ float ++++++++++");
  puts(1.0F);
  puts(2.0F);
  puts(3.0F);
  puts(4.0F);
  puts(5.0F);
  puts(6.0F, 16);
  puts(7.0F, 10);
  puts(8.0F, totext_exp);
  puts(9.0F, totext_fixed);
  puts(totext_tmp( 10.0F ));
  puts(totext_tmp( 11.0F ), totext_point);
  puts(totext_tmp( 12.0F, 0.0F ));
  puts("++++++++++++ double ++++++++++");
  puts(1.0);
  puts(2.0, 16);
  puts(3.0, 10);
  puts(4.0, totext_exp ∪ totext_precision(12));
  puts(5.0, totext_fixed);
  puts(totext_tmp( 6.0 ));
  puts(totext_tmp( 7.0 ), totext_point);
  puts(totext_tmp( 8.0, 0.0 ));
  puts("++++++++++++ long double ++++++++++");
  puts(1.0L);
  puts(2.0L, 16);
  puts(3.0L, 10);
  puts(4.0L, totext_exp);
  puts(5.0L, totext_fixed);
  puts(totext_tmp( 6.0L ));
  puts(totext_tmp( 7.0L ), totext_point);
  puts(totext_tmp( 8.0L, 9.0L ));
  puts("++++++++++++ float, capitalized ++++++++++");
  puts(1.0F, 0 ∪ totext_up);
  puts(2.0F, 16 ∪ totext_up);
  puts(3.0F, 10 ∪ totext_up);
  puts(4.0F, totext_exp ∪ totext_up);
  puts(5.0F, totext_fixed ∪ totext_up);
  puts(totext_tmp( 6.0F ), 0 ∪ totext_up);
  puts(totext_tmp( 8.0F ), totext_point ∪ totext_up);
  puts(totext_tmp( 9.0F, 0.0F ), 0 ∪ totext_up);
  puts("++++++++++++ double, capitalized ++++++++++");
  puts(10.0, 0 ∪ totext_up);
  puts(11.0, 16 ∪ totext_up);
  puts(12.0, 10 ∪ totext_up);
  puts(13.0, totext_exp ∪ totext_up);
  puts(14.0, totext_fixed ∪ totext_up);
  puts(totext_tmp( 15.0 ), 0 ∪ totext_up);
  puts(totext_tmp( 17.0 ), totext_point ∪ totext_up);
  puts(totext_tmp( 18.0, 0.0 ), 0 ∪ totext_up);
  puts("++++++++++++ long double, capitalized ++++++++++");
  puts(19.0L, 0 ∪ totext_up);
  puts(20.0L, 16 ∪ totext_up);
  puts(21.0L, 10 ∪ totext_up);
  puts(22.0L, totext_exp ∪ totext_up);
  puts(23.0L, totext_fixed ∪ totext_up);
  puts(totext_tmp( 24.0L ), 0 ∪ totext_up);
  puts(totext_tmp( 26.0L ), totext_point ∪ totext_up);
  puts(totext_tmp( 27.0L, 0.0L ), 0 ∪ totext_up);
  puts("++++++++++++ short ++++++++++");
  puts((short)28);
  puts((short)29, 16);
  puts((short)30, 10);
  puts((short)31, 8);
  puts((short)32, 2);
  puts("++++++++++++ int ++++++++++");
  puts(33);
  puts(34, 16 ∪ totext_precision(20));
  puts(35, 10);
  puts(36l, 8);
  puts(37ll, 2);
  puts("++++++++++++ int, capitalized ++++++++++");
  puts(38, 0 ∪ totext_up);
  puts(39, 16 ∪ totext_up);
  puts(40, 10 ∪ totext_up);
  puts(41l, 8 ∪ totext_up);
  puts(42ll, 2 ∪ totext_up);
  puts("++++++++++++ array of unsigned ++++++++++");
  puts(totext_tmp( 43u ));
  puts(totext_tmp( 45u ), totext_point);
  puts(totext_tmp(46u, -46u));
  puts("++++++++++++ pointers ++++++++++");
  puts((signed*)47);
  puts((signed const*)48);
  puts((void*)0xFFFF);
  puts((void const*)50);
  puts(totext_tmp((signed const*)51));
  puts("++++++++++++ array of some struct ++++++++++");
  puts(totext_tmp((ahs){}));
  puts(totext_tmp((ahs){}, (ahs){}));
  puts("++++++++++++ char strings ++++++++++");
  puts("a fishy string: \U0001F41F", totext_precision(32));
  puts("ring");
  puts("++++++++++++ wide strings ++++++++++");
  puts(L"a string 2");
  puts(L"räng");
  puts("++++++++++++ wide characters ++++++++++");
  puts(L'r');
  puts(L'\u00e4');
  puts("++++++++++++ utf16 strings ++++++++++");
  puts(u"a string 3");
  puts(u"ring");
  puts(u"a string 3", 16);
  puts(u"ring", 16 ∪ totext_up);
  puts("++++++++++++ utf16 characters ++++++++++");
  puts(u'r');
  puts(u'\u00e4');
  puts("++++++++++++ utf32 strings ++++++++++");
  puts(U"a string 4");
  puts(U"ring");
  puts(U"a string 4", 16);
  puts(U"ring", 16 ∪ totext_up);
  puts("++++++++++++ utf32 characters ++++++++++");
  puts(U'r');
  puts(U'\U0001F41F');
  puts("++++++++++++ utf8 strings ++++++++++");
  puts(u8"a string 5");
  puts(u8"ring");
  puts(u8"a string 5", 16);
  puts(u8"ring", 16 ∪ totext_up);
  puts("++++++++++++ misc ++++++++++");
  puts(35.0);
  puts(15u, 16 ∪ totext_alt ∪ totext_precision(10));
  double A[2] = { 35.0, 21.7, };
  puts(A);
  int* B[2] = { 0, &argc, };
  puts(B);
  unsigned C[2] = { 67, 33, };
  puts(C);
  unsigned D[3] = { 67, 33, 77, };
  puts(D);
  unsigned* c = C;
  puts(c);
  puts(totext_array(2, c));
  print_ahs(0, _Core_AHS(c, 0));
  print_ahs(1, _Core_AHS(totext_array(2, c), 0));
  print_ahs(2, _Core_AHS((unsigned(*)[2])c, 0));
  print_ahs(3, _Core_AHS(*((unsigned(*)[2])c), 0));
  unsigned* d = D;
  puts(d);
  puts(totext_array(3, d));
  print_ahs(0, _Core_AHS(d, 0));
  print_ahs(1, _Core_AHS(totext_array(3, d), 0));
  print_ahs(2, _Core_AHS((unsigned(*)[3])d, 0));
  print_ahs(3, _Core_AHS(*((unsigned(*)[3])d), 0));
  print_ahs(4, _Core_AHS(L'r', 0));
  print_ahs(5, _Core_AHS((char const*)"hellö", 16));
  print_ahs(5, _Core_AHS("hellö", 16));
  print_ahs(6, _Core_AHS("hellö", 16 ∪ totext_precision(2)));
  puts("++++++++++++ strings as number arrays ++++++++++");
  puts("hellö");
  puts("hellö", 10);
  puts("hellö", 8 ∪ totext_precision(2), "\\");
  puts("hellö", 16 ∪ totext_precision(2));
  puts(totext_tmp("hellö", "holla"), totext_char);
  puts(totext_tmp("hellö", "holla"), totext_char, " ");
  puts(totext_tmp("hellö", "holla"), totext_char, ",	");
  puts(totext_tmp(L"hellö", L"holla"), totext_char);
  puts(totext_tmp(L"hellö", L"holla"), totext_char, " ");
  puts(totext_tmp(L"hellö", L"holla"), totext_char, ",	");
  puts(totext_tmp(L"hellö", L"holla"));
  puts(totext_tmp(L"hellö", L"holla"), 0, " ");
  puts(totext_tmp(L"hellö", L"holla"), 0, ",	");
  puts("++++++++++++ bool ++++++++++");
  puts(false);
  puts(true);
  puts(false, totext_char);
  puts(true, totext_char);
  puts(false, totext_char ∪ totext_precision(1));
  puts(true, totext_char ∪ totext_precision(1));
  puts(totext_tmp(true, false, true, true), totext_precision(1));
  puts(totext_tmp(true, false, true, true), totext_char ∪ totext_precision(1));
  bool boolA[] = { true, false, true, true, };
  puts(&boolA, totext_precision(1));
  puts(boolA, totext_precision(1));
  puts(boolA, totext_char ∪ totext_precision(1));
  puts("++++++++++++ binary ++++++++++");
  puts(0xF0F0F0F0, 2 ∪ totext_alt);
  puts(0x12345678, 2 ∪ totext_alt);
  puts(0x12345678, 2 ∪ totext_precision(32));
  puts(15u, 2);
  puts(0x1234567812345678, 2);
  puts(0x1234567812345678, 2 ∪ totext_precision(32));
  puts("++++++++++++ test strdup and strndup ++++++++++");
  char* sepa = strdup(u"…fish…");
  char* sepb = strndup(u"…fish…", 8);
  printf("%s (%zu, %zu), %s (%zu)\n", sepa, strlen(sepa), strlen(u"…fish…"), sepb, strlen(sepb));
  puts("++++++++++++ unicode ++++++++++");
  puts(u"aber");
  puts(u"ähnlich");
  puts(u"\U0001F41F", 0, sepa);
  puts(U"aber");
  puts(U"ähnlich");
  puts(U"\U0001F41F", 0, sepa);
  puts("++++++++++++ unicode wide-string arrays ++++++++++");
  puts(totext_tmp(u"UTF—16: a", u"ä", u"\U0001F41F"), totext_char, sepa);
  puts(totext_tmp(U"UTF—32: a", U"ä", U"\U0001F41F"), totext_char, sepa);
  puts(totext_tmp(u"UTF—16: a", u"ä", u"\U0001F41F"), totext_char, sepb);
  puts(totext_tmp(U"UTF—32: a", U"ä", U"\U0001F41F"), totext_char, sepb);
  puts("++++++++++++ unicode as numbers ++++++++++");
  puts(u"aber", 16);
  puts(u"ähnlich", 16);
  puts(u"\U0001F41F", 16, sepa);
  puts(U"aber", 16);
  puts(U"ähnlich", 16);
  puts(U"\U0001F41F", 16, sepa);
  puts("++++++++++++ fputs ++++++++++");
  fputs(u"aber\n", stdout, 16);
  fputs(u"ähnlich\n", stdout, 16);
  fputs(u"aber\n", stdout);
  fputs(u"ähnlich\n", stdout);
  fputs(8*7.5L, stdout, 16 ∪ totext_precision(32));
  fputs("\n", stdout);
  fputs(8*7.5, stdout, 16 ∪ totext_precision(32));
  fputs("\n", stdout);
  fputs(8*7.5f, stdout, 16 ∪ totext_precision(32));
  fputs("\n", stdout);
  fputs(8*7.5, stdout);
  fputs("\n", stdout);
  fputs(-1u, stdout);
  fputs("\n", stdout);
  free(sepb);
  free(sepa);
  puts("++++++++++++ test evaluation ++++++++++");
  size_t evals = 0;
  puts(evals++);
  puts(evals);
}
