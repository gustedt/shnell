// -*- c++ -*-

#ifndef __clang__
#pragma GCC implementation "stdintwide.hh"
#endif
#include "stdintwide.hh"
#include <cstdint>
#include <cstdio>

//++++++++++ instantiation +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


template char operator + < char, true >(intwide_t t, char o);
template char operator + < char, true >(char o, intwide_t t);
template char operator - < char, true >(intwide_t t, char o);
template char operator - < char, true >(char o, intwide_t t);
template char operator * < char, true >(intwide_t t, char o);
template char operator * < char, true >(char o, intwide_t t);
template char operator / < char, true >(intwide_t t, char o);
template char operator / < char, true >(char o, intwide_t t);

template char operator % < char >(intwide_t t, char o);
template char operator % < char >(char o, intwide_t t);
template char operator xor < char >(intwide_t t, char o);
template char operator xor < char >(char o, intwide_t t);
template char operator bitor < char >(intwide_t t, char o);
template char operator bitor < char >(char o, intwide_t t);
template char operator bitand < char >(intwide_t t, char o);
template char operator bitand < char >(char o, intwide_t t);

template char operator << < char >(char o, intwide_t t);
template char operator >> < char >(char o, intwide_t t);

template intwide_t operator << < char >(intwide_t t, char o);
template intwide_t operator >> < char >(intwide_t t, char o);

template char& operator -= < char, true >(char& o, intwide_t t);
template char& operator *= < char, true >(char& o, intwide_t t);
template char& operator /= < char, true >(char& o, intwide_t t);

template char& operator xor_eq < char >(char& o, intwide_t t);
template char& operator or_eq < char >(char& o, intwide_t t);
template char& operator and_eq < char >(char& o, intwide_t t);
template char& operator >>= < char >(char& o, intwide_t t);
template char& operator <<= < char >(char& o, intwide_t t);

template bool operator == < char >(intwide_t const& t, char o) noexcept;
template bool operator == < char >(char o, intwide_t const& t) noexcept;
template bool operator != < char >(intwide_t const& t, char o) noexcept;
template bool operator != < char >(char o, intwide_t const& t) noexcept;
template bool operator < < char >(intwide_t const& t, char o) noexcept;
template bool operator < < char >(char o, intwide_t const& t) noexcept;
template bool operator > < char >(intwide_t const& t, char o) noexcept;
template bool operator > < char >(char o, intwide_t const& t) noexcept;
template bool operator <= < char >(intwide_t const& t, char o) noexcept;
template bool operator <= < char >(char o, intwide_t const& t) noexcept;
template bool operator >= < char >(intwide_t const& t, char o) noexcept;
template bool operator >= < char >(char o, intwide_t const& t) noexcept;


template signed char operator + < signed char, true >(intwide_t t, signed char o);
template signed char operator + < signed char, true >(signed char o, intwide_t t);
template signed char operator - < signed char, true >(intwide_t t, signed char o);
template signed char operator - < signed char, true >(signed char o, intwide_t t);
template signed char operator * < signed char, true >(intwide_t t, signed char o);
template signed char operator * < signed char, true >(signed char o, intwide_t t);
template signed char operator / < signed char, true >(intwide_t t, signed char o);
template signed char operator / < signed char, true >(signed char o, intwide_t t);

template signed char operator % < signed char >(intwide_t t, signed char o);
template signed char operator % < signed char >(signed char o, intwide_t t);
template signed char operator xor < signed char >(intwide_t t, signed char o);
template signed char operator xor < signed char >(signed char o, intwide_t t);
template signed char operator bitor < signed char >(intwide_t t, signed char o);
template signed char operator bitor < signed char >(signed char o, intwide_t t);
template signed char operator bitand < signed char >(intwide_t t, signed char o);
template signed char operator bitand < signed char >(signed char o, intwide_t t);

template signed char operator << < signed char >(signed char o, intwide_t t);
template signed char operator >> < signed char >(signed char o, intwide_t t);

template intwide_t operator << < signed char >(intwide_t t, signed char o);
template intwide_t operator >> < signed char >(intwide_t t, signed char o);

template signed char& operator -= < signed char, true >(signed char& o, intwide_t t);
template signed char& operator *= < signed char, true >(signed char& o, intwide_t t);
template signed char& operator /= < signed char, true >(signed char& o, intwide_t t);

template signed char& operator xor_eq < signed char >(signed char& o, intwide_t t);
template signed char& operator or_eq < signed char >(signed char& o, intwide_t t);
template signed char& operator and_eq < signed char >(signed char& o, intwide_t t);
template signed char& operator >>= < signed char >(signed char& o, intwide_t t);
template signed char& operator <<= < signed char >(signed char& o, intwide_t t);

template bool operator == < signed char >(intwide_t const& t, signed char o) noexcept;
template bool operator == < signed char >(signed char o, intwide_t const& t) noexcept;
template bool operator != < signed char >(intwide_t const& t, signed char o) noexcept;
template bool operator != < signed char >(signed char o, intwide_t const& t) noexcept;
template bool operator < < signed char >(intwide_t const& t, signed char o) noexcept;
template bool operator < < signed char >(signed char o, intwide_t const& t) noexcept;
template bool operator > < signed char >(intwide_t const& t, signed char o) noexcept;
template bool operator > < signed char >(signed char o, intwide_t const& t) noexcept;
template bool operator <= < signed char >(intwide_t const& t, signed char o) noexcept;
template bool operator <= < signed char >(signed char o, intwide_t const& t) noexcept;
template bool operator >= < signed char >(intwide_t const& t, signed char o) noexcept;
template bool operator >= < signed char >(signed char o, intwide_t const& t) noexcept;


template unsigned char operator + < unsigned char, true >(intwide_t t, unsigned char o);
template unsigned char operator + < unsigned char, true >(unsigned char o, intwide_t t);
template unsigned char operator - < unsigned char, true >(intwide_t t, unsigned char o);
template unsigned char operator - < unsigned char, true >(unsigned char o, intwide_t t);
template unsigned char operator * < unsigned char, true >(intwide_t t, unsigned char o);
template unsigned char operator * < unsigned char, true >(unsigned char o, intwide_t t);
template unsigned char operator / < unsigned char, true >(intwide_t t, unsigned char o);
template unsigned char operator / < unsigned char, true >(unsigned char o, intwide_t t);

template unsigned char operator % < unsigned char >(intwide_t t, unsigned char o);
template unsigned char operator % < unsigned char >(unsigned char o, intwide_t t);
template unsigned char operator xor < unsigned char >(intwide_t t, unsigned char o);
template unsigned char operator xor < unsigned char >(unsigned char o, intwide_t t);
template unsigned char operator bitor < unsigned char >(intwide_t t, unsigned char o);
template unsigned char operator bitor < unsigned char >(unsigned char o, intwide_t t);
template unsigned char operator bitand < unsigned char >(intwide_t t, unsigned char o);
template unsigned char operator bitand < unsigned char >(unsigned char o, intwide_t t);

template unsigned char operator << < unsigned char >(unsigned char o, intwide_t t);
template unsigned char operator >> < unsigned char >(unsigned char o, intwide_t t);

template intwide_t operator << < unsigned char >(intwide_t t, unsigned char o);
template intwide_t operator >> < unsigned char >(intwide_t t, unsigned char o);

template unsigned char& operator -= < unsigned char, true >(unsigned char& o, intwide_t t);
template unsigned char& operator *= < unsigned char, true >(unsigned char& o, intwide_t t);
template unsigned char& operator /= < unsigned char, true >(unsigned char& o, intwide_t t);

template unsigned char& operator xor_eq < unsigned char >(unsigned char& o, intwide_t t);
template unsigned char& operator or_eq < unsigned char >(unsigned char& o, intwide_t t);
template unsigned char& operator and_eq < unsigned char >(unsigned char& o, intwide_t t);
template unsigned char& operator >>= < unsigned char >(unsigned char& o, intwide_t t);
template unsigned char& operator <<= < unsigned char >(unsigned char& o, intwide_t t);

template bool operator == < unsigned char >(intwide_t const& t, unsigned char o) noexcept;
template bool operator == < unsigned char >(unsigned char o, intwide_t const& t) noexcept;
template bool operator != < unsigned char >(intwide_t const& t, unsigned char o) noexcept;
template bool operator != < unsigned char >(unsigned char o, intwide_t const& t) noexcept;
template bool operator < < unsigned char >(intwide_t const& t, unsigned char o) noexcept;
template bool operator < < unsigned char >(unsigned char o, intwide_t const& t) noexcept;
template bool operator > < unsigned char >(intwide_t const& t, unsigned char o) noexcept;
template bool operator > < unsigned char >(unsigned char o, intwide_t const& t) noexcept;
template bool operator <= < unsigned char >(intwide_t const& t, unsigned char o) noexcept;
template bool operator <= < unsigned char >(unsigned char o, intwide_t const& t) noexcept;
template bool operator >= < unsigned char >(intwide_t const& t, unsigned char o) noexcept;
template bool operator >= < unsigned char >(unsigned char o, intwide_t const& t) noexcept;


template signed short operator + < signed short, true >(intwide_t t, signed short o);
template signed short operator + < signed short, true >(signed short o, intwide_t t);
template signed short operator - < signed short, true >(intwide_t t, signed short o);
template signed short operator - < signed short, true >(signed short o, intwide_t t);
template signed short operator * < signed short, true >(intwide_t t, signed short o);
template signed short operator * < signed short, true >(signed short o, intwide_t t);
template signed short operator / < signed short, true >(intwide_t t, signed short o);
template signed short operator / < signed short, true >(signed short o, intwide_t t);

template signed short operator % < signed short >(intwide_t t, signed short o);
template signed short operator % < signed short >(signed short o, intwide_t t);
template signed short operator xor < signed short >(intwide_t t, signed short o);
template signed short operator xor < signed short >(signed short o, intwide_t t);
template signed short operator bitor < signed short >(intwide_t t, signed short o);
template signed short operator bitor < signed short >(signed short o, intwide_t t);
template signed short operator bitand < signed short >(intwide_t t, signed short o);
template signed short operator bitand < signed short >(signed short o, intwide_t t);

template signed short operator << < signed short >(signed short o, intwide_t t);
template signed short operator >> < signed short >(signed short o, intwide_t t);

template intwide_t operator << < signed short >(intwide_t t, signed short o);
template intwide_t operator >> < signed short >(intwide_t t, signed short o);

template signed short& operator -= < signed short, true >(signed short& o, intwide_t t);
template signed short& operator *= < signed short, true >(signed short& o, intwide_t t);
template signed short& operator /= < signed short, true >(signed short& o, intwide_t t);

template signed short& operator xor_eq < signed short >(signed short& o, intwide_t t);
template signed short& operator or_eq < signed short >(signed short& o, intwide_t t);
template signed short& operator and_eq < signed short >(signed short& o, intwide_t t);
template signed short& operator >>= < signed short >(signed short& o, intwide_t t);
template signed short& operator <<= < signed short >(signed short& o, intwide_t t);

template bool operator == < signed short >(intwide_t const& t, signed short o) noexcept;
template bool operator == < signed short >(signed short o, intwide_t const& t) noexcept;
template bool operator != < signed short >(intwide_t const& t, signed short o) noexcept;
template bool operator != < signed short >(signed short o, intwide_t const& t) noexcept;
template bool operator < < signed short >(intwide_t const& t, signed short o) noexcept;
template bool operator < < signed short >(signed short o, intwide_t const& t) noexcept;
template bool operator > < signed short >(intwide_t const& t, signed short o) noexcept;
template bool operator > < signed short >(signed short o, intwide_t const& t) noexcept;
template bool operator <= < signed short >(intwide_t const& t, signed short o) noexcept;
template bool operator <= < signed short >(signed short o, intwide_t const& t) noexcept;
template bool operator >= < signed short >(intwide_t const& t, signed short o) noexcept;
template bool operator >= < signed short >(signed short o, intwide_t const& t) noexcept;


template unsigned short operator + < unsigned short, true >(intwide_t t, unsigned short o);
template unsigned short operator + < unsigned short, true >(unsigned short o, intwide_t t);
template unsigned short operator - < unsigned short, true >(intwide_t t, unsigned short o);
template unsigned short operator - < unsigned short, true >(unsigned short o, intwide_t t);
template unsigned short operator * < unsigned short, true >(intwide_t t, unsigned short o);
template unsigned short operator * < unsigned short, true >(unsigned short o, intwide_t t);
template unsigned short operator / < unsigned short, true >(intwide_t t, unsigned short o);
template unsigned short operator / < unsigned short, true >(unsigned short o, intwide_t t);

template unsigned short operator % < unsigned short >(intwide_t t, unsigned short o);
template unsigned short operator % < unsigned short >(unsigned short o, intwide_t t);
template unsigned short operator xor < unsigned short >(intwide_t t, unsigned short o);
template unsigned short operator xor < unsigned short >(unsigned short o, intwide_t t);
template unsigned short operator bitor < unsigned short >(intwide_t t, unsigned short o);
template unsigned short operator bitor < unsigned short >(unsigned short o, intwide_t t);
template unsigned short operator bitand < unsigned short >(intwide_t t, unsigned short o);
template unsigned short operator bitand < unsigned short >(unsigned short o, intwide_t t);

template unsigned short operator << < unsigned short >(unsigned short o, intwide_t t);
template unsigned short operator >> < unsigned short >(unsigned short o, intwide_t t);

template intwide_t operator << < unsigned short >(intwide_t t, unsigned short o);
template intwide_t operator >> < unsigned short >(intwide_t t, unsigned short o);

template unsigned short& operator -= < unsigned short, true >(unsigned short& o, intwide_t t);
template unsigned short& operator *= < unsigned short, true >(unsigned short& o, intwide_t t);
template unsigned short& operator /= < unsigned short, true >(unsigned short& o, intwide_t t);

template unsigned short& operator xor_eq < unsigned short >(unsigned short& o, intwide_t t);
template unsigned short& operator or_eq < unsigned short >(unsigned short& o, intwide_t t);
template unsigned short& operator and_eq < unsigned short >(unsigned short& o, intwide_t t);
template unsigned short& operator >>= < unsigned short >(unsigned short& o, intwide_t t);
template unsigned short& operator <<= < unsigned short >(unsigned short& o, intwide_t t);

template bool operator == < unsigned short >(intwide_t const& t, unsigned short o) noexcept;
template bool operator == < unsigned short >(unsigned short o, intwide_t const& t) noexcept;
template bool operator != < unsigned short >(intwide_t const& t, unsigned short o) noexcept;
template bool operator != < unsigned short >(unsigned short o, intwide_t const& t) noexcept;
template bool operator < < unsigned short >(intwide_t const& t, unsigned short o) noexcept;
template bool operator < < unsigned short >(unsigned short o, intwide_t const& t) noexcept;
template bool operator > < unsigned short >(intwide_t const& t, unsigned short o) noexcept;
template bool operator > < unsigned short >(unsigned short o, intwide_t const& t) noexcept;
template bool operator <= < unsigned short >(intwide_t const& t, unsigned short o) noexcept;
template bool operator <= < unsigned short >(unsigned short o, intwide_t const& t) noexcept;
template bool operator >= < unsigned short >(intwide_t const& t, unsigned short o) noexcept;
template bool operator >= < unsigned short >(unsigned short o, intwide_t const& t) noexcept;


template signed operator + < signed, true >(intwide_t t, signed o);
template signed operator + < signed, true >(signed o, intwide_t t);
template signed operator - < signed, true >(intwide_t t, signed o);
template signed operator - < signed, true >(signed o, intwide_t t);
template signed operator * < signed, true >(intwide_t t, signed o);
template signed operator * < signed, true >(signed o, intwide_t t);
template signed operator / < signed, true >(intwide_t t, signed o);
template signed operator / < signed, true >(signed o, intwide_t t);

template signed operator % < signed >(intwide_t t, signed o);
template signed operator % < signed >(signed o, intwide_t t);
template signed operator xor < signed >(intwide_t t, signed o);
template signed operator xor < signed >(signed o, intwide_t t);
template signed operator bitor < signed >(intwide_t t, signed o);
template signed operator bitor < signed >(signed o, intwide_t t);
template signed operator bitand < signed >(intwide_t t, signed o);
template signed operator bitand < signed >(signed o, intwide_t t);

template signed operator << < signed >(signed o, intwide_t t);
template signed operator >> < signed >(signed o, intwide_t t);

template intwide_t operator << < signed >(intwide_t t, signed o);
template intwide_t operator >> < signed >(intwide_t t, signed o);

template signed& operator -= < signed, true >(signed& o, intwide_t t);
template signed& operator *= < signed, true >(signed& o, intwide_t t);
template signed& operator /= < signed, true >(signed& o, intwide_t t);

template signed& operator xor_eq < signed >(signed& o, intwide_t t);
template signed& operator or_eq < signed >(signed& o, intwide_t t);
template signed& operator and_eq < signed >(signed& o, intwide_t t);
template signed& operator >>= < signed >(signed& o, intwide_t t);
template signed& operator <<= < signed >(signed& o, intwide_t t);

template bool operator == < signed >(intwide_t const& t, signed o) noexcept;
template bool operator == < signed >(signed o, intwide_t const& t) noexcept;
template bool operator != < signed >(intwide_t const& t, signed o) noexcept;
template bool operator != < signed >(signed o, intwide_t const& t) noexcept;
template bool operator < < signed >(intwide_t const& t, signed o) noexcept;
template bool operator < < signed >(signed o, intwide_t const& t) noexcept;
template bool operator > < signed >(intwide_t const& t, signed o) noexcept;
template bool operator > < signed >(signed o, intwide_t const& t) noexcept;
template bool operator <= < signed >(intwide_t const& t, signed o) noexcept;
template bool operator <= < signed >(signed o, intwide_t const& t) noexcept;
template bool operator >= < signed >(intwide_t const& t, signed o) noexcept;
template bool operator >= < signed >(signed o, intwide_t const& t) noexcept;


template unsigned operator + < unsigned, true >(intwide_t t, unsigned o);
template unsigned operator + < unsigned, true >(unsigned o, intwide_t t);
template unsigned operator - < unsigned, true >(intwide_t t, unsigned o);
template unsigned operator - < unsigned, true >(unsigned o, intwide_t t);
template unsigned operator * < unsigned, true >(intwide_t t, unsigned o);
template unsigned operator * < unsigned, true >(unsigned o, intwide_t t);
template unsigned operator / < unsigned, true >(intwide_t t, unsigned o);
template unsigned operator / < unsigned, true >(unsigned o, intwide_t t);

template unsigned operator % < unsigned >(intwide_t t, unsigned o);
template unsigned operator % < unsigned >(unsigned o, intwide_t t);
template unsigned operator xor < unsigned >(intwide_t t, unsigned o);
template unsigned operator xor < unsigned >(unsigned o, intwide_t t);
template unsigned operator bitor < unsigned >(intwide_t t, unsigned o);
template unsigned operator bitor < unsigned >(unsigned o, intwide_t t);
template unsigned operator bitand < unsigned >(intwide_t t, unsigned o);
template unsigned operator bitand < unsigned >(unsigned o, intwide_t t);

template unsigned operator << < unsigned >(unsigned o, intwide_t t);
template unsigned operator >> < unsigned >(unsigned o, intwide_t t);

template intwide_t operator << < unsigned >(intwide_t t, unsigned o);
template intwide_t operator >> < unsigned >(intwide_t t, unsigned o);

template unsigned& operator -= < unsigned, true >(unsigned& o, intwide_t t);
template unsigned& operator *= < unsigned, true >(unsigned& o, intwide_t t);
template unsigned& operator /= < unsigned, true >(unsigned& o, intwide_t t);

template unsigned& operator xor_eq < unsigned >(unsigned& o, intwide_t t);
template unsigned& operator or_eq < unsigned >(unsigned& o, intwide_t t);
template unsigned& operator and_eq < unsigned >(unsigned& o, intwide_t t);
template unsigned& operator >>= < unsigned >(unsigned& o, intwide_t t);
template unsigned& operator <<= < unsigned >(unsigned& o, intwide_t t);

template bool operator == < unsigned >(intwide_t const& t, unsigned o) noexcept;
template bool operator == < unsigned >(unsigned o, intwide_t const& t) noexcept;
template bool operator != < unsigned >(intwide_t const& t, unsigned o) noexcept;
template bool operator != < unsigned >(unsigned o, intwide_t const& t) noexcept;
template bool operator < < unsigned >(intwide_t const& t, unsigned o) noexcept;
template bool operator < < unsigned >(unsigned o, intwide_t const& t) noexcept;
template bool operator > < unsigned >(intwide_t const& t, unsigned o) noexcept;
template bool operator > < unsigned >(unsigned o, intwide_t const& t) noexcept;
template bool operator <= < unsigned >(intwide_t const& t, unsigned o) noexcept;
template bool operator <= < unsigned >(unsigned o, intwide_t const& t) noexcept;
template bool operator >= < unsigned >(intwide_t const& t, unsigned o) noexcept;
template bool operator >= < unsigned >(unsigned o, intwide_t const& t) noexcept;


template signed long operator + < signed long, true >(intwide_t t, signed long o);
template signed long operator + < signed long, true >(signed long o, intwide_t t);
template signed long operator - < signed long, true >(intwide_t t, signed long o);
template signed long operator - < signed long, true >(signed long o, intwide_t t);
template signed long operator * < signed long, true >(intwide_t t, signed long o);
template signed long operator * < signed long, true >(signed long o, intwide_t t);
template signed long operator / < signed long, true >(intwide_t t, signed long o);
template signed long operator / < signed long, true >(signed long o, intwide_t t);

template signed long operator % < signed long >(intwide_t t, signed long o);
template signed long operator % < signed long >(signed long o, intwide_t t);
template signed long operator xor < signed long >(intwide_t t, signed long o);
template signed long operator xor < signed long >(signed long o, intwide_t t);
template signed long operator bitor < signed long >(intwide_t t, signed long o);
template signed long operator bitor < signed long >(signed long o, intwide_t t);
template signed long operator bitand < signed long >(intwide_t t, signed long o);
template signed long operator bitand < signed long >(signed long o, intwide_t t);

template signed long operator << < signed long >(signed long o, intwide_t t);
template signed long operator >> < signed long >(signed long o, intwide_t t);

template intwide_t operator << < signed long >(intwide_t t, signed long o);
template intwide_t operator >> < signed long >(intwide_t t, signed long o);

template signed long& operator -= < signed long, true >(signed long& o, intwide_t t);
template signed long& operator *= < signed long, true >(signed long& o, intwide_t t);
template signed long& operator /= < signed long, true >(signed long& o, intwide_t t);

template signed long& operator xor_eq < signed long >(signed long& o, intwide_t t);
template signed long& operator or_eq < signed long >(signed long& o, intwide_t t);
template signed long& operator and_eq < signed long >(signed long& o, intwide_t t);
template signed long& operator >>= < signed long >(signed long& o, intwide_t t);
template signed long& operator <<= < signed long >(signed long& o, intwide_t t);

template bool operator == < signed long >(intwide_t const& t, signed long o) noexcept;
template bool operator == < signed long >(signed long o, intwide_t const& t) noexcept;
template bool operator != < signed long >(intwide_t const& t, signed long o) noexcept;
template bool operator != < signed long >(signed long o, intwide_t const& t) noexcept;
template bool operator < < signed long >(intwide_t const& t, signed long o) noexcept;
template bool operator < < signed long >(signed long o, intwide_t const& t) noexcept;
template bool operator > < signed long >(intwide_t const& t, signed long o) noexcept;
template bool operator > < signed long >(signed long o, intwide_t const& t) noexcept;
template bool operator <= < signed long >(intwide_t const& t, signed long o) noexcept;
template bool operator <= < signed long >(signed long o, intwide_t const& t) noexcept;
template bool operator >= < signed long >(intwide_t const& t, signed long o) noexcept;
template bool operator >= < signed long >(signed long o, intwide_t const& t) noexcept;


template unsigned long operator + < unsigned long, true >(intwide_t t, unsigned long o);
template unsigned long operator + < unsigned long, true >(unsigned long o, intwide_t t);
template unsigned long operator - < unsigned long, true >(intwide_t t, unsigned long o);
template unsigned long operator - < unsigned long, true >(unsigned long o, intwide_t t);
template unsigned long operator * < unsigned long, true >(intwide_t t, unsigned long o);
template unsigned long operator * < unsigned long, true >(unsigned long o, intwide_t t);
template unsigned long operator / < unsigned long, true >(intwide_t t, unsigned long o);
template unsigned long operator / < unsigned long, true >(unsigned long o, intwide_t t);

template unsigned long operator % < unsigned long >(intwide_t t, unsigned long o);
template unsigned long operator % < unsigned long >(unsigned long o, intwide_t t);
template unsigned long operator xor < unsigned long >(intwide_t t, unsigned long o);
template unsigned long operator xor < unsigned long >(unsigned long o, intwide_t t);
template unsigned long operator bitor < unsigned long >(intwide_t t, unsigned long o);
template unsigned long operator bitor < unsigned long >(unsigned long o, intwide_t t);
template unsigned long operator bitand < unsigned long >(intwide_t t, unsigned long o);
template unsigned long operator bitand < unsigned long >(unsigned long o, intwide_t t);

template unsigned long operator << < unsigned long >(unsigned long o, intwide_t t);
template unsigned long operator >> < unsigned long >(unsigned long o, intwide_t t);

template intwide_t operator << < unsigned long >(intwide_t t, unsigned long o);
template intwide_t operator >> < unsigned long >(intwide_t t, unsigned long o);

template unsigned long& operator -= < unsigned long, true >(unsigned long& o, intwide_t t);
template unsigned long& operator *= < unsigned long, true >(unsigned long& o, intwide_t t);
template unsigned long& operator /= < unsigned long, true >(unsigned long& o, intwide_t t);

template unsigned long& operator xor_eq < unsigned long >(unsigned long& o, intwide_t t);
template unsigned long& operator or_eq < unsigned long >(unsigned long& o, intwide_t t);
template unsigned long& operator and_eq < unsigned long >(unsigned long& o, intwide_t t);
template unsigned long& operator >>= < unsigned long >(unsigned long& o, intwide_t t);
template unsigned long& operator <<= < unsigned long >(unsigned long& o, intwide_t t);

template bool operator == < unsigned long >(intwide_t const& t, unsigned long o) noexcept;
template bool operator == < unsigned long >(unsigned long o, intwide_t const& t) noexcept;
template bool operator != < unsigned long >(intwide_t const& t, unsigned long o) noexcept;
template bool operator != < unsigned long >(unsigned long o, intwide_t const& t) noexcept;
template bool operator < < unsigned long >(intwide_t const& t, unsigned long o) noexcept;
template bool operator < < unsigned long >(unsigned long o, intwide_t const& t) noexcept;
template bool operator > < unsigned long >(intwide_t const& t, unsigned long o) noexcept;
template bool operator > < unsigned long >(unsigned long o, intwide_t const& t) noexcept;
template bool operator <= < unsigned long >(intwide_t const& t, unsigned long o) noexcept;
template bool operator <= < unsigned long >(unsigned long o, intwide_t const& t) noexcept;
template bool operator >= < unsigned long >(intwide_t const& t, unsigned long o) noexcept;
template bool operator >= < unsigned long >(unsigned long o, intwide_t const& t) noexcept;


template signed long long operator + < signed long long, true >(intwide_t t, signed long long o);
template signed long long operator + < signed long long, true >(signed long long o, intwide_t t);
template signed long long operator - < signed long long, true >(intwide_t t, signed long long o);
template signed long long operator - < signed long long, true >(signed long long o, intwide_t t);
template signed long long operator * < signed long long, true >(intwide_t t, signed long long o);
template signed long long operator * < signed long long, true >(signed long long o, intwide_t t);
template signed long long operator / < signed long long, true >(intwide_t t, signed long long o);
template signed long long operator / < signed long long, true >(signed long long o, intwide_t t);

template signed long long operator % < signed long long >(intwide_t t, signed long long o);
template signed long long operator % < signed long long >(signed long long o, intwide_t t);
template signed long long operator xor < signed long long >(intwide_t t, signed long long o);
template signed long long operator xor < signed long long >(signed long long o, intwide_t t);
template signed long long operator bitor < signed long long >(intwide_t t, signed long long o);
template signed long long operator bitor < signed long long >(signed long long o, intwide_t t);
template signed long long operator bitand < signed long long >(intwide_t t, signed long long o);
template signed long long operator bitand < signed long long >(signed long long o, intwide_t t);

template signed long long operator << < signed long long >(signed long long o, intwide_t t);
template signed long long operator >> < signed long long >(signed long long o, intwide_t t);

template intwide_t operator << < signed long long >(intwide_t t, signed long long o);
template intwide_t operator >> < signed long long >(intwide_t t, signed long long o);

template signed long long& operator -= < signed long long, true >(signed long long& o, intwide_t t);
template signed long long& operator *= < signed long long, true >(signed long long& o, intwide_t t);
template signed long long& operator /= < signed long long, true >(signed long long& o, intwide_t t);

template signed long long& operator xor_eq < signed long long >(signed long long& o, intwide_t t);
template signed long long& operator or_eq < signed long long >(signed long long& o, intwide_t t);
template signed long long& operator and_eq < signed long long >(signed long long& o, intwide_t t);
template signed long long& operator >>= < signed long long >(signed long long& o, intwide_t t);
template signed long long& operator <<= < signed long long >(signed long long& o, intwide_t t);

template bool operator == < signed long long >(intwide_t const& t, signed long long o) noexcept;
template bool operator == < signed long long >(signed long long o, intwide_t const& t) noexcept;
template bool operator != < signed long long >(intwide_t const& t, signed long long o) noexcept;
template bool operator != < signed long long >(signed long long o, intwide_t const& t) noexcept;
template bool operator < < signed long long >(intwide_t const& t, signed long long o) noexcept;
template bool operator < < signed long long >(signed long long o, intwide_t const& t) noexcept;
template bool operator > < signed long long >(intwide_t const& t, signed long long o) noexcept;
template bool operator > < signed long long >(signed long long o, intwide_t const& t) noexcept;
template bool operator <= < signed long long >(intwide_t const& t, signed long long o) noexcept;
template bool operator <= < signed long long >(signed long long o, intwide_t const& t) noexcept;
template bool operator >= < signed long long >(intwide_t const& t, signed long long o) noexcept;
template bool operator >= < signed long long >(signed long long o, intwide_t const& t) noexcept;


template unsigned long long operator + < unsigned long long, true >(intwide_t t, unsigned long long o);
template unsigned long long operator + < unsigned long long, true >(unsigned long long o, intwide_t t);
template unsigned long long operator - < unsigned long long, true >(intwide_t t, unsigned long long o);
template unsigned long long operator - < unsigned long long, true >(unsigned long long o, intwide_t t);
template unsigned long long operator * < unsigned long long, true >(intwide_t t, unsigned long long o);
template unsigned long long operator * < unsigned long long, true >(unsigned long long o, intwide_t t);
template unsigned long long operator / < unsigned long long, true >(intwide_t t, unsigned long long o);
template unsigned long long operator / < unsigned long long, true >(unsigned long long o, intwide_t t);

template unsigned long long operator % < unsigned long long >(intwide_t t, unsigned long long o);
template unsigned long long operator % < unsigned long long >(unsigned long long o, intwide_t t);
template unsigned long long operator xor < unsigned long long >(intwide_t t, unsigned long long o);
template unsigned long long operator xor < unsigned long long >(unsigned long long o, intwide_t t);
template unsigned long long operator bitor < unsigned long long >(intwide_t t, unsigned long long o);
template unsigned long long operator bitor < unsigned long long >(unsigned long long o, intwide_t t);
template unsigned long long operator bitand < unsigned long long >(intwide_t t, unsigned long long o);
template unsigned long long operator bitand < unsigned long long >(unsigned long long o, intwide_t t);

template unsigned long long operator << < unsigned long long >(unsigned long long o, intwide_t t);
template unsigned long long operator >> < unsigned long long >(unsigned long long o, intwide_t t);

template intwide_t operator << < unsigned long long >(intwide_t t, unsigned long long o);
template intwide_t operator >> < unsigned long long >(intwide_t t, unsigned long long o);

template unsigned long long& operator -= < unsigned long long, true >(unsigned long long& o, intwide_t t);
template unsigned long long& operator *= < unsigned long long, true >(unsigned long long& o, intwide_t t);
template unsigned long long& operator /= < unsigned long long, true >(unsigned long long& o, intwide_t t);

template unsigned long long& operator xor_eq < unsigned long long >(unsigned long long& o, intwide_t t);
template unsigned long long& operator or_eq < unsigned long long >(unsigned long long& o, intwide_t t);
template unsigned long long& operator and_eq < unsigned long long >(unsigned long long& o, intwide_t t);
template unsigned long long& operator >>= < unsigned long long >(unsigned long long& o, intwide_t t);
template unsigned long long& operator <<= < unsigned long long >(unsigned long long& o, intwide_t t);

template bool operator == < unsigned long long >(intwide_t const& t, unsigned long long o) noexcept;
template bool operator == < unsigned long long >(unsigned long long o, intwide_t const& t) noexcept;
template bool operator != < unsigned long long >(intwide_t const& t, unsigned long long o) noexcept;
template bool operator != < unsigned long long >(unsigned long long o, intwide_t const& t) noexcept;
template bool operator < < unsigned long long >(intwide_t const& t, unsigned long long o) noexcept;
template bool operator < < unsigned long long >(unsigned long long o, intwide_t const& t) noexcept;
template bool operator > < unsigned long long >(intwide_t const& t, unsigned long long o) noexcept;
template bool operator > < unsigned long long >(unsigned long long o, intwide_t const& t) noexcept;
template bool operator <= < unsigned long long >(intwide_t const& t, unsigned long long o) noexcept;
template bool operator <= < unsigned long long >(unsigned long long o, intwide_t const& t) noexcept;
template bool operator >= < unsigned long long >(intwide_t const& t, unsigned long long o) noexcept;
template bool operator >= < unsigned long long >(unsigned long long o, intwide_t const& t) noexcept;



template intwide_t::operator float () const;

template float operator + < float, true >(intwide_t t, float o);
template float operator + < float, true >(float o, intwide_t t);
template float operator - < float, true >(intwide_t t, float o);
template float operator - < float, true >(float o, intwide_t t);
template float operator * < float, true >(intwide_t t, float o);
template float operator * < float, true >(float o, intwide_t t);
template float operator / < float, true >(intwide_t t, float o);
template float operator / < float, true >(float o, intwide_t t);

template float& operator -= < float, true >(float& o, intwide_t t);
template float& operator *= < float, true >(float& o, intwide_t t);
template float& operator /= < float, true >(float& o, intwide_t t);

template bool operator == < float >(intwide_t const& t, float o) noexcept;
template bool operator == < float >(float o, intwide_t const& t) noexcept;
template bool operator != < float >(intwide_t const& t, float o) noexcept;
template bool operator != < float >(float o, intwide_t const& t) noexcept;
template bool operator < < float >(intwide_t const& t, float o) noexcept;
template bool operator < < float >(float o, intwide_t const& t) noexcept;
template bool operator > < float >(intwide_t const& t, float o) noexcept;
template bool operator > < float >(float o, intwide_t const& t) noexcept;
template bool operator <= < float >(intwide_t const& t, float o) noexcept;
template bool operator <= < float >(float o, intwide_t const& t) noexcept;
template bool operator >= < float >(intwide_t const& t, float o) noexcept;
template bool operator >= < float >(float o, intwide_t const& t) noexcept;


template intwide_t::operator double () const;

template double operator + < double, true >(intwide_t t, double o);
template double operator + < double, true >(double o, intwide_t t);
template double operator - < double, true >(intwide_t t, double o);
template double operator - < double, true >(double o, intwide_t t);
template double operator * < double, true >(intwide_t t, double o);
template double operator * < double, true >(double o, intwide_t t);
template double operator / < double, true >(intwide_t t, double o);
template double operator / < double, true >(double o, intwide_t t);

template double& operator -= < double, true >(double& o, intwide_t t);
template double& operator *= < double, true >(double& o, intwide_t t);
template double& operator /= < double, true >(double& o, intwide_t t);

template bool operator == < double >(intwide_t const& t, double o) noexcept;
template bool operator == < double >(double o, intwide_t const& t) noexcept;
template bool operator != < double >(intwide_t const& t, double o) noexcept;
template bool operator != < double >(double o, intwide_t const& t) noexcept;
template bool operator < < double >(intwide_t const& t, double o) noexcept;
template bool operator < < double >(double o, intwide_t const& t) noexcept;
template bool operator > < double >(intwide_t const& t, double o) noexcept;
template bool operator > < double >(double o, intwide_t const& t) noexcept;
template bool operator <= < double >(intwide_t const& t, double o) noexcept;
template bool operator <= < double >(double o, intwide_t const& t) noexcept;
template bool operator >= < double >(intwide_t const& t, double o) noexcept;
template bool operator >= < double >(double o, intwide_t const& t) noexcept;


template intwide_t::operator long double () const;

template long double operator + < long double, true >(intwide_t t, long double o);
template long double operator + < long double, true >(long double o, intwide_t t);
template long double operator - < long double, true >(intwide_t t, long double o);
template long double operator - < long double, true >(long double o, intwide_t t);
template long double operator * < long double, true >(intwide_t t, long double o);
template long double operator * < long double, true >(long double o, intwide_t t);
template long double operator / < long double, true >(intwide_t t, long double o);
template long double operator / < long double, true >(long double o, intwide_t t);

template long double& operator -= < long double, true >(long double& o, intwide_t t);
template long double& operator *= < long double, true >(long double& o, intwide_t t);
template long double& operator /= < long double, true >(long double& o, intwide_t t);

template bool operator == < long double >(intwide_t const& t, long double o) noexcept;
template bool operator == < long double >(long double o, intwide_t const& t) noexcept;
template bool operator != < long double >(intwide_t const& t, long double o) noexcept;
template bool operator != < long double >(long double o, intwide_t const& t) noexcept;
template bool operator < < long double >(intwide_t const& t, long double o) noexcept;
template bool operator < < long double >(long double o, intwide_t const& t) noexcept;
template bool operator > < long double >(intwide_t const& t, long double o) noexcept;
template bool operator > < long double >(long double o, intwide_t const& t) noexcept;
template bool operator <= < long double >(intwide_t const& t, long double o) noexcept;
template bool operator <= < long double >(long double o, intwide_t const& t) noexcept;
template bool operator >= < long double >(intwide_t const& t, long double o) noexcept;
template bool operator >= < long double >(long double o, intwide_t const& t) noexcept;




template float const* operator+ < float const*, true >(intwide_t t, float const* o);
template float const* operator+ < float const*, true >(float const* o, intwide_t t);
template float const* operator- < float const*, true >(float const* o, intwide_t t);

template float const*& operator -= < float const*, true >(float const*& o, intwide_t t);


template double const* operator+ < double const*, true >(intwide_t t, double const* o);
template double const* operator+ < double const*, true >(double const* o, intwide_t t);
template double const* operator- < double const*, true >(double const* o, intwide_t t);

template double const*& operator -= < double const*, true >(double const*& o, intwide_t t);


template long double const* operator+ < long double const*, true >(intwide_t t, long double const* o);
template long double const* operator+ < long double const*, true >(long double const* o, intwide_t t);
template long double const* operator- < long double const*, true >(long double const* o, intwide_t t);

template long double const*& operator -= < long double const*, true >(long double const*& o, intwide_t t);


template char const* operator+ < char const*, true >(intwide_t t, char const* o);
template char const* operator+ < char const*, true >(char const* o, intwide_t t);
template char const* operator- < char const*, true >(char const* o, intwide_t t);

template char const*& operator -= < char const*, true >(char const*& o, intwide_t t);


template signed char const* operator+ < signed char const*, true >(intwide_t t, signed char const* o);
template signed char const* operator+ < signed char const*, true >(signed char const* o, intwide_t t);
template signed char const* operator- < signed char const*, true >(signed char const* o, intwide_t t);

template signed char const*& operator -= < signed char const*, true >(signed char const*& o, intwide_t t);


template unsigned char const* operator+ < unsigned char const*, true >(intwide_t t, unsigned char const* o);
template unsigned char const* operator+ < unsigned char const*, true >(unsigned char const* o, intwide_t t);
template unsigned char const* operator- < unsigned char const*, true >(unsigned char const* o, intwide_t t);

template unsigned char const*& operator -= < unsigned char const*, true >(unsigned char const*& o, intwide_t t);


template signed short const* operator+ < signed short const*, true >(intwide_t t, signed short const* o);
template signed short const* operator+ < signed short const*, true >(signed short const* o, intwide_t t);
template signed short const* operator- < signed short const*, true >(signed short const* o, intwide_t t);

template signed short const*& operator -= < signed short const*, true >(signed short const*& o, intwide_t t);


template unsigned short const* operator+ < unsigned short const*, true >(intwide_t t, unsigned short const* o);
template unsigned short const* operator+ < unsigned short const*, true >(unsigned short const* o, intwide_t t);
template unsigned short const* operator- < unsigned short const*, true >(unsigned short const* o, intwide_t t);

template unsigned short const*& operator -= < unsigned short const*, true >(unsigned short const*& o, intwide_t t);


template signed const* operator+ < signed const*, true >(intwide_t t, signed const* o);
template signed const* operator+ < signed const*, true >(signed const* o, intwide_t t);
template signed const* operator- < signed const*, true >(signed const* o, intwide_t t);

template signed const*& operator -= < signed const*, true >(signed const*& o, intwide_t t);


template unsigned const* operator+ < unsigned const*, true >(intwide_t t, unsigned const* o);
template unsigned const* operator+ < unsigned const*, true >(unsigned const* o, intwide_t t);
template unsigned const* operator- < unsigned const*, true >(unsigned const* o, intwide_t t);

template unsigned const*& operator -= < unsigned const*, true >(unsigned const*& o, intwide_t t);


template signed long const* operator+ < signed long const*, true >(intwide_t t, signed long const* o);
template signed long const* operator+ < signed long const*, true >(signed long const* o, intwide_t t);
template signed long const* operator- < signed long const*, true >(signed long const* o, intwide_t t);

template signed long const*& operator -= < signed long const*, true >(signed long const*& o, intwide_t t);


template unsigned long const* operator+ < unsigned long const*, true >(intwide_t t, unsigned long const* o);
template unsigned long const* operator+ < unsigned long const*, true >(unsigned long const* o, intwide_t t);
template unsigned long const* operator- < unsigned long const*, true >(unsigned long const* o, intwide_t t);

template unsigned long const*& operator -= < unsigned long const*, true >(unsigned long const*& o, intwide_t t);


template signed long long const* operator+ < signed long long const*, true >(intwide_t t, signed long long const* o);
template signed long long const* operator+ < signed long long const*, true >(signed long long const* o, intwide_t t);
template signed long long const* operator- < signed long long const*, true >(signed long long const* o, intwide_t t);

template signed long long const*& operator -= < signed long long const*, true >(signed long long const*& o, intwide_t t);


template unsigned long long const* operator+ < unsigned long long const*, true >(intwide_t t, unsigned long long const* o);
template unsigned long long const* operator+ < unsigned long long const*, true >(unsigned long long const* o, intwide_t t);
template unsigned long long const* operator- < unsigned long long const*, true >(unsigned long long const* o, intwide_t t);

template unsigned long long const*& operator -= < unsigned long long const*, true >(unsigned long long const*& o, intwide_t t);



template float volatile* operator+ < float volatile*, true >(intwide_t t, float volatile* o);
template float volatile* operator+ < float volatile*, true >(float volatile* o, intwide_t t);
template float volatile* operator- < float volatile*, true >(float volatile* o, intwide_t t);

template float volatile*& operator -= < float volatile*, true >(float volatile*& o, intwide_t t);


template double volatile* operator+ < double volatile*, true >(intwide_t t, double volatile* o);
template double volatile* operator+ < double volatile*, true >(double volatile* o, intwide_t t);
template double volatile* operator- < double volatile*, true >(double volatile* o, intwide_t t);

template double volatile*& operator -= < double volatile*, true >(double volatile*& o, intwide_t t);


template long double volatile* operator+ < long double volatile*, true >(intwide_t t, long double volatile* o);
template long double volatile* operator+ < long double volatile*, true >(long double volatile* o, intwide_t t);
template long double volatile* operator- < long double volatile*, true >(long double volatile* o, intwide_t t);

template long double volatile*& operator -= < long double volatile*, true >(long double volatile*& o, intwide_t t);


template char volatile* operator+ < char volatile*, true >(intwide_t t, char volatile* o);
template char volatile* operator+ < char volatile*, true >(char volatile* o, intwide_t t);
template char volatile* operator- < char volatile*, true >(char volatile* o, intwide_t t);

template char volatile*& operator -= < char volatile*, true >(char volatile*& o, intwide_t t);


template signed char volatile* operator+ < signed char volatile*, true >(intwide_t t, signed char volatile* o);
template signed char volatile* operator+ < signed char volatile*, true >(signed char volatile* o, intwide_t t);
template signed char volatile* operator- < signed char volatile*, true >(signed char volatile* o, intwide_t t);

template signed char volatile*& operator -= < signed char volatile*, true >(signed char volatile*& o, intwide_t t);


template unsigned char volatile* operator+ < unsigned char volatile*, true >(intwide_t t, unsigned char volatile* o);
template unsigned char volatile* operator+ < unsigned char volatile*, true >(unsigned char volatile* o, intwide_t t);
template unsigned char volatile* operator- < unsigned char volatile*, true >(unsigned char volatile* o, intwide_t t);

template unsigned char volatile*& operator -= < unsigned char volatile*, true >(unsigned char volatile*& o, intwide_t t);


template signed short volatile* operator+ < signed short volatile*, true >(intwide_t t, signed short volatile* o);
template signed short volatile* operator+ < signed short volatile*, true >(signed short volatile* o, intwide_t t);
template signed short volatile* operator- < signed short volatile*, true >(signed short volatile* o, intwide_t t);

template signed short volatile*& operator -= < signed short volatile*, true >(signed short volatile*& o, intwide_t t);


template unsigned short volatile* operator+ < unsigned short volatile*, true >(intwide_t t, unsigned short volatile* o);
template unsigned short volatile* operator+ < unsigned short volatile*, true >(unsigned short volatile* o, intwide_t t);
template unsigned short volatile* operator- < unsigned short volatile*, true >(unsigned short volatile* o, intwide_t t);

template unsigned short volatile*& operator -= < unsigned short volatile*, true >(unsigned short volatile*& o, intwide_t t);


template signed volatile* operator+ < signed volatile*, true >(intwide_t t, signed volatile* o);
template signed volatile* operator+ < signed volatile*, true >(signed volatile* o, intwide_t t);
template signed volatile* operator- < signed volatile*, true >(signed volatile* o, intwide_t t);

template signed volatile*& operator -= < signed volatile*, true >(signed volatile*& o, intwide_t t);


template unsigned volatile* operator+ < unsigned volatile*, true >(intwide_t t, unsigned volatile* o);
template unsigned volatile* operator+ < unsigned volatile*, true >(unsigned volatile* o, intwide_t t);
template unsigned volatile* operator- < unsigned volatile*, true >(unsigned volatile* o, intwide_t t);

template unsigned volatile*& operator -= < unsigned volatile*, true >(unsigned volatile*& o, intwide_t t);


template signed long volatile* operator+ < signed long volatile*, true >(intwide_t t, signed long volatile* o);
template signed long volatile* operator+ < signed long volatile*, true >(signed long volatile* o, intwide_t t);
template signed long volatile* operator- < signed long volatile*, true >(signed long volatile* o, intwide_t t);

template signed long volatile*& operator -= < signed long volatile*, true >(signed long volatile*& o, intwide_t t);


template unsigned long volatile* operator+ < unsigned long volatile*, true >(intwide_t t, unsigned long volatile* o);
template unsigned long volatile* operator+ < unsigned long volatile*, true >(unsigned long volatile* o, intwide_t t);
template unsigned long volatile* operator- < unsigned long volatile*, true >(unsigned long volatile* o, intwide_t t);

template unsigned long volatile*& operator -= < unsigned long volatile*, true >(unsigned long volatile*& o, intwide_t t);


template signed long long volatile* operator+ < signed long long volatile*, true >(intwide_t t, signed long long volatile* o);
template signed long long volatile* operator+ < signed long long volatile*, true >(signed long long volatile* o, intwide_t t);
template signed long long volatile* operator- < signed long long volatile*, true >(signed long long volatile* o, intwide_t t);

template signed long long volatile*& operator -= < signed long long volatile*, true >(signed long long volatile*& o, intwide_t t);


template unsigned long long volatile* operator+ < unsigned long long volatile*, true >(intwide_t t, unsigned long long volatile* o);
template unsigned long long volatile* operator+ < unsigned long long volatile*, true >(unsigned long long volatile* o, intwide_t t);
template unsigned long long volatile* operator- < unsigned long long volatile*, true >(unsigned long long volatile* o, intwide_t t);

template unsigned long long volatile*& operator -= < unsigned long long volatile*, true >(unsigned long long volatile*& o, intwide_t t);



template float const volatile* operator+ < float const volatile*, true >(intwide_t t, float const volatile* o);
template float const volatile* operator+ < float const volatile*, true >(float const volatile* o, intwide_t t);
template float const volatile* operator- < float const volatile*, true >(float const volatile* o, intwide_t t);

template float const volatile*& operator -= < float const volatile*, true >(float const volatile*& o, intwide_t t);


template double const volatile* operator+ < double const volatile*, true >(intwide_t t, double const volatile* o);
template double const volatile* operator+ < double const volatile*, true >(double const volatile* o, intwide_t t);
template double const volatile* operator- < double const volatile*, true >(double const volatile* o, intwide_t t);

template double const volatile*& operator -= < double const volatile*, true >(double const volatile*& o, intwide_t t);


template long double const volatile* operator+ < long double const volatile*, true >(intwide_t t, long double const volatile* o);
template long double const volatile* operator+ < long double const volatile*, true >(long double const volatile* o, intwide_t t);
template long double const volatile* operator- < long double const volatile*, true >(long double const volatile* o, intwide_t t);

template long double const volatile*& operator -= < long double const volatile*, true >(long double const volatile*& o, intwide_t t);


template char const volatile* operator+ < char const volatile*, true >(intwide_t t, char const volatile* o);
template char const volatile* operator+ < char const volatile*, true >(char const volatile* o, intwide_t t);
template char const volatile* operator- < char const volatile*, true >(char const volatile* o, intwide_t t);

template char const volatile*& operator -= < char const volatile*, true >(char const volatile*& o, intwide_t t);


template signed char const volatile* operator+ < signed char const volatile*, true >(intwide_t t, signed char const volatile* o);
template signed char const volatile* operator+ < signed char const volatile*, true >(signed char const volatile* o, intwide_t t);
template signed char const volatile* operator- < signed char const volatile*, true >(signed char const volatile* o, intwide_t t);

template signed char const volatile*& operator -= < signed char const volatile*, true >(signed char const volatile*& o, intwide_t t);


template unsigned char const volatile* operator+ < unsigned char const volatile*, true >(intwide_t t, unsigned char const volatile* o);
template unsigned char const volatile* operator+ < unsigned char const volatile*, true >(unsigned char const volatile* o, intwide_t t);
template unsigned char const volatile* operator- < unsigned char const volatile*, true >(unsigned char const volatile* o, intwide_t t);

template unsigned char const volatile*& operator -= < unsigned char const volatile*, true >(unsigned char const volatile*& o, intwide_t t);


template signed short const volatile* operator+ < signed short const volatile*, true >(intwide_t t, signed short const volatile* o);
template signed short const volatile* operator+ < signed short const volatile*, true >(signed short const volatile* o, intwide_t t);
template signed short const volatile* operator- < signed short const volatile*, true >(signed short const volatile* o, intwide_t t);

template signed short const volatile*& operator -= < signed short const volatile*, true >(signed short const volatile*& o, intwide_t t);


template unsigned short const volatile* operator+ < unsigned short const volatile*, true >(intwide_t t, unsigned short const volatile* o);
template unsigned short const volatile* operator+ < unsigned short const volatile*, true >(unsigned short const volatile* o, intwide_t t);
template unsigned short const volatile* operator- < unsigned short const volatile*, true >(unsigned short const volatile* o, intwide_t t);

template unsigned short const volatile*& operator -= < unsigned short const volatile*, true >(unsigned short const volatile*& o, intwide_t t);


template signed const volatile* operator+ < signed const volatile*, true >(intwide_t t, signed const volatile* o);
template signed const volatile* operator+ < signed const volatile*, true >(signed const volatile* o, intwide_t t);
template signed const volatile* operator- < signed const volatile*, true >(signed const volatile* o, intwide_t t);

template signed const volatile*& operator -= < signed const volatile*, true >(signed const volatile*& o, intwide_t t);


template unsigned const volatile* operator+ < unsigned const volatile*, true >(intwide_t t, unsigned const volatile* o);
template unsigned const volatile* operator+ < unsigned const volatile*, true >(unsigned const volatile* o, intwide_t t);
template unsigned const volatile* operator- < unsigned const volatile*, true >(unsigned const volatile* o, intwide_t t);

template unsigned const volatile*& operator -= < unsigned const volatile*, true >(unsigned const volatile*& o, intwide_t t);


template signed long const volatile* operator+ < signed long const volatile*, true >(intwide_t t, signed long const volatile* o);
template signed long const volatile* operator+ < signed long const volatile*, true >(signed long const volatile* o, intwide_t t);
template signed long const volatile* operator- < signed long const volatile*, true >(signed long const volatile* o, intwide_t t);

template signed long const volatile*& operator -= < signed long const volatile*, true >(signed long const volatile*& o, intwide_t t);


template unsigned long const volatile* operator+ < unsigned long const volatile*, true >(intwide_t t, unsigned long const volatile* o);
template unsigned long const volatile* operator+ < unsigned long const volatile*, true >(unsigned long const volatile* o, intwide_t t);
template unsigned long const volatile* operator- < unsigned long const volatile*, true >(unsigned long const volatile* o, intwide_t t);

template unsigned long const volatile*& operator -= < unsigned long const volatile*, true >(unsigned long const volatile*& o, intwide_t t);


template signed long long const volatile* operator+ < signed long long const volatile*, true >(intwide_t t, signed long long const volatile* o);
template signed long long const volatile* operator+ < signed long long const volatile*, true >(signed long long const volatile* o, intwide_t t);
template signed long long const volatile* operator- < signed long long const volatile*, true >(signed long long const volatile* o, intwide_t t);

template signed long long const volatile*& operator -= < signed long long const volatile*, true >(signed long long const volatile*& o, intwide_t t);


template unsigned long long const volatile* operator+ < unsigned long long const volatile*, true >(intwide_t t, unsigned long long const volatile* o);
template unsigned long long const volatile* operator+ < unsigned long long const volatile*, true >(unsigned long long const volatile* o, intwide_t t);
template unsigned long long const volatile* operator- < unsigned long long const volatile*, true >(unsigned long long const volatile* o, intwide_t t);

template unsigned long long const volatile*& operator -= < unsigned long long const volatile*, true >(unsigned long long const volatile*& o, intwide_t t);


extern consteval intwide_t operator ""_W (const char* lit);
extern consteval intwide_t operator ""_w (const char* lit);
std::to_chars_result std::to_chars(char* first, char* last, __uint128_t value, int b);

void (intwide_t::printerror)(char const* name, char const* err) const {
    if (radix) {
        printf("intwide_t:\t%s,\t(%c,0x%.16llx'%.16llx,%ld)%s, debug or test:\t%s\n",
               name,
               (xtra ? '-' : '+'),
               static_cast<unsigned long long>(static_cast<uint64_t>(bits >> 64)),static_cast<unsigned long long>(static_cast<uint64_t>(bits)),
               expo,
               (radix == 10 ? "\u2081\u2080" : "\u2082"),
               err);
    } else {
        printf("intwide_t:\t%s,\t(%c,0x%.16llx'%.16llx), debug or test:\t%s\n",
               name,
               (xtra ? '-' : '+'),
               static_cast<unsigned long long>(static_cast<uint64_t>(bits >> 64)),static_cast<unsigned long long>(static_cast<uint64_t>(bits)),
               err);
    }
}

intwide_t intwide_t::k\u0259n\u02C8v\u025Dt(const char*lit) noexcept {
    return convert(lit);
}

#ifdef __SIZEOF_INT128__
extern std::to_chars_result std::to_chars(char*, char*, __uint128_t, int);
#endif
