// -*- c++ -*-

/*
----------------------------------------------------------------------
Copyright 2021 Jens Gustedt <jens.gustedt@inria.fr>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
----------------------------------------------------------------------
*/

#ifndef _STDINTWIDE_T_
#define _STDINTWIDE_T_ 1
#ifndef __clang__
#pragma GCC interface
#endif
#include <limits>
#include <charconv>
#include <cstring>
#include <type_traits>
#include <algorithm>

/**
 ** @file
 **
 ** @brief typeless wide integer literals with suffix `w` or `W`
 **
 ** Common integer literals in C and C++ have several conceptual
 ** problems.
 **
 ** - They are typed too early. As a consequence, in border cases
 **   literals are not available to easily express values. E.g if
 **   `int` has 32 bit, the expression `-0x80000000` has type
 **   `unsigned` and thus effectively represents the value
 **   `+0x80000000`. So assignment of that value to an `int` is
 **   undefined. In contrast to that `-2147483648` has type `long` or
 **   `long long` and the expected value and so assignment to an `int`
 **   is well defined.
 **
 **   When using such a constant in a relational operator things get
 **   even weirder. If `x` is an `int` then the comparison
 **   `-0x80000000 <= x` first converts `x` to `unsigned` and then
 **   compares `0x80000000 <= (unsigned)x`, whereas `-2147483648 <= x`
 **   or `-0x800000000 <= x` (one more '0'!) are always `true`.
 **
 ** - There is no clear indication where to go with UB for
 **   expressions that only contain literals.
 **
 ** - There are no provisions for integer literals that have values
 **   that are larger than `ULLONG_MAX`, so new extended integer types
 **   or bit-precise integers possibly don't have literals.
 **
 ** The proposed wide integer literals follow a different strategy.
 **
 ** - They have a type that cannot be used for any thing else than
 **   integer constant expressions (ICE). No declaration with that
 **   type is expected to work.

 ** - Regardless if they use a binary, octal, decimal or hexadecimal
 **   representation, the type of the literal is always the same, the
 **   signed wide integer type. All arithmetic expressions that only
 **   have terminals that are wide integer literals will result in the
 **   same wide integer type.
 **
 ** - They produce an error as soon as they are used in an expression
 **   that is mathematically not defined or that overflows the
 **   internal limits. Such errors are always hard errors and should
 **   stop compilation. (Once `consteval` works well in all compilers
 **   ;-)
 **
 ** - They convert automatically into any other integer type as soon
 **   that this is necessary. Here again, if the value doesn't fit
 **   into the target type, the compilation is aborted.
 **
 ** - They can represent all integer values that are valid on the
 **   platform with a literal (regardless if binary, octal, decimal or
 **   hexadecimal) and an additional sign.
 **
 ** - They don't take part in the ABI, such that changes for them do
 **   not invalidate existing libraries or objects. (Again, once
 **   `consteval` works ...)
 **
 ** - They are as wide as the platform needs it. So if the platform
 **   adds a new integer type, wide literals can be adapted if
 **   necessary.
 **
 ** * Some further details for particular operations.
 **
 ** Normal bitwise operations (`compl`, `bitand`, `bitor`, `xor`) will
 ** never have errors.
 **
 ** All arithmetic operations (`+`, `-`, `*`, `/`, `%`, `compl`,
 ** `bitand`, `bitor`, `xor`) that are performed with another second
 ** operand of a different type, return that other type.
 **
 ** Shift operators work as far as this makes mathematical sense, and
 ** return the type of the LHS. If the LHS is a wide integer literal:
 **
 ** - Right shift performs sign extension and is always well defined
 **   for non-negative exponents. If the exponent is larger than the
 **   maximum width, the result is zero or minus one,
 **   respectively. Negative exponents are forwarded to the left shift
 **   operator in the obvious way.
 **
 ** - Left shift works for positive or negative LHS, but errors out if
 **   the mathematical result overflows or underflows, respectively.
 **   Negative exponents are forwarded to the right shift operator in
 **   the obvious way.
 **
 ** A relational operator where both operands are well defined and
 ** with at least one wide integer literal operand is well defined for
 ** all possible values and provides the mathematical result. In
 ** particular a comparison of a negative wide integer expression `a`
 ** with an unsigned value `b`, will always state that `a` is strictly
 ** less than `b`. No surprising signed to unsigned conversion behind
 ** your back.
 **
 ** Current limitations of the implementation:
 **
 ** - Underneath we use the `__uint128_t` type that is present on many
 **   modern 64 bit architectures or `unsigned long long` as a fall
 **   back. This could be re-implemented by using a bigint library if
 **   one really needs very wide fixed precision integer literals. The
 **   alternative `unsigned long long` provides a convenient maximum
 **   number of bits for platforms that don't want to handle wider
 **   types. None of this should really matter for performance, the
 **   use is mostly proportional to the number of integer literals in
 **   the program, and should only amount to unnoticeable overhead at
 **   compile time.
 **
 ** - Only the relational operators may be in use repeatedly at
 **   runtime. But they should not be more costly than usual
 **   comparison; in the contrary
 **   - if the wide integer literal expression is outside the scope of
 **     the other operands type, the operator can be optimized out;
 **   - if the wide integer literal is inside the scope, the
 **     comparison can be reduced, at translation time, to the
 **     comparison of the other operands type
 **
 **   Currently, within my limited test base, g++-10 and clang++-11
 **   both succeed very well with this optimization.
 **
 ** - The values are not always determined at compile time, but for
 **   the moment some are delegated to the static initializer
 **   functions. This is due to the fact that I don't master this
 **   aspect very well, but also that gcc bugs under my feet for
 **   certain constellations.
 **
 ** - Clang does not allow us to define extensions `w` or `W`, so we
 **   can't test with these intended extensions. The time being we
 **   also use `_w` and `_W` as replacements.
 **
 ** - To work properly with a non-standard integer type under the
 **   hood, `std::numeric_limits` for that type must be well
 **   maintained.
 **/

constexpr double log10_2 = 0.301029995664;

template< typename T >
struct is_ext_integral {
    static constexpr bool value = std::is_integral< T >::value;
};

// we use the internal 128 bit types if possible
#ifdef __SIZEOF_INT128__
typedef __uint128_t __uintbits;

// some "extended" integer types are not well covered by
// std::numeric_limits

template<> struct std::numeric_limits<__uint128_t> : public std::numeric_limits<long long> {
// is not generated easily in an automatic way
    static constexpr __uint128_t max() noexcept;
// is not generated easily in an automatic way
    static constexpr __uint128_t min() noexcept;
// is not generated easily in an automatic way
    static constexpr __uint128_t lowest() noexcept;
    static constexpr __uint128_t epsilon() noexcept {
        return 0;
    }
    static constexpr __uint128_t round_error() noexcept {
        return 0;
    }
    static constexpr __uint128_t infinity() noexcept {
        return 0;
    }
    static constexpr __uint128_t quiet_NaN() noexcept {
        return 0;
    }
    static constexpr __uint128_t signaling_NaN() noexcept {
        return 0;
    }
    static constexpr __uint128_t denorm_min() noexcept {
        return 0;
    }
    static constexpr bool is_signed         = (__uint128_t)-1 < (__uint128_t)0;
    static constexpr bool is_specialized    = true;
    static constexpr bool is_modulo         = !is_signed;
// It seems that `digits` is what C calls precision
    static constexpr int digits         = 128 - (int)is_signed;
    static constexpr int digits10       = digits * log10_2;
};

template<> struct std::numeric_limits<__int128_t> : public std::numeric_limits<long long> {
// is not generated easily in an automatic way
    static constexpr __int128_t max() noexcept;
// is not generated easily in an automatic way
    static constexpr __int128_t min() noexcept;
// is not generated easily in an automatic way
    static constexpr __int128_t lowest() noexcept;
    static constexpr __int128_t epsilon() noexcept {
        return 0;
    }
    static constexpr __int128_t round_error() noexcept {
        return 0;
    }
    static constexpr __int128_t infinity() noexcept {
        return 0;
    }
    static constexpr __int128_t quiet_NaN() noexcept {
        return 0;
    }
    static constexpr __int128_t signaling_NaN() noexcept {
        return 0;
    }
    static constexpr __int128_t denorm_min() noexcept {
        return 0;
    }
    static constexpr bool is_signed         = (__int128_t)-1 < (__int128_t)0;
    static constexpr bool is_specialized    = true;
    static constexpr bool is_modulo         = !is_signed;
// It seems that `digits` is what C calls precision
    static constexpr int digits         = 128 - (int)is_signed;
    static constexpr int digits10       = digits * log10_2;
};

// provide the concrete values

constexpr __uint128_t std::numeric_limits<__uint128_t>::max()          noexcept {
    return -1;
}
constexpr __uint128_t std::numeric_limits<__uint128_t>::min()          noexcept {
    return 0;
}
constexpr __uint128_t std::numeric_limits<__uint128_t>::lowest()       noexcept {
    return 0;
}

constexpr __int128_t std::numeric_limits<__int128_t>::max()            noexcept {
    return std::numeric_limits<__uint128_t>::max()/2;
}
constexpr __int128_t std::numeric_limits<__int128_t>::min()            noexcept {
    return -1 - std::numeric_limits<__int128_t>::max();
}
constexpr __int128_t std::numeric_limits<__int128_t>::lowest()         noexcept {
    return min();
}


template<> struct is_ext_integral<__uint128_t> {
    static constexpr bool value = true;
};
template<> struct is_ext_integral<__int128_t> {
    static constexpr bool value = true;
};

static_assert(!std::numeric_limits<__uint128_t>::is_signed, "wrong signededness of __uint128_t");
static_assert(std::numeric_limits<__int128_t>::is_signed, "wrong signededness of __int128_t");

namespace std {

inline
to_chars_result to_chars(char* first, char* last, __uint128_t value, int b = 10) {
    __uint128_t const base = b;
    char* p = first;
    for (__uint128_t div = value; div >= base && p < last; div /= base) {
        p++;
    }
    unsigned char rest = value % base;
    to_chars_result res = std::to_chars(p, last, rest, b);
    if (static_cast<int>(res.ec)) {
        while (p != first) {
            --p;
            *p = '@';
        }
    } else {
        while (p != first) {
            --p;
            value /= base;
            rest = value % base;
            std::to_chars(p, last, rest, b);
        }
    }
    return res;
}

}

#else
typedef unsigned long long __uintbits;
#endif

// forward declare numeric_limits for the new type

class intwide_t;
template<> struct std::numeric_limits<intwide_t> : public std::numeric_limits<__uintbits> {
    static constexpr intwide_t max()           noexcept;
    static constexpr intwide_t min()           noexcept;
    static constexpr intwide_t lowest()        noexcept;
    static constexpr intwide_t epsilon()       noexcept;
    static constexpr intwide_t round_error()   noexcept;
    static constexpr intwide_t infinity()      noexcept;
    static constexpr intwide_t quiet_NaN()     noexcept;
    static constexpr intwide_t signaling_NaN() noexcept;
    static constexpr intwide_t denorm_min()    noexcept;
    static constexpr bool is_signed     = true;
    static constexpr bool is_modulo     = false;
// The precision of the new type are all bits of the underlying unsigned type
    static constexpr int digits         = std::numeric_limits<__uintbits>::digits;
    static constexpr int digits10       = digits * log10_2;
};

namespace std {
inline to_chars_result to_chars(char*, char*, intwide_t, int base);
}

class intwide_t {
private:

// we use the internal 128 bit types
    typedef __uintbits uintbits;

    static constexpr size_t precision = std::numeric_limits<uintbits>::digits;
    static constexpr uintbits maximum = std::numeric_limits<uintbits>::max();

// precision bits plus one extra to represent a signed integer in two's
// complement
    uintbits bits;
    bool xtra;
    unsigned radix;
    long expo;

// all constructors are privat and constexpr
    constexpr intwide_t(bool sign0, uintbits mag, unsigned rad = 0, long ex = 0) noexcept : bits(mag), xtra(sign0), radix(rad), expo(ex) { }
    constexpr intwide_t(intwide_t const&) noexcept = default;

    template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true >
    constexpr intwide_t(T o) noexcept : bits(o), xtra(o < 0), radix(0), expo(0) { }

// Raw literal operators, the only way to create an intwide_t.
// They can only be applied at compile time to a literal.
//
// This `w` and `W` is the final form that we have in mind for standardization.
//friend consteval intwide_t operator ""W (const char* lit);
//friend consteval intwide_t operator ""w (const char* lit);
//
// Note that for all the forms of such operator declarations, the
// suffix must follow the "" immediately without space. Otherwise
// then something like _W would be undefined, and the other forms
// would risk to clash with a macro name.
//
// clang does not let us allow to do the above, so we use user space
// literals, too
    friend consteval intwide_t operator ""_W (const char* lit);
    friend consteval intwide_t operator ""_w (const char* lit);

public:
    /**
       ** @brief print error with maximal annoyance
       **
       ** Whenever such a messages appears in the output of your running
       ** program you are either testing this package (good), or abusing
       ** these functionalties to do runtime conversion of wide numbers
       ** (bad).
       **
       ** During compilation, a call to this function might be brought
       ** forward as a reason that the compilation aborts. This means that
       ** you have a wide number literal conversion or other operation
       ** that fails at compile time, e.g because it would lose
       ** precision. Usually this is an indication that you have a logical
       ** error and you number literal(s) are not what you think they are.
       **/
    void printerror(char const* name, char const* err) const;

// once we have full C++20 support, we should use their new tools
#if defined(__GNUC__) && !defined(__clang__)
#define printerror(...) printerror(__PRETTY_FUNCTION__, __VA_ARGS__)
#else
#define printerror(...) printerror(__func__, __VA_ARGS__)
#endif

private:

// the four different conversion functions for the four different
// bases (2, 8, 10, 16)

// @brief convert string as base 2 to integer
    static constexpr uintbits convert2(char const* lit)  noexcept(false) {
        constexpr uintbits bound = maximum/2;
        uintbits ret = 0;
        for (; *lit; ++lit) {
            if (ret > bound) intwide_t(false, 0).printerror("wide number literal too large");
            ret *= 2;
            switch (*lit) {
// other cases are already caught by the parser
            default:
                break;
            case '0':
                ret += 0;
                break;
            case '1':
                ret += 1;
                break;
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
            }
        }
        return ret;
    }


// @brief convert string as base 8 to integer
    static constexpr uintbits convert8(char const* lit)  noexcept(false) {
        constexpr uintbits bound = maximum/8;
        uintbits ret = 0;
        for (; *lit; ++lit) {
            if (ret > bound) intwide_t(false, 0).printerror("wide number literal too large");
            ret *= 8;
            switch (*lit) {
// other cases are already caught by the parser
            default:
                break;
            case '0':
                ret += 0;
                break;
            case '1':
                ret += 1;
                break;
            case '2':
                ret += 2;
                break;
            case '3':
                ret += 3;
                break;
            case '4':
                ret += 4;
                break;
            case '5':
                ret += 5;
                break;
            case '6':
                ret += 6;
                break;
            case '7':
                ret += 7;
                break;
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
            }
        }
        return ret;
    }


// @brief convert string as base 10 to integer
    static constexpr uintbits convert10(char const* lit)  noexcept(false) {
        constexpr uintbits bound = maximum/10;
        uintbits ret = 0;
        for (; *lit; ++lit) {
            if (ret > bound) intwide_t(false, 0).printerror("wide number literal too large");
            ret *= 10;
            switch (*lit) {
// other cases are already caught by the parser
            default:
                break;
            case '0':
                ret += 0;
                break;
            case '1':
                ret += 1;
                break;
            case '2':
                ret += 2;
                break;
            case '3':
                ret += 3;
                break;
            case '4':
                ret += 4;
                break;
            case '5':
                ret += 5;
                break;
            case '6':
                ret += 6;
                break;
            case '7':
                ret += 7;
                break;
            case '8':
                ret += 8;
                break;
            case '9':
                ret += 9;
                break;
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
            }
        }
        return ret;
    }


// @brief convert string as base 16 to integer
    static constexpr uintbits convert16(char const* lit)  noexcept(false) {
        constexpr uintbits bound = maximum/16;
        uintbits ret = 0;
        for (; *lit; ++lit) {
            if (ret > bound) intwide_t(false, 0).printerror("wide number literal too large");
            ret *= 16;
            switch (*lit) {
// other cases are already caught by the parser
            default:
                break;
            case '0':
                ret += 0;
                break;
            case '1':
                ret += 1;
                break;
            case '2':
                ret += 2;
                break;
            case '3':
                ret += 3;
                break;
            case '4':
                ret += 4;
                break;
            case '5':
                ret += 5;
                break;
            case '6':
                ret += 6;
                break;
            case '7':
                ret += 7;
                break;
            case '8':
                ret += 8;
                break;
            case '9':
                ret += 9;
                break;
//-------------------------------------------------------------------------------
            case 'A':
                ret += 10;
                break;
            case 'B':
                ret += 11;
                break;
            case 'C':
                ret += 12;
                break;
            case 'D':
                ret += 13;
                break;
            case 'E':
                ret += 14;
                break;
            case 'F':
                ret += 15;
                break;
//-------------------------------------------------------------------------------
            case 'a':
                ret += 10;
                break;
            case 'b':
                ret += 11;
                break;
            case 'c':
                ret += 12;
                break;
            case 'd':
                ret += 13;
                break;
            case 'e':
                ret += 14;
                break;
            case 'f':
                ret += 15;
                break;
//-------------------------------------------------------------------------------
            }
        }
        return ret;
    }


// @brief convert string as base 24 to integer
    static constexpr uintbits convert24(char const* lit)  noexcept(false) {
        constexpr uintbits bound = maximum/24;
        uintbits ret = 0;
        for (; *lit; ++lit) {
            if (ret > bound) intwide_t(false, 0).printerror("wide number literal too large");
            ret *= 24;
            switch (*lit) {
// other cases are already caught by the parser
            default:
                break;
            case '0':
                ret += 0;
                break;
            case '1':
                ret += 1;
                break;
            case '2':
                ret += 2;
                break;
            case '3':
                ret += 3;
                break;
            case '4':
                ret += 4;
                break;
            case '5':
                ret += 5;
                break;
            case '6':
                ret += 6;
                break;
            case '7':
                ret += 7;
                break;
            case '8':
                ret += 8;
                break;
            case '9':
                ret += 9;
                break;
//-------------------------------------------------------------------------------
            case 'A':
                ret += 10;
                break;
            case 'B':
                ret += 11;
                break;
            case 'C':
                ret += 12;
                break;
            case 'D':
                ret += 13;
                break;
            case 'E':
                ret += 14;
                break;
            case 'F':
                ret += 15;
                break;
            case 'G':
                ret += 16;
                break;
            case 'H':
                ret += 17;
                break;
            case 'I':
                ret += 18;
                break;
            case 'J':
                ret += 19;
                break;
            case 'K':
                ret += 20;
                break;
            case 'L':
                ret += 21;
                break;
            case 'M':
                ret += 22;
                break;
            case 'N':
                ret += 23;
                break;
//-------------------------------------------------------------------------------
            case 'a':
                ret += 10;
                break;
            case 'b':
                ret += 11;
                break;
            case 'c':
                ret += 12;
                break;
            case 'd':
                ret += 13;
                break;
            case 'e':
                ret += 14;
                break;
            case 'f':
                ret += 15;
                break;
            case 'g':
                ret += 16;
                break;
            case 'h':
                ret += 17;
                break;
            case 'i':
                ret += 18;
                break;
            case 'j':
                ret += 19;
                break;
            case 'k':
                ret += 20;
                break;
            case 'l':
                ret += 21;
                break;
            case 'm':
                ret += 22;
                break;
            case 'n':
                ret += 23;
                break;
//-------------------------------------------------------------------------------
            }
        }
        return ret;
    }


// @brief convert string as base 32 to integer
    static constexpr uintbits convert32(char const* lit)  noexcept(false) {
        constexpr uintbits bound = maximum/32;
        uintbits ret = 0;
        for (; *lit; ++lit) {
            if (ret > bound) intwide_t(false, 0).printerror("wide number literal too large");
            ret *= 32;
            switch (*lit) {
// other cases are already caught by the parser
            default:
                break;
            case '0':
                ret += 0;
                break;
            case '1':
                ret += 1;
                break;
            case '2':
                ret += 2;
                break;
            case '3':
                ret += 3;
                break;
            case '4':
                ret += 4;
                break;
            case '5':
                ret += 5;
                break;
            case '6':
                ret += 6;
                break;
            case '7':
                ret += 7;
                break;
            case '8':
                ret += 8;
                break;
            case '9':
                ret += 9;
                break;
//-------------------------------------------------------------------------------
            case 'A':
                ret += 10;
                break;
            case 'B':
                ret += 11;
                break;
            case 'C':
                ret += 12;
                break;
            case 'D':
                ret += 13;
                break;
            case 'E':
                ret += 14;
                break;
            case 'F':
                ret += 15;
                break;
            case 'G':
                ret += 16;
                break;
            case 'H':
                ret += 17;
                break;
            case 'I':
                ret += 18;
                break;
            case 'J':
                ret += 19;
                break;
            case 'K':
                ret += 20;
                break;
            case 'L':
                ret += 21;
                break;
            case 'M':
                ret += 22;
                break;
            case 'N':
                ret += 23;
                break;
            case 'O':
                ret += 24;
                break;
            case 'P':
                ret += 25;
                break;
            case 'Q':
                ret += 26;
                break;
            case 'R':
                ret += 27;
                break;
            case 'S':
                ret += 28;
                break;
            case 'T':
                ret += 29;
                break;
            case 'U':
                ret += 30;
                break;
            case 'V':
                ret += 31;
                break;
//-------------------------------------------------------------------------------
            case 'a':
                ret += 10;
                break;
            case 'b':
                ret += 11;
                break;
            case 'c':
                ret += 12;
                break;
            case 'd':
                ret += 13;
                break;
            case 'e':
                ret += 14;
                break;
            case 'f':
                ret += 15;
                break;
            case 'g':
                ret += 16;
                break;
            case 'h':
                ret += 17;
                break;
            case 'i':
                ret += 18;
                break;
            case 'j':
                ret += 19;
                break;
            case 'k':
                ret += 20;
                break;
            case 'l':
                ret += 21;
                break;
            case 'm':
                ret += 22;
                break;
            case 'n':
                ret += 23;
                break;
            case 'o':
                ret += 24;
                break;
            case 'p':
                ret += 25;
                break;
            case 'q':
                ret += 26;
                break;
            case 'r':
                ret += 27;
                break;
            case 's':
                ret += 28;
                break;
            case 't':
                ret += 29;
                break;
            case 'u':
                ret += 30;
                break;
            case 'v':
                ret += 31;
                break;
//-------------------------------------------------------------------------------
            }
        }
        return ret;
    }


// @brief convert string as base 36 to integer
    static constexpr uintbits convert36(char const* lit)  noexcept(false) {
        constexpr uintbits bound = maximum/36;
        uintbits ret = 0;
        for (; *lit; ++lit) {
            if (ret > bound) intwide_t(false, 0).printerror("wide number literal too large");
            ret *= 36;
            switch (*lit) {
// other cases are already caught by the parser
            default:
                break;
            case '0':
                ret += 0;
                break;
            case '1':
                ret += 1;
                break;
            case '2':
                ret += 2;
                break;
            case '3':
                ret += 3;
                break;
            case '4':
                ret += 4;
                break;
            case '5':
                ret += 5;
                break;
            case '6':
                ret += 6;
                break;
            case '7':
                ret += 7;
                break;
            case '8':
                ret += 8;
                break;
            case '9':
                ret += 9;
                break;
//-------------------------------------------------------------------------------
            case 'A':
                ret += 10;
                break;
            case 'B':
                ret += 11;
                break;
            case 'C':
                ret += 12;
                break;
            case 'D':
                ret += 13;
                break;
            case 'E':
                ret += 14;
                break;
            case 'F':
                ret += 15;
                break;
            case 'G':
                ret += 16;
                break;
            case 'H':
                ret += 17;
                break;
            case 'I':
                ret += 18;
                break;
            case 'J':
                ret += 19;
                break;
            case 'K':
                ret += 20;
                break;
            case 'L':
                ret += 21;
                break;
            case 'M':
                ret += 22;
                break;
            case 'N':
                ret += 23;
                break;
            case 'O':
                ret += 24;
                break;
            case 'P':
                ret += 25;
                break;
            case 'Q':
                ret += 26;
                break;
            case 'R':
                ret += 27;
                break;
            case 'S':
                ret += 28;
                break;
            case 'T':
                ret += 29;
                break;
            case 'U':
                ret += 30;
                break;
            case 'V':
                ret += 31;
                break;
            case 'W':
                ret += 32;
                break;
            case 'X':
                ret += 33;
                break;
            case 'Y':
                ret += 34;
                break;
            case 'Z':
                ret += 35;
                break;
//-------------------------------------------------------------------------------
            case 'a':
                ret += 10;
                break;
            case 'b':
                ret += 11;
                break;
            case 'c':
                ret += 12;
                break;
            case 'd':
                ret += 13;
                break;
            case 'e':
                ret += 14;
                break;
            case 'f':
                ret += 15;
                break;
            case 'g':
                ret += 16;
                break;
            case 'h':
                ret += 17;
                break;
            case 'i':
                ret += 18;
                break;
            case 'j':
                ret += 19;
                break;
            case 'k':
                ret += 20;
                break;
            case 'l':
                ret += 21;
                break;
            case 'm':
                ret += 22;
                break;
            case 'n':
                ret += 23;
                break;
            case 'o':
                ret += 24;
                break;
            case 'p':
                ret += 25;
                break;
            case 'q':
                ret += 26;
                break;
            case 'r':
                ret += 27;
                break;
            case 's':
                ret += 28;
                break;
            case 't':
                ret += 29;
                break;
            case 'u':
                ret += 30;
                break;
            case 'v':
                ret += 31;
                break;
            case 'w':
                ret += 32;
                break;
            case 'x':
                ret += 33;
                break;
            case 'y':
                ret += 34;
                break;
            case 'z':
                ret += 35;
                break;
//-------------------------------------------------------------------------------
            }
        }
        return ret;
    }


//! @brief Convert an number literal given in the string into an
//! the wide number type.
//!
//! Glues the four conversions together, selected by the start
//! sequence "0x", "0b", "0" or otherwise.
//!
//! If there is neither decimal point nor exponent, the number is a
//! wide integer literal, otherwise it is a wide floating point
//! literal.
//!
//! The exponent, if given, has to be indicated by "e" or "E" for
//! decimal floating point literals. It is then accounted as radix 10.
//!
//! For hex or binary it has to be "p" or "P". The radix for that
//! exponent is then taken to be 2.
//!
//! The allowed format is looser than for the usual floating point
//! literals. E.g hexfloats here don't need to have a decimal point,
//! and we add formats for binary floating point literals. But this
//! does not serve much for the literals themselves, because the C++
//! parser eliminates them before we ever see them.
    static constexpr intwide_t convert(const char*lit) noexcept {
// This is the conversion base, that is how the characters in the
// string are seen.
        unsigned base = 10;
        if (*lit == '0') {
            ++lit;
            switch (*lit) {
            case 'x':
            case 'X':
                base = 16;
                ++lit;
                break;
            case 'b':
            case 'B':
                base = 2;
                ++lit;
                break;
            default:
                base = 8;
                break;
            }
        }
        while (*lit == '0')
            ++lit;
        char buffer[256] = { };
        char* buf = buffer;
        char const e = base == 16 ? 'p' : 'e';
        char const E = base == 16 ? 'P' : 'E';
// This accounts for the radix shifts that are to be performed if
// there is a decimal point.
        long ex = 0;
// This is the radix in which the exponent is counted. 0 means
// that it is an integer constant, not a floating point. Other
// possible final values are 2 (derived from bases 2, 8 and 16) or
// 10 (derived from base 10).
        unsigned rad = 0;
        for (; lit[0] && lit[0] != e && lit[0] != E; ++lit) {
            if (lit[0] == '.') {
                rad = base;
            } else {
                buf[0] = lit[0];
                ++buf;
                if (rad) --ex;
            }
        }
        buf[0] = 0;
// This captures the special case that we have a floating point
// literal that starts with a 0 digit. If there would be no
// decimal point or no exponent, it would be an octal integer
// literal.
        if (rad == 8 || lit[0] == 'e' || lit[0] == 'E') {
            base = 10;
            rad = 10;
        }
// The shifts for the period are collected in base digits.
        if (rad) {
            switch (base) {
            default:
                break;
            case 8:
                ex *= 3;
                rad = 2;
                break;
            case 16:
                ex *= 4;
                rad = 2;
                break;
            case 32:
                ex *= 5;
                rad = 2;
                break;
            }
        }
// The explicit exponent is counted for radix 10 or 2.
        if (lit[0] == e || lit[0] == E) {
            ++lit;
            rad = (base == 10) ? 10 : 2;
            bool sign = (lit[0] == '-');
            if (lit[0] == '-' || lit[0] == '+') {
                ++lit;
            }
            long expl = convert10(lit);
            if (sign) ex -= expl;
            else ex += expl;
        }
        uintbits bits = 0;
        switch (base) {
        case 16:
            bits = convert16(buffer);
            break;
        case  2:
            bits = convert2(buffer);
            break;
        case  8:
            bits = convert8(buffer);
            break;
        default:
            bits = convert10(buffer);
        }
        return intwide_t(false, bits, rad, ex);
    }

public:

//! @brief A public interface to ::convert, but its use is deprecated
    [[deprecated("please use for testing, only")]] static intwide_t k\u0259n\u02C8v\u025Dt(const char*lit) noexcept;

    template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true >
    constexpr bool in_range() const {
        constexpr uintbits max = std::numeric_limits<T>::max();
        return
            xtra
            ? (-bits-1 <= max)
            : (bits <= max);
    }

    template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true >
    constexpr bool in_range() const {
        constexpr T max = std::numeric_limits<T>::max();
        return
            xtra
            ? (-bits >= -max)
            : (bits <= max);
    }

    template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true >
    constexpr bool is_exact() const {
        uintbits val = xtra ? -bits : bits;
        constexpr uintbits one = 1;
        constexpr uintbits max = one << std::numeric_limits< T >::digits;
        if (radix) {
            long ex = expo;
            while (!(val % radix)) {
                val /= radix;
                ++ex;
            }
        }
        return val <= max;
    }

    constexpr bool is_negative() const {
        return xtra;
    }

    constexpr bool is_zero() const {
        return !xtra && !bits;
    }

    constexpr bool is_positive() const {
        return !xtra && bits;
    }

//* @brief Convert a number from our internal N+1 two's complement
//* representation to a target type.
//*
//* Bounds of the target type are respected as follows:
//*  - If the target type is signed, the value must be in range.
//*
//*  - If it is unsigned, the value must not be too large. If it is
//*    negative, it must not be smaller than the negative maximum
//*    value.
//*
//* This can only be used after a rvalue of `intwide_t` has been
//* created, so this is always in a constant, compile time context.
//* If a value is out of range, this should lead to abortion of the
//* compilation.
//*
//* This is "specialized" for `bool` because the semantics are
//* different.
    template< typename T, std::enable_if_t<is_ext_integral<T>::value
                                           && !std::is_same< T, bool>::value, bool > ENABLED = true >
    constexpr T tointeger () const {
        constexpr uintbits max = std::numeric_limits<T>::max();
// If this is a floating point literal, we have to adjust the mantissa.
        if (radix && expo) {
            if (expo > 0) {
                intwide_t mant(xtra, bits);
                intwide_t const rad(false, radix);
                intwide_t fact(false, 1);
                for (unsigned i = 0; i < expo; ++i) {
                    fact = fact * rad;
                }
                return (fact*mant).tointeger< T >();
            } else {
                uintbits b = xtra ? -bits : bits;
                long ex = -expo;
                bool err = false;
                while (ex) {
                    if (b % radix && !err) {
                        printerror("loosing precision when converting wide floating literal to integral");
                        err = true;
                    }
                    b /= radix;
                    --ex;
                }
                return (intwide_t(xtra, xtra ? -b : b)).tointeger< T >();
            }
        }
        if (xtra) {
            if (-bits-1 > max)
                printerror("negative wide literal too small in conversion");
            else if (std::numeric_limits<T>::is_signed) {
// translate the bitpattern into a signed value if necessary
                return -1-static_cast< T >(maximum - bits);
            } else {
                return -bits;
            }
        } else {
            if (bits > max)
                printerror("positive wide literal too large in conversion");
            else
                return bits;
        }
        return 0;
    }

//* @brief A specialization for `bool` is needed because the
//* semantics are different than for other conversions.
    template< typename T, std::enable_if_t<std::is_same< T, bool>::value, bool > ENABLED = true >
    constexpr T tointeger () const {
        return !is_zero();
    }

//* @brief A template conversion for integer types.
//*
//* This has an exception, `size_t`, because this has to be the
//* default conversion in contexts where an unspecified integer is
//* expected.
    template< typename T, std::enable_if_t<is_ext_integral<T>::value
                                           && !std::is_same< T, size_t>::value, bool > ENABLED = true >
    constexpr operator T () const {
        return tointeger< T >();
    }

//* @brief A non-template conversion to `size_t` is needed for array
//* contexts where any integer is allowed.
    constexpr operator size_t () const {
        return tointeger< size_t, true >();
    }

//* @brief A template conversion function for floating point types.
//*
//* This one may truncate.
    template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true >
    constexpr T tofloating() const {
        T ret = xtra ? static_cast< T >(-bits) : static_cast< T >(bits);
        T po = 1;
        for (long ex = expo < 0 ? -expo : expo; ex != 0; --ex) {
            po *= radix;
        }
        if (expo < 0) {
            ret /= po;
        } else {
            ret *= po;
        }
        if (xtra) {
            return -ret;
        } else {
            return ret;
        }
    }


//* @brief A template conversion for floating point types.
//*
//* This throws an error if the result would be truncated.
    template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true >
    constexpr operator T () const {
        if (!is_exact< T >())
            printerror("wide number literal losing precision in conversion to floating point type");
        return tofloating< T >();
    }

    template< typename T, bool >
    friend constexpr T trunc(intwide_t const&);

//+++++++++++++++++++++++ unary operators +++++++++++++++++++++++++++++++

//* @brief unary minus
//*
//* This can fail for the minimum of `intwide_t`, because that has
//* no positive correspondent.
    constexpr intwide_t operator-() const noexcept(false) {
// the value for zero stays unchanged
        if (is_zero()) return *this;
        if (xtra && !bits) {
            switch (radix) {
            default:
                printerror("no more digits in wide literal negation");
                return *this;
            case 0:
                printerror("wide literal negation of minimal integer value");
                return *this;
            case 2:
                if (expo == std::numeric_limits< decltype(expo) >::max()) {
                    printerror("wide literal negation of minimal floating value");
                    return *this;
                }
                return intwide_t(false, (std::numeric_limits< decltype(bits) >::max()/2)+1, radix, expo+1);
            }
        }
        return intwide_t(!xtra, -bits, radix, expo);
    }

//* @brief unary plus
    constexpr intwide_t operator+() const noexcept(false) {
        return *this;
    }

    constexpr intwide_t operator~() const noexcept {
        if (radix) {
            printerror("wide floating point literal is not permitted");
            return *this;
        } else {
            return intwide_t(!xtra, ~bits);
        }
    }

    constexpr bool operator!() const noexcept {
        return !xtra && !bits;
    }

//+++++++++++++++++++++++ helpers ++++++++++++++++++++++++++++++++++++++++

    static constexpr uintbits gcd(uintbits a, uintbits b) {
        for (;;) {
            uintbits c = a % b;
            if (!c) return b;
            a = b % c;
            if (!a) return c;
            b = c % a;
            if (!b) return a;
        }
    }

    constexpr intwide_t& normalize() noexcept {
        if (radix && bits) {
            bool sign = xtra;
            if (sign) {
                xtra = false;
                bits = -bits;
            }
            uintbits const bound = std::numeric_limits< decltype(bits) >::max()/radix;
            while ((bits <= bound) && (expo > std::numeric_limits< decltype(expo) >::min())) {
                bits *= radix;
                --expo;
            }
            if (sign) {
                xtra = true;
                bits = -bits;
            }
        }
        return *this;
    }

    static constexpr int cmp(intwide_t a, intwide_t b) noexcept {
        if (a.radix != b.radix) {
            if (!a.radix) a.radix = b.radix;
            else if (!b.radix) b.radix = a.radix;
            else {
                a.printerror("different radix in wide floating point literal comparison, LHS");
                b.printerror("different radix in wide floating point literal comparison, RHS");
            }
        }
        if (a.xtra && !b.xtra) return -1;
        if (!a.xtra && b.xtra) return +1;
        int sign = a.xtra ? -1 : +1;
        a.normalize();
        b.normalize();
        if (a.expo < b.expo) return +sign;
        if (a.expo > b.expo) return -sign;
// now we have (a.expo == b.expo)
        if (a.bits < b.bits) return -1;
        if (a.bits > b.bits) return +1;
// now we have (a.bits == b.bits)
        else                 return  0;
    }

//+++++++++++++++++++++++ binary operators +++++++++++++++++++++++++++++++

    friend constexpr intwide_t operator +(intwide_t, intwide_t);
    friend constexpr intwide_t operator -(intwide_t, intwide_t);
    friend constexpr intwide_t operator *(intwide_t, intwide_t);
    friend constexpr intwide_t operator /(intwide_t, intwide_t);
    friend constexpr intwide_t operator %(intwide_t, intwide_t);
    friend constexpr intwide_t operator >>(intwide_t, intwide_t);
    friend constexpr intwide_t operator <<(intwide_t, intwide_t);

    friend constexpr intwide_t operator xor(intwide_t, intwide_t) noexcept;
    friend constexpr intwide_t operator bitor(intwide_t, intwide_t) noexcept;
    friend constexpr intwide_t operator bitand(intwide_t, intwide_t) noexcept;

    friend constexpr auto operator <=>(intwide_t const&, intwide_t const&) noexcept;

    friend constexpr auto operator ==(intwide_t const&, intwide_t const&) noexcept;
    template< typename T, bool > friend constexpr bool operator ==(intwide_t t, T o) noexcept;
    template< typename T, bool > friend constexpr bool operator ==(T o, intwide_t t) noexcept;
    friend constexpr auto operator !=(intwide_t const&, intwide_t const&) noexcept;
    template< typename T, bool > friend constexpr bool operator !=(intwide_t t, T o) noexcept;
    template< typename T, bool > friend constexpr bool operator !=(T o, intwide_t t) noexcept;
    friend constexpr auto operator <(intwide_t const&, intwide_t const&) noexcept;
    template< typename T, bool > friend constexpr bool operator <(intwide_t t, T o) noexcept;
    template< typename T, bool > friend constexpr bool operator <(T o, intwide_t t) noexcept;
    friend constexpr auto operator >(intwide_t const&, intwide_t const&) noexcept;
    template< typename T, bool > friend constexpr bool operator >(intwide_t t, T o) noexcept;
    template< typename T, bool > friend constexpr bool operator >(T o, intwide_t t) noexcept;
    friend constexpr auto operator <=(intwide_t const&, intwide_t const&) noexcept;
    template< typename T, bool > friend constexpr bool operator <=(intwide_t t, T o) noexcept;
    template< typename T, bool > friend constexpr bool operator <=(T o, intwide_t t) noexcept;
    friend constexpr auto operator >=(intwide_t const&, intwide_t const&) noexcept;
    template< typename T, bool > friend constexpr bool operator >=(intwide_t t, T o) noexcept;
    template< typename T, bool > friend constexpr bool operator >=(T o, intwide_t t) noexcept;

    template< typename T > friend constexpr intwide_t operator <<(intwide_t t, T o);
    template< typename T > friend constexpr intwide_t operator >>(intwide_t t, T o);

//+++++++++++++++++++++++ implement numeric limits for the new type +++++++++++++++++++++++++++++++

    friend constexpr intwide_t std::numeric_limits<intwide_t>::max() noexcept;
    friend constexpr intwide_t std::numeric_limits<intwide_t>::min() noexcept;
    friend constexpr intwide_t std::numeric_limits<intwide_t>::lowest() noexcept;
    friend constexpr intwide_t std::numeric_limits<intwide_t>::epsilon() noexcept;
    friend constexpr intwide_t std::numeric_limits<intwide_t>::round_error() noexcept;
    friend constexpr intwide_t std::numeric_limits<intwide_t>::infinity() noexcept;
    friend constexpr intwide_t std::numeric_limits<intwide_t>::quiet_NaN() noexcept;
    friend constexpr intwide_t std::numeric_limits<intwide_t>::signaling_NaN() noexcept;
    friend constexpr intwide_t std::numeric_limits<intwide_t>::denorm_min() noexcept;

//+++++++++++++++++++++++++++ printing ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    friend inline std::to_chars_result std::to_chars(char*, char*, intwide_t, int base);
public:

//+++++++++++++++++++++++++++ limits and features ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    static const intwide_t zero;
    static const intwide_t \u03C0;
    static const intwide_t e;
    static const intwide_t \u03C6;
    static const intwide_t \u03B3;
    static const intwide_t Ki;
    static const intwide_t Mi;
    static const intwide_t Gi;
    static const intwide_t Ti;
    static const intwide_t Pi;
    static const intwide_t Ei;
    static const intwide_t Zi;
    static const intwide_t Yi;
    static const intwide_t da;
    static const intwide_t h;
    static const intwide_t k;
    static const intwide_t M;
    static const intwide_t G;
    static const intwide_t T;
    static const intwide_t P;
    static const intwide_t E;
    static const intwide_t Z;
    static const intwide_t Y;
    static const intwide_t d;
    static const intwide_t c;
    static const intwide_t m;
    static const intwide_t \u03BC;
    static const intwide_t n;
    static const intwide_t p;
    static const intwide_t f;
    static const intwide_t a;
    static const intwide_t z;
    static const intwide_t y;

    template< unsigned b >
    static constexpr intwide_t max() noexcept {
        zero.printerror("invalid radix for floating point literal");
        return zero;
    }
    template< unsigned b >
    static constexpr intwide_t min() noexcept {
        zero.printerror("invalid radix for floating point literal");
        return zero;
    }
    template< unsigned b >
    static constexpr intwide_t lowest() noexcept {
        zero.printerror("invalid radix for floating point literal");
        return zero;
    }
    template< unsigned b >
    static constexpr intwide_t epsilon() noexcept {
        zero.printerror("invalid radix for floating point literal");
        return zero;
    }

    template< unsigned b >
    static constexpr int digits() noexcept {
        zero.printerror("invalid radix for floating point literal");
        return 0;
    }

    template< unsigned rad = 2, typename T >
    static constexpr long ilog(T const x\u2080) noexcept {
        long ex = 0;
        for(T x\u2082 = x\u2080 < 0 ? -x\u2080 : x\u2080; x\u2082 > 1; x\u2082 /= rad) {
            ++ex;
        }
        return ex;
    }

    template< typename T >
    static constexpr bool is_pow\u2082(T const x\u2080) noexcept {
        return !(x\u2080 & (x\u2080 - 1));
    }

    template< unsigned rad = 2, typename T >
    static constexpr T pow(T x\u2080, long ex) noexcept {
        for(long i = 0; i < ex; ++i) {
            x\u2080 *= rad;
        }
        return x\u2080;
    }


    static const int digits10;


    template< typename T >
    friend constexpr intwide_t inverse(T x\u2080) noexcept;

};

constexpr int intwide_t::digits10 = intwide_t::ilog< 10 >(intwide_t::maximum) - 1;

constexpr intwide_t intwide_t::zero{ 0 };


template<>  constexpr intwide_t intwide_t::max< 0 >() noexcept {
    return intwide_t(false, -1);
}
template<>  constexpr intwide_t intwide_t::lowest< 0 >() noexcept {
    return intwide_t(true, 0);
}
// For integer types min and lowest are the same
template<>  constexpr intwide_t intwide_t::min< 0 >() noexcept {
    return intwide_t(true, 0);
}
template<>  constexpr int intwide_t::digits< 0 >() noexcept {
    return std::numeric_limits< decltype(bits) >::digits;
}

template<>  constexpr intwide_t intwide_t::max< 2 >() noexcept  {
    return intwide_t(false, -1, 2, std::numeric_limits< decltype(expo) >::max());
}
template<>  constexpr intwide_t intwide_t::lowest< 2 >() noexcept {
    return intwide_t(true, 0, 2, std::numeric_limits< decltype(expo) >::max());
}
// For floating types min is the smallest strictly positive value
template<>  constexpr intwide_t intwide_t::min< 2 >() noexcept  {
    return intwide_t(false, 1, 2, std::numeric_limits< decltype(expo) >::min());
}
template<>  constexpr intwide_t intwide_t::max< 10 >() noexcept  {
    return intwide_t(false, -1, 10, std::numeric_limits< decltype(expo) >::max());
}
template<>  constexpr intwide_t intwide_t::lowest< 10 >() noexcept {
    return intwide_t(true, 0, 10, std::numeric_limits< decltype(expo) >::max());
}
// For floating types min is the smallest strictly positive value
template<>  constexpr intwide_t intwide_t::min< 10 >() noexcept  {
    return intwide_t(false, 1, 10, std::numeric_limits< decltype(expo) >::min());
}

template<>  constexpr int intwide_t::digits< 2 >() noexcept {
    return precision;
}
template<>  constexpr intwide_t intwide_t::epsilon< 2 >() noexcept  {
    return intwide_t(false, 1, 2, 1-precision);
}

template<>  constexpr int intwide_t::digits< 10 >() noexcept {
    return intwide_t::digits10;
}
template<>  constexpr intwide_t intwide_t::epsilon< 10 >() noexcept  {
    return intwide_t(false, 1, 10, -digits10);
}

constexpr intwide_t std::numeric_limits<intwide_t>::max() noexcept {
    return intwide_t(false, -1);
}
constexpr intwide_t std::numeric_limits<intwide_t>::min() noexcept {
    return intwide_t(true, 0);
}
constexpr intwide_t std::numeric_limits<intwide_t>::lowest() noexcept {
    return intwide_t(true, 0);
}

constexpr intwide_t std::numeric_limits<intwide_t>::epsilon() noexcept {
    return intwide_t(false, 0);
}
constexpr intwide_t std::numeric_limits<intwide_t>::round_error() noexcept {
    return intwide_t(false, 0);
}
constexpr intwide_t std::numeric_limits<intwide_t>::infinity() noexcept {
    return intwide_t(false, 0);
}
constexpr intwide_t std::numeric_limits<intwide_t>::quiet_NaN() noexcept {
    return intwide_t(false, 0);
}
constexpr intwide_t std::numeric_limits<intwide_t>::signaling_NaN() noexcept {
    return intwide_t(false, 0);
}
constexpr intwide_t std::numeric_limits<intwide_t>::denorm_min() noexcept {
    return intwide_t(false, 0);
}

// Raw literal operators
consteval intwide_t operator ""_W (const char* lit) {
    return intwide_t::convert(lit);
}
consteval intwide_t operator ""_w (const char* lit) {
    return intwide_t::convert(lit);
}

// conversion that cuts off bits if necessary

template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true >
constexpr T trunc(intwide_t const& t) {
    return t.tofloating< T >();
}

constexpr auto operator==(intwide_t const& t, intwide_t const& o) noexcept {
    return intwide_t::cmp(t, o) == 0;
}
constexpr auto operator!=(intwide_t const& t, intwide_t const& o) noexcept {
    return intwide_t::cmp(t, o) != 0;
}
constexpr auto operator<(intwide_t const& t, intwide_t const& o) noexcept {
    return intwide_t::cmp(t, o) < 0;
}
constexpr auto operator<=(intwide_t const& t, intwide_t const& o) noexcept {
    return intwide_t::cmp(t, o) <= 0;
}
constexpr auto operator>(intwide_t const& t, intwide_t const& o) noexcept {
    return intwide_t::cmp(t, o) > 0;
}
constexpr auto operator>=(intwide_t const& t, intwide_t const& o) noexcept {
    return intwide_t::cmp(t, o) >= 0;
}

constexpr auto operator<=>(intwide_t const& t, intwide_t const& o) noexcept {
    return intwide_t::cmp(t, o);
}

//+++++++++++++++++++++++ implementation binary operators +++++++++++++++++++++++++++++++

//* @brief division, can fail for division by zero or the minimum
constexpr intwide_t operator/(intwide_t t, intwide_t o) {
    if (!o.bits) {
        if (!o.xtra) o.printerror("wide number literal division by zero");
        else o.printerror("wide number literal division by minimum value");
    } else {
        if (t.xtra) {
            if (o.xtra)
                return (-t / -o);
            else
                return -(-t / o);
        } else {
            if (o.xtra) {
                return -(t / -o);
            } else {
// special case if both are integer literals
                if (!t.radix && !o.radix) {
                    return intwide_t(false, t.bits / o.bits);
                } else {
// At least one of them is floating point. Put the other one into
// the same radix, if possible.
                    if (!t.radix) t.radix = o.radix;
                    if (!o.radix) o.radix = t.radix;
                    if (o.radix != t.radix) {
                        t.printerror("different radix in wide floating point literal division, LHS");
                        o.printerror("different radix in wide floating point literal division, RHS");
                    } else {
// use intwide_t integers to capture overflow of the following subtraction
                        intwide_t r(false, t.bits, t.radix, intwide_t(t.expo) - intwide_t(o.expo));
                        auto const lim = intwide_t::maximum/t.radix;
                        for (;;) {
// Try to find a common divisor to reduce the RHS
                            intwide_t::uintbits gc = intwide_t::gcd(r.bits, o.bits);
                            r.bits /= gc;
                            o.bits /= gc;
                            if (o.bits <= 1) break;
                            if ((r.bits > lim) || (r.expo == std::numeric_limits< decltype(r.expo) >::min())) {
                                t.printerror("wide floating point literal division, result not representable, LHS");
                                o.printerror("wide floating point literal division, result not representable, RHS");
                                break;
                            }
// Since there still is a RHS, try to expand the LHS
                            r.bits *= r.radix;
                            --r.expo;
                        }
// this will just be wrong if we detected an error above
                        return r;
                    }
                }
            }
        }
    }
    return t;
}

//* @brief division, can fail for overflow
constexpr intwide_t operator*(intwide_t t, intwide_t o) {
    if (t.xtra) {
        if (o.xtra)
            return (-t * -o);
        else
            return -(-t * o);
    } else {
        if (o.xtra) {
            return -(t * -o);
        } else {
// special case if both are integer literals
            if (!t.radix && !o.radix) {
                intwide_t::uintbits res = t.bits * o.bits;
                if ((res/t.bits) == o.bits)
                    return intwide_t(false, res);
                else {
                    t.printerror("overflow in wide integer literal multiplication, LHS");
                    o.printerror("overflow in wide integer literal multiplication, RHS");
                }
            } else {
// At least one of them is floating point. Put the other one into
// the same radix, if possible.
                if (!t.radix) t.radix = o.radix;
                if (!o.radix) o.radix = t.radix;
                if (o.radix != t.radix) {
                    t.printerror("different radix in wide floating point literal multiplication, LHS");
                    o.printerror("different radix in wide floating point literal multiplication, RHS");
                }
// recurse for the integral parts, here both sides are positive
                intwide_t ret = intwide_t(false, t.bits) * intwide_t(false, o.bits);
                ret.radix = t.radix;
// this will just be wrong if we detected an error above
// use intwide_t integers to capture overflow of the following addition
                ret.expo = intwide_t(t.expo) + intwide_t(o.expo);
                return ret;
            }
        }
    }
    return t;
}

//* @brief division, can fail for under- or overflow
constexpr intwide_t operator+(intwide_t t, intwide_t o) {
// special case if both are integer literals
    if (!t.radix && !o.radix) {
        if (t.is_zero()) return o;
        if (o.is_zero()) return t;
// First detect over- or underflow. If the signs are different,
// the result will always be in range.
        if (t.xtra) {
// This is >= because the minimum negative value has bits == 0
            if (o.xtra && (-t.bits >= (intwide_t::maximum+o.bits))) {
                t.printerror("overflow in wide number literal addition, LHS");
                o.printerror("overflow in wide number literal addition, RHS");
            }
        } else {
            if (!o.xtra && (t.bits > (intwide_t::maximum-o.bits))) {
                t.printerror("overflow in wide number literal addition, LHS");
                o.printerror("overflow in wide number literal addition, RHS");
            }
        }
// Then detect the sign bit of the result.
        bool sign =
            t.xtra
            ? (o.xtra
               ? true
               : (-o.bits > t.bits))
            : (o.xtra
               ? (-t.bits > o.bits)
               : false);
        return intwide_t(sign, t.bits+o.bits);
    } else {
// At least one of them is floating point. Put the other one into
// the same radix, if possible.
        if (!t.radix) t.radix = o.radix;
        if (!o.radix) o.radix = t.radix;
        if (o.radix != t.radix) {
            t.printerror("different radix in wide floating point literal addition, LHS");
            o.printerror("different radix in wide floating point literal addition, RHS");
        }
        auto mini = std::min(t.expo, o.expo);
        auto rad = t.radix;
        auto const bound = intwide_t::maximum/rad;
        while (t.expo > mini) {
            if (t.bits > bound) {
                t.printerror("overflow in wide floating point literal addition, LHS cannot be shifted further");
                o.printerror("overflow in wide floating point literal addition, RHS");
                return t;
            }
            t.bits *= rad;
            --t.expo;
        }
        while (o.expo > mini) {
            if (o.bits > bound) {
                o.printerror("overflow in wide floating point literal addition, RHS cannot be shifted further");
                t.printerror("overflow in wide floating point literal addition, LHS");
                return o;
            }
            o.bits *= rad;
            --t.expo;
        }
        t.expo = 0;
        o.expo = 0;
        t.radix = 0;
        o.radix = 0;
        t = t + o;
        t.radix = rad;
        t.expo = mini;
        return t;
    }
}

//* @brief subtraction, delegates to unary minus, equality and
//* addition, but will only error on overflow, not for the minimum
//* value.
constexpr intwide_t operator-(intwide_t t, intwide_t o) {
    if (!o.radix && o == intwide_t::lowest< 0 >())
        return (t + intwide_t(1)) + intwide_t::max< 0 >();
    else
        return t + -o;
}

//* @brief remainder, delegates to division, multiplication and
//* subtraction.
//*
//* Only fails for division by zero.
constexpr intwide_t operator%(intwide_t t, intwide_t o) {
    if (o.radix) {
        o.printerror("remainder of wide floating literal");
        return t;
    }
    if (!o.bits) {
        o.printerror("wide number literal remainder by zero");
        return t;
    }
    intwide_t quot = t / o;
    intwide_t rem = t - (quot * o);
    return rem;
}

//* @brief bitwise and, cannot fail
constexpr intwide_t operator bitand(intwide_t t, intwide_t o) noexcept {
    if (t.radix) {
        t.printerror("LHS, wide floating point literal is not permitted");
        return o;
    }
    if (o.radix) {
        o.printerror("RHS, wide floating point literal is not permitted");
        return t;
    }
    return intwide_t(t.xtra and o.xtra, t.bits bitand o.bits);
}

//* @brief bitwise or, cannot fail
constexpr intwide_t operator bitor(intwide_t t, intwide_t o) noexcept {
    if (t.radix) {
        t.printerror("LHS, wide floating point literal is not permitted");
        return o;
    }
    if (o.radix) {
        o.printerror("RHS, wide floating point literal is not permitted");
        return t;
    }
    return intwide_t(t.xtra or o.xtra, t.bits bitor o.bits);
}

//* @brief bitwise exclusive or, cannot fail
constexpr intwide_t operator xor(intwide_t t, intwide_t o) noexcept {
    if (t.radix) {
        t.printerror("LHS, wide floating point literal is not permitted");
        return o;
    }
    if (o.radix) {
        o.printerror("RHS, wide floating point literal is not permitted");
        return t;
    }
    return intwide_t(t.xtra != o.xtra, t.bits xor o.bits);
}

//* @brief left shift
//
//* negative RHS delegates to right shift
//*
//* Left shift by more than the precision errors out.
//
//* Shift of a positive value errors as soon as the highest
//* significant bit overflows into the sign bit. Similarly, shift of a
//* negative value errors if the highest zero overflows in the sign
//* bit.
constexpr intwide_t operator<<(intwide_t t, intwide_t o) {
    if (t.radix) {
        t.printerror("LHS, wide floating point literal is not permitted");
        return t;
    }
    if (o.radix) {
        o.printerror("RHS, wide floating point literal is not permitted");
        return t;
    }
    if (o.xtra) return t >> -o;
    if (!t || !o) return t;
    const size_t w = o;
    if (w >= intwide_t::precision) o.printerror("left shift too large");
    intwide_t::uintbits hig = (t.xtra ? ~t.bits : t.bits) >> (intwide_t::precision - w);
    if (hig) {
        t.printerror("overflow in wide number literal left shift, LHS");
        o.printerror("overflow in wide number literal left shift, RHS");
    }
    return intwide_t(t.xtra, t.bits << w);
}

template< typename T > constexpr intwide_t operator <<(intwide_t t, T o) {
    if (t.radix) {
        t.printerror("LHS, wide floating point literal is not permitted");
        return t;
    }
    return t << intwide_t((o < 0), o);
}


//* @brief right shift
//*
//* negative RHS delegates to left shift
//*
//* High order bits are filled with zeroes (for positive `t`) and ones
//* (for negative `t`). In particular, if `o` larger or equal to the
//* precision the overall result is zero and minus one, respectively.
constexpr intwide_t operator>>(intwide_t t, intwide_t o) {
    if (t.radix) {
        t.printerror("LHS, wide floating point literal is not permitted");
        return t;
    }
    if (o.radix) {
        o.printerror("RHS, wide floating point literal is not permitted");
        return t;
    }
    if (o.xtra) return t << -o;
    if (t.xtra) t.printerror("right shift of negative wide number literal");
    if (!t || !o) return t;
    const size_t w = o;
    intwide_t::uintbits complement = t.xtra ? -1 : 0;
    if (w >= intwide_t::precision)
        return intwide_t(t.xtra, complement);
    complement <<= (intwide_t::precision - w);
    return intwide_t(t.xtra, complement & (t.bits >> w));
}

template< typename T > constexpr intwide_t operator >>(intwide_t t, T o) {
    if (t.radix) {
        t.printerror("LHS, wide floating point literal is not permitted");
        return t;
    }
    return t >> intwide_t((o < 0), o);
}

template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr T operator +(intwide_t t, T o) {
    T ret = t;
    return ret + o;
}
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr T operator +(T o, intwide_t t) {
    T ret = t;
    return o + ret;
}
template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr T operator +(intwide_t t, T o) {
    T ret = t;
    return ret + o;
}
template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr T operator +(T o, intwide_t t) {
    T ret = t;
    return o + ret;
}
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr T operator -(intwide_t t, T o) {
    T ret = t;
    return ret - o;
}
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr T operator -(T o, intwide_t t) {
    T ret = t;
    return o - ret;
}
template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr T operator -(intwide_t t, T o) {
    T ret = t;
    return ret - o;
}
template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr T operator -(T o, intwide_t t) {
    T ret = t;
    return o - ret;
}
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr T operator *(intwide_t t, T o) {
    T ret = t;
    return ret * o;
}
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr T operator *(T o, intwide_t t) {
    T ret = t;
    return o * ret;
}
template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr T operator *(intwide_t t, T o) {
    T ret = t;
    return ret * o;
}
template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr T operator *(T o, intwide_t t) {
    T ret = t;
    return o * ret;
}
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr T operator /(intwide_t t, T o) {
    T ret = t;
    return ret / o;
}
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr T operator /(T o, intwide_t t) {
    T ret = t;
    return o / ret;
}
template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr T operator /(intwide_t t, T o) {
    T ret = t;
    return ret / o;
}
template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr T operator /(T o, intwide_t t) {
    T ret = t;
    return o / ret;
}

template< typename T, std::enable_if_t<std::is_pointer<T>::value, bool > ENABLED = true > constexpr T operator+ (intwide_t t, T o) {
    return static_cast< long long >(t) + o;
}
template< typename T, std::enable_if_t<std::is_pointer<T>::value, bool > ENABLED = true > constexpr T operator+ (T o, intwide_t t) {
    return o + static_cast< long long >(t);
}
template< typename T, std::enable_if_t<std::is_pointer<T>::value, bool > ENABLED = true > constexpr T operator- (T o, intwide_t t) {
    return o - static_cast< long long >(t);
}

template< typename T > constexpr T operator %(intwide_t t, T o) {
    T ret = t;
    return ret % o;
}
template< typename T > constexpr T operator %(T o, intwide_t t) {
    T ret = t;
    return o % ret;
}
template< typename T > constexpr T operator xor(intwide_t t, T o) {
    T ret = t;
    return ret xor o;
}
template< typename T > constexpr T operator xor(T o, intwide_t t) {
    T ret = t;
    return o xor ret;
}
template< typename T > constexpr T operator bitor(intwide_t t, T o) {
    T ret = t;
    return ret bitor o;
}
template< typename T > constexpr T operator bitor(T o, intwide_t t) {
    T ret = t;
    return o bitor ret;
}
template< typename T > constexpr T operator bitand(intwide_t t, T o) {
    T ret = t;
    return ret bitand o;
}
template< typename T > constexpr T operator bitand(T o, intwide_t t) {
    T ret = t;
    return o bitand ret;
}

template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr T& operator -=(T& o, intwide_t t) {
    T ret = t;
    return o -= ret;
}
template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr T& operator -=(T& o, intwide_t t) {
    T ret = t;
    return o -= ret;
}
template< typename T, std::enable_if_t<std::is_pointer<T>::value, bool > ENABLED = true > constexpr T& operator -=(T& o, intwide_t t) {
    return o -= static_cast< long long >(t);
}
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr T& operator *=(T& o, intwide_t t) {
    T ret = t;
    return o *= ret;
}
template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr T& operator *=(T& o, intwide_t t) {
    T ret = t;
    return o *= ret;
}
template< typename T, std::enable_if_t<std::is_pointer<T>::value, bool > ENABLED = true > constexpr T& operator *=(T& o, intwide_t t) {
    return o *= static_cast< long long >(t);
}
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr T& operator /=(T& o, intwide_t t) {
    T ret = t;
    return o /= ret;
}
template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr T& operator /=(T& o, intwide_t t) {
    T ret = t;
    return o /= ret;
}
template< typename T, std::enable_if_t<std::is_pointer<T>::value, bool > ENABLED = true > constexpr T& operator /=(T& o, intwide_t t) {
    return o /= static_cast< long long >(t);
}

template< typename T > constexpr T& operator xor_eq(T& o, intwide_t t) {
    T ret = t;
    return o xor_eq ret;
}
template< typename T > constexpr T& operator or_eq(T& o, intwide_t t) {
    T ret = t;
    return o or_eq ret;
}
template< typename T > constexpr T& operator and_eq(T& o, intwide_t t) {
    T ret = t;
    return o and_eq ret;
}
template< typename T > constexpr T& operator >>=(T& o, intwide_t t) {
    T ret = t;
    return o >>= ret;
}
template< typename T > constexpr T& operator <<=(T& o, intwide_t t) {
    T ret = t;
    return o <<= ret;
}

template< typename T > constexpr T operator <<(T o, intwide_t t) {
    T ret = t;
    return o << ret;
}
template< typename T > constexpr T operator >>(T o, intwide_t t) {
    T ret = t;
    return o >> ret;
}


//* @brief Relational operators with one wide integer literal always
//* provide the correct mathematical results, no errors are possible.
//*
//* In contrast to the arithmetic operators that are defined above, this
//* operator may be executed repeatedly during the program. But this
//* can only happen with a `w` that is known at compile time.
//*
//* The algorithm optimized for that case. Namely the first `if`, can
//* be determined at compile-time. Then it is down to either
//* - a normal comparison within the range of `T`, we cannot expect much better
//* - or, an `intwide_t` comparison for which we know that it is
//*   constant, because `w` lies either left or right of the complete
//*   interval of type `T`. Thus result is the same if we do not use `s`
//*   but zero for the comparison.
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr bool operator ==(intwide_t const& w, T s) noexcept {
// is compile time evaluated for any `w`
    if (w.in_range< T >())
// Normal comparison for type `T`
        return static_cast< T >(w) == s;
    else
        return w == intwide_t::zero;
}

//* @brief Relational operators with one wide integer literal always
//* provide the correct mathematical results, no errors are possible.
//*
//* In contrast to the arithmetic operators that are defined above, this
//* operator may be executed repeatedly during the program. But this
//* can only happen with a `w` that is known at compile time.
//*
//* The algorithm optimized for that case. Namely the first `if`, can
//* be determined at compile-time. Then it is down to either
//* - a normal comparison within the range of `T`, we cannot expect much better
//* - or, an `intwide_t` comparison for which we know that it is
//*   constant, because `w` lies either left or right of the complete
//*   interval of type `T`. Thus result is the same if we do not use `s`
//*   but zero for the comparison.
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr bool operator ==(T s, intwide_t const& w) noexcept {
// is compile time evaluated for any `w`
    if (w.in_range< T >())
// Normal comparison for type `T`
        return s == static_cast< T >(w);
    else
        return intwide_t::zero == w;
}

//* @brief Relational operators with one wide integer literal always
//* provide the correct mathematical results, no errors are possible.
//*
//* In contrast to the arithmetic operators that are defined above, this
//* operator may be executed repeatedly during the program. But this
//* can only happen with a `w` that is known at compile time.
//*
//* The algorithm optimized for that case. Namely the first `if`, can
//* be determined at compile-time. Then it is down to either
//* - a normal comparison within the range of `T`, we cannot expect much better
//* - or, an `intwide_t` comparison for which we know that it is
//*   constant, because `w` lies either left or right of the complete
//*   interval of type `T`. Thus result is the same if we do not use `s`
//*   but zero for the comparison.
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr bool operator !=(intwide_t const& w, T s) noexcept {
// is compile time evaluated for any `w`
    if (w.in_range< T >())
// Normal comparison for type `T`
        return static_cast< T >(w) != s;
    else
        return w != intwide_t::zero;
}

//* @brief Relational operators with one wide integer literal always
//* provide the correct mathematical results, no errors are possible.
//*
//* In contrast to the arithmetic operators that are defined above, this
//* operator may be executed repeatedly during the program. But this
//* can only happen with a `w` that is known at compile time.
//*
//* The algorithm optimized for that case. Namely the first `if`, can
//* be determined at compile-time. Then it is down to either
//* - a normal comparison within the range of `T`, we cannot expect much better
//* - or, an `intwide_t` comparison for which we know that it is
//*   constant, because `w` lies either left or right of the complete
//*   interval of type `T`. Thus result is the same if we do not use `s`
//*   but zero for the comparison.
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr bool operator !=(T s, intwide_t const& w) noexcept {
// is compile time evaluated for any `w`
    if (w.in_range< T >())
// Normal comparison for type `T`
        return s != static_cast< T >(w);
    else
        return intwide_t::zero != w;
}

//* @brief Relational operators with one wide integer literal always
//* provide the correct mathematical results, no errors are possible.
//*
//* In contrast to the arithmetic operators that are defined above, this
//* operator may be executed repeatedly during the program. But this
//* can only happen with a `w` that is known at compile time.
//*
//* The algorithm optimized for that case. Namely the first `if`, can
//* be determined at compile-time. Then it is down to either
//* - a normal comparison within the range of `T`, we cannot expect much better
//* - or, an `intwide_t` comparison for which we know that it is
//*   constant, because `w` lies either left or right of the complete
//*   interval of type `T`. Thus result is the same if we do not use `s`
//*   but zero for the comparison.
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr bool operator <(intwide_t const& w, T s) noexcept {
// is compile time evaluated for any `w`
    if (w.in_range< T >())
// Normal comparison for type `T`
        return static_cast< T >(w) < s;
    else
        return w < intwide_t::zero;
}

//* @brief Relational operators with one wide integer literal always
//* provide the correct mathematical results, no errors are possible.
//*
//* In contrast to the arithmetic operators that are defined above, this
//* operator may be executed repeatedly during the program. But this
//* can only happen with a `w` that is known at compile time.
//*
//* The algorithm optimized for that case. Namely the first `if`, can
//* be determined at compile-time. Then it is down to either
//* - a normal comparison within the range of `T`, we cannot expect much better
//* - or, an `intwide_t` comparison for which we know that it is
//*   constant, because `w` lies either left or right of the complete
//*   interval of type `T`. Thus result is the same if we do not use `s`
//*   but zero for the comparison.
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr bool operator <(T s, intwide_t const& w) noexcept {
// is compile time evaluated for any `w`
    if (w.in_range< T >())
// Normal comparison for type `T`
        return s < static_cast< T >(w);
    else
        return intwide_t::zero < w;
}

//* @brief Relational operators with one wide integer literal always
//* provide the correct mathematical results, no errors are possible.
//*
//* In contrast to the arithmetic operators that are defined above, this
//* operator may be executed repeatedly during the program. But this
//* can only happen with a `w` that is known at compile time.
//*
//* The algorithm optimized for that case. Namely the first `if`, can
//* be determined at compile-time. Then it is down to either
//* - a normal comparison within the range of `T`, we cannot expect much better
//* - or, an `intwide_t` comparison for which we know that it is
//*   constant, because `w` lies either left or right of the complete
//*   interval of type `T`. Thus result is the same if we do not use `s`
//*   but zero for the comparison.
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr bool operator >(intwide_t const& w, T s) noexcept {
// is compile time evaluated for any `w`
    if (w.in_range< T >())
// Normal comparison for type `T`
        return static_cast< T >(w) > s;
    else
        return w > intwide_t::zero;
}

//* @brief Relational operators with one wide integer literal always
//* provide the correct mathematical results, no errors are possible.
//*
//* In contrast to the arithmetic operators that are defined above, this
//* operator may be executed repeatedly during the program. But this
//* can only happen with a `w` that is known at compile time.
//*
//* The algorithm optimized for that case. Namely the first `if`, can
//* be determined at compile-time. Then it is down to either
//* - a normal comparison within the range of `T`, we cannot expect much better
//* - or, an `intwide_t` comparison for which we know that it is
//*   constant, because `w` lies either left or right of the complete
//*   interval of type `T`. Thus result is the same if we do not use `s`
//*   but zero for the comparison.
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr bool operator >(T s, intwide_t const& w) noexcept {
// is compile time evaluated for any `w`
    if (w.in_range< T >())
// Normal comparison for type `T`
        return s > static_cast< T >(w);
    else
        return intwide_t::zero > w;
}

//* @brief Relational operators with one wide integer literal always
//* provide the correct mathematical results, no errors are possible.
//*
//* In contrast to the arithmetic operators that are defined above, this
//* operator may be executed repeatedly during the program. But this
//* can only happen with a `w` that is known at compile time.
//*
//* The algorithm optimized for that case. Namely the first `if`, can
//* be determined at compile-time. Then it is down to either
//* - a normal comparison within the range of `T`, we cannot expect much better
//* - or, an `intwide_t` comparison for which we know that it is
//*   constant, because `w` lies either left or right of the complete
//*   interval of type `T`. Thus result is the same if we do not use `s`
//*   but zero for the comparison.
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr bool operator <=(intwide_t const& w, T s) noexcept {
// is compile time evaluated for any `w`
    if (w.in_range< T >())
// Normal comparison for type `T`
        return static_cast< T >(w) <= s;
    else
        return w <= intwide_t::zero;
}

//* @brief Relational operators with one wide integer literal always
//* provide the correct mathematical results, no errors are possible.
//*
//* In contrast to the arithmetic operators that are defined above, this
//* operator may be executed repeatedly during the program. But this
//* can only happen with a `w` that is known at compile time.
//*
//* The algorithm optimized for that case. Namely the first `if`, can
//* be determined at compile-time. Then it is down to either
//* - a normal comparison within the range of `T`, we cannot expect much better
//* - or, an `intwide_t` comparison for which we know that it is
//*   constant, because `w` lies either left or right of the complete
//*   interval of type `T`. Thus result is the same if we do not use `s`
//*   but zero for the comparison.
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr bool operator <=(T s, intwide_t const& w) noexcept {
// is compile time evaluated for any `w`
    if (w.in_range< T >())
// Normal comparison for type `T`
        return s <= static_cast< T >(w);
    else
        return intwide_t::zero <= w;
}

//* @brief Relational operators with one wide integer literal always
//* provide the correct mathematical results, no errors are possible.
//*
//* In contrast to the arithmetic operators that are defined above, this
//* operator may be executed repeatedly during the program. But this
//* can only happen with a `w` that is known at compile time.
//*
//* The algorithm optimized for that case. Namely the first `if`, can
//* be determined at compile-time. Then it is down to either
//* - a normal comparison within the range of `T`, we cannot expect much better
//* - or, an `intwide_t` comparison for which we know that it is
//*   constant, because `w` lies either left or right of the complete
//*   interval of type `T`. Thus result is the same if we do not use `s`
//*   but zero for the comparison.
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr bool operator >=(intwide_t const& w, T s) noexcept {
// is compile time evaluated for any `w`
    if (w.in_range< T >())
// Normal comparison for type `T`
        return static_cast< T >(w) >= s;
    else
        return w >= intwide_t::zero;
}

//* @brief Relational operators with one wide integer literal always
//* provide the correct mathematical results, no errors are possible.
//*
//* In contrast to the arithmetic operators that are defined above, this
//* operator may be executed repeatedly during the program. But this
//* can only happen with a `w` that is known at compile time.
//*
//* The algorithm optimized for that case. Namely the first `if`, can
//* be determined at compile-time. Then it is down to either
//* - a normal comparison within the range of `T`, we cannot expect much better
//* - or, an `intwide_t` comparison for which we know that it is
//*   constant, because `w` lies either left or right of the complete
//*   interval of type `T`. Thus result is the same if we do not use `s`
//*   but zero for the comparison.
template< typename T, std::enable_if_t<is_ext_integral<T>::value, bool > ENABLED = true > constexpr bool operator >=(T s, intwide_t const& w) noexcept {
// is compile time evaluated for any `w`
    if (w.in_range< T >())
// Normal comparison for type `T`
        return s >= static_cast< T >(w);
    else
        return intwide_t::zero >= w;
}

//* @brief Equality of one wide integer literal with a floating point
//* always provides the correct mathematical results, no errors are
//* possible.
//*
//* In particular, if the literal has no exact representation or is
//* out of range of the target type, the operator will always return
//* `false`. Since the literal is considered to be known at compile
//* time, for such literals the equality operator can always
//* completely be optimized.
//*
//* The only other case that is left is that the literal converts
//* exactly to the target floating point type. Then we do this
//* conversion exactly once at compile time, and reduce the runtime
//* comparison to the usual comparison of the floating point type.
template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr bool operator ==(intwide_t const& w, T s) noexcept {
// is compile time evaluated for any `w`
    if (w.is_exact< T >() && w.in_range< T >())
// Normal comparison for type `T`
        return static_cast< T >(w) == s;
    else
        return false;
}

template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr bool operator ==(T s, intwide_t const& w) noexcept {
    return w == s;
}

template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr bool operator !=(intwide_t const& w, T s) noexcept {
    return !(w == s);
}

template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr bool operator !=(T s, intwide_t const& w) noexcept {
    return !(w == s);
}


//* @brief Strictly less operator with one wide integer literal and
//* the other one a floating point always provides the correct
//* mathematical results, no errors are possible.
//*
//* Special care is taken such that rounding errors when converting to
//* the floating point type fall on the correct side of the
//* comparison.
//*
//* In contrast to the arithmetic operators that are defined above, this
//* operator may be executed repeatedly during the program. But this
//* can only happen with a `w` that is known at compile time.
//*
//* As for comparison operators with other integer types, this should
//* reduce to comparison within the target type, for the cases where
//* this is delayed to execution time.
template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr bool operator< (intwide_t const& w, T s) noexcept {
// is compile time evaluated for any `w`
    if (w.in_range< T >()) {
        if (w.is_negative()) {
            if (trunc< T >(w) == s)
// If negative `w` truncates to `s`, it is strictly less if it
// is not exactly converted.
                return !w.is_exact< T >();
            else
// If negative `w` does not truncate to `s`, we are on the
// safe side.
                return trunc< T >(w) < s;
        } else {
// Normal comparison for type `T`. If positive `w` truncates to
// `s`, it cannot be strictly less, so we are on the safe
// side.
            return trunc< T >(w) < s;
        }
    } else {
// `w` can be compared to any element in the range of `T`.
        return w.is_negative();
    }
}

//* @brief Strictly greater operator with one wide integer literal and
//* the other one a floating point always provides the correct
//* mathematical results, no errors are possible.
//*
//* Special care is taken such that rounding errors when converting to
//* the floating point type fall on the correct side of the
//* comparison.
//*
//* In contrast to the arithmetic operators that are defined above, this
//* operator may be executed repeatedly during the program. But this
//* can only happen with a `w` that is known at compile time.
//*
//* As for comparison operators with other integer types, this should
//* reduce to comparison within the target type, for the cases where
//* this is delayed to execution time.
template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr bool operator> (intwide_t const& w, T s) noexcept {
// is compile time evaluated for any `w`
    if (w.in_range< T >()) {
        if (w.is_negative()) {
// Normal comparison for type `T`.  If negative `w` truncates to
// `s`, it cannot be strictly greater, so we are on the safe
// side.
            return trunc< T >(w) > s;
        } else {
            if (trunc< T >(w) == s)
// If postive `w` truncates to `s`, it is strictly greater if
// it is not exactly converted.
                return !w.is_exact< T >();
            else
// If positive `w` does not truncate to `s`, we are on the
// safe side.
                return trunc< T >(w) > s;
        }
    } else {
// `w` can be compared to any element in the range of `T`.
        return w.is_positive();
    }
}

template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr bool operator< (T s, intwide_t const& w) noexcept {
    return w > s;
}

template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr bool operator> (T s, intwide_t const& w) noexcept {
    return w < s;
}

template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr bool operator<= (intwide_t const& w, T s) noexcept {
    return (w < s) || (w == s);
}

template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr bool operator>= (intwide_t const& w, T s) noexcept {
    return (w > s) || (w == s);
}

template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr bool operator<= (T s, intwide_t const& w) noexcept {
    return w >= s;
}

template< typename T, std::enable_if_t<std::is_floating_point<T>::value, bool > ENABLED = true > constexpr bool operator>= (T s, intwide_t const& w) noexcept {
    return w <= s;
}

namespace std {
inline
to_chars_result to_chars(char* first, char* last, intwide_t value, int base = 10) {
    intwide_t::uintbits bits = value.bits;
    if (first != last) {
        if (value.xtra) {
            if (!bits) {
                static char const minf[] = "-\u221E";
                to_chars_result res{ strncpy(first, minf, last-first) };
                if (!res.ptr) {
                    res.ec = std::errc::result_out_of_range;
                }
                return res;
            } else {
                bits = -bits;
            }
            *first = '-';
            first++;
        }
        switch (base) {
        case 2:
            if (last - first > 2) {
                first[0] = '0';
                first[1] = 'b';
                first += 2;
            }
            break;
        case 16:
            if (last - first > 2) {
                first[0] = '0';
                first[1] = 'x';
                first += 2;
            }
            break;
        case 32:
            if (last - first > 2) {
                first[0] = '0';
                first[1] = 'y';
                first += 2;
            }
            break;
        case 8:
            if (last - first > 1 && value) {
                first[0] = '0';
                first += 1;
            }
            break;
        }
        to_chars_result ret = to_chars(first, last, bits, base);
        if (ret.ptr) {
            if (base == 32) {
                unsigned zeroes = 0;
                char* p = ret.ptr - 1;
                for (; p != first; p--) {
                    if (*p == '0') {
                        *p = 0;
                        ++zeroes;
                    } else {
                        break;
                    }
                }
                if (zeroes > 1) {
                    p++;
                    *p = 'y';
                    ret = to_chars(p+1, last, zeroes, base);
                } else if (zeroes == 1) {
                    ret = to_chars(p+1, last, 0U, base);
                }
            }
            ret.ptr[0] = 0;
        }
        return ret;
    }
    return to_chars_result();
}
}

constexpr intwide_t intwide_t::\u03C0 = 0x3.243f6a8885a308d313198a2e0370734P0_w;
constexpr intwide_t intwide_t::e = 2.71828182845904523536028747135266249775_w;
constexpr intwide_t intwide_t::\u03C6 = 1.61803398874989484820458683436563811772_w;
constexpr intwide_t intwide_t::\u03B3 = 0.57721566490153286060651209008240243104_w;

constexpr intwide_t intwide_t::Ki{ false, 1, 2, 10};
constexpr intwide_t intwide_t::Mi{ false, 1, 2, 20};
constexpr intwide_t intwide_t::Gi{ false, 1, 2, 30};
constexpr intwide_t intwide_t::Ti{ false, 1, 2, 40};
constexpr intwide_t intwide_t::Pi{ false, 1, 2, 50};
constexpr intwide_t intwide_t::Ei{ false, 1, 2, 60};
constexpr intwide_t intwide_t::Zi{ false, 1, 2, 70};
constexpr intwide_t intwide_t::Yi{ false, 1, 2, 80};

constexpr intwide_t intwide_t::da = []() {
    intwide_t ret = 10_w;
    ret.radix = 2;
    return ret;
}
();
constexpr intwide_t intwide_t::h = []() {
    intwide_t ret = 100_w;
    ret.radix = 2;
    return ret;
}
();
constexpr intwide_t intwide_t::k = []() {
    intwide_t ret = 1000_w;
    ret.radix = 2;
    return ret;
}
();
constexpr intwide_t intwide_t::M = intwide_t::k * intwide_t::k;
constexpr intwide_t intwide_t::G = intwide_t::k * intwide_t::M;
constexpr intwide_t intwide_t::T = intwide_t::k * intwide_t::G;
constexpr intwide_t intwide_t::P = intwide_t::k * intwide_t::T;
constexpr intwide_t intwide_t::E = intwide_t::k * intwide_t::P;
constexpr intwide_t intwide_t::Z = intwide_t::k * intwide_t::E;
constexpr intwide_t intwide_t::Y = intwide_t::k * intwide_t::Z;

template< typename T >
constexpr intwide_t inverse(T x\u2080) noexcept {
    bool const sign = (x\u2080 < 0);
    if (sign) x\u2080 = -x\u2080;
    intwide_t::uintbits quo = 1;
    long ex = -intwide_t::precision;;
// if x\u2080 is a power of two things are simple
    if (intwide_t::is_pow\u2082(x\u2080)) {
        ex = -intwide_t::ilog(x\u2080);
    } else {
// if it is not, this is as a good as (maximum+1)/x\u2080
        quo = (intwide_t::maximum / x\u2080);
// +1 to compensate for not taking maximum+1
        intwide_t::uintbits rem = (intwide_t::maximum % x\u2080)+1;
// Shift the result to the left to gain as much bits as possible in
// the mantissa. The lower order bits are filled with the
// information that we have from the remainder.
        for (T p\u2082 = 1, x\u2082 = x\u2080; quo <= intwide_t::maximum/2; ) {
            quo *= 2;
            p\u2082 *= 2;
            x\u2082 /= 2;
            --ex;
// quo is now even, test for the parity bit
            if (rem >= x\u2082) {
                ++quo;
                rem = (p\u2082*rem - x\u2080)/p\u2082;
            }
        }
    }
    return intwide_t(sign, sign ? -quo : quo, 2, ex);
};


constexpr intwide_t intwide_t::d = inverse(10);
constexpr intwide_t intwide_t::c = inverse(100);
constexpr intwide_t intwide_t::m = inverse(1000);
constexpr intwide_t intwide_t::\u03BC = inverse(1000000);
constexpr intwide_t intwide_t::n = inverse(1000000000);
constexpr intwide_t intwide_t::p = inverse(1000000000000);
constexpr intwide_t intwide_t::f = inverse(1000000000000000);
constexpr intwide_t intwide_t::a = inverse(1000000000000000000);
constexpr intwide_t intwide_t::z = inverse((intwide_t::uintbits)1000000000000*1000000000);
constexpr intwide_t intwide_t::y = inverse((intwide_t::uintbits)1000000000000*1000000000000);

#endif
