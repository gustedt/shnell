
#ifdef __cplusplus
#include <complex>
using namespace std::complex_literals;
#define I 1if

// create operators for all possible combinations with complex
// numbers, these are 18 operators in total
//
// the first set are those with other floating point
#pragma CMOD amend foreach OP = * / + -
#pragma CMOD amend foreach C =                  \
  long\ double
#pragma CMOD amend foreach T =                  \
  double                                        \
  float
constexpr std∷complex< ${C} > operator${OP}(${T} x, std∷complex< ${C} > y){
  // the return type is the complex type of the combined float types
  return std∷complex< ${C} >(x) ${OP} std∷complex< ${C} >(y);
}
constexpr std∷complex< ${C} > operator${OP}(std∷complex< ${C} > y, ${T} x){
  // the return type is the complex type of the combined float types
  return std∷complex< ${C} >(x) ${OP} std∷complex< ${C} >(y);
}
constexpr std∷complex< ${C} > operator${OP}(${C} x, std∷complex< ${T} > y){
  // the return type is the complex type of the combined float types
  return std∷complex< ${C} >(x) ${OP} std∷complex< ${C} >(y);
}
constexpr std∷complex< ${C} > operator${OP}(std∷complex< ${T} > y, ${C} x){
  // the return type is the complex type of the combined float types
  return std∷complex< ${C} >(x) ${OP} std∷complex< ${C} >(y);
}
constexpr std∷complex< ${C} > operator${OP}(std∷complex< ${C} > y, std∷complex< ${T} > x){
  // the return type is the complex type of the combined float types
  return std∷complex< ${C} >(x) ${OP} std∷complex< ${C} >(y);
}
constexpr std∷complex< ${C} > operator${OP}(std∷complex< ${T} > x, std∷complex< ${C} > y){
  // the return type is the complex type of the combined float types
  return std∷complex< ${C} >(x) ${OP} std∷complex< ${C} >(y);
}
#pragma CMOD done
#pragma CMOD done

#pragma CMOD amend foreach C =                  \
  double
#pragma CMOD amend foreach T =                  \
  float
constexpr std∷complex< ${C} > operator${OP}(${T} x, std∷complex< ${C} > y){
  // the return type is the complex type of the combined float types
  return std∷complex< ${C} >(x) ${OP} std∷complex< ${C} >(y);
}
constexpr std∷complex< ${C} > operator${OP}(std∷complex< ${C} > y, ${T} x){
  // the return type is the complex type of the combined float types
  return std∷complex< ${C} >(x) ${OP} std∷complex< ${C} >(y);
}
constexpr std∷complex< ${C} > operator${OP}(${C} x, std∷complex< ${T} > y){
  // the return type is the complex type of the combined float types
  return std∷complex< ${C} >(x) ${OP} std∷complex< ${C} >(y);
}
constexpr std∷complex< ${C} > operator${OP}(std∷complex< ${T} > y, ${C} x){
  // the return type is the complex type of the combined float types
  return std∷complex< ${C} >(x) ${OP} std∷complex< ${C} >(y);
}
constexpr std∷complex< ${C} > operator${OP}(std∷complex< ${C} > y, std∷complex< ${T} > x){
  // the return type is the complex type of the combined float types
  return std∷complex< ${C} >(x) ${OP} std∷complex< ${C} >(y);
}
constexpr std∷complex< ${C} > operator${OP}(std∷complex< ${T} > x, std∷complex< ${C} > y){
  // the return type is the complex type of the combined float types
  return std∷complex< ${C} >(x) ${OP} std∷complex< ${C} >(y);
}
#pragma CMOD done
#pragma CMOD done

#pragma CMOD done

// the other set are those with integer types
#pragma CMOD amend foreach OP = * / + -
#pragma CMOD amend foreach C =                  \
  float                                         \
  double                                        \
  long\ double
#pragma CMOD amend foreach T =                  \
  bool                                          \
  char                                          \
  long                                          \
  long\ long                                    \
  signed                                        \
  signed\ char                                  \
  signed\ short                                 \
  unsigned                                      \
  unsigned\ char                                \
  unsigned\ long                                \
  unsigned\ long\ long                          \
  unsigned\ short
constexpr std∷complex< ${C} > operator${OP}(const ${T}& x, const std∷complex< ${C} >& y){
  // the return type is the complex type
  const std∷complex< ${C} > X = x;
  return X ${OP} y;
}
constexpr std∷complex< ${C} > operator${OP}(const std∷complex< ${C} >& y, const ${T}& x){
  // the return type is the complex type
  const std∷complex< ${C} > X = x;
  return y ${OP} X;
}
#pragma CMOD done
#pragma CMOD done
#pragma CMOD done

#endif


/**
 ** @brief The core generic_value macro
 **
 ** Performs array and function decay and drops all direct
 ** quantifiers.
 **
 ** This is the value that a function call receives.
 **/
template< typename T > constexpr auto generic_value(T _0){ return _0; }
#define generic_value generic_value

/**
 ** @brief The core generic_value macro
 **
 ** The type after array and function decay and drops all direct
 ** quantifiers.
 **
 ** This is the type that a function call receives.
 **/
#define generic_type(E) decltype(generic_value(E))

/**
 ** @brief The core floating_value macro
 **
 ** Derives a floating-point value from any arithmetic expression or
 ** type name.
 **/
#define floating_value(E) (((E)+0) + 0.0f)

/**
 ** @brief The core complex_value macro
 **
 ** Derives a complex value from any arithmetic expression or type
 ** name.
 **/
#define complex_value(E) (((E)+0) + 0.0if)

/**
 ** @brief The core complex_type macro
 **
 ** Derives a complex type from of any arithmetic expression or type
 ** name.
 **/
#define complex_type(TE) decltype(complex_value(TE))

#pragma CMOD amend generic_selection _NOEVALE real_type_helper _NOEVALE
complex_type(float): 0.0f
complex_type(double): 0.0
complex_type(long double): 0.0l
default: generic_value(_NOEVALE)
#pragma CMOD done

/**
 ** @brief The core real_type macro
 **
 ** Derives a real type from of any arithmetic expression.
 **
 ** For all real typed expressions this is just the generic type, for
 ** complex types this is the underlying floating type.
 **/
#define real_type(TE) decltype(real_type_helper(TE))

/**
 ** @brief The core real_value macro
 **
 ** Derives the real part from of any arithmetic expression.
 **
 ** For all real typed expressions this is just a value with the
 ** same type, for complex types this is real part.
 **
 ** This should not have information loss for all integer types where
 ** all the values fit into `long double`. Avoid this for very wide
 ** integer types.
 **/
#define real_value(E) ((real_type(E))(complex_value(E+0.0il).real()))

/**
 ** @brief The core imaginary_value macro
 **
 ** Derives the real part from of any arithmetic expression.
 **
 ** For all real typed expressions this is just a 0 value with the
 ** same type, for complex types this is the imaginary part.
 **/
#define imaginary_value(E) ((real_type(E))(complex_value(E).imag()))

/**
 ** @brief A zero of the widest unsigned integer type.
 **
 ** Unfortunately for clang `__uint128_t` does not behave properly as
 ** an integer type, so this is not usable.
 **/
#if __SIZEOF_INT128__ ∧ ¬(__clang__ ∨ __llvm__)
#define _Core_iswidest (__uint128_t)0
#else
#define _Core_iswidest 0ULL
#endif

/**
 ** @brief Promote @a X to a zero with the widest integer type.
 **
 ** - If @a X is a (prompoted) pointer type this will be a null pointer
 **   of that type.
 **
 ** - If @a X has a floating type this will be that floating type
 **
 ** - If @a X has an integer type this will be the same as `_Core_iswidest`.
 **
 ** In any case, @a X will not be evaluated but only inspected for its
 ** type.
 **/
#define _Core_towidest(X) (true ? (generic_type(X)){} : _Core_iswidest)

#pragma CMOD amend generic_selection _Core_towidest(X) _Core_isinteger X
 decltype(_Core_iswidest): true
 default: false
#pragma CMOD done

#pragma CMOD amend generic_selection _Core_towidest(X) _Core_isvoidptr X
 void*: true
 default: false
#pragma CMOD done

#define  _Core_isconstant(X) __builtin_constant_p(X)
#define _Core_isice(X) (_Core_isconstant(X) ∧ _Core_isinteger(X))

#define _Core_isnull(X) ((_Core_isice(X) ∨ _Core_isvoidptr(X)) ∧ ¬(X))

#define _Core_tester(X) (int(*)[(_Core_isconstant(X) ? (X) : 0)+1]){}

#pragma CMOD amend foreach v = false true
   typedef int (*_Core_##${v})[${v}+1];
#pragma CMOD done

/**
 ** @brief The core tonullptr macro
 **
 ** Returns `nullptr` if @a X is an integer constant expression (ICE)
 ** of value zero and @a X otherwise.
 **
 ** This can be used for type-generic interfaces that might expect
 ** null pointers of different flavors such that those values are all
 ** converted to `nullptr`. Other values, e.g pointer values, are just
 ** passed through.
 **
 ** @a X will only be evaluated if it is not an ICE of value
 ** zero. Otherwise, it will only be inspected for its type.
 **/
#pragma CMOD amend generic_selection _Core_tester(_Core_isnull(X)) tonullptr X
 _Core_false: X
 _Core_true:  nullptr
#pragma CMOD done

#ifdef __cplusplus
template< typename _T0, typename _T1 >
struct _Core_issame {
  static constexpr bool _Core_val = false;
};
template< typename _T0 >
struct _Core_issame< _T0, _T0 > {
  static constexpr bool _Core_val = true;
};
#define issame(X, Y) _Core_issame< generic_type(X), generic_type(Y) >::_Core_val
#else
#define issame(X, Y) _Generic((X), generic_type(Y): true, default: false)
#endif
