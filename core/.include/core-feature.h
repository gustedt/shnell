#line 1 "phase2"
// helper macros
#ifndef __LINENO
#define __LINENO(X) X##LL
#define __LINENO_(X) __LINENO(X)
#define __line(X) #X
#define __line_(X) __line(X)
// @brief use this instead of __LINE__ where you need an integer
#define __LINENO__ __LINENO_(__LINE__)
// @brief use this instead of __LINE__ where you need a string
#define __line__ __line_(__LINE__)
#endif
#line 1 "core-feature.c"

#ifdef __cplusplus
#include <complex>
#line 4 "core-feature.c"
using namespace std::complex_literals;
#define I 1if
#line 6 "core-feature.c"

// create operators for all possible combinations with complex
// numbers, these are 18 operators in total
//
// the first set are those with other floating point
#line 12 "core-feature.c"
#line 14 "core-feature.c"
#line 17 "core-feature.c"
constexpr std::complex< long double > operator*(double x, std::complex< long double > y) {
#line 18 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) * std::complex< long double >(y);
}
#line 21 "core-feature.c"
constexpr std::complex< long double > operator*(std::complex< long double > y, double x) {
#line 22 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) * std::complex< long double >(y);
}
#line 25 "core-feature.c"
constexpr std::complex< long double > operator*(long double x, std::complex< double > y) {
#line 26 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) * std::complex< long double >(y);
}
#line 29 "core-feature.c"
constexpr std::complex< long double > operator*(std::complex< double > y, long double x) {
#line 30 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) * std::complex< long double >(y);
}
#line 33 "core-feature.c"
constexpr std::complex< long double > operator*(std::complex< long double > y, std::complex< double > x) {
#line 34 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) * std::complex< long double >(y);
}
#line 37 "core-feature.c"
constexpr std::complex< long double > operator*(std::complex< double > x, std::complex< long double > y) {
#line 38 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) * std::complex< long double >(y);
}
#line 17 "core-feature.c"
constexpr std::complex< long double > operator*(float x, std::complex< long double > y) {
#line 18 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) * std::complex< long double >(y);
}
#line 21 "core-feature.c"
constexpr std::complex< long double > operator*(std::complex< long double > y, float x) {
#line 22 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) * std::complex< long double >(y);
}
#line 25 "core-feature.c"
constexpr std::complex< long double > operator*(long double x, std::complex< float > y) {
#line 26 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) * std::complex< long double >(y);
}
#line 29 "core-feature.c"
constexpr std::complex< long double > operator*(std::complex< float > y, long double x) {
#line 30 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) * std::complex< long double >(y);
}
#line 33 "core-feature.c"
constexpr std::complex< long double > operator*(std::complex< long double > y, std::complex< float > x) {
#line 34 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) * std::complex< long double >(y);
}
#line 37 "core-feature.c"
constexpr std::complex< long double > operator*(std::complex< float > x, std::complex< long double > y) {
#line 38 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) * std::complex< long double >(y);
}
#line 43 "core-feature.c"

#line 46 "core-feature.c"
#line 48 "core-feature.c"
constexpr std::complex< double > operator*(float x, std::complex< double > y) {
#line 49 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) * std::complex< double >(y);
}
#line 52 "core-feature.c"
constexpr std::complex< double > operator*(std::complex< double > y, float x) {
#line 53 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) * std::complex< double >(y);
}
#line 56 "core-feature.c"
constexpr std::complex< double > operator*(double x, std::complex< float > y) {
#line 57 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) * std::complex< double >(y);
}
#line 60 "core-feature.c"
constexpr std::complex< double > operator*(std::complex< float > y, double x) {
#line 61 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) * std::complex< double >(y);
}
#line 64 "core-feature.c"
constexpr std::complex< double > operator*(std::complex< double > y, std::complex< float > x) {
#line 65 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) * std::complex< double >(y);
}
#line 68 "core-feature.c"
constexpr std::complex< double > operator*(std::complex< float > x, std::complex< double > y) {
#line 69 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) * std::complex< double >(y);
}
#line 74 "core-feature.c"

#line 12 "core-feature.c"
#line 14 "core-feature.c"
#line 17 "core-feature.c"
constexpr std::complex< long double > operator/(double x, std::complex< long double > y) {
#line 18 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) / std::complex< long double >(y);
}
#line 21 "core-feature.c"
constexpr std::complex< long double > operator/(std::complex< long double > y, double x) {
#line 22 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) / std::complex< long double >(y);
}
#line 25 "core-feature.c"
constexpr std::complex< long double > operator/(long double x, std::complex< double > y) {
#line 26 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) / std::complex< long double >(y);
}
#line 29 "core-feature.c"
constexpr std::complex< long double > operator/(std::complex< double > y, long double x) {
#line 30 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) / std::complex< long double >(y);
}
#line 33 "core-feature.c"
constexpr std::complex< long double > operator/(std::complex< long double > y, std::complex< double > x) {
#line 34 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) / std::complex< long double >(y);
}
#line 37 "core-feature.c"
constexpr std::complex< long double > operator/(std::complex< double > x, std::complex< long double > y) {
#line 38 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) / std::complex< long double >(y);
}
#line 17 "core-feature.c"
constexpr std::complex< long double > operator/(float x, std::complex< long double > y) {
#line 18 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) / std::complex< long double >(y);
}
#line 21 "core-feature.c"
constexpr std::complex< long double > operator/(std::complex< long double > y, float x) {
#line 22 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) / std::complex< long double >(y);
}
#line 25 "core-feature.c"
constexpr std::complex< long double > operator/(long double x, std::complex< float > y) {
#line 26 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) / std::complex< long double >(y);
}
#line 29 "core-feature.c"
constexpr std::complex< long double > operator/(std::complex< float > y, long double x) {
#line 30 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) / std::complex< long double >(y);
}
#line 33 "core-feature.c"
constexpr std::complex< long double > operator/(std::complex< long double > y, std::complex< float > x) {
#line 34 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) / std::complex< long double >(y);
}
#line 37 "core-feature.c"
constexpr std::complex< long double > operator/(std::complex< float > x, std::complex< long double > y) {
#line 38 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) / std::complex< long double >(y);
}
#line 43 "core-feature.c"

#line 46 "core-feature.c"
#line 48 "core-feature.c"
constexpr std::complex< double > operator/(float x, std::complex< double > y) {
#line 49 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) / std::complex< double >(y);
}
#line 52 "core-feature.c"
constexpr std::complex< double > operator/(std::complex< double > y, float x) {
#line 53 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) / std::complex< double >(y);
}
#line 56 "core-feature.c"
constexpr std::complex< double > operator/(double x, std::complex< float > y) {
#line 57 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) / std::complex< double >(y);
}
#line 60 "core-feature.c"
constexpr std::complex< double > operator/(std::complex< float > y, double x) {
#line 61 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) / std::complex< double >(y);
}
#line 64 "core-feature.c"
constexpr std::complex< double > operator/(std::complex< double > y, std::complex< float > x) {
#line 65 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) / std::complex< double >(y);
}
#line 68 "core-feature.c"
constexpr std::complex< double > operator/(std::complex< float > x, std::complex< double > y) {
#line 69 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) / std::complex< double >(y);
}
#line 74 "core-feature.c"

#line 12 "core-feature.c"
#line 14 "core-feature.c"
#line 17 "core-feature.c"
constexpr std::complex< long double > operator+(double x, std::complex< long double > y) {
#line 18 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) + std::complex< long double >(y);
}
#line 21 "core-feature.c"
constexpr std::complex< long double > operator+(std::complex< long double > y, double x) {
#line 22 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) + std::complex< long double >(y);
}
#line 25 "core-feature.c"
constexpr std::complex< long double > operator+(long double x, std::complex< double > y) {
#line 26 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) + std::complex< long double >(y);
}
#line 29 "core-feature.c"
constexpr std::complex< long double > operator+(std::complex< double > y, long double x) {
#line 30 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) + std::complex< long double >(y);
}
#line 33 "core-feature.c"
constexpr std::complex< long double > operator+(std::complex< long double > y, std::complex< double > x) {
#line 34 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) + std::complex< long double >(y);
}
#line 37 "core-feature.c"
constexpr std::complex< long double > operator+(std::complex< double > x, std::complex< long double > y) {
#line 38 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) + std::complex< long double >(y);
}
#line 17 "core-feature.c"
constexpr std::complex< long double > operator+(float x, std::complex< long double > y) {
#line 18 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) + std::complex< long double >(y);
}
#line 21 "core-feature.c"
constexpr std::complex< long double > operator+(std::complex< long double > y, float x) {
#line 22 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) + std::complex< long double >(y);
}
#line 25 "core-feature.c"
constexpr std::complex< long double > operator+(long double x, std::complex< float > y) {
#line 26 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) + std::complex< long double >(y);
}
#line 29 "core-feature.c"
constexpr std::complex< long double > operator+(std::complex< float > y, long double x) {
#line 30 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) + std::complex< long double >(y);
}
#line 33 "core-feature.c"
constexpr std::complex< long double > operator+(std::complex< long double > y, std::complex< float > x) {
#line 34 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) + std::complex< long double >(y);
}
#line 37 "core-feature.c"
constexpr std::complex< long double > operator+(std::complex< float > x, std::complex< long double > y) {
#line 38 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) + std::complex< long double >(y);
}
#line 43 "core-feature.c"

#line 46 "core-feature.c"
#line 48 "core-feature.c"
constexpr std::complex< double > operator+(float x, std::complex< double > y) {
#line 49 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) + std::complex< double >(y);
}
#line 52 "core-feature.c"
constexpr std::complex< double > operator+(std::complex< double > y, float x) {
#line 53 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) + std::complex< double >(y);
}
#line 56 "core-feature.c"
constexpr std::complex< double > operator+(double x, std::complex< float > y) {
#line 57 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) + std::complex< double >(y);
}
#line 60 "core-feature.c"
constexpr std::complex< double > operator+(std::complex< float > y, double x) {
#line 61 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) + std::complex< double >(y);
}
#line 64 "core-feature.c"
constexpr std::complex< double > operator+(std::complex< double > y, std::complex< float > x) {
#line 65 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) + std::complex< double >(y);
}
#line 68 "core-feature.c"
constexpr std::complex< double > operator+(std::complex< float > x, std::complex< double > y) {
#line 69 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) + std::complex< double >(y);
}
#line 74 "core-feature.c"

#line 12 "core-feature.c"
#line 14 "core-feature.c"
#line 17 "core-feature.c"
constexpr std::complex< long double > operator-(double x, std::complex< long double > y) {
#line 18 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) - std::complex< long double >(y);
}
#line 21 "core-feature.c"
constexpr std::complex< long double > operator-(std::complex< long double > y, double x) {
#line 22 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) - std::complex< long double >(y);
}
#line 25 "core-feature.c"
constexpr std::complex< long double > operator-(long double x, std::complex< double > y) {
#line 26 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) - std::complex< long double >(y);
}
#line 29 "core-feature.c"
constexpr std::complex< long double > operator-(std::complex< double > y, long double x) {
#line 30 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) - std::complex< long double >(y);
}
#line 33 "core-feature.c"
constexpr std::complex< long double > operator-(std::complex< long double > y, std::complex< double > x) {
#line 34 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) - std::complex< long double >(y);
}
#line 37 "core-feature.c"
constexpr std::complex< long double > operator-(std::complex< double > x, std::complex< long double > y) {
#line 38 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) - std::complex< long double >(y);
}
#line 17 "core-feature.c"
constexpr std::complex< long double > operator-(float x, std::complex< long double > y) {
#line 18 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) - std::complex< long double >(y);
}
#line 21 "core-feature.c"
constexpr std::complex< long double > operator-(std::complex< long double > y, float x) {
#line 22 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) - std::complex< long double >(y);
}
#line 25 "core-feature.c"
constexpr std::complex< long double > operator-(long double x, std::complex< float > y) {
#line 26 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) - std::complex< long double >(y);
}
#line 29 "core-feature.c"
constexpr std::complex< long double > operator-(std::complex< float > y, long double x) {
#line 30 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) - std::complex< long double >(y);
}
#line 33 "core-feature.c"
constexpr std::complex< long double > operator-(std::complex< long double > y, std::complex< float > x) {
#line 34 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) - std::complex< long double >(y);
}
#line 37 "core-feature.c"
constexpr std::complex< long double > operator-(std::complex< float > x, std::complex< long double > y) {
#line 38 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< long double >(x) - std::complex< long double >(y);
}
#line 43 "core-feature.c"

#line 46 "core-feature.c"
#line 48 "core-feature.c"
constexpr std::complex< double > operator-(float x, std::complex< double > y) {
#line 49 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) - std::complex< double >(y);
}
#line 52 "core-feature.c"
constexpr std::complex< double > operator-(std::complex< double > y, float x) {
#line 53 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) - std::complex< double >(y);
}
#line 56 "core-feature.c"
constexpr std::complex< double > operator-(double x, std::complex< float > y) {
#line 57 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) - std::complex< double >(y);
}
#line 60 "core-feature.c"
constexpr std::complex< double > operator-(std::complex< float > y, double x) {
#line 61 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) - std::complex< double >(y);
}
#line 64 "core-feature.c"
constexpr std::complex< double > operator-(std::complex< double > y, std::complex< float > x) {
#line 65 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) - std::complex< double >(y);
}
#line 68 "core-feature.c"
constexpr std::complex< double > operator-(std::complex< float > x, std::complex< double > y) {
#line 69 "core-feature.c"
// the return type is the complex type of the combined float types
    return std::complex< double >(x) - std::complex< double >(y);
}
#line 74 "core-feature.c"

#line 76 "core-feature.c"

// the other set are those with integer types
#line 79 "core-feature.c"
#line 83 "core-feature.c"
#line 96 "core-feature.c"
constexpr std::complex< float > operator*(const bool& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator*(const std::complex< float >& y, const bool& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator*(const char& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator*(const std::complex< float >& y, const char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator*(const long& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator*(const std::complex< float >& y, const long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator*(const long long& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator*(const std::complex< float >& y, const long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator*(const signed& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator*(const std::complex< float >& y, const signed& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator*(const signed char& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator*(const std::complex< float >& y, const signed char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator*(const signed short& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator*(const std::complex< float >& y, const signed short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator*(const unsigned& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator*(const std::complex< float >& y, const unsigned& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator*(const unsigned char& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator*(const std::complex< float >& y, const unsigned char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator*(const unsigned long& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator*(const std::complex< float >& y, const unsigned long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator*(const unsigned long long& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator*(const std::complex< float >& y, const unsigned long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator*(const unsigned short& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator*(const std::complex< float >& y, const unsigned short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y * X;
}
#line 83 "core-feature.c"
#line 96 "core-feature.c"
constexpr std::complex< double > operator*(const bool& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator*(const std::complex< double >& y, const bool& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator*(const char& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator*(const std::complex< double >& y, const char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator*(const long& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator*(const std::complex< double >& y, const long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator*(const long long& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator*(const std::complex< double >& y, const long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator*(const signed& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator*(const std::complex< double >& y, const signed& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator*(const signed char& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator*(const std::complex< double >& y, const signed char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator*(const signed short& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator*(const std::complex< double >& y, const signed short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator*(const unsigned& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator*(const std::complex< double >& y, const unsigned& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator*(const unsigned char& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator*(const std::complex< double >& y, const unsigned char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator*(const unsigned long& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator*(const std::complex< double >& y, const unsigned long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator*(const unsigned long long& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator*(const std::complex< double >& y, const unsigned long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator*(const unsigned short& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator*(const std::complex< double >& y, const unsigned short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y * X;
}
#line 83 "core-feature.c"
#line 96 "core-feature.c"
constexpr std::complex< long double > operator*(const bool& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator*(const std::complex< long double >& y, const bool& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator*(const char& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator*(const std::complex< long double >& y, const char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator*(const long& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator*(const std::complex< long double >& y, const long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator*(const long long& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator*(const std::complex< long double >& y, const long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator*(const signed& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator*(const std::complex< long double >& y, const signed& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator*(const signed char& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator*(const std::complex< long double >& y, const signed char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator*(const signed short& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator*(const std::complex< long double >& y, const signed short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator*(const unsigned& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator*(const std::complex< long double >& y, const unsigned& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator*(const unsigned char& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator*(const std::complex< long double >& y, const unsigned char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator*(const unsigned long& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator*(const std::complex< long double >& y, const unsigned long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator*(const unsigned long long& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator*(const std::complex< long double >& y, const unsigned long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y * X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator*(const unsigned short& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X * y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator*(const std::complex< long double >& y, const unsigned short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y * X;
}
#line 79 "core-feature.c"
#line 83 "core-feature.c"
#line 96 "core-feature.c"
constexpr std::complex< float > operator/(const bool& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator/(const std::complex< float >& y, const bool& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator/(const char& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator/(const std::complex< float >& y, const char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator/(const long& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator/(const std::complex< float >& y, const long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator/(const long long& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator/(const std::complex< float >& y, const long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator/(const signed& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator/(const std::complex< float >& y, const signed& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator/(const signed char& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator/(const std::complex< float >& y, const signed char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator/(const signed short& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator/(const std::complex< float >& y, const signed short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator/(const unsigned& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator/(const std::complex< float >& y, const unsigned& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator/(const unsigned char& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator/(const std::complex< float >& y, const unsigned char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator/(const unsigned long& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator/(const std::complex< float >& y, const unsigned long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator/(const unsigned long long& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator/(const std::complex< float >& y, const unsigned long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator/(const unsigned short& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator/(const std::complex< float >& y, const unsigned short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y / X;
}
#line 83 "core-feature.c"
#line 96 "core-feature.c"
constexpr std::complex< double > operator/(const bool& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator/(const std::complex< double >& y, const bool& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator/(const char& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator/(const std::complex< double >& y, const char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator/(const long& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator/(const std::complex< double >& y, const long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator/(const long long& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator/(const std::complex< double >& y, const long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator/(const signed& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator/(const std::complex< double >& y, const signed& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator/(const signed char& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator/(const std::complex< double >& y, const signed char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator/(const signed short& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator/(const std::complex< double >& y, const signed short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator/(const unsigned& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator/(const std::complex< double >& y, const unsigned& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator/(const unsigned char& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator/(const std::complex< double >& y, const unsigned char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator/(const unsigned long& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator/(const std::complex< double >& y, const unsigned long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator/(const unsigned long long& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator/(const std::complex< double >& y, const unsigned long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator/(const unsigned short& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator/(const std::complex< double >& y, const unsigned short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y / X;
}
#line 83 "core-feature.c"
#line 96 "core-feature.c"
constexpr std::complex< long double > operator/(const bool& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator/(const std::complex< long double >& y, const bool& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator/(const char& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator/(const std::complex< long double >& y, const char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator/(const long& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator/(const std::complex< long double >& y, const long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator/(const long long& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator/(const std::complex< long double >& y, const long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator/(const signed& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator/(const std::complex< long double >& y, const signed& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator/(const signed char& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator/(const std::complex< long double >& y, const signed char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator/(const signed short& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator/(const std::complex< long double >& y, const signed short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator/(const unsigned& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator/(const std::complex< long double >& y, const unsigned& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator/(const unsigned char& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator/(const std::complex< long double >& y, const unsigned char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator/(const unsigned long& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator/(const std::complex< long double >& y, const unsigned long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator/(const unsigned long long& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator/(const std::complex< long double >& y, const unsigned long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y / X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator/(const unsigned short& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X / y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator/(const std::complex< long double >& y, const unsigned short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y / X;
}
#line 79 "core-feature.c"
#line 83 "core-feature.c"
#line 96 "core-feature.c"
constexpr std::complex< float > operator+(const bool& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator+(const std::complex< float >& y, const bool& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator+(const char& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator+(const std::complex< float >& y, const char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator+(const long& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator+(const std::complex< float >& y, const long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator+(const long long& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator+(const std::complex< float >& y, const long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator+(const signed& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator+(const std::complex< float >& y, const signed& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator+(const signed char& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator+(const std::complex< float >& y, const signed char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator+(const signed short& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator+(const std::complex< float >& y, const signed short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator+(const unsigned& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator+(const std::complex< float >& y, const unsigned& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator+(const unsigned char& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator+(const std::complex< float >& y, const unsigned char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator+(const unsigned long& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator+(const std::complex< float >& y, const unsigned long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator+(const unsigned long long& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator+(const std::complex< float >& y, const unsigned long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator+(const unsigned short& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator+(const std::complex< float >& y, const unsigned short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y + X;
}
#line 83 "core-feature.c"
#line 96 "core-feature.c"
constexpr std::complex< double > operator+(const bool& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator+(const std::complex< double >& y, const bool& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator+(const char& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator+(const std::complex< double >& y, const char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator+(const long& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator+(const std::complex< double >& y, const long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator+(const long long& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator+(const std::complex< double >& y, const long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator+(const signed& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator+(const std::complex< double >& y, const signed& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator+(const signed char& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator+(const std::complex< double >& y, const signed char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator+(const signed short& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator+(const std::complex< double >& y, const signed short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator+(const unsigned& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator+(const std::complex< double >& y, const unsigned& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator+(const unsigned char& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator+(const std::complex< double >& y, const unsigned char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator+(const unsigned long& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator+(const std::complex< double >& y, const unsigned long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator+(const unsigned long long& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator+(const std::complex< double >& y, const unsigned long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator+(const unsigned short& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator+(const std::complex< double >& y, const unsigned short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y + X;
}
#line 83 "core-feature.c"
#line 96 "core-feature.c"
constexpr std::complex< long double > operator+(const bool& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator+(const std::complex< long double >& y, const bool& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator+(const char& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator+(const std::complex< long double >& y, const char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator+(const long& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator+(const std::complex< long double >& y, const long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator+(const long long& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator+(const std::complex< long double >& y, const long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator+(const signed& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator+(const std::complex< long double >& y, const signed& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator+(const signed char& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator+(const std::complex< long double >& y, const signed char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator+(const signed short& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator+(const std::complex< long double >& y, const signed short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator+(const unsigned& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator+(const std::complex< long double >& y, const unsigned& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator+(const unsigned char& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator+(const std::complex< long double >& y, const unsigned char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator+(const unsigned long& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator+(const std::complex< long double >& y, const unsigned long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator+(const unsigned long long& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator+(const std::complex< long double >& y, const unsigned long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y + X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator+(const unsigned short& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X + y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator+(const std::complex< long double >& y, const unsigned short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y + X;
}
#line 79 "core-feature.c"
#line 83 "core-feature.c"
#line 96 "core-feature.c"
constexpr std::complex< float > operator-(const bool& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator-(const std::complex< float >& y, const bool& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator-(const char& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator-(const std::complex< float >& y, const char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator-(const long& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator-(const std::complex< float >& y, const long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator-(const long long& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator-(const std::complex< float >& y, const long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator-(const signed& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator-(const std::complex< float >& y, const signed& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator-(const signed char& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator-(const std::complex< float >& y, const signed char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator-(const signed short& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator-(const std::complex< float >& y, const signed short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator-(const unsigned& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator-(const std::complex< float >& y, const unsigned& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator-(const unsigned char& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator-(const std::complex< float >& y, const unsigned char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator-(const unsigned long& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator-(const std::complex< float >& y, const unsigned long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator-(const unsigned long long& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator-(const std::complex< float >& y, const unsigned long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< float > operator-(const unsigned short& x, const std::complex< float >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< float > operator-(const std::complex< float >& y, const unsigned short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< float > X = x;
    return y - X;
}
#line 83 "core-feature.c"
#line 96 "core-feature.c"
constexpr std::complex< double > operator-(const bool& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator-(const std::complex< double >& y, const bool& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator-(const char& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator-(const std::complex< double >& y, const char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator-(const long& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator-(const std::complex< double >& y, const long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator-(const long long& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator-(const std::complex< double >& y, const long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator-(const signed& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator-(const std::complex< double >& y, const signed& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator-(const signed char& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator-(const std::complex< double >& y, const signed char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator-(const signed short& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator-(const std::complex< double >& y, const signed short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator-(const unsigned& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator-(const std::complex< double >& y, const unsigned& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator-(const unsigned char& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator-(const std::complex< double >& y, const unsigned char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator-(const unsigned long& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator-(const std::complex< double >& y, const unsigned long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator-(const unsigned long long& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator-(const std::complex< double >& y, const unsigned long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< double > operator-(const unsigned short& x, const std::complex< double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< double > operator-(const std::complex< double >& y, const unsigned short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< double > X = x;
    return y - X;
}
#line 83 "core-feature.c"
#line 96 "core-feature.c"
constexpr std::complex< long double > operator-(const bool& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator-(const std::complex< long double >& y, const bool& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator-(const char& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator-(const std::complex< long double >& y, const char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator-(const long& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator-(const std::complex< long double >& y, const long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator-(const long long& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator-(const std::complex< long double >& y, const long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator-(const signed& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator-(const std::complex< long double >& y, const signed& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator-(const signed char& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator-(const std::complex< long double >& y, const signed char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator-(const signed short& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator-(const std::complex< long double >& y, const signed short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator-(const unsigned& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator-(const std::complex< long double >& y, const unsigned& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator-(const unsigned char& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator-(const std::complex< long double >& y, const unsigned char& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator-(const unsigned long& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator-(const std::complex< long double >& y, const unsigned long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator-(const unsigned long long& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator-(const std::complex< long double >& y, const unsigned long long& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y - X;
}
#line 96 "core-feature.c"
constexpr std::complex< long double > operator-(const unsigned short& x, const std::complex< long double >& y) {
#line 97 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return X - y;
}
#line 101 "core-feature.c"
constexpr std::complex< long double > operator-(const std::complex< long double >& y, const unsigned short& x) {
#line 102 "core-feature.c"
// the return type is the complex type
    const std::complex< long double > X = x;
    return y - X;
}
#line 109 "core-feature.c"

#endif
#line 111 "core-feature.c"


/**
 ** @brief The core generic_value macro
 **
 ** Performs array and function decay and drops all direct
 ** quantifiers.
 **
 ** This is the value that a function call receives.
 **/
template< typename T > constexpr auto generic_value(T _0) {
    return _0;
}
#define generic_value generic_value
#line 123 "core-feature.c"

/**
 ** @brief The core generic_value macro
 **
 ** The type after array and function decay and drops all direct
 ** quantifiers.
 **
 ** This is the type that a function call receives.
 **/
#define generic_type(E) decltype(generic_value(E))
#line 133 "core-feature.c"

/**
 ** @brief The core floating_value macro
 **
 ** Derives a floating-point value from any arithmetic expression or
 ** type name.
 **/
#define floating_value(E) (((E)+0) + 0.0f)
#line 141 "core-feature.c"

/**
 ** @brief The core complex_value macro
 **
 ** Derives a complex value from any arithmetic expression or type
 ** name.
 **/
#define complex_value(E) (((E)+0) + 0.0f*I)
#line 149 "core-feature.c"

/**
 ** @brief The core complex_type macro
 **
 ** Derives a complex type from of any arithmetic expression or type
 ** name.
 **/
#define complex_type(TE) decltype(complex_value(TE))
#line 157 "core-feature.c"

#ifdef __cplusplus
// A series of 4 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '_NOEVALE'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 159 "core-feature.c"
template< typename _T1 >
#line 160 "core-feature.c"
constexpr auto real_type_helper(generic_type((complex_type(float)) {}), _T1 _NOEVALE) {
#line 160 "core-feature.c"
    return 0.0f;
}
template< typename _T1 >
#line 161 "core-feature.c"
constexpr auto real_type_helper(generic_type((complex_type(double)) {}), _T1 _NOEVALE) {
#line 161 "core-feature.c"
    return 0.0;
}
template< typename _T1 >
#line 162 "core-feature.c"
constexpr auto real_type_helper(generic_type((complex_type(long double)) {}), _T1 _NOEVALE) {
#line 162 "core-feature.c"
    return 0.0l;
}
#line 163 "core-feature.c"
template< typename _T0, typename _T1 >
#line 163 "core-feature.c"
constexpr auto real_type_helper(_T0, _T1 _NOEVALE) {
#line 163 "core-feature.c"
    return generic_value(_NOEVALE);
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 159 "core-feature.c"
constexpr auto real_type_helper(_T1 _NOEVALE) {
#line 159 "core-feature.c"
    return real_type_helper((generic_type(_NOEVALE)) {}, ((generic_type(_NOEVALE)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro real_type_helper.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 159 "core-feature.c"
#define real_type_helper(_NOEVALE) real_type_helper((generic_type(_NOEVALE)){}, ((generic_type(_NOEVALE)){}))
#else
#line 159 "core-feature.c"
#define real_type_helper(_NOEVALE) _Generic((_NOEVALE),\
	complex_type(float): 0.0f,\
	complex_type(double): 0.0,\
	complex_type(long double): 0.0l,\
	default: generic_value(_NOEVALE))
#endif
#line 164 "core-feature.c"

/**
 ** @brief The core real_type macro
 **
 ** Derives a real type from of any arithmetic expression.
 **
 ** For all real typed expressions this is just the generic type, for
 ** complex types this is the underlying floating type.
 **/
#define real_type(TE) decltype(real_type_helper(TE))
#line 174 "core-feature.c"

/**
 ** @brief The core real_value macro
 **
 ** Derives the real part from of any arithmetic expression.
 **
 ** For all real typed expressions this is just a value with the
 ** same type, for complex types this is real part.
 **
 ** This should not have information loss for all integer types where
 ** all the values fit into `long double`. Avoid this for very wide
 ** integer types.
 **/
#define real_value(E) ((real_type(E))(complex_value(E+0.0l*I).real()))
#line 188 "core-feature.c"

/**
 ** @brief The core imaginary_value macro
 **
 ** Derives the real part from of any arithmetic expression.
 **
 ** For all real typed expressions this is just a 0 value with the
 ** same type, for complex types this is the imaginary part.
 **/
#define imaginary_value(E) ((real_type(E))(complex_value(E).imag()))
#line 198 "core-feature.c"

/**
 ** @brief A zero of the widest unsigned integer type.
 **
 ** Unfortunately for clang `__uint128_t` does not behave properly as
 ** an integer type, so this is not usable.
 **/
#if __SIZEOF_INT128__ && !(__clang__ || __llvm__)
#define _Core_iswidest (__uint128_t)0
#else
#define _Core_iswidest 0ULL
#endif
#line 210 "core-feature.c"

/**
 ** @brief Promote @a X to a zero with the widest integer type.
 **
 ** - If @a X is a (prompoted) pointer type this will be a null pointer
 **   of that type.
 **
 ** - If @a X has a floating type this will be that floating type
 **
 ** - If @a X has an integer type this will be the same as `_Core_iswidest`.
 **
 ** In any case, @a X will not be evaluated but only inspected for its
 ** type.
 **/
#define _Core_towidest(X) (true ? (generic_type(X)){} : _Core_iswidest)
#line 225 "core-feature.c"

#ifdef __cplusplus
// A series of 2 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '_Core_towidest(X)'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 227 "core-feature.c"
template< typename _T1 >
#line 228 "core-feature.c"
constexpr auto _Core_isinteger(generic_type((decltype(_Core_iswidest)) {}), _T1 X) {
#line 228 "core-feature.c"
    return true;
}
#line 229 "core-feature.c"
template< typename _T0, typename _T1 >
#line 229 "core-feature.c"
constexpr auto _Core_isinteger(_T0, _T1 X) {
#line 229 "core-feature.c"
    return false;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 227 "core-feature.c"
constexpr auto _Core_isinteger(_T1 X) {
#line 227 "core-feature.c"
    return _Core_isinteger((generic_type(_Core_towidest(X))) {}, ((generic_type(X)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro _Core_isinteger.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 227 "core-feature.c"
#define _Core_isinteger(X) _Core_isinteger((generic_type(_Core_towidest(X))){}, ((generic_type(X)){}))
#else
#line 227 "core-feature.c"
#define _Core_isinteger(X) _Generic((_Core_towidest(X)),\
	decltype(_Core_iswidest): true,\
	default: false)
#endif
#line 230 "core-feature.c"

#ifdef __cplusplus
// A series of 2 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '_Core_towidest(X)'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 232 "core-feature.c"
template< typename _T1 >
#line 233 "core-feature.c"
constexpr auto _Core_isvoidptr(generic_type((void*) {}), _T1 X) {
#line 233 "core-feature.c"
    return true;
}
#line 234 "core-feature.c"
template< typename _T0, typename _T1 >
#line 234 "core-feature.c"
constexpr auto _Core_isvoidptr(_T0, _T1 X) {
#line 234 "core-feature.c"
    return false;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 232 "core-feature.c"
constexpr auto _Core_isvoidptr(_T1 X) {
#line 232 "core-feature.c"
    return _Core_isvoidptr((generic_type(_Core_towidest(X))) {}, ((generic_type(X)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro _Core_isvoidptr.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 232 "core-feature.c"
#define _Core_isvoidptr(X) _Core_isvoidptr((generic_type(_Core_towidest(X))){}, ((generic_type(X)){}))
#else
#line 232 "core-feature.c"
#define _Core_isvoidptr(X) _Generic((_Core_towidest(X)),\
	void*: true,\
	default: false)
#endif
#line 235 "core-feature.c"

#define _Core_isconstant(X) __builtin_constant_p(X)
#define _Core_isice(X) (_Core_isconstant(X) && _Core_isinteger(X))
#line 238 "core-feature.c"

#define _Core_isnull(X) ((_Core_isice(X) || _Core_isvoidptr(X)) && !(X))
#line 240 "core-feature.c"

#define _Core_tester(X) (int(*)[(_Core_isconstant(X) ? (X) : 0)+1]){}
#line 242 "core-feature.c"

#line 244 "core-feature.c"
typedef int (*_Core_false)[false+1];
#line 244 "core-feature.c"
typedef int (*_Core_true)[true+1];
#line 246 "core-feature.c"

/**
 ** @brief The core tonullptr macro
 **
 ** Returns `nullptr` if @a X is an integer constant expression (ICE)
 ** of value zero and @a X otherwise.
 **
 ** This can be used for type-generic interfaces that might expect
 ** null pointers of different flavors such that those values are all
 ** converted to `nullptr`. Other values, e.g pointer values, are just
 ** passed through.
 **
 ** @a X will only be evaluated if it is not an ICE of value
 ** zero. Otherwise, it will only be inspected for its type.
 **/
#ifdef __cplusplus
// A series of 2 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '_Core_tester(_Core_isnull(X))'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 262 "core-feature.c"
template< typename _T1 >
#line 263 "core-feature.c"
constexpr auto tonullptr(generic_type((_Core_false) {}), _T1 X) {
#line 263 "core-feature.c"
    return X;
}
template< typename _T1 >
#line 264 "core-feature.c"
constexpr auto tonullptr(generic_type((_Core_true) {}), _T1 X) {
#line 264 "core-feature.c"
    return  nullptr;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 262 "core-feature.c"
constexpr auto tonullptr(_T1 X) {
#line 262 "core-feature.c"
    return tonullptr((generic_type(_Core_tester(_Core_isnull(X)))) {}, (X));
}
/**
 ** The second is the principal user interface and is implemented as macro tonullptr.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** Other then that, the following arguments may be evaluated for the result: X
 **/
#line 262 "core-feature.c"
#define tonullptr(X) tonullptr((generic_type(_Core_tester(_Core_isnull(X)))){}, (X))
#else
#line 262 "core-feature.c"
#define tonullptr(X) _Generic((_Core_tester(_Core_isnull(X))),\
	_Core_false: X,\
	_Core_true:  nullptr)
#endif
#line 265 "core-feature.c"

#ifdef __cplusplus
#line 267 "core-feature.c"
template< typename _T0, typename _T1 >
struct _Core_issame {
#line 269 "core-feature.c"
    static constexpr bool _Core_val = false;
};
template< typename _T0 >
struct _Core_issame< _T0, _T0 > {
#line 273 "core-feature.c"
    static constexpr bool _Core_val = true;
};
#define issame(X, Y) _Core_issame< generic_type(X), generic_type(Y) >::_Core_val
#else
#define issame(X, Y) _Generic((X), generic_type(Y): true, default: false)
#endif
