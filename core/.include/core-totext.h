#line 1 "phase2"
// helper macros
#ifndef __LINENO
#define __LINENO(X) X##LL
#define __LINENO_(X) __LINENO(X)
#define __line(X) #X
#define __line_(X) __line(X)
// @brief use this instead of __LINE__ where you need an integer
#define __LINENO__ __LINENO_(__LINE__)
// @brief use this instead of __LINE__ where you need a string
#define __line__ __line_(__LINE__)
#endif
#line 1 "core-totext.c"
#include "core-feature.h"
#line 2 "core-totext.c"

#ifdef __cplusplus
// A series of 12 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '_Core_towidest(_NOEVAL0)'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 4 "core-totext.c"
template< typename _T1 >
#line 5 "core-totext.c"
constexpr auto _Core_totext_default(generic_type((float) {}), _T1 _NOEVAL0) {
#line 5 "core-totext.c"
    return                     "g";
}
template< typename _T1 >
#line 6 "core-totext.c"
constexpr auto _Core_totext_default(generic_type((double) {}), _T1 _NOEVAL0) {
#line 6 "core-totext.c"
    return                    "g";
}
template< typename _T1 >
#line 7 "core-totext.c"
constexpr auto _Core_totext_default(generic_type((long double) {}), _T1 _NOEVAL0) {
#line 7 "core-totext.c"
    return               "g";
}
template< typename _T1 >
#line 8 "core-totext.c"
constexpr auto _Core_totext_default(generic_type((complex_type(float)) {}), _T1 _NOEVAL0) {
#line 8 "core-totext.c"
    return       "g";
}
template< typename _T1 >
#line 9 "core-totext.c"
constexpr auto _Core_totext_default(generic_type((complex_type(double)) {}), _T1 _NOEVAL0) {
#line 9 "core-totext.c"
    return      "g";
}
template< typename _T1 >
#line 10 "core-totext.c"
constexpr auto _Core_totext_default(generic_type((complex_type(long double)) {}), _T1 _NOEVAL0) {
#line 10 "core-totext.c"
    return "g";
}
template< typename _T1 >
#line 11 "core-totext.c"
constexpr auto _Core_totext_default(generic_type((decltype(_Core_iswidest)) {}), _T1 _NOEVAL0) {
#line 11 "core-totext.c"
    return  [](auto x) {
        return issigned(x) ? "i" : "u";
    }
    (_NOEVAL0);
}
template< typename _T1 >
#line 12 "core-totext.c"
constexpr auto _Core_totext_default(generic_type((char const volatile*) {}), _T1 _NOEVAL0) {
#line 12 "core-totext.c"
    return      "s";
}
template< typename _T1 >
#line 13 "core-totext.c"
constexpr auto _Core_totext_default(generic_type((char volatile*) {}), _T1 _NOEVAL0) {
#line 13 "core-totext.c"
    return            "s";
}
template< typename _T1 >
#line 14 "core-totext.c"
constexpr auto _Core_totext_default(generic_type((char const*) {}), _T1 _NOEVAL0) {
#line 14 "core-totext.c"
    return               "s";
}
template< typename _T1 >
#line 15 "core-totext.c"
constexpr auto _Core_totext_default(generic_type((char*) {}), _T1 _NOEVAL0) {
#line 15 "core-totext.c"
    return                     "s";
}
#line 16 "core-totext.c"
template< typename _T0, typename _T1 >
#line 16 "core-totext.c"
constexpr auto _Core_totext_default(_T0, _T1 _NOEVAL0) {
#line 16 "core-totext.c"
    return                   "p";
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 4 "core-totext.c"
constexpr auto _Core_totext_default(_T1 _NOEVAL0) {
#line 4 "core-totext.c"
    return _Core_totext_default((generic_type(_Core_towidest(_NOEVAL0))) {}, ((generic_type(_NOEVAL0)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro _Core_totext_default.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 4 "core-totext.c"
#define _Core_totext_default(_NOEVAL0) _Core_totext_default((generic_type(_Core_towidest(_NOEVAL0))){}, ((generic_type(_NOEVAL0)){}))
#else
#line 4 "core-totext.c"
#define _Core_totext_default(_NOEVAL0) _Generic((_Core_towidest(_NOEVAL0)),\
	float:                     "g",\
	double:                    "g",\
	long double:               "g",\
	complex_type(float):       "g",\
	complex_type(double):      "g",\
	complex_type(long double): "g",\
	decltype(_Core_iswidest):  [](auto x){ return issigned(x) ? "i" : "u"; }(_NOEVAL0),\
	char const volatile*:      "s",\
	char volatile*:            "s",\
	char const*:               "s",\
	char*:                     "s",\
	default:                   "p")
#endif
#line 17 "core-totext.c"

#ifdef __cplusplus
// A series of 10 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '_NOEVAL0'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 19 "core-totext.c"
template< typename _T1 >
#line 20 "core-totext.c"
constexpr auto _Core_totext_length(generic_type((long double) {}), _T1 _NOEVAL0) {
#line 20 "core-totext.c"
    return               "L";
}
template< typename _T1 >
#line 21 "core-totext.c"
constexpr auto _Core_totext_length(generic_type((signed char) {}), _T1 _NOEVAL0) {
#line 21 "core-totext.c"
    return               "hh";
}
template< typename _T1 >
#line 22 "core-totext.c"
constexpr auto _Core_totext_length(generic_type((signed short) {}), _T1 _NOEVAL0) {
#line 22 "core-totext.c"
    return              "hh";
}
template< typename _T1 >
#line 23 "core-totext.c"
constexpr auto _Core_totext_length(generic_type((signed long) {}), _T1 _NOEVAL0) {
#line 23 "core-totext.c"
    return               "l";
}
template< typename _T1 >
#line 24 "core-totext.c"
constexpr auto _Core_totext_length(generic_type((signed long long) {}), _T1 _NOEVAL0) {
#line 24 "core-totext.c"
    return          "ll";
}
template< typename _T1 >
#line 25 "core-totext.c"
constexpr auto _Core_totext_length(generic_type((unsigned char) {}), _T1 _NOEVAL0) {
#line 25 "core-totext.c"
    return             "hh";
}
template< typename _T1 >
#line 26 "core-totext.c"
constexpr auto _Core_totext_length(generic_type((unsigned short) {}), _T1 _NOEVAL0) {
#line 26 "core-totext.c"
    return            "hh";
}
template< typename _T1 >
#line 27 "core-totext.c"
constexpr auto _Core_totext_length(generic_type((unsigned long) {}), _T1 _NOEVAL0) {
#line 27 "core-totext.c"
    return             "l";
}
template< typename _T1 >
#line 28 "core-totext.c"
constexpr auto _Core_totext_length(generic_type((unsigned long long) {}), _T1 _NOEVAL0) {
#line 28 "core-totext.c"
    return        "ll";
}
#line 29 "core-totext.c"
template< typename _T0, typename _T1 >
#line 29 "core-totext.c"
constexpr auto _Core_totext_length(_T0, _T1 _NOEVAL0) {
#line 29 "core-totext.c"
    return                   "";
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 19 "core-totext.c"
constexpr auto _Core_totext_length(_T1 _NOEVAL0) {
#line 19 "core-totext.c"
    return _Core_totext_length((generic_type(_NOEVAL0)) {}, ((generic_type(_NOEVAL0)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro _Core_totext_length.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 19 "core-totext.c"
#define _Core_totext_length(_NOEVAL0) _Core_totext_length((generic_type(_NOEVAL0)){}, ((generic_type(_NOEVAL0)){}))
#else
#line 19 "core-totext.c"
#define _Core_totext_length(_NOEVAL0) _Generic((_NOEVAL0),\
	long double:               "L",\
	signed char:               "hh",\
	signed short:              "hh",\
	signed long:               "l",\
	signed long long:          "ll",\
	unsigned char:             "hh",\
	unsigned short:            "hh",\
	unsigned long:             "l",\
	unsigned long long:        "ll",\
	default:                   "")
#endif
#line 30 "core-totext.c"

char const* array[] = {
#line 32 "core-totext.c"
    _Core_totext_length(double),
    _Core_totext_length(long double),
    _Core_totext_length(short),
    _Core_totext_default(double),
    _Core_totext_default(long double),
    _Core_totext_default(short),
};
