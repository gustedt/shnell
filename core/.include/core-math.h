#line 1 "phase2"
// helper macros
#ifndef __LINENO
#define __LINENO(X) X##LL
#define __LINENO_(X) __LINENO(X)
#define __line(X) #X
#define __line_(X) __line(X)
// @brief use this instead of __LINE__ where you need an integer
#define __LINENO__ __LINENO_(__LINE__)
// @brief use this instead of __LINE__ where you need a string
#define __line__ __line_(__LINE__)
#endif
#line 1 "core-math.c"
#include "core-feature.h"
#line 2 "core-math.c"

#include <math.h>
#include <limits.h>
#line 5 "core-math.c"

/**
 ** @brief The core isinteger macro
 **
 ** Is true if and only if the expression is an integer expression.
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#define isinteger _Core_isinteger
#line 14 "core-math.c"

/**
 ** @brief The core isfloating macro
 **
 ** Is true if and only if the expression is a floating expression.
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#ifdef __cplusplus
// A series of 7 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '_Core_towidest(X)'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 23 "core-math.c"
template< typename _T1 >
#line 24 "core-math.c"
constexpr auto isfloating(generic_type((float) {}), _T1 X) {
#line 24 "core-math.c"
    return true;
}
template< typename _T1 >
#line 25 "core-math.c"
constexpr auto isfloating(generic_type((double) {}), _T1 X) {
#line 25 "core-math.c"
    return true;
}
template< typename _T1 >
#line 26 "core-math.c"
constexpr auto isfloating(generic_type((long double) {}), _T1 X) {
#line 26 "core-math.c"
    return true;
}
template< typename _T1 >
#line 27 "core-math.c"
constexpr auto isfloating(generic_type((complex_type(float)) {}), _T1 X) {
#line 27 "core-math.c"
    return true;
}
template< typename _T1 >
#line 28 "core-math.c"
constexpr auto isfloating(generic_type((complex_type(double)) {}), _T1 X) {
#line 28 "core-math.c"
    return true;
}
template< typename _T1 >
#line 29 "core-math.c"
constexpr auto isfloating(generic_type((complex_type(long double)) {}), _T1 X) {
#line 29 "core-math.c"
    return true;
}
#line 30 "core-math.c"
template< typename _T0, typename _T1 >
#line 30 "core-math.c"
constexpr auto isfloating(_T0, _T1 X) {
#line 30 "core-math.c"
    return false;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 23 "core-math.c"
constexpr auto isfloating(_T1 X) {
#line 23 "core-math.c"
    return isfloating((generic_type(_Core_towidest(X))) {}, ((generic_type(X)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro isfloating.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 23 "core-math.c"
#define isfloating(X) isfloating((generic_type(_Core_towidest(X))){}, ((generic_type(X)){}))
#else
#line 23 "core-math.c"
#define isfloating(X) _Generic((_Core_towidest(X)),\
	float: true,\
	double: true,\
	long double: true,\
	complex_type(float): true,\
	complex_type(double): true,\
	complex_type(long double): true,\
	default: false)
#endif
#line 31 "core-math.c"

/**
 ** @brief The core iscomplex macro
 **
 ** Is true if and only if the expression is a complex expression.
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#ifdef __cplusplus
// A series of 4 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '_Core_towidest(X)'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 40 "core-math.c"
template< typename _T1 >
#line 41 "core-math.c"
constexpr auto iscomplex(generic_type((complex_type(float)) {}), _T1 X) {
#line 41 "core-math.c"
    return true;
}
template< typename _T1 >
#line 42 "core-math.c"
constexpr auto iscomplex(generic_type((complex_type(double)) {}), _T1 X) {
#line 42 "core-math.c"
    return true;
}
template< typename _T1 >
#line 43 "core-math.c"
constexpr auto iscomplex(generic_type((complex_type(long double)) {}), _T1 X) {
#line 43 "core-math.c"
    return true;
}
#line 44 "core-math.c"
template< typename _T0, typename _T1 >
#line 44 "core-math.c"
constexpr auto iscomplex(_T0, _T1 X) {
#line 44 "core-math.c"
    return false;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 40 "core-math.c"
constexpr auto iscomplex(_T1 X) {
#line 40 "core-math.c"
    return iscomplex((generic_type(_Core_towidest(X))) {}, ((generic_type(X)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro iscomplex.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 40 "core-math.c"
#define iscomplex(X) iscomplex((generic_type(_Core_towidest(X))){}, ((generic_type(X)){}))
#else
#line 40 "core-math.c"
#define iscomplex(X) _Generic((_Core_towidest(X)),\
	complex_type(float): true,\
	complex_type(double): true,\
	complex_type(long double): true,\
	default: false)
#endif
#line 45 "core-math.c"

/**
 ** @brief The core iswide macro
 **
 ** Is true if and only if the expression is a wide integer
 ** expression.
 **
 ** @a _NOEVAL0 will not be evaluated but only inspected for its type.
 **/
#ifdef __cplusplus
// A series of 2 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '_Core_towidest(_NOEVAL0)'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 55 "core-math.c"
template< typename _T1 >
#line 56 "core-math.c"
constexpr auto iswide(generic_type((decltype(_Core_iswidest)) {}), _T1 _NOEVAL0) {
#line 56 "core-math.c"
    return issame(((generic_type(_NOEVAL0))0)+1, (generic_type(_NOEVAL0))0);
}
#line 57 "core-math.c"
template< typename _T0, typename _T1 >
#line 57 "core-math.c"
constexpr auto iswide(_T0, _T1 _NOEVAL0) {
#line 57 "core-math.c"
    return false;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 55 "core-math.c"
constexpr auto iswide(_T1 _NOEVAL0) {
#line 55 "core-math.c"
    return iswide((generic_type(_Core_towidest(_NOEVAL0))) {}, ((generic_type(_NOEVAL0)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro iswide.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 55 "core-math.c"
#define iswide(_NOEVAL0) iswide((generic_type(_Core_towidest(_NOEVAL0))){}, ((generic_type(_NOEVAL0)){}))
#else
#line 55 "core-math.c"
#define iswide(_NOEVAL0) _Generic((_Core_towidest(_NOEVAL0)),\
	decltype(_Core_iswidest): issame(((generic_type(_NOEVAL0))0)+1, (generic_type(_NOEVAL0))0),\
	default: false)
#endif
#line 58 "core-math.c"

/**
 ** @brief The core isnarrow macro
 **
 ** Is true if and only if the expression is a narrow integer
 ** expression.
 **
 ** @a _NOEVAL0 will not be evaluated but only inspected for its type.
 **/
#ifdef __cplusplus
// A series of 2 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '_Core_towidest(_NOEVAL0)'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 68 "core-math.c"
template< typename _T1 >
#line 69 "core-math.c"
constexpr auto isnarrow(generic_type((decltype(_Core_iswidest)) {}), _T1 _NOEVAL0) {
#line 69 "core-math.c"
    return !issame(((generic_type(_NOEVAL0))0)+1, (generic_type(_NOEVAL0))0);
}
#line 70 "core-math.c"
template< typename _T0, typename _T1 >
#line 70 "core-math.c"
constexpr auto isnarrow(_T0, _T1 _NOEVAL0) {
#line 70 "core-math.c"
    return false;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 68 "core-math.c"
constexpr auto isnarrow(_T1 _NOEVAL0) {
#line 68 "core-math.c"
    return isnarrow((generic_type(_Core_towidest(_NOEVAL0))) {}, ((generic_type(_NOEVAL0)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro isnarrow.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 68 "core-math.c"
#define isnarrow(_NOEVAL0) isnarrow((generic_type(_Core_towidest(_NOEVAL0))){}, ((generic_type(_NOEVAL0)){}))
#else
#line 68 "core-math.c"
#define isnarrow(_NOEVAL0) _Generic((_Core_towidest(_NOEVAL0)),\
	decltype(_Core_iswidest): !issame(((generic_type(_NOEVAL0))0)+1, (generic_type(_NOEVAL0))0),\
	default: false)
#endif
#line 71 "core-math.c"

/**
 ** @brief The core isstandard macro
 **
 ** Is true if and only if the expression has standard integer type.
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#ifdef __cplusplus
// A series of 13 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression 'TE'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 80 "core-math.c"
template< typename _T1 >
#line 81 "core-math.c"
constexpr auto isstandard(generic_type((unsigned long long) {}), _T1 TE) {
#line 81 "core-math.c"
    return true;
}
template< typename _T1 >
#line 82 "core-math.c"
constexpr auto isstandard(generic_type((unsigned long) {}), _T1 TE) {
#line 82 "core-math.c"
    return true;
}
template< typename _T1 >
#line 83 "core-math.c"
constexpr auto isstandard(generic_type((unsigned int) {}), _T1 TE) {
#line 83 "core-math.c"
    return true;
}
template< typename _T1 >
#line 84 "core-math.c"
constexpr auto isstandard(generic_type((unsigned short) {}), _T1 TE) {
#line 84 "core-math.c"
    return true;
}
template< typename _T1 >
#line 85 "core-math.c"
constexpr auto isstandard(generic_type((unsigned char) {}), _T1 TE) {
#line 85 "core-math.c"
    return true;
}
template< typename _T1 >
#line 86 "core-math.c"
constexpr auto isstandard(generic_type((signed long long) {}), _T1 TE) {
#line 86 "core-math.c"
    return true;
}
template< typename _T1 >
#line 87 "core-math.c"
constexpr auto isstandard(generic_type((signed long) {}), _T1 TE) {
#line 87 "core-math.c"
    return true;
}
template< typename _T1 >
#line 88 "core-math.c"
constexpr auto isstandard(generic_type((signed int) {}), _T1 TE) {
#line 88 "core-math.c"
    return true;
}
template< typename _T1 >
#line 89 "core-math.c"
constexpr auto isstandard(generic_type((signed short) {}), _T1 TE) {
#line 89 "core-math.c"
    return true;
}
template< typename _T1 >
#line 90 "core-math.c"
constexpr auto isstandard(generic_type((signed char) {}), _T1 TE) {
#line 90 "core-math.c"
    return true;
}
template< typename _T1 >
#line 91 "core-math.c"
constexpr auto isstandard(generic_type((char) {}), _T1 TE) {
#line 91 "core-math.c"
    return true;
}
template< typename _T1 >
#line 92 "core-math.c"
constexpr auto isstandard(generic_type((bool) {}), _T1 TE) {
#line 92 "core-math.c"
    return true;
}
#line 93 "core-math.c"
template< typename _T0, typename _T1 >
#line 93 "core-math.c"
constexpr auto isstandard(_T0, _T1 TE) {
#line 93 "core-math.c"
    return false;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 80 "core-math.c"
constexpr auto isstandard(_T1 TE) {
#line 80 "core-math.c"
    return isstandard((generic_type(TE)) {}, ((generic_type(TE)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro isstandard.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 80 "core-math.c"
#define isstandard(TE) isstandard((generic_type(TE)){}, ((generic_type(TE)){}))
#else
#line 80 "core-math.c"
#define isstandard(TE) _Generic((TE),\
	unsigned long long: true,\
	unsigned long: true,\
	unsigned int: true,\
	unsigned short: true,\
	unsigned char: true,\
	signed long long: true,\
	signed long: true,\
	signed int: true,\
	signed short: true,\
	signed char: true,\
	char: true,\
	bool: true,\
	default: false)
#endif
#line 94 "core-math.c"

/**
 ** @brief The core isextended macro
 **
 ** Is true if and only if the expression has extended integer type.
 **
 ** @a _NOEVAL0 will not be evaluated but only inspected for its type.
 **/
#ifdef __cplusplus
// A series of 2 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '_Core_towidest(_NOEVAL0)'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 103 "core-math.c"
template< typename _T1 >
#line 104 "core-math.c"
constexpr auto isextended(generic_type((decltype(_Core_iswidest)) {}), _T1 _NOEVAL0) {
#line 104 "core-math.c"
    return !isstandard(_NOEVAL0);
}
#line 105 "core-math.c"
template< typename _T0, typename _T1 >
#line 105 "core-math.c"
constexpr auto isextended(_T0, _T1 _NOEVAL0) {
#line 105 "core-math.c"
    return false;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 103 "core-math.c"
constexpr auto isextended(_T1 _NOEVAL0) {
#line 103 "core-math.c"
    return isextended((generic_type(_Core_towidest(_NOEVAL0))) {}, ((generic_type(_NOEVAL0)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro isextended.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 103 "core-math.c"
#define isextended(_NOEVAL0) isextended((generic_type(_Core_towidest(_NOEVAL0))){}, ((generic_type(_NOEVAL0)){}))
#else
#line 103 "core-math.c"
#define isextended(_NOEVAL0) _Generic((_Core_towidest(_NOEVAL0)),\
	decltype(_Core_iswidest): !isstandard(_NOEVAL0),\
	default: false)
#endif
#line 106 "core-math.c"

#ifdef __cplusplus
// A series of 2 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '(TE)+0ULL'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 108 "core-math.c"
template< typename _T1 >
#line 109 "core-math.c"
constexpr auto isULL(generic_type((unsigned long long) {}), _T1 TE) {
#line 109 "core-math.c"
    return true;
}
#line 110 "core-math.c"
template< typename _T0, typename _T1 >
#line 110 "core-math.c"
constexpr auto isULL(_T0, _T1 TE) {
#line 110 "core-math.c"
    return false;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 108 "core-math.c"
constexpr auto isULL(_T1 TE) {
#line 108 "core-math.c"
    return isULL((generic_type((TE)+0ULL)) {}, ((generic_type(TE)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro isULL.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 108 "core-math.c"
#define isULL(TE) isULL((generic_type((TE)+0ULL)){}, ((generic_type(TE)){}))
#else
#line 108 "core-math.c"
#define isULL(TE) _Generic(((TE)+0ULL),\
	unsigned long long: true,\
	default: false)
#endif
#line 111 "core-math.c"

/**
 ** @brief The core isunsigned macro
 **
 ** Is true if and only if the expression has an unsigned integer
 ** type.
 **
 ** @a _NOEVAL0 will not be evaluated but only inspected for its type.
 **/
#ifdef __cplusplus
// A series of 2 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '_Core_towidest(_NOEVAL0)'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 121 "core-math.c"
template< typename _T1 >
#line 122 "core-math.c"
constexpr auto isunsigned(generic_type((decltype(_Core_iswidest)) {}), _T1 _NOEVAL0) {
#line 122 "core-math.c"
    return (generic_type(_NOEVAL0))-1 > 0;
}
#line 123 "core-math.c"
template< typename _T0, typename _T1 >
#line 123 "core-math.c"
constexpr auto isunsigned(_T0, _T1 _NOEVAL0) {
#line 123 "core-math.c"
    return false;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 121 "core-math.c"
constexpr auto isunsigned(_T1 _NOEVAL0) {
#line 121 "core-math.c"
    return isunsigned((generic_type(_Core_towidest(_NOEVAL0))) {}, ((generic_type(_NOEVAL0)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro isunsigned.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 121 "core-math.c"
#define isunsigned(_NOEVAL0) isunsigned((generic_type(_Core_towidest(_NOEVAL0))){}, ((generic_type(_NOEVAL0)){}))
#else
#line 121 "core-math.c"
#define isunsigned(_NOEVAL0) _Generic((_Core_towidest(_NOEVAL0)),\
	decltype(_Core_iswidest): (generic_type(_NOEVAL0))-1 > 0,\
	default: false)
#endif
#line 124 "core-math.c"


/**
 ** @brief The core issigned macro
 **
 ** Is true if and only if the expression has a signed integer
 ** type.
 **
 ** @a _NOEVAL0 will not be evaluated but only inspected for its type.
 **/
#ifdef __cplusplus
// A series of 2 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '_Core_towidest(_NOEVAL0)'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 135 "core-math.c"
template< typename _T1 >
#line 136 "core-math.c"
constexpr auto issigned(generic_type((decltype(_Core_iswidest)) {}), _T1 _NOEVAL0) {
#line 136 "core-math.c"
    return (generic_type(_NOEVAL0))-1 < 0;
}
#line 137 "core-math.c"
template< typename _T0, typename _T1 >
#line 137 "core-math.c"
constexpr auto issigned(_T0, _T1 _NOEVAL0) {
#line 137 "core-math.c"
    return false;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 135 "core-math.c"
constexpr auto issigned(_T1 _NOEVAL0) {
#line 135 "core-math.c"
    return issigned((generic_type(_Core_towidest(_NOEVAL0))) {}, ((generic_type(_NOEVAL0)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro issigned.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 135 "core-math.c"
#define issigned(_NOEVAL0) issigned((generic_type(_Core_towidest(_NOEVAL0))){}, ((generic_type(_NOEVAL0)){}))
#else
#line 135 "core-math.c"
#define issigned(_NOEVAL0) _Generic((_Core_towidest(_NOEVAL0)),\
	decltype(_Core_iswidest): (generic_type(_NOEVAL0))-1 < 0,\
	default: false)
#endif
#line 138 "core-math.c"

#define tominus1(_NOEVAL0) ((generic_type(_NOEVAL0))-1)
#line 140 "core-math.c"

/**
 ** @brief The core tohighest macro
 **
 ** Returns the highest value representable in the type of @a _NOEVAL0.
 **
 ** @a _NOEVAL0 will not be evaluated but only inspected for its type.
 **/
#ifdef __cplusplus
// A series of 10 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '_NOEVAL0'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 150 "core-math.c"
template< typename _T1 >
#line 151 "core-math.c"
constexpr auto tohighest(generic_type((signed char) {}), _T1 _NOEVAL0) {
#line 151 "core-math.c"
    return                  (signed char)(((unsigned char)-1)/2);
}
#line 150 "core-math.c"
template< typename _T1 >
#line 151 "core-math.c"
constexpr auto tohighest(generic_type((signed short) {}), _T1 _NOEVAL0) {
#line 151 "core-math.c"
    return                  (signed short)(((unsigned short)-1)/2);
}
#line 150 "core-math.c"
template< typename _T1 >
#line 151 "core-math.c"
constexpr auto tohighest(generic_type((signed int) {}), _T1 _NOEVAL0) {
#line 151 "core-math.c"
    return                  (signed int)(((unsigned int)-1)/2);
}
#line 150 "core-math.c"
template< typename _T1 >
#line 151 "core-math.c"
constexpr auto tohighest(generic_type((signed long) {}), _T1 _NOEVAL0) {
#line 151 "core-math.c"
    return                  (signed long)(((unsigned long)-1)/2);
}
#line 150 "core-math.c"
template< typename _T1 >
#line 151 "core-math.c"
constexpr auto tohighest(generic_type((signed long long) {}), _T1 _NOEVAL0) {
#line 151 "core-math.c"
    return                  (signed long long)(((unsigned long long)-1)/2);
}
#line 152 "core-math.c"
template< typename _T1 >
#line 153 "core-math.c"
constexpr auto tohighest(generic_type((char) {}), _T1 _NOEVAL0) {
#line 153 "core-math.c"
    return         (char)(((unsigned char)-1)/(((char)-1 > 0) ? 1 : 2));
}
template< typename _T1 >
#line 154 "core-math.c"
constexpr auto tohighest(generic_type((float) {}), _T1 _NOEVAL0) {
#line 154 "core-math.c"
    return                                                   HUGE_VALF;
}
template< typename _T1 >
#line 155 "core-math.c"
constexpr auto tohighest(generic_type((double) {}), _T1 _NOEVAL0) {
#line 155 "core-math.c"
    return                                                   HUGE_VAL;
}
template< typename _T1 >
#line 156 "core-math.c"
constexpr auto tohighest(generic_type((long double) {}), _T1 _NOEVAL0) {
#line 156 "core-math.c"
    return                                             HUGE_VALL;
}
#line 157 "core-math.c"
template< typename _T0, typename _T1 >
#line 157 "core-math.c"
constexpr auto tohighest(_T0, _T1 _NOEVAL0) {
#line 157 "core-math.c"
    return (generic_type(_NOEVAL0))(((generic_type(_NOEVAL0))-1)/(((generic_type(_NOEVAL0))-1) < 0 ? 2 : 1));
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 152 "core-math.c"
constexpr auto tohighest(_T1 _NOEVAL0) {
#line 152 "core-math.c"
    return tohighest((generic_type(_NOEVAL0)) {}, ((generic_type(_NOEVAL0)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro tohighest.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 152 "core-math.c"
#define tohighest(_NOEVAL0) tohighest((generic_type(_NOEVAL0)){}, ((generic_type(_NOEVAL0)){}))
#else
#line 152 "core-math.c"
#define tohighest(_NOEVAL0) _Generic((_NOEVAL0),\
	signed char:                  (signed char)(((unsigned char)-1)/2),\
	signed short:                  (signed short)(((unsigned short)-1)/2),\
	signed int:                  (signed int)(((unsigned int)-1)/2),\
	signed long:                  (signed long)(((unsigned long)-1)/2),\
	signed long long:                  (signed long long)(((unsigned long long)-1)/2),\
	char:         (char)(((unsigned char)-1)/(((char)-1 > 0) ? 1 : 2)),\
	float:                                                   HUGE_VALF,\
	double:                                                   HUGE_VAL,\
	long double:                                             HUGE_VALL,\
	default: (generic_type(_NOEVAL0))(((generic_type(_NOEVAL0))-1)/(((generic_type(_NOEVAL0))-1) < 0 ? 2 : 1)))
#endif
#line 158 "core-math.c"

/**
 ** @brief The core tolowest macro
 **
 ** Returns the lowest value representable in the type of @a _NOEVAL0.
 **
 ** @a _NOEVAL0 will not be evaluated but only inspected for its type.
 **/
#ifdef __cplusplus
// A series of 2 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '_NOEVAL0'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 167 "core-math.c"
#line 168 "core-math.c"
template< typename _T0, typename _T1 >
#line 168 "core-math.c"
constexpr auto tolowest(_T0, _T1 _NOEVAL0) {
#line 168 "core-math.c"
    return (generic_type(_NOEVAL0))(-tohighest(_NOEVAL0)-isinteger(_NOEVAL0));
}
template< typename _T1 >
#line 169 "core-math.c"
constexpr auto tolowest(generic_type((bool) {}), _T1 _NOEVAL0) {
#line 169 "core-math.c"
    return                                            false;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 167 "core-math.c"
constexpr auto tolowest(_T1 _NOEVAL0) {
#line 167 "core-math.c"
    return tolowest((generic_type(_NOEVAL0)) {}, ((generic_type(_NOEVAL0)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro tolowest.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 167 "core-math.c"
#define tolowest(_NOEVAL0) tolowest((generic_type(_NOEVAL0)){}, ((generic_type(_NOEVAL0)){}))
#else
#line 167 "core-math.c"
#define tolowest(_NOEVAL0) _Generic((_NOEVAL0),\
	default: (generic_type(_NOEVAL0))(-tohighest(_NOEVAL0)-isinteger(_NOEVAL0)),\
	bool:                                            false)
#endif
#line 170 "core-math.c"

/**
 ** @brief The core isconstant macro
 **
 ** Returns @a true if and only if @a X is considered to be a constant
 ** expression.
 **
 ** The effect of this macro is implementation-dependent, because not
 ** all implementations will have the same idea what a constant
 ** expression is. E.g for any variable `x` the expression `x-x` is
 ** known to be zero, but not all implementations will realize that
 ** this can be considered constant.
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#define isconstant(X) _Core_isconstant(X)
#line 186 "core-math.c"

/**
 ** @brief The core isice macro
 **
 ** Returns @a true if and only if @a X is considered to be an integer
 ** constant expression (ICE).
 **
 ** The effect of this macro is implementation-dependent, because not
 ** all implementations will have the same idea what a constant
 ** expression is. E.g for any variable `x` the expression `x-x` is
 ** known to be zero, but not all implementations will realize that
 ** this can be considered constant.
 **
 ** ICEs of value zero are also null pointer constants, see `isnull`
 ** and `tonull`.
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#define isice(X) _Core_isice(X)
#line 205 "core-math.c"

/**
 ** @brief The core isnull macro
 **
 ** Returns @a true if and only if @a X is considered to be an integer
 ** constant expression (ICE) of value zero.
 **
 ** The effect of this macro is implementation-dependent, because not
 ** all implementations will have the same idea what a constant
 ** expression is. E.g for any variable `x` the expression `x-x` is
 ** known to be zero, but not all implementations will realize that
 ** this can be considered constant.
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#define isnull(X) _Core_isnull(X)
#line 221 "core-math.c"

/**
 ** @brief The core isvla macro
 **
 ** Returns `true` if @a X is a variable length array (VLA).
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#define isvla(X) (!isconstant(sizeof(X)))
#line 230 "core-math.c"

/**
 ** @brief The core ispointer macro
 **
 ** Returns `true` if the generic type of @a X is a pointer type, that
 ** if it is effectively a pointer type, an array type or a function
 ** type.
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#ifdef __cplusplus
// A series of 8 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '_Core_towidest(X)'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 241 "core-math.c"
template< typename _T1 >
#line 242 "core-math.c"
constexpr auto ispointer(generic_type((float) {}), _T1 X) {
#line 242 "core-math.c"
    return false;
}
template< typename _T1 >
#line 243 "core-math.c"
constexpr auto ispointer(generic_type((double) {}), _T1 X) {
#line 243 "core-math.c"
    return false;
}
template< typename _T1 >
#line 244 "core-math.c"
constexpr auto ispointer(generic_type((long double) {}), _T1 X) {
#line 244 "core-math.c"
    return false;
}
template< typename _T1 >
#line 245 "core-math.c"
constexpr auto ispointer(generic_type((complex_type(float)) {}), _T1 X) {
#line 245 "core-math.c"
    return false;
}
template< typename _T1 >
#line 246 "core-math.c"
constexpr auto ispointer(generic_type((complex_type(double)) {}), _T1 X) {
#line 246 "core-math.c"
    return false;
}
template< typename _T1 >
#line 247 "core-math.c"
constexpr auto ispointer(generic_type((complex_type(long double)) {}), _T1 X) {
#line 247 "core-math.c"
    return false;
}
template< typename _T1 >
#line 248 "core-math.c"
constexpr auto ispointer(generic_type((decltype(_Core_iswidest)) {}), _T1 X) {
#line 248 "core-math.c"
    return false;
}
#line 249 "core-math.c"
template< typename _T0, typename _T1 >
#line 249 "core-math.c"
constexpr auto ispointer(_T0, _T1 X) {
#line 249 "core-math.c"
    return true;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 241 "core-math.c"
constexpr auto ispointer(_T1 X) {
#line 241 "core-math.c"
    return ispointer((generic_type(_Core_towidest(X))) {}, ((generic_type(X)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro ispointer.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 241 "core-math.c"
#define ispointer(X) ispointer((generic_type(_Core_towidest(X))){}, ((generic_type(X)){}))
#else
#line 241 "core-math.c"
#define ispointer(X) _Generic((_Core_towidest(X)),\
	float: false,\
	double: false,\
	long double: false,\
	complex_type(float): false,\
	complex_type(double): false,\
	complex_type(long double): false,\
	decltype(_Core_iswidest): false,\
	default: true)
#endif
#line 250 "core-math.c"

#ifdef __cplusplus
// A series of 5 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '(_NOEVAL0)'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 252 "core-math.c"
template< typename _T1 >
#line 253 "core-math.c"
constexpr auto isnonvoidpointer(generic_type((void*) {}), _T1 _NOEVAL0) {
#line 253 "core-math.c"
    return false;
}
template< typename _T1 >
#line 254 "core-math.c"
constexpr auto isnonvoidpointer(generic_type((void const*) {}), _T1 _NOEVAL0) {
#line 254 "core-math.c"
    return false;
}
template< typename _T1 >
#line 255 "core-math.c"
constexpr auto isnonvoidpointer(generic_type((void volatile*) {}), _T1 _NOEVAL0) {
#line 255 "core-math.c"
    return false;
}
template< typename _T1 >
#line 256 "core-math.c"
constexpr auto isnonvoidpointer(generic_type((void const volatile*) {}), _T1 _NOEVAL0) {
#line 256 "core-math.c"
    return false;
}
#line 257 "core-math.c"
template< typename _T0, typename _T1 >
#line 257 "core-math.c"
constexpr auto isnonvoidpointer(_T0, _T1 _NOEVAL0) {
#line 257 "core-math.c"
    return ispointer(_NOEVAL0);
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 252 "core-math.c"
constexpr auto isnonvoidpointer(_T1 _NOEVAL0) {
#line 252 "core-math.c"
    return isnonvoidpointer((generic_type((_NOEVAL0))) {}, ((generic_type(_NOEVAL0)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro isnonvoidpointer.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 252 "core-math.c"
#define isnonvoidpointer(_NOEVAL0) isnonvoidpointer((generic_type((_NOEVAL0))){}, ((generic_type(_NOEVAL0)){}))
#else
#line 252 "core-math.c"
#define isnonvoidpointer(_NOEVAL0) _Generic(((_NOEVAL0)),\
	void*: false,\
	void const*: false,\
	void volatile*: false,\
	void const volatile*: false,\
	default: ispointer(_NOEVAL0))
#endif
#line 258 "core-math.c"

/**
 ** @brief The core isfunction macro
 **
 ** Returns `true` if the generic type of @a _NOEVAL0 is a function pointer
 ** type, that if it is effectively a function pointer type or a
 ** function type.
 **
 ** @a _NOEVAL0 will not be evaluated but only inspected for its type.
 **/
#ifdef __cplusplus
// A series of 8 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '_Core_towidest(_NOEVAL0)'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 269 "core-math.c"
template< typename _T1 >
#line 270 "core-math.c"
constexpr auto isfunction(generic_type((float) {}), _T1 _NOEVAL0) {
#line 270 "core-math.c"
    return false;
}
template< typename _T1 >
#line 271 "core-math.c"
constexpr auto isfunction(generic_type((double) {}), _T1 _NOEVAL0) {
#line 271 "core-math.c"
    return false;
}
template< typename _T1 >
#line 272 "core-math.c"
constexpr auto isfunction(generic_type((long double) {}), _T1 _NOEVAL0) {
#line 272 "core-math.c"
    return false;
}
template< typename _T1 >
#line 273 "core-math.c"
constexpr auto isfunction(generic_type((complex_type(float)) {}), _T1 _NOEVAL0) {
#line 273 "core-math.c"
    return false;
}
template< typename _T1 >
#line 274 "core-math.c"
constexpr auto isfunction(generic_type((complex_type(double)) {}), _T1 _NOEVAL0) {
#line 274 "core-math.c"
    return false;
}
template< typename _T1 >
#line 275 "core-math.c"
constexpr auto isfunction(generic_type((complex_type(long double)) {}), _T1 _NOEVAL0) {
#line 275 "core-math.c"
    return false;
}
template< typename _T1 >
#line 276 "core-math.c"
constexpr auto isfunction(generic_type((decltype(_Core_iswidest)) {}), _T1 _NOEVAL0) {
#line 276 "core-math.c"
    return false;
}
#line 277 "core-math.c"
template< typename _T0, typename _T1 >
#line 277 "core-math.c"
constexpr auto isfunction(_T0, _T1 _NOEVAL0) {
#line 277 "core-math.c"
    return issame((_NOEVAL0), *(_NOEVAL0));
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 269 "core-math.c"
constexpr auto isfunction(_T1 _NOEVAL0) {
#line 269 "core-math.c"
    return isfunction((generic_type(_Core_towidest(_NOEVAL0))) {}, ((generic_type(_NOEVAL0)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro isfunction.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 269 "core-math.c"
#define isfunction(_NOEVAL0) isfunction((generic_type(_Core_towidest(_NOEVAL0))){}, ((generic_type(_NOEVAL0)){}))
#else
#line 269 "core-math.c"
#define isfunction(_NOEVAL0) _Generic((_Core_towidest(_NOEVAL0)),\
	float: false,\
	double: false,\
	long double: false,\
	complex_type(float): false,\
	complex_type(double): false,\
	complex_type(long double): false,\
	decltype(_Core_iswidest): false,\
	default: issame((_NOEVAL0), *(_NOEVAL0)))
#endif
#line 278 "core-math.c"

/**
 ** @brief The core towidth macro
 **
 ** Returns the width of the type of @a X, result has type `size_t`.
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#define towidth(X) [](auto x){ auto const y = tohighest(x); size_t lo = 1; size_t hi = sizeof(x)*CHAR_BIT; while (true) { size_t mi = (lo + hi)/2; if (y >> mi) { lo = mi; if (lo+1 < hi) return hi; } else { hi = mi; if (lo+1 < hi) return lo; } } }((generic_type(X)){})
#line 302 "core-math.c"

#ifndef __cplusplus
#line 304 "core-math.c"

#line 322 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all floating types
 **
 ** default floating type is `double`
 **/
#define acos [](auto x){ return _Generic(x, float: acosf, long double: acosl, complex_type(float): cacosf, complex_type(double): cacos, complex_type(long double): cacosl, default: acos)(x); }
#line 322 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all floating types
 **
 ** default floating type is `double`
 **/
#define acosh [](auto x){ return _Generic(x, float: acoshf, long double: acoshl, complex_type(float): cacoshf, complex_type(double): cacosh, complex_type(long double): cacoshl, default: acosh)(x); }
#line 322 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all floating types
 **
 ** default floating type is `double`
 **/
#define asin [](auto x){ return _Generic(x, float: asinf, long double: asinl, complex_type(float): casinf, complex_type(double): casin, complex_type(long double): casinl, default: asin)(x); }
#line 322 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all floating types
 **
 ** default floating type is `double`
 **/
#define asinh [](auto x){ return _Generic(x, float: asinhf, long double: asinhl, complex_type(float): casinhf, complex_type(double): casinh, complex_type(long double): casinhl, default: asinh)(x); }
#line 322 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all floating types
 **
 ** default floating type is `double`
 **/
#define atan [](auto x){ return _Generic(x, float: atanf, long double: atanl, complex_type(float): catanf, complex_type(double): catan, complex_type(long double): catanl, default: atan)(x); }
#line 322 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all floating types
 **
 ** default floating type is `double`
 **/
#define atanh [](auto x){ return _Generic(x, float: atanhf, long double: atanhl, complex_type(float): catanhf, complex_type(double): catanh, complex_type(long double): catanhl, default: atanh)(x); }
#line 322 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all floating types
 **
 ** default floating type is `double`
 **/
#define cos [](auto x){ return _Generic(x, float: cosf, long double: cosl, complex_type(float): ccosf, complex_type(double): ccos, complex_type(long double): ccosl, default: cos)(x); }
#line 322 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all floating types
 **
 ** default floating type is `double`
 **/
#define cosh [](auto x){ return _Generic(x, float: coshf, long double: coshl, complex_type(float): ccoshf, complex_type(double): ccosh, complex_type(long double): ccoshl, default: cosh)(x); }
#line 322 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all floating types
 **
 ** default floating type is `double`
 **/
#define exp [](auto x){ return _Generic(x, float: expf, long double: expl, complex_type(float): cexpf, complex_type(double): cexp, complex_type(long double): cexpl, default: exp)(x); }
#line 322 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all floating types
 **
 ** default floating type is `double`
 **/
#define log [](auto x){ return _Generic(x, float: logf, long double: logl, complex_type(float): clogf, complex_type(double): clog, complex_type(long double): clogl, default: log)(x); }
#line 322 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all floating types
 **
 ** default floating type is `double`
 **/
#define log10 [](auto x){ return _Generic(x, float: log10f, long double: log10l, complex_type(float): clog10f, complex_type(double): clog10, complex_type(long double): clog10l, default: log10)(x); }
#line 322 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all floating types
 **
 ** default floating type is `double`
 **/
#define sin [](auto x){ return _Generic(x, float: sinf, long double: sinl, complex_type(float): csinf, complex_type(double): csin, complex_type(long double): csinl, default: sin)(x); }
#line 322 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all floating types
 **
 ** default floating type is `double`
 **/
#define sinh [](auto x){ return _Generic(x, float: sinhf, long double: sinhl, complex_type(float): csinhf, complex_type(double): csinh, complex_type(long double): csinhl, default: sinh)(x); }
#line 322 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all floating types
 **
 ** default floating type is `double`
 **/
#define sqrt [](auto x){ return _Generic(x, float: sqrtf, long double: sqrtl, complex_type(float): csqrtf, complex_type(double): csqrt, complex_type(long double): csqrtl, default: sqrt)(x); }
#line 322 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all floating types
 **
 ** default floating type is `double`
 **/
#define tan [](auto x){ return _Generic(x, float: tanf, long double: tanl, complex_type(float): ctanf, complex_type(double): ctan, complex_type(long double): ctanl, default: tan)(x); }
#line 322 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all floating types
 **
 ** default floating type is `double`
 **/
#define tanh [](auto x){ return _Generic(x, float: tanhf, long double: tanhl, complex_type(float): ctanhf, complex_type(double): ctanh, complex_type(long double): ctanhl, default: tanh)(x); }
#line 341 "core-math.c"

#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define cbrt [](auto x){ return _Generic(x, float: cbrtf, long double: cbrtl, default: cbrt)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define ceil [](auto x){ return _Generic(x, float: ceilf, long double: ceill, default: ceil)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define erf [](auto x){ return _Generic(x, float: erff, long double: erfl, default: erf)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define erfc [](auto x){ return _Generic(x, float: erfcf, long double: erfcl, default: erfc)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define exp2 [](auto x){ return _Generic(x, float: exp2f, long double: exp2l, default: exp2)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define expm1 [](auto x){ return _Generic(x, float: expm1f, long double: expm1l, default: expm1)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define fabs [](auto x){ return _Generic(x, float: fabsf, long double: fabsl, default: fabs)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define floor [](auto x){ return _Generic(x, float: floorf, long double: floorl, default: floor)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define ilogb [](auto x){ return _Generic(x, float: ilogbf, long double: ilogbl, default: ilogb)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define lgamma [](auto x){ return _Generic(x, float: lgammaf, long double: lgammal, default: lgamma)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define llrint [](auto x){ return _Generic(x, float: llrintf, long double: llrintl, default: llrint)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define llround [](auto x){ return _Generic(x, float: llroundf, long double: llroundl, default: llround)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define log1p [](auto x){ return _Generic(x, float: log1pf, long double: log1pl, default: log1p)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define log2 [](auto x){ return _Generic(x, float: log2f, long double: log2l, default: log2)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define logb [](auto x){ return _Generic(x, float: logbf, long double: logbl, default: logb)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define lrint [](auto x){ return _Generic(x, float: lrintf, long double: lrintl, default: lrint)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define lround [](auto x){ return _Generic(x, float: lroundf, long double: lroundl, default: lround)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define nearbyint [](auto x){ return _Generic(x, float: nearbyintf, long double: nearbyintl, default: nearbyint)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define rint [](auto x){ return _Generic(x, float: rintf, long double: rintl, default: rint)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define round [](auto x){ return _Generic(x, float: roundf, long double: roundl, default: round)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define tgamma [](auto x){ return _Generic(x, float: tgammaf, long double: tgammal, default: tgamma)(x); }
#line 365 "core-math.c"
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#define trunc [](auto x){ return _Generic(x, float: truncf, long double: truncl, default: trunc)(x); }
#line 381 "core-math.c"

#line 391 "core-math.c"

#define atan2 [](auto x, auto y){ return _Generic(x+y, float: atan2f, long double: atan2l, default: atan2)(x, y); }
#line 391 "core-math.c"

#define fdim [](auto x, auto y){ return _Generic(x+y, float: fdimf, long double: fdiml, default: fdim)(x, y); }
#line 391 "core-math.c"

#define fmax [](auto x, auto y){ return _Generic(x+y, float: fmaxf, long double: fmaxl, default: fmax)(x, y); }
#line 391 "core-math.c"

#define fmin [](auto x, auto y){ return _Generic(x+y, float: fminf, long double: fminl, default: fmin)(x, y); }
#line 391 "core-math.c"

#define fmod [](auto x, auto y){ return _Generic(x+y, float: fmodf, long double: fmodl, default: fmod)(x, y); }
#line 391 "core-math.c"

#define hypot [](auto x, auto y){ return _Generic(x+y, float: hypotf, long double: hypotl, default: hypot)(x, y); }
#line 391 "core-math.c"

#define pow [](auto x, auto y){ return _Generic(x+y, float: powf, long double: powl, default: pow)(x, y); }
#line 391 "core-math.c"

#define remainder [](auto x, auto y){ return _Generic(x+y, float: remainderf, long double: remainderl, default: remainder)(x, y); }
#line 403 "core-math.c"

#line 406 "core-math.c"
#define fma [](auto x, auto y, auto z){ return _Generic(x+y+z, float: fmaf, long double: fmal, default: fma)(x, y, z); }
#line 417 "core-math.c"

#line 420 "core-math.c"
#define ldexp [](auto x, int k){ return _Generic(x, float: ldexpf, long double: ldexpl, default: ldexp)(x, k); }
#line 431 "core-math.c"

#line 434 "core-math.c"
#define remquo [](auto x, auto y, int quo[1]){ return _Generic(x+y, float: remquof, long double: remquol, default: remquo)(x, y, quo); }
#line 445 "core-math.c"

#line 448 "core-math.c"
#define frexp [](auto x, int k[1]){ return _Generic(x, float: frexpf, long double: frexpl, default: frexp)(x, k); }
#line 459 "core-math.c"

#line 462 "core-math.c"
#define modf [](auto x, generic_type(x)* iptr[1]){ return _Generic(x, float: modff, long double: modfl, default: modf)(x, iptr); }
#line 473 "core-math.c"

/*


// only for real types

F copysign(R x, S y);
F nextafter(R x, F y);
F nexttoward(R x, long double y);

U math_pdiff(R x, S y);

*/

#endif
#line 488 "core-math.c"

#undef carg
#define carg [](auto z){ printf("found %g %g\n", real_value(z), imaginary_value(z)); return atan2(imaginary_value(z), real_value(z)); }
#line 497 "core-math.c"


#define _Core_cproj [](auto z){ if (isinf(real_value(z)) || isinf(imaginary_value(z))) { z = INFINITY + 1.0f*I * copysign(0.0f, imaginary_value(z)); } return z; }
#line 508 "core-math.c"

#define _Core_proj [](auto z){ if (isinf(z)) { z = INFINITY; } return z; }
#line 518 "core-math.c"


#ifdef __cplusplus
// A series of 7 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression 'X'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 521 "core-math.c"
template< typename _T1 >
#line 522 "core-math.c"
constexpr auto _Core_cproj2(generic_type((float) {}), _T1 X) {
#line 522 "core-math.c"
    return _Core_proj;
}
template< typename _T1 >
#line 523 "core-math.c"
constexpr auto _Core_cproj2(generic_type((double) {}), _T1 X) {
#line 523 "core-math.c"
    return _Core_proj;
}
template< typename _T1 >
#line 524 "core-math.c"
constexpr auto _Core_cproj2(generic_type((long double) {}), _T1 X) {
#line 524 "core-math.c"
    return _Core_proj;
}
template< typename _T1 >
#line 525 "core-math.c"
constexpr auto _Core_cproj2(generic_type((complex_type(float)) {}), _T1 X) {
#line 525 "core-math.c"
    return _Core_cproj;
}
template< typename _T1 >
#line 526 "core-math.c"
constexpr auto _Core_cproj2(generic_type((complex_type(double)) {}), _T1 X) {
#line 526 "core-math.c"
    return _Core_cproj;
}
template< typename _T1 >
#line 527 "core-math.c"
constexpr auto _Core_cproj2(generic_type((complex_type(long double)) {}), _T1 X) {
#line 527 "core-math.c"
    return _Core_cproj;
}
#line 528 "core-math.c"
template< typename _T0, typename _T1 >
#line 528 "core-math.c"
constexpr auto _Core_cproj2(_T0, _T1 X) {
#line 528 "core-math.c"
    return (double(*)(double))_Core_proj;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 521 "core-math.c"
constexpr auto _Core_cproj2(_T1 X) {
#line 521 "core-math.c"
    return _Core_cproj2((generic_type(X)) {}, ((generic_type(X)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro _Core_cproj2.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 521 "core-math.c"
#define _Core_cproj2(X) _Core_cproj2((generic_type(X)){}, ((generic_type(X)){}))
#else
#line 521 "core-math.c"
#define _Core_cproj2(X) _Generic((X),\
	float: _Core_proj,\
	double: _Core_proj,\
	long double: _Core_proj,\
	complex_type(float): _Core_cproj,\
	complex_type(double): _Core_cproj,\
	complex_type(long double): _Core_cproj,\
	default: (double(*)(double))_Core_proj)
#endif
#line 529 "core-math.c"

#define cproj(X) _Core_cproj2(X)(X)
#line 531 "core-math.c"

#define _Core_cabs [](auto z){ return hypot(real_value(z), imaginary_value(z)); }
#line 538 "core-math.c"

#define _Core_fabs [](auto x){ return x < 0 ? -x : x; }
#line 545 "core-math.c"

/**
 ** @brief The core tounsigned macro
 **
 ** Returns the unsigned value representable in the type of @a X.
 **/
#ifdef __cplusplus
// A series of 7 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression 'X'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 553 "core-math.c"
template< typename _T1 >
#line 554 "core-math.c"
constexpr auto tounsigned(generic_type((signed char) {}), _T1 X) {
#line 554 "core-math.c"
    return                  ((unsigned char)X);
}
#line 553 "core-math.c"
template< typename _T1 >
#line 554 "core-math.c"
constexpr auto tounsigned(generic_type((signed short) {}), _T1 X) {
#line 554 "core-math.c"
    return                  ((unsigned short)X);
}
#line 553 "core-math.c"
template< typename _T1 >
#line 554 "core-math.c"
constexpr auto tounsigned(generic_type((signed int) {}), _T1 X) {
#line 554 "core-math.c"
    return                  ((unsigned int)X);
}
#line 553 "core-math.c"
template< typename _T1 >
#line 554 "core-math.c"
constexpr auto tounsigned(generic_type((signed long) {}), _T1 X) {
#line 554 "core-math.c"
    return                  ((unsigned long)X);
}
#line 553 "core-math.c"
template< typename _T1 >
#line 554 "core-math.c"
constexpr auto tounsigned(generic_type((signed long long) {}), _T1 X) {
#line 554 "core-math.c"
    return                  ((unsigned long long)X);
}
#line 555 "core-math.c"
template< typename _T1 >
#line 556 "core-math.c"
constexpr auto tounsigned(generic_type((char) {}), _T1 X) {
#line 556 "core-math.c"
    return                         ((unsigned char)X);
}
#line 557 "core-math.c"
template< typename _T0, typename _T1 >
#line 557 "core-math.c"
constexpr auto tounsigned(_T0, _T1 X) {
#line 557 "core-math.c"
    return                                     X;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 555 "core-math.c"
constexpr auto tounsigned(_T1 X) {
#line 555 "core-math.c"
    return tounsigned((generic_type(X)) {}, (X));
}
/**
 ** The second is the principal user interface and is implemented as macro tounsigned.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** Other then that, the following arguments may be evaluated for the result: X
 **/
#line 555 "core-math.c"
#define tounsigned(X) tounsigned((generic_type(X)){}, (X))
#else
#line 555 "core-math.c"
#define tounsigned(X) _Generic((X),\
	signed char:                  ((unsigned char)X),\
	signed short:                  ((unsigned short)X),\
	signed int:                  ((unsigned int)X),\
	signed long:                  ((unsigned long)X),\
	signed long long:                  ((unsigned long long)X),\
	char:                         ((unsigned char)X),\
	default:                                     X)
#endif
#line 558 "core-math.c"


#define _Core_iabs [](auto x){ auto ret = tounsigned(x); return ret > tohighest(x) ? -ret : ret; }
#line 567 "core-math.c"

#ifdef __cplusplus
// A series of 7 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression 'X'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 569 "core-math.c"
template< typename _T1 >
#line 570 "core-math.c"
constexpr auto _Core_abs(generic_type((float) {}), _T1 X) {
#line 570 "core-math.c"
    return _Core_fabs;
}
template< typename _T1 >
#line 571 "core-math.c"
constexpr auto _Core_abs(generic_type((double) {}), _T1 X) {
#line 571 "core-math.c"
    return _Core_fabs;
}
template< typename _T1 >
#line 572 "core-math.c"
constexpr auto _Core_abs(generic_type((long double) {}), _T1 X) {
#line 572 "core-math.c"
    return _Core_fabs;
}
template< typename _T1 >
#line 573 "core-math.c"
constexpr auto _Core_abs(generic_type((complex_type(float)) {}), _T1 X) {
#line 573 "core-math.c"
    return _Core_cabs;
}
template< typename _T1 >
#line 574 "core-math.c"
constexpr auto _Core_abs(generic_type((complex_type(double)) {}), _T1 X) {
#line 574 "core-math.c"
    return _Core_cabs;
}
template< typename _T1 >
#line 575 "core-math.c"
constexpr auto _Core_abs(generic_type((complex_type(long double)) {}), _T1 X) {
#line 575 "core-math.c"
    return _Core_cabs;
}
#line 576 "core-math.c"
template< typename _T0, typename _T1 >
#line 576 "core-math.c"
constexpr auto _Core_abs(_T0, _T1 X) {
#line 576 "core-math.c"
    return _Core_iabs;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 569 "core-math.c"
constexpr auto _Core_abs(_T1 X) {
#line 569 "core-math.c"
    return _Core_abs((generic_type(X)) {}, ((generic_type(X)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro _Core_abs.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 569 "core-math.c"
#define _Core_abs(X) _Core_abs((generic_type(X)){}, ((generic_type(X)){}))
#else
#line 569 "core-math.c"
#define _Core_abs(X) _Generic((X),\
	float: _Core_fabs,\
	double: _Core_fabs,\
	long double: _Core_fabs,\
	complex_type(float): _Core_cabs,\
	complex_type(double): _Core_cabs,\
	complex_type(long double): _Core_cabs,\
	default: _Core_iabs)
#endif
#line 577 "core-math.c"

#define abs [](auto x){ return _Core_abs(x)(x); }
#line 579 "core-math.c"

#define _Core_cabs\u00B2 [](auto z){ auto r = real_value(z); auto i = imaginary_value(z); if (isinf(r) || isinf(i)) { r = INFINITY; } else { r = r * r + i * i; } return r; }
#line 593 "core-math.c"

#define _Core_fabs\u00B2 [](auto x){ return x * x; }
#line 600 "core-math.c"

#ifdef __cplusplus
// A series of 6 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression 'X'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 602 "core-math.c"
template< typename _T1 >
#line 603 "core-math.c"
constexpr auto _Core_abs\u00B2(generic_type((float) {}), _T1 X) {
#line 603 "core-math.c"
    return _Core_fabs\u00B2;
}
template< typename _T1 >
#line 604 "core-math.c"
constexpr auto _Core_abs\u00B2(generic_type((long double) {}), _T1 X) {
#line 604 "core-math.c"
    return _Core_fabs\u00B2;
}
template< typename _T1 >
#line 605 "core-math.c"
constexpr auto _Core_abs\u00B2(generic_type((complex_type(float)) {}), _T1 X) {
#line 605 "core-math.c"
    return _Core_cabs\u00B2;
}
template< typename _T1 >
#line 606 "core-math.c"
constexpr auto _Core_abs\u00B2(generic_type((complex_type(double)) {}), _T1 X) {
#line 606 "core-math.c"
    return _Core_cabs\u00B2;
}
template< typename _T1 >
#line 607 "core-math.c"
constexpr auto _Core_abs\u00B2(generic_type((complex_type(long double)) {}), _T1 X) {
#line 607 "core-math.c"
    return _Core_cabs\u00B2;
}
#line 608 "core-math.c"
template< typename _T0, typename _T1 >
#line 608 "core-math.c"
constexpr auto _Core_abs\u00B2(_T0, _T1 X) {
#line 608 "core-math.c"
    return (double(*)(double))_Core_fabs\u00B2;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 602 "core-math.c"
constexpr auto _Core_abs\u00B2(_T1 X) {
#line 602 "core-math.c"
    return _Core_abs\u00B2((generic_type(X)) {}, ((generic_type(X)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro _Core_abs\u00B2.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 602 "core-math.c"
#define _Core_abs\u00B2(X) _Core_abs\u00B2((generic_type(X)){}, ((generic_type(X)){}))
#else
#line 602 "core-math.c"
#define _Core_abs\u00B2(X) _Generic((X),\
	float: _Core_fabs\u00B2,\
	long double: _Core_fabs\u00B2,\
	complex_type(float): _Core_cabs\u00B2,\
	complex_type(double): _Core_cabs\u00B2,\
	complex_type(long double): _Core_cabs\u00B2,\
	default: (double(*)(double))_Core_fabs\u00B2)
#endif
#line 609 "core-math.c"

#define abs\u00B2 [](auto x){ return _Core_abs\u00B2(x)(x); }
#line 611 "core-math.c"

#ifdef __cplusplus
// A series of 3 overloaded interfaces with 3 parameters that
// trigger generic selection by the type of expression 'X'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 2 parameters, that is without that first parameter.
#line 613 "core-math.c"
template< typename _T1, typename _T2 >
#line 614 "core-math.c"
constexpr auto _CORE_scalbn(generic_type((float) {}), _T1 X, _T2 N) {
#line 614 "core-math.c"
    return scalbnf((X), (N));
}
template< typename _T1, typename _T2 >
#line 615 "core-math.c"
constexpr auto _CORE_scalbn(generic_type((long double) {}), _T1 X, _T2 N) {
#line 615 "core-math.c"
    return scalbnl((X), (N));
}
#line 616 "core-math.c"
template< typename _T0, typename _T1, typename _T2 >
#line 616 "core-math.c"
constexpr auto _CORE_scalbn(_T0, _T1 X, _T2 N) {
#line 616 "core-math.c"
    return scalbn((X), (N));
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1, typename _T2 >
#line 613 "core-math.c"
constexpr auto _CORE_scalbn(_T1 X, _T2 N) {
#line 613 "core-math.c"
    return _CORE_scalbn((generic_type(X)) {}, (X), (N));
}
/**
 ** The second is the principal user interface and is implemented as macro _CORE_scalbn.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** Other then that, the following arguments may be evaluated for the result: N X
 **/
#line 613 "core-math.c"
#define _CORE_scalbn(X, N) _CORE_scalbn((generic_type(X)){}, (X), (N))
#else
#line 613 "core-math.c"
#define _CORE_scalbn(X, N) _Generic((X),\
	float: scalbnf((X), (N)),\
	long double: scalbnl((X), (N)),\
	default: scalbn((X), (N)))
#endif
#line 617 "core-math.c"

#ifdef __cplusplus
// A series of 3 overloaded interfaces with 3 parameters that
// trigger generic selection by the type of expression 'X'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 2 parameters, that is without that first parameter.
#line 619 "core-math.c"
template< typename _T1, typename _T2 >
#line 620 "core-math.c"
constexpr auto _CORE_scalbln(generic_type((float) {}), _T1 X, _T2 N) {
#line 620 "core-math.c"
    return scalblnf((X), (N));
}
template< typename _T1, typename _T2 >
#line 621 "core-math.c"
constexpr auto _CORE_scalbln(generic_type((long double) {}), _T1 X, _T2 N) {
#line 621 "core-math.c"
    return scalblnl((X), (N));
}
#line 622 "core-math.c"
template< typename _T0, typename _T1, typename _T2 >
#line 622 "core-math.c"
constexpr auto _CORE_scalbln(_T0, _T1 X, _T2 N) {
#line 622 "core-math.c"
    return scalbln((X), (N));
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1, typename _T2 >
#line 619 "core-math.c"
constexpr auto _CORE_scalbln(_T1 X, _T2 N) {
#line 619 "core-math.c"
    return _CORE_scalbln((generic_type(X)) {}, (X), (N));
}
/**
 ** The second is the principal user interface and is implemented as macro _CORE_scalbln.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** Other then that, the following arguments may be evaluated for the result: N X
 **/
#line 619 "core-math.c"
#define _CORE_scalbln(X, N) _CORE_scalbln((generic_type(X)){}, (X), (N))
#else
#line 619 "core-math.c"
#define _CORE_scalbln(X, N) _Generic((X),\
	float: scalblnf((X), (N)),\
	long double: scalblnl((X), (N)),\
	default: scalbln((X), (N)))
#endif
#line 623 "core-math.c"

#ifdef __cplusplus
// A series of 3 overloaded interfaces with 3 parameters that
// trigger generic selection by the type of expression 'N'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 2 parameters, that is without that first parameter.
#line 625 "core-math.c"
template< typename _T1, typename _T2 >
#line 626 "core-math.c"
constexpr auto _Core_scalbn(generic_type((long) {}), _T1 X, _T2 N) {
#line 626 "core-math.c"
    return _CORE_scalbln((X), (N));
}
template< typename _T1, typename _T2 >
#line 627 "core-math.c"
constexpr auto _Core_scalbn(generic_type((long long) {}), _T1 X, _T2 N) {
#line 627 "core-math.c"
    return _CORE_scalbln((X), (long)(N));
}
#line 628 "core-math.c"
template< typename _T0, typename _T1, typename _T2 >
#line 628 "core-math.c"
constexpr auto _Core_scalbn(_T0, _T1 X, _T2 N) {
#line 628 "core-math.c"
    return _CORE_scalbn((X), (int)(N));
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1, typename _T2 >
#line 625 "core-math.c"
constexpr auto _Core_scalbn(_T1 X, _T2 N) {
#line 625 "core-math.c"
    return _Core_scalbn((generic_type(N)) {}, (X), (N));
}
/**
 ** The second is the principal user interface and is implemented as macro _Core_scalbn.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** Other then that, the following arguments may be evaluated for the result: N X
 **/
#line 625 "core-math.c"
#define _Core_scalbn(X, N) _Core_scalbn((generic_type(N)){}, (X), (N))
#else
#line 625 "core-math.c"
#define _Core_scalbn(X, N) _Generic((N),\
	long: _CORE_scalbln((X), (N)),\
	long long: _CORE_scalbln((X), (long)(N)),\
	default: _CORE_scalbn((X), (int)(N)))
#endif
#line 629 "core-math.c"


#undef _Core_scalbn
#define scalbn [](auto x, auto n){ return _Core_scalbn(n, x, n); }
#line 633 "core-math.c"


/**
 ** @brief The core max type-generic macro
 **
 ** Returns the maximum of the two arguments. The type is the usual
 ** type for arithmetic conversion of binary operators.
 **/
#define max [](auto a, auto b){ decltype(a+b) A = a; decltype(a+b) B = b; /* We only have to be precocious if the types have different      signedness. */ if (isinteger(A) && (isunsigned(a) != isunsigned(b))) { /* If one is negative, the other is not because the type is an        unsigned type. So the other one is the maximum. Only one of two        lines may trigger. */ if (a < 0) return B; if (b < 0) return A; } /* Here both have the same sign, so the conversion did the right      thing */ return (A < B) ? B : A; }
#line 660 "core-math.c"

/**
 ** @brief The core tosigned macro
 **
 ** Returns the signed value representable in the type of @a _NOEVAL0.
 **
 ** @a _NOEVAL0 will not be evaluated but only inspected for its type.
 **/
#ifdef __cplusplus
// A series of 7 overloaded interfaces with 2 parameters that
// trigger generic selection by the type of expression '_NOEVAL0'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 1 parameters, that is without that first parameter.
#line 670 "core-math.c"
template< typename _T1 >
#line 671 "core-math.c"
constexpr auto tosigned(generic_type((unsigned char) {}), _T1 _NOEVAL0) {
#line 671 "core-math.c"
    return                  tolowest((signed char)0);
}
#line 670 "core-math.c"
template< typename _T1 >
#line 671 "core-math.c"
constexpr auto tosigned(generic_type((unsigned short) {}), _T1 _NOEVAL0) {
#line 671 "core-math.c"
    return                  tolowest((signed short)0);
}
#line 670 "core-math.c"
template< typename _T1 >
#line 671 "core-math.c"
constexpr auto tosigned(generic_type((unsigned int) {}), _T1 _NOEVAL0) {
#line 671 "core-math.c"
    return                  tolowest((signed int)0);
}
#line 670 "core-math.c"
template< typename _T1 >
#line 671 "core-math.c"
constexpr auto tosigned(generic_type((unsigned long) {}), _T1 _NOEVAL0) {
#line 671 "core-math.c"
    return                  tolowest((signed long)0);
}
#line 670 "core-math.c"
template< typename _T1 >
#line 671 "core-math.c"
constexpr auto tosigned(generic_type((unsigned long long) {}), _T1 _NOEVAL0) {
#line 671 "core-math.c"
    return                  tolowest((signed long long)0);
}
#line 672 "core-math.c"
template< typename _T1 >
#line 673 "core-math.c"
constexpr auto tosigned(generic_type((char) {}), _T1 _NOEVAL0) {
#line 673 "core-math.c"
    return                         tolowest((signed char)0);
}
#line 674 "core-math.c"
template< typename _T0, typename _T1 >
#line 674 "core-math.c"
constexpr auto tosigned(_T0, _T1 _NOEVAL0) {
#line 674 "core-math.c"
    return                      tolowest(_NOEVAL0);
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1 >
#line 672 "core-math.c"
constexpr auto tosigned(_T1 _NOEVAL0) {
#line 672 "core-math.c"
    return tosigned((generic_type(_NOEVAL0)) {}, ((generic_type(_NOEVAL0)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro tosigned.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 672 "core-math.c"
#define tosigned(_NOEVAL0) tosigned((generic_type(_NOEVAL0)){}, ((generic_type(_NOEVAL0)){}))
#else
#line 672 "core-math.c"
#define tosigned(_NOEVAL0) _Generic((_NOEVAL0),\
	unsigned char:                  tolowest((signed char)0),\
	unsigned short:                  tolowest((signed short)0),\
	unsigned int:                  tolowest((signed int)0),\
	unsigned long:                  tolowest((signed long)0),\
	unsigned long long:                  tolowest((signed long long)0),\
	char:                         tolowest((signed char)0),\
	default:                      tolowest(_NOEVAL0))
#endif
#line 675 "core-math.c"

/*
 * The implementation of the min feature is particularly difficult
 * because the system to compute a common type is not favorable. There
 * are 3 signed, 3 unsigned and 3 floating types: computing all
 * possible combinations would need 81 prototypes.
 */

#define _Core_min [](auto a, auto b){ return (a < b) ? a : b; }
#line 684 "core-math.c"

#ifdef __cplusplus
// A series of 2 overloaded interfaces with 3 parameters that
// trigger generic selection by the type of expression '+(_NOEVALX)'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 2 parameters, that is without that first parameter.
#line 686 "core-math.c"
template< typename _T1, typename _T2 >
#line 687 "core-math.c"
constexpr auto _Core_mintypeYul(generic_type((unsigned) {}), _T1 _NOEVALX, _T2 _NOEVALY) {
#line 687 "core-math.c"
    return  0u;
}
#line 688 "core-math.c"
template< typename _T0, typename _T1, typename _T2 >
#line 688 "core-math.c"
constexpr auto _Core_mintypeYul(_T0, _T1 _NOEVALX, _T2 _NOEVALY) {
#line 688 "core-math.c"
    return   0ul;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1, typename _T2 >
#line 686 "core-math.c"
constexpr auto _Core_mintypeYul(_T1 _NOEVALX, _T2 _NOEVALY) {
#line 686 "core-math.c"
    return _Core_mintypeYul((generic_type(+(_NOEVALX))) {}, ((generic_type(_NOEVALX)) {}), ((generic_type(_NOEVALY)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro _Core_mintypeYul.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 686 "core-math.c"
#define _Core_mintypeYul(_NOEVALX, _NOEVALY) _Core_mintypeYul((generic_type(+(_NOEVALX))){}, ((generic_type(_NOEVALX)){}), ((generic_type(_NOEVALY)){}))
#else
#line 686 "core-math.c"
#define _Core_mintypeYul(_NOEVALX, _NOEVALY) _Generic((+(_NOEVALX)),\
	unsigned:  0u,\
	default:   0ul)
#endif
#line 689 "core-math.c"

#ifdef __cplusplus
// A series of 6 overloaded interfaces with 3 parameters that
// trigger generic selection by the type of expression '+(_NOEVALY)'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 2 parameters, that is without that first parameter.
#line 691 "core-math.c"
template< typename _T1, typename _T2 >
#line 692 "core-math.c"
constexpr auto _Core_mintypeXuYs(generic_type((signed) {}), _T1 _NOEVALX, _T2 _NOEVALY) {
#line 692 "core-math.c"
    return 0                   /* X unsigned, Y unsigned */;
}
template< typename _T1, typename _T2 >
#line 693 "core-math.c"
constexpr auto _Core_mintypeXuYs(generic_type((signed long) {}), _T1 _NOEVALX, _T2 _NOEVALY) {
#line 693 "core-math.c"
    return 0l;
}
template< typename _T1, typename _T2 >
#line 694 "core-math.c"
constexpr auto _Core_mintypeXuYs(generic_type((signed long long) {}), _T1 _NOEVALX, _T2 _NOEVALY) {
#line 694 "core-math.c"
    return 0ll;
}
template< typename _T1, typename _T2 >
#line 695 "core-math.c"
constexpr auto _Core_mintypeXuYs(generic_type((unsigned) {}), _T1 _NOEVALX, _T2 _NOEVALY) {
#line 695 "core-math.c"
    return  0u               /* X unsigned, Y unsigned */;
}
template< typename _T1, typename _T2 >
#line 696 "core-math.c"
constexpr auto _Core_mintypeXuYs(generic_type((unsigned long) {}), _T1 _NOEVALX, _T2 _NOEVALY) {
#line 696 "core-math.c"
    return  _Core_mintypeYul(_NOEVALX, _NOEVALY);
}
template< typename _T1, typename _T2 >
#line 697 "core-math.c"
constexpr auto _Core_mintypeXuYs(generic_type((unsigned long long) {}), _T1 _NOEVALX, _T2 _NOEVALY) {
#line 697 "core-math.c"
    return  (generic_type(_NOEVALX))0;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1, typename _T2 >
#line 691 "core-math.c"
constexpr auto _Core_mintypeXuYs(_T1 _NOEVALX, _T2 _NOEVALY) {
#line 691 "core-math.c"
    return _Core_mintypeXuYs((generic_type(+(_NOEVALY))) {}, ((generic_type(_NOEVALX)) {}), ((generic_type(_NOEVALY)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro _Core_mintypeXuYs.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 691 "core-math.c"
#define _Core_mintypeXuYs(_NOEVALX, _NOEVALY) _Core_mintypeXuYs((generic_type(+(_NOEVALY))){}, ((generic_type(_NOEVALX)){}), ((generic_type(_NOEVALY)){}))
#else
#line 691 "core-math.c"
#define _Core_mintypeXuYs(_NOEVALX, _NOEVALY) _Generic((+(_NOEVALY)),\
	signed: 0                   /* X unsigned, Y unsigned */,\
	signed long: 0l,\
	signed long long: 0ll,\
	unsigned:  0u               /* X unsigned, Y unsigned */,\
	unsigned long:  _Core_mintypeYul(_NOEVALX, _NOEVALY),\
	unsigned long long:  (generic_type(_NOEVALX))0)
#endif
#line 698 "core-math.c"

#ifdef __cplusplus
// A series of 4 overloaded interfaces with 3 parameters that
// trigger generic selection by the type of expression '+(_NOEVALX)'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 2 parameters, that is without that first parameter.
#line 700 "core-math.c"
#line 701 "core-math.c"
template< typename _T0, typename _T1, typename _T2 >
#line 701 "core-math.c"
constexpr auto _Core_mintypeXs(_T0, _T1 _NOEVALX, _T2 _NOEVALY) {
#line 701 "core-math.c"
    return _Core_mintypeXuYs(_NOEVALX, _NOEVALY);
}
template< typename _T1, typename _T2 >
#line 702 "core-math.c"
constexpr auto _Core_mintypeXs(generic_type((signed) {}), _T1 _NOEVALX, _T2 _NOEVALY) {
#line 702 "core-math.c"
    return 0                  /* X signed, Y unsigned */;
}
template< typename _T1, typename _T2 >
#line 703 "core-math.c"
constexpr auto _Core_mintypeXs(generic_type((signed long) {}), _T1 _NOEVALX, _T2 _NOEVALY) {
#line 703 "core-math.c"
    return 0l;
}
template< typename _T1, typename _T2 >
#line 704 "core-math.c"
constexpr auto _Core_mintypeXs(generic_type((signed long long) {}), _T1 _NOEVALX, _T2 _NOEVALY) {
#line 704 "core-math.c"
    return 0ll;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1, typename _T2 >
#line 700 "core-math.c"
constexpr auto _Core_mintypeXs(_T1 _NOEVALX, _T2 _NOEVALY) {
#line 700 "core-math.c"
    return _Core_mintypeXs((generic_type(+(_NOEVALX))) {}, ((generic_type(_NOEVALX)) {}), ((generic_type(_NOEVALY)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro _Core_mintypeXs.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 700 "core-math.c"
#define _Core_mintypeXs(_NOEVALX, _NOEVALY) _Core_mintypeXs((generic_type(+(_NOEVALX))){}, ((generic_type(_NOEVALX)){}), ((generic_type(_NOEVALY)){}))
#else
#line 700 "core-math.c"
#define _Core_mintypeXs(_NOEVALX, _NOEVALY) _Generic((+(_NOEVALX)),\
	default: _Core_mintypeXuYs(_NOEVALX, _NOEVALY),\
	signed: 0                  /* X signed, Y unsigned */,\
	signed long: 0l,\
	signed long long: 0ll)
#endif
#line 705 "core-math.c"

#ifdef __cplusplus
// A series of 7 overloaded interfaces with 3 parameters that
// trigger generic selection by the type of expression '(_NOEVALX)+(_NOEVALY)'
// passed as their first parameter, plus two dispatch interfaces of
// the same name with 2 parameters, that is without that first parameter.
#line 707 "core-math.c"
#line 708 "core-math.c"
template< typename _T0, typename _T1, typename _T2 >
#line 708 "core-math.c"
constexpr auto _Core_mintype(_T0, _T1 _NOEVALX, _T2 _NOEVALY) {
#line 708 "core-math.c"
    return _Core_mintypeXs(_NOEVALX, _NOEVALY);
}
template< typename _T1, typename _T2 >
#line 709 "core-math.c"
constexpr auto _Core_mintype(generic_type((float) {}), _T1 _NOEVALX, _T2 _NOEVALY) {
#line 709 "core-math.c"
    return  0.0f;
}
template< typename _T1, typename _T2 >
#line 710 "core-math.c"
constexpr auto _Core_mintype(generic_type((double) {}), _T1 _NOEVALX, _T2 _NOEVALY) {
#line 710 "core-math.c"
    return  0.0;
}
template< typename _T1, typename _T2 >
#line 711 "core-math.c"
constexpr auto _Core_mintype(generic_type((long double) {}), _T1 _NOEVALX, _T2 _NOEVALY) {
#line 711 "core-math.c"
    return  0.0l;
}
template< typename _T1, typename _T2 >
#line 712 "core-math.c"
constexpr auto _Core_mintype(generic_type((signed) {}), _T1 _NOEVALX, _T2 _NOEVALY) {
#line 712 "core-math.c"
    return 0                  /* X signed, Y signed */;
}
template< typename _T1, typename _T2 >
#line 713 "core-math.c"
constexpr auto _Core_mintype(generic_type((signed long) {}), _T1 _NOEVALX, _T2 _NOEVALY) {
#line 713 "core-math.c"
    return 0l;
}
template< typename _T1, typename _T2 >
#line 714 "core-math.c"
constexpr auto _Core_mintype(generic_type((signed long long) {}), _T1 _NOEVALX, _T2 _NOEVALY) {
#line 714 "core-math.c"
    return 0ll;
}
/*
 * There are two wrappers that compute the type of the expression
 *
 * The first is a function. If this one is used, the arguments are still evaluated
 * for their side effects. So for features that are supposed to be constant, this
 * is not conforming.
 */
template< typename _T1, typename _T2 >
#line 707 "core-math.c"
constexpr auto _Core_mintype(_T1 _NOEVALX, _T2 _NOEVALY) {
#line 707 "core-math.c"
    return _Core_mintype((generic_type((_NOEVALX)+(_NOEVALY))) {}, ((generic_type(_NOEVALX)) {}), ((generic_type(_NOEVALY)) {}));
}
/**
 ** The second is the principal user interface and is implemented as macro _Core_mintype.
 **
 ** Only if this one is used, the arguments are not evaluated
 ** to compute the expression. In particular for features that are constant, this is
 ** the only way to be conforming.
 **
 ** None of the arguments is evaluated to compute the result, either.
 **/
#line 707 "core-math.c"
#define _Core_mintype(_NOEVALX, _NOEVALY) _Core_mintype((generic_type((_NOEVALX)+(_NOEVALY))){}, ((generic_type(_NOEVALX)){}), ((generic_type(_NOEVALY)){}))
#else
#line 707 "core-math.c"
#define _Core_mintype(_NOEVALX, _NOEVALY) _Generic(((_NOEVALX)+(_NOEVALY)),\
	default: _Core_mintypeXs(_NOEVALX, _NOEVALY),\
	float:  0.0f,\
	double:  0.0,\
	long double:  0.0l,\
	signed: 0                  /* X signed, Y signed */,\
	signed long: 0l,\
	signed long long: 0ll)
#endif
#line 715 "core-math.c"

#define min [](auto b, auto a) { decltype(_Core_mintype(a, b)) ret; /* We only have to be precocious if the types have different      signedness. */ if (isinteger(ret) && (isunsigned(a) != isunsigned(b))) { /* If one is negative, the other is not because the type is an        unsigned type. So the negative one is the minimum. Only one        of two if selections may trigger, and the one that possibly        could can be detected at compile time. */ if (issigned(a) && a < 0) { ret = a; } else { if (issigned(b) && b < 0) { ret = b; } else { /* Here both values are positive so the conversion does the            right thing */ ret = _Core_min(tounsigned(a), tounsigned(b)); } } } else { /* Here both types have the same signedness, so the conversions        do the right thing */ ret = _Core_min((decltype(ret))a, (decltype(ret))b); } return ret; }
#line 746 "core-math.c"

template< typename _T0 >
constexpr bool isarray(_T0) {
    return false;
}
#line 749 "core-math.c"

template< typename _T0, size_t N >
constexpr bool isarray(const _T0(&)[N]) {
    return true;
}
