#line 1 "phase2"
// helper macros
#ifndef __LINENO
#define __LINENO(X) X##LL
#define __LINENO_(X) __LINENO(X)
#define __line(X) #X
#define __line_(X) __line(X)
// @brief use this instead of __LINE__ where you need an integer
#define __LINENO__ __LINENO_(__LINE__)
// @brief use this instead of __LINE__ where you need a string
#define __line__ __line_(__LINE__)
#endif
#line 1 "core-uintwidth.c"

#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#line 7 "core-uintwidth.c"

#ifdef __cplusplus
#include <type_traits>
#include <stdexcept>
#endif
#line 12 "core-uintwidth.c"

// shnell: stuff needed for the "let" directive, these special
// characters are not allowed in directives
#define OP_LSHIFT <<=
#define OP_RSHIFT >>=
#define OP_INTERSECTION &=
#line 18 "core-uintwidth.c"
// shnell end

#line 21 "core-uintwidth.c"
/**
 ** @brief an error string that provides compile time information on
 ** the type, the width, and the bounds
 **/
template< unsigned M >
constexpr char const* _Core_uint_error(void);
// generate typedefs for all fixed width types that we have
#line 29 "core-uintwidth.c"
typedef uint8_t _Core_uint8_t;
#line 29 "core-uintwidth.c"
typedef uint16_t _Core_uint16_t;
#line 29 "core-uintwidth.c"
typedef uint32_t _Core_uint32_t;
#line 29 "core-uintwidth.c"
typedef uint64_t _Core_uint64_t;
#line 31 "core-uintwidth.c"
typedef __uint128_t _Core_uint128_t;
#line 21 "core-uintwidth.c"
/**
 ** @brief an error string that provides compile time information on
 ** the type, the width, and the bounds
 **/
template< unsigned M >
constexpr char const* _Core_int_error(void);
// generate typedefs for all fixed width types that we have
#line 29 "core-uintwidth.c"
typedef int8_t _Core_int8_t;
#line 29 "core-uintwidth.c"
typedef int16_t _Core_int16_t;
#line 29 "core-uintwidth.c"
typedef int32_t _Core_int32_t;
#line 29 "core-uintwidth.c"
typedef int64_t _Core_int64_t;
#line 31 "core-uintwidth.c"
typedef __int128_t _Core_int128_t;
#line 33 "core-uintwidth.c"
// UINT

#line 37 "core-uintwidth.c"
// For each of the fixed width types, define a template class that
// encapsulates it and provides an unsigned integer storage with mask.
template< unsigned M >
class _Core_uintwidth8 {
#line 41 "core-uintwidth.c"
private:
    _Core_uint8_t data;
public:
    static constexpr _Core_uint8_t one = 1;
    static constexpr _Core_uint8_t mask = (((one << (M-1))-1)<<1) + one;
    static constexpr _Core_uint8_t minval();
    static constexpr _Core_uint8_t maxval();
    constexpr operator _Core_uint8_t() const {
#line 49 "core-uintwidth.c"
        return data;
    }
#line 51 "core-uintwidth.c"
    constexpr _Core_uint8_t operator+ () const {
#line 52 "core-uintwidth.c"
        return data;
    }
#line 54 "core-uintwidth.c"
    /**
       ** @brief test if @a val is in the range of the type and throw up
       ** if it isn't.
       **/
    template< typename T >
    static constexpr _Core_uint8_t test_range_and_throw(T val);

    template< typename T >
    constexpr _Core_uintwidth8(T val);
    template< typename T >
    _Core_uintwidth8& operator=(T val);
// nothing particular for the increment and decrement operators, we
// just have to apply the mask
#line 68 "core-uintwidth.c"

    _Core_uintwidth8& operator ++ () {
#line 70 "core-uintwidth.c"
        ++data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_uint8_t operator ++ (int) {
#line 76 "core-uintwidth.c"
        _Core_uint8_t old = (data++);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 68 "core-uintwidth.c"

    _Core_uintwidth8& operator -- () {
#line 70 "core-uintwidth.c"
        --data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_uint8_t operator -- (int) {
#line 76 "core-uintwidth.c"
        _Core_uint8_t old = (data--);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 82 "core-uintwidth.c"

// compound assignments for arithmetic operations are synthesized
// from the underlying operation, we just have to apply the mask
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth8& operator *= (T val) {
#line 88 "core-uintwidth.c"
        data *= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth8& operator /= (T val) {
#line 88 "core-uintwidth.c"
        data /= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth8& operator += (T val) {
#line 88 "core-uintwidth.c"
        data += val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth8& operator -= (T val) {
#line 88 "core-uintwidth.c"
        data -= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth8& operator OP_LSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_LSHIFT val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth8& operator OP_RSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_RSHIFT val;
        data &= mask;
        return *this;
    }
#line 93 "core-uintwidth.c"
// compound assignments for bitwise operations are synthesized by
// masking the other operand
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth8& operator OP_INTERSECTION (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data OP_INTERSECTION m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth8& operator |= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data |= m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth8& operator ^= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data ^= m;
        return *this;
    }
#line 103 "core-uintwidth.c"
};

#line 37 "core-uintwidth.c"
// For each of the fixed width types, define a template class that
// encapsulates it and provides an unsigned integer storage with mask.
template< unsigned M >
class _Core_intwidth8 {
#line 41 "core-uintwidth.c"
private:
    _Core_int8_t data;
public:
    static constexpr _Core_int8_t one = 1;
    static constexpr _Core_int8_t mask = (((one << (M-1))-1)<<1) + one;
    static constexpr _Core_int8_t minval();
    static constexpr _Core_int8_t maxval();
    constexpr operator _Core_int8_t() const {
#line 49 "core-uintwidth.c"
        return data;
    }
#line 51 "core-uintwidth.c"
    constexpr _Core_int8_t operator+ () const {
#line 52 "core-uintwidth.c"
        return data;
    }
#line 54 "core-uintwidth.c"
    /**
       ** @brief test if @a val is in the range of the type and throw up
       ** if it isn't.
       **/
    template< typename T >
    static constexpr _Core_int8_t test_range_and_throw(T val);

    template< typename T >
    constexpr _Core_intwidth8(T val);
    template< typename T >
    _Core_intwidth8& operator=(T val);
// nothing particular for the increment and decrement operators, we
// just have to apply the mask
#line 68 "core-uintwidth.c"

    _Core_intwidth8& operator ++ () {
#line 70 "core-uintwidth.c"
        ++data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_int8_t operator ++ (int) {
#line 76 "core-uintwidth.c"
        _Core_int8_t old = (data++);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 68 "core-uintwidth.c"

    _Core_intwidth8& operator -- () {
#line 70 "core-uintwidth.c"
        --data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_int8_t operator -- (int) {
#line 76 "core-uintwidth.c"
        _Core_int8_t old = (data--);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 82 "core-uintwidth.c"

// compound assignments for arithmetic operations are synthesized
// from the underlying operation, we just have to apply the mask
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth8& operator *= (T val) {
#line 88 "core-uintwidth.c"
        data *= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth8& operator /= (T val) {
#line 88 "core-uintwidth.c"
        data /= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth8& operator += (T val) {
#line 88 "core-uintwidth.c"
        data += val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth8& operator -= (T val) {
#line 88 "core-uintwidth.c"
        data -= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth8& operator OP_LSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_LSHIFT val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth8& operator OP_RSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_RSHIFT val;
        data &= mask;
        return *this;
    }
#line 93 "core-uintwidth.c"
// compound assignments for bitwise operations are synthesized by
// masking the other operand
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth8& operator OP_INTERSECTION (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data OP_INTERSECTION m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth8& operator |= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data |= m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth8& operator ^= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data ^= m;
        return *this;
    }
#line 103 "core-uintwidth.c"
};

#line 106 "core-uintwidth.c"
// UINT

#line 109 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_uint8_t _Core_uintwidth8< M >::maxval() {
#line 112 "core-uintwidth.c"
    return _Core_uintwidth8< M >::mask;
}
#line 114 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_uint8_t _Core_uintwidth8< M >::minval() {
#line 117 "core-uintwidth.c"
    return 0;
}
#line 119 "core-uintwidth.c"

template< unsigned M >
template< typename T >
constexpr _Core_uint8_t _Core_uintwidth8< M >::test_range_and_throw(T val) {
#line 123 "core-uintwidth.c"
    const decltype(val+maxval()) ual = val;
    if ((val < 0) || (ual > maxval())) {
#line 125 "core-uintwidth.c"
        throw std::range_error(_Core_uint_error< M >());
    }
#line 127 "core-uintwidth.c"
    return val;
}
#line 129 "core-uintwidth.c"

/**
 ** @brief Construction from integer is wrap around
 **/
template< unsigned M >
template< typename T >
constexpr _Core_uintwidth8< M >::_Core_uintwidth8(T val) : data(mask & static_cast< _Core_uint8_t >(val)) {
#ifndef NDEBUG
#line 137 "core-uintwidth.c"
    test_range_and_throw(val);
#endif
#line 139 "core-uintwidth.c"
}
#line 140 "core-uintwidth.c"

/**
 ** @brief Assignment from integer is wrap around
 **/
template< unsigned M >
template< typename T >
_Core_uintwidth8< M >& _Core_uintwidth8< M >::operator=(T val) {
#line 147 "core-uintwidth.c"
    data = (mask & static_cast< _Core_uint8_t >(val));
    return *this;
}
#line 151 "core-uintwidth.c"
// uint

#line 154 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_int8_t _Core_intwidth8< M >::maxval() {
#line 157 "core-uintwidth.c"
    return ((_Core_intwidth8< M >::one << (M-1))-1);
}
#line 159 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_int8_t _Core_intwidth8< M >::minval() {
#line 162 "core-uintwidth.c"
    return -_Core_intwidth8< M >::maxval()-1;
}
#line 164 "core-uintwidth.c"

template< unsigned M >
template< typename T >
constexpr _Core_int8_t _Core_intwidth8< M >::test_range_and_throw(T val) {
#line 168 "core-uintwidth.c"
    const decltype(val+maxval()) ual = val;
    constexpr decltype(val+maxval()) mal = maxval();
    if (((val < 0) && val < minval()) || ((val > 0) && (ual > mal))) {
#line 171 "core-uintwidth.c"
        throw std::range_error(_Core_int_error< M >());
    }
#line 173 "core-uintwidth.c"
    return val;
}
#line 175 "core-uintwidth.c"


/**
 ** @brief Construction throws up when @a val is out of range.
 **
 ** If the object to be constructed is `constexpr`, this has the
 ** effect that an out-of-range initialization will abort
 ** compilation. (The error message may be a bit weird, though.)
 **/
template< unsigned M >
template< typename T >
constexpr _Core_intwidth8< M >::_Core_intwidth8(T val) : data(_Core_intwidth8< M >::test_range_and_throw< T >(val)) {
#line 187 "core-uintwidth.c"
    /* empty */
}
#line 189 "core-uintwidth.c"

/**
 ** @brief Assignment is undefined if the value is out of range.
 **
 ** Check manually by using test_range_and_throw().
 **/
template< unsigned M >
template< typename T >
_Core_intwidth8< M >& _Core_intwidth8< M >::operator=(T val) {
#line 198 "core-uintwidth.c"
    data = val;
    return *this;
}
#line 202 "core-uintwidth.c"
// int
#line 37 "core-uintwidth.c"
// For each of the fixed width types, define a template class that
// encapsulates it and provides an unsigned integer storage with mask.
template< unsigned M >
class _Core_uintwidth16 {
#line 41 "core-uintwidth.c"
private:
    _Core_uint16_t data;
public:
    static constexpr _Core_uint16_t one = 1;
    static constexpr _Core_uint16_t mask = (((one << (M-1))-1)<<1) + one;
    static constexpr _Core_uint16_t minval();
    static constexpr _Core_uint16_t maxval();
    constexpr operator _Core_uint16_t() const {
#line 49 "core-uintwidth.c"
        return data;
    }
#line 51 "core-uintwidth.c"
    constexpr _Core_uint16_t operator+ () const {
#line 52 "core-uintwidth.c"
        return data;
    }
#line 54 "core-uintwidth.c"
    /**
       ** @brief test if @a val is in the range of the type and throw up
       ** if it isn't.
       **/
    template< typename T >
    static constexpr _Core_uint16_t test_range_and_throw(T val);

    template< typename T >
    constexpr _Core_uintwidth16(T val);
    template< typename T >
    _Core_uintwidth16& operator=(T val);
// nothing particular for the increment and decrement operators, we
// just have to apply the mask
#line 68 "core-uintwidth.c"

    _Core_uintwidth16& operator ++ () {
#line 70 "core-uintwidth.c"
        ++data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_uint16_t operator ++ (int) {
#line 76 "core-uintwidth.c"
        _Core_uint16_t old = (data++);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 68 "core-uintwidth.c"

    _Core_uintwidth16& operator -- () {
#line 70 "core-uintwidth.c"
        --data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_uint16_t operator -- (int) {
#line 76 "core-uintwidth.c"
        _Core_uint16_t old = (data--);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 82 "core-uintwidth.c"

// compound assignments for arithmetic operations are synthesized
// from the underlying operation, we just have to apply the mask
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth16& operator *= (T val) {
#line 88 "core-uintwidth.c"
        data *= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth16& operator /= (T val) {
#line 88 "core-uintwidth.c"
        data /= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth16& operator += (T val) {
#line 88 "core-uintwidth.c"
        data += val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth16& operator -= (T val) {
#line 88 "core-uintwidth.c"
        data -= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth16& operator OP_LSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_LSHIFT val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth16& operator OP_RSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_RSHIFT val;
        data &= mask;
        return *this;
    }
#line 93 "core-uintwidth.c"
// compound assignments for bitwise operations are synthesized by
// masking the other operand
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth16& operator OP_INTERSECTION (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data OP_INTERSECTION m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth16& operator |= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data |= m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth16& operator ^= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data ^= m;
        return *this;
    }
#line 103 "core-uintwidth.c"
};

#line 37 "core-uintwidth.c"
// For each of the fixed width types, define a template class that
// encapsulates it and provides an unsigned integer storage with mask.
template< unsigned M >
class _Core_intwidth16 {
#line 41 "core-uintwidth.c"
private:
    _Core_int16_t data;
public:
    static constexpr _Core_int16_t one = 1;
    static constexpr _Core_int16_t mask = (((one << (M-1))-1)<<1) + one;
    static constexpr _Core_int16_t minval();
    static constexpr _Core_int16_t maxval();
    constexpr operator _Core_int16_t() const {
#line 49 "core-uintwidth.c"
        return data;
    }
#line 51 "core-uintwidth.c"
    constexpr _Core_int16_t operator+ () const {
#line 52 "core-uintwidth.c"
        return data;
    }
#line 54 "core-uintwidth.c"
    /**
       ** @brief test if @a val is in the range of the type and throw up
       ** if it isn't.
       **/
    template< typename T >
    static constexpr _Core_int16_t test_range_and_throw(T val);

    template< typename T >
    constexpr _Core_intwidth16(T val);
    template< typename T >
    _Core_intwidth16& operator=(T val);
// nothing particular for the increment and decrement operators, we
// just have to apply the mask
#line 68 "core-uintwidth.c"

    _Core_intwidth16& operator ++ () {
#line 70 "core-uintwidth.c"
        ++data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_int16_t operator ++ (int) {
#line 76 "core-uintwidth.c"
        _Core_int16_t old = (data++);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 68 "core-uintwidth.c"

    _Core_intwidth16& operator -- () {
#line 70 "core-uintwidth.c"
        --data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_int16_t operator -- (int) {
#line 76 "core-uintwidth.c"
        _Core_int16_t old = (data--);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 82 "core-uintwidth.c"

// compound assignments for arithmetic operations are synthesized
// from the underlying operation, we just have to apply the mask
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth16& operator *= (T val) {
#line 88 "core-uintwidth.c"
        data *= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth16& operator /= (T val) {
#line 88 "core-uintwidth.c"
        data /= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth16& operator += (T val) {
#line 88 "core-uintwidth.c"
        data += val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth16& operator -= (T val) {
#line 88 "core-uintwidth.c"
        data -= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth16& operator OP_LSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_LSHIFT val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth16& operator OP_RSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_RSHIFT val;
        data &= mask;
        return *this;
    }
#line 93 "core-uintwidth.c"
// compound assignments for bitwise operations are synthesized by
// masking the other operand
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth16& operator OP_INTERSECTION (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data OP_INTERSECTION m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth16& operator |= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data |= m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth16& operator ^= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data ^= m;
        return *this;
    }
#line 103 "core-uintwidth.c"
};

#line 106 "core-uintwidth.c"
// UINT

#line 109 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_uint16_t _Core_uintwidth16< M >::maxval() {
#line 112 "core-uintwidth.c"
    return _Core_uintwidth16< M >::mask;
}
#line 114 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_uint16_t _Core_uintwidth16< M >::minval() {
#line 117 "core-uintwidth.c"
    return 0;
}
#line 119 "core-uintwidth.c"

template< unsigned M >
template< typename T >
constexpr _Core_uint16_t _Core_uintwidth16< M >::test_range_and_throw(T val) {
#line 123 "core-uintwidth.c"
    const decltype(val+maxval()) ual = val;
    if ((val < 0) || (ual > maxval())) {
#line 125 "core-uintwidth.c"
        throw std::range_error(_Core_uint_error< M >());
    }
#line 127 "core-uintwidth.c"
    return val;
}
#line 129 "core-uintwidth.c"

/**
 ** @brief Construction from integer is wrap around
 **/
template< unsigned M >
template< typename T >
constexpr _Core_uintwidth16< M >::_Core_uintwidth16(T val) : data(mask & static_cast< _Core_uint16_t >(val)) {
#ifndef NDEBUG
#line 137 "core-uintwidth.c"
    test_range_and_throw(val);
#endif
#line 139 "core-uintwidth.c"
}
#line 140 "core-uintwidth.c"

/**
 ** @brief Assignment from integer is wrap around
 **/
template< unsigned M >
template< typename T >
_Core_uintwidth16< M >& _Core_uintwidth16< M >::operator=(T val) {
#line 147 "core-uintwidth.c"
    data = (mask & static_cast< _Core_uint16_t >(val));
    return *this;
}
#line 151 "core-uintwidth.c"
// uint

#line 154 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_int16_t _Core_intwidth16< M >::maxval() {
#line 157 "core-uintwidth.c"
    return ((_Core_intwidth16< M >::one << (M-1))-1);
}
#line 159 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_int16_t _Core_intwidth16< M >::minval() {
#line 162 "core-uintwidth.c"
    return -_Core_intwidth16< M >::maxval()-1;
}
#line 164 "core-uintwidth.c"

template< unsigned M >
template< typename T >
constexpr _Core_int16_t _Core_intwidth16< M >::test_range_and_throw(T val) {
#line 168 "core-uintwidth.c"
    const decltype(val+maxval()) ual = val;
    constexpr decltype(val+maxval()) mal = maxval();
    if (((val < 0) && val < minval()) || ((val > 0) && (ual > mal))) {
#line 171 "core-uintwidth.c"
        throw std::range_error(_Core_int_error< M >());
    }
#line 173 "core-uintwidth.c"
    return val;
}
#line 175 "core-uintwidth.c"


/**
 ** @brief Construction throws up when @a val is out of range.
 **
 ** If the object to be constructed is `constexpr`, this has the
 ** effect that an out-of-range initialization will abort
 ** compilation. (The error message may be a bit weird, though.)
 **/
template< unsigned M >
template< typename T >
constexpr _Core_intwidth16< M >::_Core_intwidth16(T val) : data(_Core_intwidth16< M >::test_range_and_throw< T >(val)) {
#line 187 "core-uintwidth.c"
    /* empty */
}
#line 189 "core-uintwidth.c"

/**
 ** @brief Assignment is undefined if the value is out of range.
 **
 ** Check manually by using test_range_and_throw().
 **/
template< unsigned M >
template< typename T >
_Core_intwidth16< M >& _Core_intwidth16< M >::operator=(T val) {
#line 198 "core-uintwidth.c"
    data = val;
    return *this;
}
#line 202 "core-uintwidth.c"
// int
#line 37 "core-uintwidth.c"
// For each of the fixed width types, define a template class that
// encapsulates it and provides an unsigned integer storage with mask.
template< unsigned M >
class _Core_uintwidth32 {
#line 41 "core-uintwidth.c"
private:
    _Core_uint32_t data;
public:
    static constexpr _Core_uint32_t one = 1;
    static constexpr _Core_uint32_t mask = (((one << (M-1))-1)<<1) + one;
    static constexpr _Core_uint32_t minval();
    static constexpr _Core_uint32_t maxval();
    constexpr operator _Core_uint32_t() const {
#line 49 "core-uintwidth.c"
        return data;
    }
#line 51 "core-uintwidth.c"
    constexpr _Core_uint32_t operator+ () const {
#line 52 "core-uintwidth.c"
        return data;
    }
#line 54 "core-uintwidth.c"
    /**
       ** @brief test if @a val is in the range of the type and throw up
       ** if it isn't.
       **/
    template< typename T >
    static constexpr _Core_uint32_t test_range_and_throw(T val);

    template< typename T >
    constexpr _Core_uintwidth32(T val);
    template< typename T >
    _Core_uintwidth32& operator=(T val);
// nothing particular for the increment and decrement operators, we
// just have to apply the mask
#line 68 "core-uintwidth.c"

    _Core_uintwidth32& operator ++ () {
#line 70 "core-uintwidth.c"
        ++data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_uint32_t operator ++ (int) {
#line 76 "core-uintwidth.c"
        _Core_uint32_t old = (data++);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 68 "core-uintwidth.c"

    _Core_uintwidth32& operator -- () {
#line 70 "core-uintwidth.c"
        --data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_uint32_t operator -- (int) {
#line 76 "core-uintwidth.c"
        _Core_uint32_t old = (data--);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 82 "core-uintwidth.c"

// compound assignments for arithmetic operations are synthesized
// from the underlying operation, we just have to apply the mask
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth32& operator *= (T val) {
#line 88 "core-uintwidth.c"
        data *= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth32& operator /= (T val) {
#line 88 "core-uintwidth.c"
        data /= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth32& operator += (T val) {
#line 88 "core-uintwidth.c"
        data += val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth32& operator -= (T val) {
#line 88 "core-uintwidth.c"
        data -= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth32& operator OP_LSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_LSHIFT val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth32& operator OP_RSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_RSHIFT val;
        data &= mask;
        return *this;
    }
#line 93 "core-uintwidth.c"
// compound assignments for bitwise operations are synthesized by
// masking the other operand
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth32& operator OP_INTERSECTION (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data OP_INTERSECTION m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth32& operator |= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data |= m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth32& operator ^= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data ^= m;
        return *this;
    }
#line 103 "core-uintwidth.c"
};

#line 37 "core-uintwidth.c"
// For each of the fixed width types, define a template class that
// encapsulates it and provides an unsigned integer storage with mask.
template< unsigned M >
class _Core_intwidth32 {
#line 41 "core-uintwidth.c"
private:
    _Core_int32_t data;
public:
    static constexpr _Core_int32_t one = 1;
    static constexpr _Core_int32_t mask = (((one << (M-1))-1)<<1) + one;
    static constexpr _Core_int32_t minval();
    static constexpr _Core_int32_t maxval();
    constexpr operator _Core_int32_t() const {
#line 49 "core-uintwidth.c"
        return data;
    }
#line 51 "core-uintwidth.c"
    constexpr _Core_int32_t operator+ () const {
#line 52 "core-uintwidth.c"
        return data;
    }
#line 54 "core-uintwidth.c"
    /**
       ** @brief test if @a val is in the range of the type and throw up
       ** if it isn't.
       **/
    template< typename T >
    static constexpr _Core_int32_t test_range_and_throw(T val);

    template< typename T >
    constexpr _Core_intwidth32(T val);
    template< typename T >
    _Core_intwidth32& operator=(T val);
// nothing particular for the increment and decrement operators, we
// just have to apply the mask
#line 68 "core-uintwidth.c"

    _Core_intwidth32& operator ++ () {
#line 70 "core-uintwidth.c"
        ++data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_int32_t operator ++ (int) {
#line 76 "core-uintwidth.c"
        _Core_int32_t old = (data++);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 68 "core-uintwidth.c"

    _Core_intwidth32& operator -- () {
#line 70 "core-uintwidth.c"
        --data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_int32_t operator -- (int) {
#line 76 "core-uintwidth.c"
        _Core_int32_t old = (data--);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 82 "core-uintwidth.c"

// compound assignments for arithmetic operations are synthesized
// from the underlying operation, we just have to apply the mask
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth32& operator *= (T val) {
#line 88 "core-uintwidth.c"
        data *= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth32& operator /= (T val) {
#line 88 "core-uintwidth.c"
        data /= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth32& operator += (T val) {
#line 88 "core-uintwidth.c"
        data += val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth32& operator -= (T val) {
#line 88 "core-uintwidth.c"
        data -= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth32& operator OP_LSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_LSHIFT val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth32& operator OP_RSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_RSHIFT val;
        data &= mask;
        return *this;
    }
#line 93 "core-uintwidth.c"
// compound assignments for bitwise operations are synthesized by
// masking the other operand
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth32& operator OP_INTERSECTION (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data OP_INTERSECTION m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth32& operator |= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data |= m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth32& operator ^= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data ^= m;
        return *this;
    }
#line 103 "core-uintwidth.c"
};

#line 106 "core-uintwidth.c"
// UINT

#line 109 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_uint32_t _Core_uintwidth32< M >::maxval() {
#line 112 "core-uintwidth.c"
    return _Core_uintwidth32< M >::mask;
}
#line 114 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_uint32_t _Core_uintwidth32< M >::minval() {
#line 117 "core-uintwidth.c"
    return 0;
}
#line 119 "core-uintwidth.c"

template< unsigned M >
template< typename T >
constexpr _Core_uint32_t _Core_uintwidth32< M >::test_range_and_throw(T val) {
#line 123 "core-uintwidth.c"
    const decltype(val+maxval()) ual = val;
    if ((val < 0) || (ual > maxval())) {
#line 125 "core-uintwidth.c"
        throw std::range_error(_Core_uint_error< M >());
    }
#line 127 "core-uintwidth.c"
    return val;
}
#line 129 "core-uintwidth.c"

/**
 ** @brief Construction from integer is wrap around
 **/
template< unsigned M >
template< typename T >
constexpr _Core_uintwidth32< M >::_Core_uintwidth32(T val) : data(mask & static_cast< _Core_uint32_t >(val)) {
#ifndef NDEBUG
#line 137 "core-uintwidth.c"
    test_range_and_throw(val);
#endif
#line 139 "core-uintwidth.c"
}
#line 140 "core-uintwidth.c"

/**
 ** @brief Assignment from integer is wrap around
 **/
template< unsigned M >
template< typename T >
_Core_uintwidth32< M >& _Core_uintwidth32< M >::operator=(T val) {
#line 147 "core-uintwidth.c"
    data = (mask & static_cast< _Core_uint32_t >(val));
    return *this;
}
#line 151 "core-uintwidth.c"
// uint

#line 154 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_int32_t _Core_intwidth32< M >::maxval() {
#line 157 "core-uintwidth.c"
    return ((_Core_intwidth32< M >::one << (M-1))-1);
}
#line 159 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_int32_t _Core_intwidth32< M >::minval() {
#line 162 "core-uintwidth.c"
    return -_Core_intwidth32< M >::maxval()-1;
}
#line 164 "core-uintwidth.c"

template< unsigned M >
template< typename T >
constexpr _Core_int32_t _Core_intwidth32< M >::test_range_and_throw(T val) {
#line 168 "core-uintwidth.c"
    const decltype(val+maxval()) ual = val;
    constexpr decltype(val+maxval()) mal = maxval();
    if (((val < 0) && val < minval()) || ((val > 0) && (ual > mal))) {
#line 171 "core-uintwidth.c"
        throw std::range_error(_Core_int_error< M >());
    }
#line 173 "core-uintwidth.c"
    return val;
}
#line 175 "core-uintwidth.c"


/**
 ** @brief Construction throws up when @a val is out of range.
 **
 ** If the object to be constructed is `constexpr`, this has the
 ** effect that an out-of-range initialization will abort
 ** compilation. (The error message may be a bit weird, though.)
 **/
template< unsigned M >
template< typename T >
constexpr _Core_intwidth32< M >::_Core_intwidth32(T val) : data(_Core_intwidth32< M >::test_range_and_throw< T >(val)) {
#line 187 "core-uintwidth.c"
    /* empty */
}
#line 189 "core-uintwidth.c"

/**
 ** @brief Assignment is undefined if the value is out of range.
 **
 ** Check manually by using test_range_and_throw().
 **/
template< unsigned M >
template< typename T >
_Core_intwidth32< M >& _Core_intwidth32< M >::operator=(T val) {
#line 198 "core-uintwidth.c"
    data = val;
    return *this;
}
#line 202 "core-uintwidth.c"
// int
#line 37 "core-uintwidth.c"
// For each of the fixed width types, define a template class that
// encapsulates it and provides an unsigned integer storage with mask.
template< unsigned M >
class _Core_uintwidth64 {
#line 41 "core-uintwidth.c"
private:
    _Core_uint64_t data;
public:
    static constexpr _Core_uint64_t one = 1;
    static constexpr _Core_uint64_t mask = (((one << (M-1))-1)<<1) + one;
    static constexpr _Core_uint64_t minval();
    static constexpr _Core_uint64_t maxval();
    constexpr operator _Core_uint64_t() const {
#line 49 "core-uintwidth.c"
        return data;
    }
#line 51 "core-uintwidth.c"
    constexpr _Core_uint64_t operator+ () const {
#line 52 "core-uintwidth.c"
        return data;
    }
#line 54 "core-uintwidth.c"
    /**
       ** @brief test if @a val is in the range of the type and throw up
       ** if it isn't.
       **/
    template< typename T >
    static constexpr _Core_uint64_t test_range_and_throw(T val);

    template< typename T >
    constexpr _Core_uintwidth64(T val);
    template< typename T >
    _Core_uintwidth64& operator=(T val);
// nothing particular for the increment and decrement operators, we
// just have to apply the mask
#line 68 "core-uintwidth.c"

    _Core_uintwidth64& operator ++ () {
#line 70 "core-uintwidth.c"
        ++data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_uint64_t operator ++ (int) {
#line 76 "core-uintwidth.c"
        _Core_uint64_t old = (data++);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 68 "core-uintwidth.c"

    _Core_uintwidth64& operator -- () {
#line 70 "core-uintwidth.c"
        --data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_uint64_t operator -- (int) {
#line 76 "core-uintwidth.c"
        _Core_uint64_t old = (data--);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 82 "core-uintwidth.c"

// compound assignments for arithmetic operations are synthesized
// from the underlying operation, we just have to apply the mask
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth64& operator *= (T val) {
#line 88 "core-uintwidth.c"
        data *= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth64& operator /= (T val) {
#line 88 "core-uintwidth.c"
        data /= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth64& operator += (T val) {
#line 88 "core-uintwidth.c"
        data += val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth64& operator -= (T val) {
#line 88 "core-uintwidth.c"
        data -= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth64& operator OP_LSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_LSHIFT val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth64& operator OP_RSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_RSHIFT val;
        data &= mask;
        return *this;
    }
#line 93 "core-uintwidth.c"
// compound assignments for bitwise operations are synthesized by
// masking the other operand
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth64& operator OP_INTERSECTION (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data OP_INTERSECTION m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth64& operator |= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data |= m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth64& operator ^= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data ^= m;
        return *this;
    }
#line 103 "core-uintwidth.c"
};

#line 37 "core-uintwidth.c"
// For each of the fixed width types, define a template class that
// encapsulates it and provides an unsigned integer storage with mask.
template< unsigned M >
class _Core_intwidth64 {
#line 41 "core-uintwidth.c"
private:
    _Core_int64_t data;
public:
    static constexpr _Core_int64_t one = 1;
    static constexpr _Core_int64_t mask = (((one << (M-1))-1)<<1) + one;
    static constexpr _Core_int64_t minval();
    static constexpr _Core_int64_t maxval();
    constexpr operator _Core_int64_t() const {
#line 49 "core-uintwidth.c"
        return data;
    }
#line 51 "core-uintwidth.c"
    constexpr _Core_int64_t operator+ () const {
#line 52 "core-uintwidth.c"
        return data;
    }
#line 54 "core-uintwidth.c"
    /**
       ** @brief test if @a val is in the range of the type and throw up
       ** if it isn't.
       **/
    template< typename T >
    static constexpr _Core_int64_t test_range_and_throw(T val);

    template< typename T >
    constexpr _Core_intwidth64(T val);
    template< typename T >
    _Core_intwidth64& operator=(T val);
// nothing particular for the increment and decrement operators, we
// just have to apply the mask
#line 68 "core-uintwidth.c"

    _Core_intwidth64& operator ++ () {
#line 70 "core-uintwidth.c"
        ++data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_int64_t operator ++ (int) {
#line 76 "core-uintwidth.c"
        _Core_int64_t old = (data++);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 68 "core-uintwidth.c"

    _Core_intwidth64& operator -- () {
#line 70 "core-uintwidth.c"
        --data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_int64_t operator -- (int) {
#line 76 "core-uintwidth.c"
        _Core_int64_t old = (data--);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 82 "core-uintwidth.c"

// compound assignments for arithmetic operations are synthesized
// from the underlying operation, we just have to apply the mask
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth64& operator *= (T val) {
#line 88 "core-uintwidth.c"
        data *= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth64& operator /= (T val) {
#line 88 "core-uintwidth.c"
        data /= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth64& operator += (T val) {
#line 88 "core-uintwidth.c"
        data += val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth64& operator -= (T val) {
#line 88 "core-uintwidth.c"
        data -= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth64& operator OP_LSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_LSHIFT val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth64& operator OP_RSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_RSHIFT val;
        data &= mask;
        return *this;
    }
#line 93 "core-uintwidth.c"
// compound assignments for bitwise operations are synthesized by
// masking the other operand
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth64& operator OP_INTERSECTION (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data OP_INTERSECTION m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth64& operator |= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data |= m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth64& operator ^= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data ^= m;
        return *this;
    }
#line 103 "core-uintwidth.c"
};

#line 106 "core-uintwidth.c"
// UINT

#line 109 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_uint64_t _Core_uintwidth64< M >::maxval() {
#line 112 "core-uintwidth.c"
    return _Core_uintwidth64< M >::mask;
}
#line 114 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_uint64_t _Core_uintwidth64< M >::minval() {
#line 117 "core-uintwidth.c"
    return 0;
}
#line 119 "core-uintwidth.c"

template< unsigned M >
template< typename T >
constexpr _Core_uint64_t _Core_uintwidth64< M >::test_range_and_throw(T val) {
#line 123 "core-uintwidth.c"
    const decltype(val+maxval()) ual = val;
    if ((val < 0) || (ual > maxval())) {
#line 125 "core-uintwidth.c"
        throw std::range_error(_Core_uint_error< M >());
    }
#line 127 "core-uintwidth.c"
    return val;
}
#line 129 "core-uintwidth.c"

/**
 ** @brief Construction from integer is wrap around
 **/
template< unsigned M >
template< typename T >
constexpr _Core_uintwidth64< M >::_Core_uintwidth64(T val) : data(mask & static_cast< _Core_uint64_t >(val)) {
#ifndef NDEBUG
#line 137 "core-uintwidth.c"
    test_range_and_throw(val);
#endif
#line 139 "core-uintwidth.c"
}
#line 140 "core-uintwidth.c"

/**
 ** @brief Assignment from integer is wrap around
 **/
template< unsigned M >
template< typename T >
_Core_uintwidth64< M >& _Core_uintwidth64< M >::operator=(T val) {
#line 147 "core-uintwidth.c"
    data = (mask & static_cast< _Core_uint64_t >(val));
    return *this;
}
#line 151 "core-uintwidth.c"
// uint

#line 154 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_int64_t _Core_intwidth64< M >::maxval() {
#line 157 "core-uintwidth.c"
    return ((_Core_intwidth64< M >::one << (M-1))-1);
}
#line 159 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_int64_t _Core_intwidth64< M >::minval() {
#line 162 "core-uintwidth.c"
    return -_Core_intwidth64< M >::maxval()-1;
}
#line 164 "core-uintwidth.c"

template< unsigned M >
template< typename T >
constexpr _Core_int64_t _Core_intwidth64< M >::test_range_and_throw(T val) {
#line 168 "core-uintwidth.c"
    const decltype(val+maxval()) ual = val;
    constexpr decltype(val+maxval()) mal = maxval();
    if (((val < 0) && val < minval()) || ((val > 0) && (ual > mal))) {
#line 171 "core-uintwidth.c"
        throw std::range_error(_Core_int_error< M >());
    }
#line 173 "core-uintwidth.c"
    return val;
}
#line 175 "core-uintwidth.c"


/**
 ** @brief Construction throws up when @a val is out of range.
 **
 ** If the object to be constructed is `constexpr`, this has the
 ** effect that an out-of-range initialization will abort
 ** compilation. (The error message may be a bit weird, though.)
 **/
template< unsigned M >
template< typename T >
constexpr _Core_intwidth64< M >::_Core_intwidth64(T val) : data(_Core_intwidth64< M >::test_range_and_throw< T >(val)) {
#line 187 "core-uintwidth.c"
    /* empty */
}
#line 189 "core-uintwidth.c"

/**
 ** @brief Assignment is undefined if the value is out of range.
 **
 ** Check manually by using test_range_and_throw().
 **/
template< unsigned M >
template< typename T >
_Core_intwidth64< M >& _Core_intwidth64< M >::operator=(T val) {
#line 198 "core-uintwidth.c"
    data = val;
    return *this;
}
#line 202 "core-uintwidth.c"
// int
#line 37 "core-uintwidth.c"
// For each of the fixed width types, define a template class that
// encapsulates it and provides an unsigned integer storage with mask.
template< unsigned M >
class _Core_uintwidth128 {
#line 41 "core-uintwidth.c"
private:
    _Core_uint128_t data;
public:
    static constexpr _Core_uint128_t one = 1;
    static constexpr _Core_uint128_t mask = (((one << (M-1))-1)<<1) + one;
    static constexpr _Core_uint128_t minval();
    static constexpr _Core_uint128_t maxval();
    constexpr operator _Core_uint128_t() const {
#line 49 "core-uintwidth.c"
        return data;
    }
#line 51 "core-uintwidth.c"
    constexpr _Core_uint128_t operator+ () const {
#line 52 "core-uintwidth.c"
        return data;
    }
#line 54 "core-uintwidth.c"
    /**
       ** @brief test if @a val is in the range of the type and throw up
       ** if it isn't.
       **/
    template< typename T >
    static constexpr _Core_uint128_t test_range_and_throw(T val);

    template< typename T >
    constexpr _Core_uintwidth128(T val);
    template< typename T >
    _Core_uintwidth128& operator=(T val);
// nothing particular for the increment and decrement operators, we
// just have to apply the mask
#line 68 "core-uintwidth.c"

    _Core_uintwidth128& operator ++ () {
#line 70 "core-uintwidth.c"
        ++data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_uint128_t operator ++ (int) {
#line 76 "core-uintwidth.c"
        _Core_uint128_t old = (data++);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 68 "core-uintwidth.c"

    _Core_uintwidth128& operator -- () {
#line 70 "core-uintwidth.c"
        --data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_uint128_t operator -- (int) {
#line 76 "core-uintwidth.c"
        _Core_uint128_t old = (data--);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 82 "core-uintwidth.c"

// compound assignments for arithmetic operations are synthesized
// from the underlying operation, we just have to apply the mask
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth128& operator *= (T val) {
#line 88 "core-uintwidth.c"
        data *= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth128& operator /= (T val) {
#line 88 "core-uintwidth.c"
        data /= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth128& operator += (T val) {
#line 88 "core-uintwidth.c"
        data += val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth128& operator -= (T val) {
#line 88 "core-uintwidth.c"
        data -= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth128& operator OP_LSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_LSHIFT val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth128& operator OP_RSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_RSHIFT val;
        data &= mask;
        return *this;
    }
#line 93 "core-uintwidth.c"
// compound assignments for bitwise operations are synthesized by
// masking the other operand
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth128& operator OP_INTERSECTION (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data OP_INTERSECTION m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth128& operator |= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data |= m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_uintwidth128& operator ^= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data ^= m;
        return *this;
    }
#line 103 "core-uintwidth.c"
};

#line 37 "core-uintwidth.c"
// For each of the fixed width types, define a template class that
// encapsulates it and provides an unsigned integer storage with mask.
template< unsigned M >
class _Core_intwidth128 {
#line 41 "core-uintwidth.c"
private:
    _Core_int128_t data;
public:
    static constexpr _Core_int128_t one = 1;
    static constexpr _Core_int128_t mask = (((one << (M-1))-1)<<1) + one;
    static constexpr _Core_int128_t minval();
    static constexpr _Core_int128_t maxval();
    constexpr operator _Core_int128_t() const {
#line 49 "core-uintwidth.c"
        return data;
    }
#line 51 "core-uintwidth.c"
    constexpr _Core_int128_t operator+ () const {
#line 52 "core-uintwidth.c"
        return data;
    }
#line 54 "core-uintwidth.c"
    /**
       ** @brief test if @a val is in the range of the type and throw up
       ** if it isn't.
       **/
    template< typename T >
    static constexpr _Core_int128_t test_range_and_throw(T val);

    template< typename T >
    constexpr _Core_intwidth128(T val);
    template< typename T >
    _Core_intwidth128& operator=(T val);
// nothing particular for the increment and decrement operators, we
// just have to apply the mask
#line 68 "core-uintwidth.c"

    _Core_intwidth128& operator ++ () {
#line 70 "core-uintwidth.c"
        ++data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_int128_t operator ++ (int) {
#line 76 "core-uintwidth.c"
        _Core_int128_t old = (data++);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 68 "core-uintwidth.c"

    _Core_intwidth128& operator -- () {
#line 70 "core-uintwidth.c"
        --data;
        data &= mask;
        return *this;
    }
#line 74 "core-uintwidth.c"

    _Core_int128_t operator -- (int) {
#line 76 "core-uintwidth.c"
        _Core_int128_t old = (data--);
        data &= mask;
        return old;
    }
#line 80 "core-uintwidth.c"

#line 82 "core-uintwidth.c"

// compound assignments for arithmetic operations are synthesized
// from the underlying operation, we just have to apply the mask
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth128& operator *= (T val) {
#line 88 "core-uintwidth.c"
        data *= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth128& operator /= (T val) {
#line 88 "core-uintwidth.c"
        data /= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth128& operator += (T val) {
#line 88 "core-uintwidth.c"
        data += val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth128& operator -= (T val) {
#line 88 "core-uintwidth.c"
        data -= val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth128& operator OP_LSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_LSHIFT val;
        data &= mask;
        return *this;
    }
#line 86 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth128& operator OP_RSHIFT (T val) {
#line 88 "core-uintwidth.c"
        data OP_RSHIFT val;
        data &= mask;
        return *this;
    }
#line 93 "core-uintwidth.c"
// compound assignments for bitwise operations are synthesized by
// masking the other operand
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth128& operator OP_INTERSECTION (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data OP_INTERSECTION m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth128& operator |= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data |= m;
        return *this;
    }
#line 96 "core-uintwidth.c"
    template< typename T >
    _Core_intwidth128& operator ^= (T val) {
#line 98 "core-uintwidth.c"
        auto m = mask & val;
        data ^= m;
        return *this;
    }
#line 103 "core-uintwidth.c"
};

#line 106 "core-uintwidth.c"
// UINT

#line 109 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_uint128_t _Core_uintwidth128< M >::maxval() {
#line 112 "core-uintwidth.c"
    return _Core_uintwidth128< M >::mask;
}
#line 114 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_uint128_t _Core_uintwidth128< M >::minval() {
#line 117 "core-uintwidth.c"
    return 0;
}
#line 119 "core-uintwidth.c"

template< unsigned M >
template< typename T >
constexpr _Core_uint128_t _Core_uintwidth128< M >::test_range_and_throw(T val) {
#line 123 "core-uintwidth.c"
    const decltype(val+maxval()) ual = val;
    if ((val < 0) || (ual > maxval())) {
#line 125 "core-uintwidth.c"
        throw std::range_error(_Core_uint_error< M >());
    }
#line 127 "core-uintwidth.c"
    return val;
}
#line 129 "core-uintwidth.c"

/**
 ** @brief Construction from integer is wrap around
 **/
template< unsigned M >
template< typename T >
constexpr _Core_uintwidth128< M >::_Core_uintwidth128(T val) : data(mask & static_cast< _Core_uint128_t >(val)) {
#ifndef NDEBUG
#line 137 "core-uintwidth.c"
    test_range_and_throw(val);
#endif
#line 139 "core-uintwidth.c"
}
#line 140 "core-uintwidth.c"

/**
 ** @brief Assignment from integer is wrap around
 **/
template< unsigned M >
template< typename T >
_Core_uintwidth128< M >& _Core_uintwidth128< M >::operator=(T val) {
#line 147 "core-uintwidth.c"
    data = (mask & static_cast< _Core_uint128_t >(val));
    return *this;
}
#line 151 "core-uintwidth.c"
// uint

#line 154 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_int128_t _Core_intwidth128< M >::maxval() {
#line 157 "core-uintwidth.c"
    return ((_Core_intwidth128< M >::one << (M-1))-1);
}
#line 159 "core-uintwidth.c"

template< unsigned M >
constexpr _Core_int128_t _Core_intwidth128< M >::minval() {
#line 162 "core-uintwidth.c"
    return -_Core_intwidth128< M >::maxval()-1;
}
#line 164 "core-uintwidth.c"

template< unsigned M >
template< typename T >
constexpr _Core_int128_t _Core_intwidth128< M >::test_range_and_throw(T val) {
#line 168 "core-uintwidth.c"
    const decltype(val+maxval()) ual = val;
    constexpr decltype(val+maxval()) mal = maxval();
    if (((val < 0) && val < minval()) || ((val > 0) && (ual > mal))) {
#line 171 "core-uintwidth.c"
        throw std::range_error(_Core_int_error< M >());
    }
#line 173 "core-uintwidth.c"
    return val;
}
#line 175 "core-uintwidth.c"


/**
 ** @brief Construction throws up when @a val is out of range.
 **
 ** If the object to be constructed is `constexpr`, this has the
 ** effect that an out-of-range initialization will abort
 ** compilation. (The error message may be a bit weird, though.)
 **/
template< unsigned M >
template< typename T >
constexpr _Core_intwidth128< M >::_Core_intwidth128(T val) : data(_Core_intwidth128< M >::test_range_and_throw< T >(val)) {
#line 187 "core-uintwidth.c"
    /* empty */
}
#line 189 "core-uintwidth.c"

/**
 ** @brief Assignment is undefined if the value is out of range.
 **
 ** Check manually by using test_range_and_throw().
 **/
template< unsigned M >
template< typename T >
_Core_intwidth128< M >& _Core_intwidth128< M >::operator=(T val) {
#line 198 "core-uintwidth.c"
    data = val;
    return *this;
}
#line 202 "core-uintwidth.c"
// int
#line 204 "core-uintwidth.c"
// N

#line 207 "core-uintwidth.c"

template< unsigned M > struct _Core_uintwidth;

// Create class wrappers for typedefs for each possible width.
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 1 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth8< 1 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 2 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth8< 2 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 3 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth8< 3 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 4 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth8< 4 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 5 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth8< 5 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 6 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth8< 6 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 7 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth8< 7 > type;
};
#line 221 "core-uintwidth.c"
// Those widths that correspond to a fixed width type take those
// directly.
template< >
struct _Core_uintwidth< 8 > {
#line 225 "core-uintwidth.c"
    typedef _Core_uint#"8"##_t type;
};

#line 229 "core-uintwidth.c"

#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 9 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth16< 9 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 10 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth16< 10 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 11 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth16< 11 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 12 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth16< 12 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 13 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth16< 13 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 14 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth16< 14 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 15 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth16< 15 > type;
};
#line 221 "core-uintwidth.c"
// Those widths that correspond to a fixed width type take those
// directly.
template< >
struct _Core_uintwidth< 16 > {
#line 225 "core-uintwidth.c"
    typedef _Core_uint#"16"##_t type;
};

#line 229 "core-uintwidth.c"

#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 17 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth32< 17 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 18 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth32< 18 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 19 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth32< 19 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 20 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth32< 20 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 21 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth32< 21 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 22 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth32< 22 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 23 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth32< 23 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 24 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth32< 24 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 25 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth32< 25 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 26 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth32< 26 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 27 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth32< 27 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 28 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth32< 28 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 29 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth32< 29 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 30 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth32< 30 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 31 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth32< 31 > type;
};
#line 221 "core-uintwidth.c"
// Those widths that correspond to a fixed width type take those
// directly.
template< >
struct _Core_uintwidth< 32 > {
#line 225 "core-uintwidth.c"
    typedef _Core_uint#"32"##_t type;
};

#line 229 "core-uintwidth.c"

#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 33 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 33 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 34 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 34 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 35 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 35 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 36 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 36 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 37 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 37 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 38 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 38 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 39 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 39 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 40 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 40 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 41 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 41 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 42 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 42 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 43 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 43 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 44 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 44 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 45 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 45 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 46 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 46 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 47 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 47 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 48 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 48 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 49 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 49 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 50 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 50 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 51 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 51 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 52 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 52 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 53 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 53 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 54 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 54 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 55 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 55 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 56 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 56 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 57 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 57 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 58 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 58 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 59 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 59 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 60 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 60 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 61 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 61 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 62 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 62 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 63 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth64< 63 > type;
};
#line 221 "core-uintwidth.c"
// Those widths that correspond to a fixed width type take those
// directly.
template< >
struct _Core_uintwidth< 64 > {
#line 225 "core-uintwidth.c"
    typedef _Core_uint#"64"##_t type;
};

#line 229 "core-uintwidth.c"

#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 65 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 65 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 66 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 66 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 67 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 67 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 68 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 68 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 69 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 69 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 70 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 70 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 71 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 71 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 72 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 72 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 73 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 73 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 74 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 74 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 75 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 75 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 76 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 76 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 77 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 77 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 78 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 78 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 79 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 79 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 80 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 80 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 81 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 81 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 82 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 82 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 83 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 83 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 84 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 84 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 85 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 85 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 86 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 86 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 87 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 87 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 88 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 88 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 89 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 89 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 90 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 90 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 91 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 91 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 92 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 92 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 93 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 93 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 94 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 94 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 95 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 95 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 96 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 96 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 97 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 97 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 98 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 98 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 99 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 99 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 100 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 100 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 101 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 101 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 102 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 102 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 103 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 103 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 104 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 104 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 105 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 105 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 106 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 106 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 107 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 107 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 108 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 108 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 109 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 109 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 110 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 110 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 111 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 111 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 112 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 112 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 113 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 113 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 114 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 114 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 115 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 115 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 116 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 116 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 117 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 117 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 118 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 118 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 119 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 119 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 120 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 120 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 121 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 121 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 122 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 122 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 123 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 123 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 124 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 124 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 125 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 125 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 126 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 126 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_uintwidth< 127 > {
#line 217 "core-uintwidth.c"
    typedef _Core_uintwidth128< 127 > type;
};
#line 221 "core-uintwidth.c"
// Those widths that correspond to a fixed width type take those
// directly.
template< >
struct _Core_uintwidth< 128 > {
#line 225 "core-uintwidth.c"
    typedef _Core_uint#"128"##_t type;
};

#line 229 "core-uintwidth.c"

#line 231 "core-uintwidth.c"
// B
#line 233 "core-uintwidth.c"

/**
 ** @brief The core macros for fixed-width integer types.
 **
 ** They don't have arithmetic defined by themselves, but only
 ** conversion to and from the type `[u]intXX_t` where `XX` is the
 ** next power of 2 that is larger than @a N.
 **
 ** For the unsigned types, such a conversion wraps around mod \$2^{N}\$.
 **
 ** For the signed types, provisions to capture overflow are only made
 ** for the constructor. For automatic or modifiable objects, a
 ** `range_error` is thrown. For `constexpr` objects, compilation is
 ** aborted.
 **/
#define uintwidth(N) _Core_uintwidth< N >::type
#line 249 "core-uintwidth.c"

#line 207 "core-uintwidth.c"

template< unsigned M > struct _Core_intwidth;

// Create class wrappers for typedefs for each possible width.
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 1 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth8< 1 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 2 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth8< 2 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 3 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth8< 3 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 4 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth8< 4 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 5 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth8< 5 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 6 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth8< 6 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 7 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth8< 7 > type;
};
#line 221 "core-uintwidth.c"
// Those widths that correspond to a fixed width type take those
// directly.
template< >
struct _Core_intwidth< 8 > {
#line 225 "core-uintwidth.c"
    typedef _Core_int#"8"##_t type;
};

#line 229 "core-uintwidth.c"

#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 9 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth16< 9 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 10 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth16< 10 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 11 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth16< 11 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 12 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth16< 12 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 13 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth16< 13 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 14 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth16< 14 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 15 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth16< 15 > type;
};
#line 221 "core-uintwidth.c"
// Those widths that correspond to a fixed width type take those
// directly.
template< >
struct _Core_intwidth< 16 > {
#line 225 "core-uintwidth.c"
    typedef _Core_int#"16"##_t type;
};

#line 229 "core-uintwidth.c"

#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 17 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth32< 17 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 18 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth32< 18 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 19 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth32< 19 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 20 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth32< 20 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 21 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth32< 21 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 22 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth32< 22 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 23 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth32< 23 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 24 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth32< 24 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 25 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth32< 25 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 26 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth32< 26 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 27 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth32< 27 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 28 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth32< 28 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 29 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth32< 29 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 30 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth32< 30 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 31 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth32< 31 > type;
};
#line 221 "core-uintwidth.c"
// Those widths that correspond to a fixed width type take those
// directly.
template< >
struct _Core_intwidth< 32 > {
#line 225 "core-uintwidth.c"
    typedef _Core_int#"32"##_t type;
};

#line 229 "core-uintwidth.c"

#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 33 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 33 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 34 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 34 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 35 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 35 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 36 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 36 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 37 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 37 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 38 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 38 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 39 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 39 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 40 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 40 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 41 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 41 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 42 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 42 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 43 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 43 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 44 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 44 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 45 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 45 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 46 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 46 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 47 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 47 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 48 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 48 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 49 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 49 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 50 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 50 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 51 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 51 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 52 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 52 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 53 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 53 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 54 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 54 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 55 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 55 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 56 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 56 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 57 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 57 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 58 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 58 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 59 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 59 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 60 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 60 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 61 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 61 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 62 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 62 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 63 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth64< 63 > type;
};
#line 221 "core-uintwidth.c"
// Those widths that correspond to a fixed width type take those
// directly.
template< >
struct _Core_intwidth< 64 > {
#line 225 "core-uintwidth.c"
    typedef _Core_int#"64"##_t type;
};

#line 229 "core-uintwidth.c"

#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 65 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 65 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 66 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 66 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 67 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 67 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 68 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 68 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 69 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 69 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 70 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 70 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 71 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 71 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 72 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 72 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 73 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 73 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 74 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 74 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 75 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 75 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 76 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 76 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 77 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 77 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 78 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 78 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 79 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 79 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 80 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 80 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 81 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 81 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 82 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 82 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 83 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 83 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 84 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 84 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 85 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 85 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 86 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 86 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 87 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 87 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 88 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 88 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 89 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 89 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 90 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 90 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 91 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 91 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 92 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 92 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 93 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 93 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 94 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 94 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 95 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 95 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 96 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 96 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 97 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 97 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 98 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 98 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 99 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 99 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 100 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 100 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 101 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 101 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 102 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 102 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 103 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 103 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 104 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 104 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 105 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 105 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 106 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 106 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 107 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 107 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 108 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 108 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 109 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 109 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 110 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 110 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 111 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 111 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 112 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 112 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 113 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 113 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 114 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 114 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 115 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 115 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 116 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 116 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 117 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 117 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 118 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 118 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 119 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 119 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 120 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 120 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 121 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 121 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 122 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 122 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 123 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 123 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 124 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 124 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 125 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 125 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 126 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 126 > type;
};
#line 216 "core-uintwidth.c"
template< > struct _Core_intwidth< 127 > {
#line 217 "core-uintwidth.c"
    typedef _Core_intwidth128< 127 > type;
};
#line 221 "core-uintwidth.c"
// Those widths that correspond to a fixed width type take those
// directly.
template< >
struct _Core_intwidth< 128 > {
#line 225 "core-uintwidth.c"
    typedef _Core_int#"128"##_t type;
};

#line 229 "core-uintwidth.c"

#line 231 "core-uintwidth.c"
// B
#line 233 "core-uintwidth.c"

/**
 ** @brief The core macros for fixed-width integer types.
 **
 ** They don't have arithmetic defined by themselves, but only
 ** conversion to and from the type `[u]intXX_t` where `XX` is the
 ** next power of 2 that is larger than @a N.
 **
 ** For the unsigned types, such a conversion wraps around mod \$2^{N}\$.
 **
 ** For the signed types, provisions to capture overflow are only made
 ** for the constructor. For automatic or modifiable objects, a
 ** `range_error` is thrown. For `constexpr` objects, compilation is
 ** aborted.
 **/
#define intwidth(N) _Core_intwidth< N >::type
#line 249 "core-uintwidth.c"

#line 251 "core-uintwidth.c"
// UINT

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 1 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "1" ") range error [-2^(" "0" "), (2^(" "0" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 1 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "1" ") range error [0, (2^" "1" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 2 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "2" ") range error [-2^(" "1" "), (2^(" "1" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 2 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "2" ") range error [0, (2^" "2" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 3 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "3" ") range error [-2^(" "2" "), (2^(" "2" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 3 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "3" ") range error [0, (2^" "3" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 4 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "4" ") range error [-2^(" "3" "), (2^(" "3" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 4 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "4" ") range error [0, (2^" "4" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 5 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "5" ") range error [-2^(" "4" "), (2^(" "4" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 5 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "5" ") range error [0, (2^" "5" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 6 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "6" ") range error [-2^(" "5" "), (2^(" "5" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 6 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "6" ") range error [0, (2^" "6" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 7 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "7" ") range error [-2^(" "6" "), (2^(" "6" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 7 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "7" ") range error [0, (2^" "7" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 9 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "9" ") range error [-2^(" "8" "), (2^(" "8" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 9 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "9" ") range error [0, (2^" "9" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 10 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "10" ") range error [-2^(" "9" "), (2^(" "9" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 10 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "10" ") range error [0, (2^" "10" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 11 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "11" ") range error [-2^(" "10" "), (2^(" "10" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 11 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "11" ") range error [0, (2^" "11" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 12 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "12" ") range error [-2^(" "11" "), (2^(" "11" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 12 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "12" ") range error [0, (2^" "12" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 13 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "13" ") range error [-2^(" "12" "), (2^(" "12" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 13 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "13" ") range error [0, (2^" "13" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 14 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "14" ") range error [-2^(" "13" "), (2^(" "13" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 14 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "14" ") range error [0, (2^" "14" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 15 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "15" ") range error [-2^(" "14" "), (2^(" "14" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 15 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "15" ") range error [0, (2^" "15" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 17 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "17" ") range error [-2^(" "16" "), (2^(" "16" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 17 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "17" ") range error [0, (2^" "17" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 18 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "18" ") range error [-2^(" "17" "), (2^(" "17" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 18 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "18" ") range error [0, (2^" "18" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 19 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "19" ") range error [-2^(" "18" "), (2^(" "18" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 19 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "19" ") range error [0, (2^" "19" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 20 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "20" ") range error [-2^(" "19" "), (2^(" "19" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 20 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "20" ") range error [0, (2^" "20" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 21 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "21" ") range error [-2^(" "20" "), (2^(" "20" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 21 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "21" ") range error [0, (2^" "21" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 22 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "22" ") range error [-2^(" "21" "), (2^(" "21" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 22 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "22" ") range error [0, (2^" "22" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 23 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "23" ") range error [-2^(" "22" "), (2^(" "22" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 23 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "23" ") range error [0, (2^" "23" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 24 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "24" ") range error [-2^(" "23" "), (2^(" "23" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 24 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "24" ") range error [0, (2^" "24" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 25 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "25" ") range error [-2^(" "24" "), (2^(" "24" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 25 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "25" ") range error [0, (2^" "25" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 26 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "26" ") range error [-2^(" "25" "), (2^(" "25" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 26 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "26" ") range error [0, (2^" "26" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 27 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "27" ") range error [-2^(" "26" "), (2^(" "26" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 27 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "27" ") range error [0, (2^" "27" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 28 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "28" ") range error [-2^(" "27" "), (2^(" "27" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 28 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "28" ") range error [0, (2^" "28" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 29 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "29" ") range error [-2^(" "28" "), (2^(" "28" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 29 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "29" ") range error [0, (2^" "29" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 30 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "30" ") range error [-2^(" "29" "), (2^(" "29" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 30 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "30" ") range error [0, (2^" "30" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 31 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "31" ") range error [-2^(" "30" "), (2^(" "30" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 31 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "31" ") range error [0, (2^" "31" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 33 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "33" ") range error [-2^(" "32" "), (2^(" "32" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 33 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "33" ") range error [0, (2^" "33" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 34 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "34" ") range error [-2^(" "33" "), (2^(" "33" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 34 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "34" ") range error [0, (2^" "34" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 35 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "35" ") range error [-2^(" "34" "), (2^(" "34" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 35 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "35" ") range error [0, (2^" "35" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 36 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "36" ") range error [-2^(" "35" "), (2^(" "35" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 36 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "36" ") range error [0, (2^" "36" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 37 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "37" ") range error [-2^(" "36" "), (2^(" "36" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 37 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "37" ") range error [0, (2^" "37" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 38 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "38" ") range error [-2^(" "37" "), (2^(" "37" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 38 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "38" ") range error [0, (2^" "38" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 39 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "39" ") range error [-2^(" "38" "), (2^(" "38" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 39 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "39" ") range error [0, (2^" "39" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 40 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "40" ") range error [-2^(" "39" "), (2^(" "39" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 40 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "40" ") range error [0, (2^" "40" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 41 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "41" ") range error [-2^(" "40" "), (2^(" "40" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 41 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "41" ") range error [0, (2^" "41" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 42 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "42" ") range error [-2^(" "41" "), (2^(" "41" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 42 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "42" ") range error [0, (2^" "42" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 43 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "43" ") range error [-2^(" "42" "), (2^(" "42" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 43 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "43" ") range error [0, (2^" "43" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 44 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "44" ") range error [-2^(" "43" "), (2^(" "43" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 44 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "44" ") range error [0, (2^" "44" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 45 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "45" ") range error [-2^(" "44" "), (2^(" "44" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 45 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "45" ") range error [0, (2^" "45" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 46 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "46" ") range error [-2^(" "45" "), (2^(" "45" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 46 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "46" ") range error [0, (2^" "46" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 47 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "47" ") range error [-2^(" "46" "), (2^(" "46" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 47 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "47" ") range error [0, (2^" "47" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 48 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "48" ") range error [-2^(" "47" "), (2^(" "47" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 48 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "48" ") range error [0, (2^" "48" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 49 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "49" ") range error [-2^(" "48" "), (2^(" "48" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 49 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "49" ") range error [0, (2^" "49" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 50 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "50" ") range error [-2^(" "49" "), (2^(" "49" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 50 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "50" ") range error [0, (2^" "50" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 51 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "51" ") range error [-2^(" "50" "), (2^(" "50" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 51 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "51" ") range error [0, (2^" "51" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 52 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "52" ") range error [-2^(" "51" "), (2^(" "51" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 52 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "52" ") range error [0, (2^" "52" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 53 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "53" ") range error [-2^(" "52" "), (2^(" "52" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 53 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "53" ") range error [0, (2^" "53" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 54 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "54" ") range error [-2^(" "53" "), (2^(" "53" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 54 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "54" ") range error [0, (2^" "54" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 55 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "55" ") range error [-2^(" "54" "), (2^(" "54" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 55 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "55" ") range error [0, (2^" "55" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 56 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "56" ") range error [-2^(" "55" "), (2^(" "55" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 56 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "56" ") range error [0, (2^" "56" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 57 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "57" ") range error [-2^(" "56" "), (2^(" "56" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 57 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "57" ") range error [0, (2^" "57" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 58 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "58" ") range error [-2^(" "57" "), (2^(" "57" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 58 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "58" ") range error [0, (2^" "58" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 59 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "59" ") range error [-2^(" "58" "), (2^(" "58" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 59 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "59" ") range error [0, (2^" "59" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 60 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "60" ") range error [-2^(" "59" "), (2^(" "59" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 60 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "60" ") range error [0, (2^" "60" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 61 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "61" ") range error [-2^(" "60" "), (2^(" "60" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 61 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "61" ") range error [0, (2^" "61" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 62 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "62" ") range error [-2^(" "61" "), (2^(" "61" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 62 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "62" ") range error [0, (2^" "62" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 63 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "63" ") range error [-2^(" "62" "), (2^(" "62" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 63 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "63" ") range error [0, (2^" "63" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 65 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "65" ") range error [-2^(" "64" "), (2^(" "64" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 65 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "65" ") range error [0, (2^" "65" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 66 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "66" ") range error [-2^(" "65" "), (2^(" "65" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 66 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "66" ") range error [0, (2^" "66" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 67 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "67" ") range error [-2^(" "66" "), (2^(" "66" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 67 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "67" ") range error [0, (2^" "67" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 68 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "68" ") range error [-2^(" "67" "), (2^(" "67" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 68 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "68" ") range error [0, (2^" "68" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 69 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "69" ") range error [-2^(" "68" "), (2^(" "68" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 69 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "69" ") range error [0, (2^" "69" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 70 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "70" ") range error [-2^(" "69" "), (2^(" "69" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 70 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "70" ") range error [0, (2^" "70" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 71 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "71" ") range error [-2^(" "70" "), (2^(" "70" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 71 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "71" ") range error [0, (2^" "71" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 72 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "72" ") range error [-2^(" "71" "), (2^(" "71" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 72 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "72" ") range error [0, (2^" "72" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 73 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "73" ") range error [-2^(" "72" "), (2^(" "72" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 73 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "73" ") range error [0, (2^" "73" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 74 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "74" ") range error [-2^(" "73" "), (2^(" "73" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 74 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "74" ") range error [0, (2^" "74" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 75 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "75" ") range error [-2^(" "74" "), (2^(" "74" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 75 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "75" ") range error [0, (2^" "75" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 76 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "76" ") range error [-2^(" "75" "), (2^(" "75" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 76 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "76" ") range error [0, (2^" "76" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 77 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "77" ") range error [-2^(" "76" "), (2^(" "76" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 77 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "77" ") range error [0, (2^" "77" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 78 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "78" ") range error [-2^(" "77" "), (2^(" "77" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 78 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "78" ") range error [0, (2^" "78" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 79 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "79" ") range error [-2^(" "78" "), (2^(" "78" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 79 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "79" ") range error [0, (2^" "79" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 80 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "80" ") range error [-2^(" "79" "), (2^(" "79" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 80 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "80" ") range error [0, (2^" "80" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 81 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "81" ") range error [-2^(" "80" "), (2^(" "80" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 81 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "81" ") range error [0, (2^" "81" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 82 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "82" ") range error [-2^(" "81" "), (2^(" "81" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 82 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "82" ") range error [0, (2^" "82" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 83 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "83" ") range error [-2^(" "82" "), (2^(" "82" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 83 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "83" ") range error [0, (2^" "83" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 84 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "84" ") range error [-2^(" "83" "), (2^(" "83" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 84 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "84" ") range error [0, (2^" "84" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 85 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "85" ") range error [-2^(" "84" "), (2^(" "84" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 85 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "85" ") range error [0, (2^" "85" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 86 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "86" ") range error [-2^(" "85" "), (2^(" "85" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 86 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "86" ") range error [0, (2^" "86" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 87 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "87" ") range error [-2^(" "86" "), (2^(" "86" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 87 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "87" ") range error [0, (2^" "87" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 88 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "88" ") range error [-2^(" "87" "), (2^(" "87" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 88 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "88" ") range error [0, (2^" "88" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 89 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "89" ") range error [-2^(" "88" "), (2^(" "88" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 89 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "89" ") range error [0, (2^" "89" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 90 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "90" ") range error [-2^(" "89" "), (2^(" "89" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 90 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "90" ") range error [0, (2^" "90" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 91 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "91" ") range error [-2^(" "90" "), (2^(" "90" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 91 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "91" ") range error [0, (2^" "91" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 92 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "92" ") range error [-2^(" "91" "), (2^(" "91" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 92 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "92" ") range error [0, (2^" "92" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 93 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "93" ") range error [-2^(" "92" "), (2^(" "92" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 93 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "93" ") range error [0, (2^" "93" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 94 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "94" ") range error [-2^(" "93" "), (2^(" "93" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 94 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "94" ") range error [0, (2^" "94" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 95 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "95" ") range error [-2^(" "94" "), (2^(" "94" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 95 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "95" ") range error [0, (2^" "95" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 96 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "96" ") range error [-2^(" "95" "), (2^(" "95" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 96 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "96" ") range error [0, (2^" "96" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 97 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "97" ") range error [-2^(" "96" "), (2^(" "96" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 97 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "97" ") range error [0, (2^" "97" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 98 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "98" ") range error [-2^(" "97" "), (2^(" "97" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 98 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "98" ") range error [0, (2^" "98" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 99 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "99" ") range error [-2^(" "98" "), (2^(" "98" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 99 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "99" ") range error [0, (2^" "99" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 100 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "100" ") range error [-2^(" "99" "), (2^(" "99" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 100 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "100" ") range error [0, (2^" "100" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 101 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "101" ") range error [-2^(" "100" "), (2^(" "100" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 101 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "101" ") range error [0, (2^" "101" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 102 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "102" ") range error [-2^(" "101" "), (2^(" "101" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 102 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "102" ") range error [0, (2^" "102" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 103 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "103" ") range error [-2^(" "102" "), (2^(" "102" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 103 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "103" ") range error [0, (2^" "103" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 104 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "104" ") range error [-2^(" "103" "), (2^(" "103" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 104 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "104" ") range error [0, (2^" "104" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 105 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "105" ") range error [-2^(" "104" "), (2^(" "104" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 105 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "105" ") range error [0, (2^" "105" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 106 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "106" ") range error [-2^(" "105" "), (2^(" "105" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 106 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "106" ") range error [0, (2^" "106" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 107 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "107" ") range error [-2^(" "106" "), (2^(" "106" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 107 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "107" ") range error [0, (2^" "107" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 108 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "108" ") range error [-2^(" "107" "), (2^(" "107" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 108 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "108" ") range error [0, (2^" "108" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 109 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "109" ") range error [-2^(" "108" "), (2^(" "108" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 109 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "109" ") range error [0, (2^" "109" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 110 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "110" ") range error [-2^(" "109" "), (2^(" "109" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 110 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "110" ") range error [0, (2^" "110" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 111 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "111" ") range error [-2^(" "110" "), (2^(" "110" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 111 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "111" ") range error [0, (2^" "111" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 112 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "112" ") range error [-2^(" "111" "), (2^(" "111" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 112 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "112" ") range error [0, (2^" "112" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 113 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "113" ") range error [-2^(" "112" "), (2^(" "112" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 113 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "113" ") range error [0, (2^" "113" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 114 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "114" ") range error [-2^(" "113" "), (2^(" "113" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 114 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "114" ") range error [0, (2^" "114" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 115 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "115" ") range error [-2^(" "114" "), (2^(" "114" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 115 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "115" ") range error [0, (2^" "115" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 116 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "116" ") range error [-2^(" "115" "), (2^(" "115" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 116 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "116" ") range error [0, (2^" "116" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 117 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "117" ") range error [-2^(" "116" "), (2^(" "116" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 117 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "117" ") range error [0, (2^" "117" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 118 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "118" ") range error [-2^(" "117" "), (2^(" "117" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 118 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "118" ") range error [0, (2^" "118" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 119 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "119" ") range error [-2^(" "118" "), (2^(" "118" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 119 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "119" ") range error [0, (2^" "119" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 120 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "120" ") range error [-2^(" "119" "), (2^(" "119" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 120 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "120" ") range error [0, (2^" "120" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 121 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "121" ") range error [-2^(" "120" "), (2^(" "120" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 121 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "121" ") range error [0, (2^" "121" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 122 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "122" ") range error [-2^(" "121" "), (2^(" "121" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 122 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "122" ") range error [0, (2^" "122" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 123 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "123" ") range error [-2^(" "122" "), (2^(" "122" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 123 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "123" ") range error [0, (2^" "123" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 124 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "124" ") range error [-2^(" "123" "), (2^(" "123" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 124 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "124" ") range error [0, (2^" "124" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 125 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "125" ") range error [-2^(" "124" "), (2^(" "124" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 125 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "125" ") range error [0, (2^" "125" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 126 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "126" ") range error [-2^(" "125" "), (2^(" "125" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 126 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "126" ") range error [0, (2^" "126" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 259 "core-uintwidth.c"

template< >
constexpr char const* _Core_int_error< 127 >(void) {
#line 262 "core-uintwidth.c"
    return "intwidth(" "127" ") range error [-2^(" "126" "), (2^(" "126" "))-1]";
}
#line 264 "core-uintwidth.c"

template< >
constexpr char const* _Core_uint_error< 127 >(void) {
#line 267 "core-uintwidth.c"
    return "uintwidth(" "127" ") range error [0, (2^" "127" ")-1]";
}
#line 269 "core-uintwidth.c"

#line 275 "core-uintwidth.c"
// B
#line 277 "core-uintwidth.c"

int main(int argc, char* argv[]) {
#line 279 "core-uintwidth.c"
    printf("mask %#" PRIx32 "\n", uintwidth(4)::mask);
    printf("mask %#" PRIx32 "\n", uintwidth(31)::mask);
    printf("mask %#" PRIx64 "\n", uintwidth(47)::mask);
    printf("mask %#" PRIx32 "\n", +uintwidth(7)(128));
    printf("mask %#" PRIx32 "\n", +uintwidth(7)(129));
    printf("mask %#" PRIx32 "\n", +uintwidth(7)(129ULL));

    uintwidth(3) a = 0x1005ULL;
    uintwidth(12) b = 0x1006ULL;
    uintwidth(23) c = 0x1007ULL;
    uintwidth(61) d = 0xF00000000003L;

    printf("a %#" PRIx8 "\n", +a);
    printf("b %#" PRIx16 "\n", +b);
    printf("c %#" PRIx32 "\n", +c);
    printf("d %#" PRIx64 "\n", +d);

    a += 1;
    printf("a+1 %#" PRIx32 "\n", a+1);
    ++b;
    printf("b %#" PRIx16 "\n", +b);
    c = c+d;
    printf("c+d %#" PRIx64 "\n", c+d);
    printf("d %#" PRIx64 "\n", d++);
    printf("d %#" PRIx64 "\n", +d);

// Test the compile time overflow detection.
#ifdef NDEBUG
#line 307 "core-uintwidth.c"
    static constexpr intwidth(3) e = -4;
#else
#line 309 "core-uintwidth.c"
    static constexpr intwidth(3) e = -5;
#endif
#line 311 "core-uintwidth.c"
    printf("e %" PRId8 "\n", +e);
    intwidth(3) f = 3;
    printf("f %" PRId8 "\n", +f);
}
