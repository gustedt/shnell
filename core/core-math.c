#include "core-feature.h"

#include <math.h>
#include <limits.h>

/**
 ** @brief The core isinteger macro
 **
 ** Is true if and only if the expression is an integer expression.
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#define isinteger _Core_isinteger

/**
 ** @brief The core isfloating macro
 **
 ** Is true if and only if the expression is a floating expression.
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#pragma CMOD amend generic_selection _Core_towidest(X) isfloating X
 float: true
 double: true
 long double: true
 complex_type(float): true
 complex_type(double): true
 complex_type(long double): true
 default: false
#pragma CMOD done

/**
 ** @brief The core iscomplex macro
 **
 ** Is true if and only if the expression is a complex expression.
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#pragma CMOD amend generic_selection _Core_towidest(X) iscomplex X
 complex_type(float): true
 complex_type(double): true
 complex_type(long double): true
 default: false
#pragma CMOD done

/**
 ** @brief The core iswide macro
 **
 ** Is true if and only if the expression is a wide integer
 ** expression.
 **
 ** @a _NOEVAL0 will not be evaluated but only inspected for its type.
 **/
#pragma CMOD amend generic_selection _Core_towidest(_NOEVAL0) iswide _NOEVAL0
 decltype(_Core_iswidest): issame(((generic_type(_NOEVAL0))0)+1, (generic_type(_NOEVAL0))0)
 default: false
#pragma CMOD done

/**
 ** @brief The core isnarrow macro
 **
 ** Is true if and only if the expression is a narrow integer
 ** expression.
 **
 ** @a _NOEVAL0 will not be evaluated but only inspected for its type.
 **/
#pragma CMOD amend generic_selection _Core_towidest(_NOEVAL0) isnarrow _NOEVAL0
 decltype(_Core_iswidest): ¬issame(((generic_type(_NOEVAL0))0)+1, (generic_type(_NOEVAL0))0)
 default: false
#pragma CMOD done

/**
 ** @brief The core isstandard macro
 **
 ** Is true if and only if the expression has standard integer type.
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#pragma CMOD amend generic_selection TE isstandard TE
  unsigned long long: true
  unsigned long: true
  unsigned int: true
  unsigned short: true
  unsigned char: true
  signed long long: true
  signed long: true
  signed int: true
  signed short: true
  signed char: true
  char: true
  bool: true
  default: false
#pragma CMOD done

/**
 ** @brief The core isextended macro
 **
 ** Is true if and only if the expression has extended integer type.
 **
 ** @a _NOEVAL0 will not be evaluated but only inspected for its type.
 **/
#pragma CMOD amend generic_selection _Core_towidest(_NOEVAL0) isextended _NOEVAL0
 decltype(_Core_iswidest): ¬isstandard(_NOEVAL0)
 default: false
#pragma CMOD done

#pragma CMOD amend generic_selection (TE)+0ULL isULL TE
  unsigned long long: true
  default: false
#pragma CMOD done

/**
 ** @brief The core isunsigned macro
 **
 ** Is true if and only if the expression has an unsigned integer
 ** type.
 **
 ** @a _NOEVAL0 will not be evaluated but only inspected for its type.
 **/
#pragma CMOD amend generic_selection _Core_towidest(_NOEVAL0) isunsigned _NOEVAL0
 decltype(_Core_iswidest): (generic_type(_NOEVAL0))-1 > 0
 default: false
#pragma CMOD done


/**
 ** @brief The core issigned macro
 **
 ** Is true if and only if the expression has a signed integer
 ** type.
 **
 ** @a _NOEVAL0 will not be evaluated but only inspected for its type.
 **/
#pragma CMOD amend generic_selection _Core_towidest(_NOEVAL0) issigned _NOEVAL0
 decltype(_Core_iswidest): (generic_type(_NOEVAL0))-1 < 0
 default: false
#pragma CMOD done

#define tominus1(_NOEVAL0) ((generic_type(_NOEVAL0))-1)

/**
 ** @brief The core tohighest macro
 **
 ** Returns the highest value representable in the type of @a _NOEVAL0.
 **
 ** @a _NOEVAL0 will not be evaluated but only inspected for its type.
 **/
#pragma CMOD amend generic_selection _NOEVAL0 tohighest _NOEVAL0
#pragma CMOD amend foreach T = char short int long long\ long
 signed ${T}:                  (signed ${T})(((unsigned ${T})-1)/2)
#pragma CMOD done
 char:         (char)(((unsigned char)-1)/(((char)-1 > 0) ? 1 : 2))
 float:                                                   HUGE_VALF
 double:                                                   HUGE_VAL
 long double:                                             HUGE_VALL
 default: (generic_type(_NOEVAL0))(((generic_type(_NOEVAL0))-1)/(((generic_type(_NOEVAL0))-1) < 0 ? 2 : 1))
#pragma CMOD done

/**
 ** @brief The core tolowest macro
 **
 ** Returns the lowest value representable in the type of @a _NOEVAL0.
 **
 ** @a _NOEVAL0 will not be evaluated but only inspected for its type.
 **/
#pragma CMOD amend generic_selection _NOEVAL0 tolowest _NOEVAL0
   default: (generic_type(_NOEVAL0))(-tohighest(_NOEVAL0)-isinteger(_NOEVAL0))
   bool:                                            false
#pragma CMOD done

/**
 ** @brief The core isconstant macro
 **
 ** Returns @a true if and only if @a X is considered to be a constant
 ** expression.
 **
 ** The effect of this macro is implementation-dependent, because not
 ** all implementations will have the same idea what a constant
 ** expression is. E.g for any variable `x` the expression `x-x` is
 ** known to be zero, but not all implementations will realize that
 ** this can be considered constant.
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#define  isconstant(X) _Core_isconstant(X)

/**
 ** @brief The core isice macro
 **
 ** Returns @a true if and only if @a X is considered to be an integer
 ** constant expression (ICE).
 **
 ** The effect of this macro is implementation-dependent, because not
 ** all implementations will have the same idea what a constant
 ** expression is. E.g for any variable `x` the expression `x-x` is
 ** known to be zero, but not all implementations will realize that
 ** this can be considered constant.
 **
 ** ICEs of value zero are also null pointer constants, see `isnull`
 ** and `tonull`.
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#define isice(X) _Core_isice(X)

/**
 ** @brief The core isnull macro
 **
 ** Returns @a true if and only if @a X is considered to be an integer
 ** constant expression (ICE) of value zero.
 **
 ** The effect of this macro is implementation-dependent, because not
 ** all implementations will have the same idea what a constant
 ** expression is. E.g for any variable `x` the expression `x-x` is
 ** known to be zero, but not all implementations will realize that
 ** this can be considered constant.
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#define isnull(X) _Core_isnull(X)

/**
 ** @brief The core isvla macro
 **
 ** Returns `true` if @a X is a variable length array (VLA).
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#define isvla(X) (¬isconstant(sizeof(X)))

/**
 ** @brief The core ispointer macro
 **
 ** Returns `true` if the generic type of @a X is a pointer type, that
 ** if it is effectively a pointer type, an array type or a function
 ** type.
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#pragma CMOD amend generic_selection _Core_towidest(X) ispointer X
 float: false
 double: false
 long double: false
 complex_type(float): false
 complex_type(double): false
 complex_type(long double): false
 decltype(_Core_iswidest): false
 default: true
#pragma CMOD done

#pragma CMOD amend generic_selection (_NOEVAL0) isnonvoidpointer _NOEVAL0
   void*: false
   void const*: false
   void volatile*: false
   void const volatile*: false
   default: ispointer(_NOEVAL0)
#pragma CMOD done

/**
 ** @brief The core isfunction macro
 **
 ** Returns `true` if the generic type of @a _NOEVAL0 is a function pointer
 ** type, that if it is effectively a function pointer type or a
 ** function type.
 **
 ** @a _NOEVAL0 will not be evaluated but only inspected for its type.
 **/
#pragma CMOD amend generic_selection _Core_towidest(_NOEVAL0) isfunction _NOEVAL0
 float: false
 double: false
 long double: false
 complex_type(float): false
 complex_type(double): false
 complex_type(long double): false
 decltype(_Core_iswidest): false
 default: issame((_NOEVAL0), *(_NOEVAL0))
#pragma CMOD done

/**
 ** @brief The core towidth macro
 **
 ** Returns the width of the type of @a X, result has type `size_t`.
 **
 ** @a X will not be evaluated but only inspected for its type.
 **/
#define towidth(X)                              \
 [](auto x){                                    \
   auto const y = tohighest(x);                 \
   size_t lo = 1;                               \
   size_t hi = sizeof(x)*CHAR_BIT;              \
   while (true) {                               \
     size_t mi = (lo + hi)/2;                   \
     if (y ⌦ mi) {                              \
       lo = mi;                                 \
       if (lo+1 < hi) return hi;                \
     } else {                                   \
       hi = mi;                                 \
       if (lo+1 < hi) return lo;                \
     }                                          \
   }                                            \
 }((generic_type(X)){})

#ifndef __cplusplus

#pragma CMOD amend foreach F =                  \
  acos                                          \
  acosh                                         \
  asin                                          \
  asinh                                         \
  atan                                          \
  atanh                                         \
  cos                                           \
  cosh                                          \
  exp                                           \
  log                                           \
  log10                                         \
  sin                                           \
  sinh                                          \
  sqrt                                          \
  tan                                           \
  tanh
/**
 ** @brief type-generic macro with different versions for all floating types
 **
 ** default floating type is `double`
 **/
#pragma CMOD amend oneline
#define ${F}
[](auto x){
   return
     _Generic(x,
              float: ${F}##f,
              long double: ${F}##l,
              complex_type(float): c##${F}##f,
              complex_type(double): c##${F},
              complex_type(long double): c##${F}##l,
              default: ${F})(x);
 }
#pragma CMOD done
#pragma CMOD done

#pragma CMOD amend foreach F =                  \
  cbrt                                          \
  ceil                                          \
  erf                                           \
  erfc                                          \
  exp2                                          \
  expm1                                         \
  fabs                                          \
  floor                                         \
  ilogb                                         \
  lgamma                                        \
  llrint                                        \
  llround                                       \
  log1p                                         \
  log2                                          \
  logb                                          \
  lrint                                         \
  lround                                        \
  nearbyint                                     \
  rint                                          \
  round                                         \
  tgamma                                        \
  trunc
/**
 ** @brief type-generic macro with different versions for all real floating types
 **
 ** default floating type is `double`
 **/
#pragma CMOD amend oneline
#define ${F}
[](auto x){
   return
     _Generic(x,
              float: ${F}##f,
              long double: ${F}##l,
              default: ${F})(x);
 }
#pragma CMOD done
#pragma CMOD done

#pragma CMOD amend foreach F =                  \
  atan2                                         \
  fdim                                          \
  fmax                                          \
  fmin                                          \
  fmod                                          \
  hypot                                         \
  pow                                           \
  remainder

#pragma CMOD amend oneline
#define ${F}
[](auto x, auto y){
   return
     _Generic(x+y,
              float: ${F}##f,
              long double: ${F}##l,
              default: ${F})(x, y);
 }
#pragma CMOD done
#pragma CMOD done

#pragma CMOD amend foreach F =                  \
  fma
#pragma CMOD amend oneline
#define ${F}
[](auto x, auto y, auto z){
   return
     _Generic(x+y+z,
              float: ${F}##f,
              long double: ${F}##l,
              default: ${F})(x, y, z);
 }
#pragma CMOD done
#pragma CMOD done

#pragma CMOD amend foreach F =                  \
  ldexp
#pragma CMOD amend oneline
#define ${F}
[](auto x, int k){
   return
     _Generic(x,
              float: ${F}##f,
              long double: ${F}##l,
              default: ${F})(x, k);
 }
#pragma CMOD done
#pragma CMOD done

#pragma CMOD amend foreach F =                  \
  remquo
#pragma CMOD amend oneline
#define ${F}
[](auto x, auto y, int quo[1]){
   return
     _Generic(x+y,
              float: ${F}##f,
              long double: ${F}##l,
              default: ${F})(x, y, quo);
 }
#pragma CMOD done
#pragma CMOD done

#pragma CMOD amend foreach F =                  \
  frexp
#pragma CMOD amend oneline
#define ${F}
[](auto x, int k[1]){
   return
     _Generic(x,
              float: ${F}##f,
              long double: ${F}##l,
              default: ${F})(x, k);
 }
#pragma CMOD done
#pragma CMOD done

#pragma CMOD amend foreach F =                  \
  modf
#pragma CMOD amend oneline
#define ${F}
[](auto x, generic_type(x)* iptr[1]){
   return
     _Generic(x,
              float: ${F}##f,
              long double: ${F}##l,
              default: ${F})(x, iptr);
 }
#pragma CMOD done
#pragma CMOD done

/*


// only for real types

F copysign(R x, S y);
F nextafter(R x, F y);
F nexttoward(R x, long double y);

U math_pdiff(R x, S y);

*/

#endif

#undef carg
#pragma CMOD amend oneline
#define carg
[](auto z){
  printf("found %g %g\n", real_value(z), imaginary_value(z));
  return atan2(imaginary_value(z), real_value(z));
}
#pragma CMOD done


#pragma CMOD amend oneline
#define _Core_cproj
[](auto z){
  if (isinf(real_value(z)) ∨ isinf(imaginary_value(z))) {
    z = INFINITY + 1.0if × copysign(0.0f, imaginary_value(z));
  }
  return z;
}
#pragma CMOD done

#pragma CMOD amend oneline
#define _Core_proj
[](auto z){
  if (isinf(z)) {
    z = INFINITY;
  }
  return z;
}
#pragma CMOD done


#pragma CMOD amend generic_selection X _Core_cproj2 X
 float: _Core_proj
 double: _Core_proj
 long double: _Core_proj
 complex_type(float): _Core_cproj
 complex_type(double): _Core_cproj
 complex_type(long double): _Core_cproj
   default: (double(*)(double))_Core_proj
#pragma CMOD done

#define cproj(X) _Core_cproj2(X)(X)

#pragma CMOD amend oneline
#define _Core_cabs
[](auto z){
   return hypot(real_value(z), imaginary_value(z));
}
#pragma CMOD done

#pragma CMOD amend oneline
#define _Core_fabs
[](auto x){
  return x < 0 ? -x : x;
}
#pragma CMOD done

/**
 ** @brief The core tounsigned macro
 **
 ** Returns the unsigned value representable in the type of @a X.
 **/
#pragma CMOD amend generic_selection X tounsigned X
#pragma CMOD amend foreach T = char short int long long\ long
signed ${T}:                  ((unsigned ${T})X)
#pragma CMOD done
char:                         ((unsigned char)X)
 default:                                     X
#pragma CMOD done


#pragma CMOD amend oneline
#define _Core_iabs
[](auto x){
   auto ret = tounsigned(x);
   return ret > tohighest(x) ? -ret : ret;
}
#pragma CMOD done

#pragma CMOD amend generic_selection X _Core_abs X
   float: _Core_fabs
   double: _Core_fabs
   long double: _Core_fabs
   complex_type(float): _Core_cabs
   complex_type(double): _Core_cabs
   complex_type(long double): _Core_cabs
   default: _Core_iabs
#pragma CMOD done

#define abs [](auto x){ return _Core_abs(x)(x); }

#pragma CMOD amend oneline
#define _Core_cabs²
[](auto z){
     auto r = real_value(z);
     auto i = imaginary_value(z);
     if (isinf(r) ∨ isinf(i)) {
       r = INFINITY;
     } else {
       r = r * r + i * i;
     }
     return r;
}
#pragma CMOD done

#pragma CMOD amend oneline
#define _Core_fabs²
[](auto x){
  return x × x;
}
#pragma CMOD done

#pragma CMOD amend generic_selection X _Core_abs² X
   float: _Core_fabs²
   long double: _Core_fabs²
   complex_type(float): _Core_cabs²
   complex_type(double): _Core_cabs²
   complex_type(long double): _Core_cabs²
     default: (double(*)(double))_Core_fabs²
#pragma CMOD done

#define abs² [](auto x){ return _Core_abs²(x)(x); }

#pragma CMOD amend generic_selection X _CORE_scalbn X N
     float: scalbnf((X), (N))
     long double: scalbnl((X), (N))
     default: scalbn((X), (N))
#pragma CMOD done

#pragma CMOD amend generic_selection X _CORE_scalbln X N
     float: scalblnf((X), (N))
     long double: scalblnl((X), (N))
     default: scalbln((X), (N))
#pragma CMOD done

#pragma CMOD amend generic_selection N _Core_scalbn X N
     long: _CORE_scalbln((X), (N))
     long long: _CORE_scalbln((X), (long)(N))
     default: _CORE_scalbn((X), (int)(N))
#pragma CMOD done


#undef _Core_scalbn
#define scalbn [](auto x, auto n){ return _Core_scalbn(n, x, n); }


/**
 ** @brief The core max type-generic macro
 **
 ** Returns the maximum of the two arguments. The type is the usual
 ** type for arithmetic conversion of binary operators.
 **/
#pragma CMOD amend oneline
#define max
[](auto a, auto b){
  decltype(a+b) A = a;
  decltype(a+b) B = b;
  /* We only have to be precocious if the types have different
     signedness. */
  if (isinteger(A) ∧ (isunsigned(a) ≠ isunsigned(b))) {
    /* If one is negative, the other is not because the type is an
       unsigned type. So the other one is the maximum. Only one of two
       lines may trigger. */
    if (a < 0) return B;
    if (b < 0) return A;
  }
  /* Here both have the same sign, so the conversion did the right
     thing */
  return (A < B) ? B : A;
}
#pragma CMOD done

/**
 ** @brief The core tosigned macro
 **
 ** Returns the signed value representable in the type of @a _NOEVAL0.
 **
 ** @a _NOEVAL0 will not be evaluated but only inspected for its type.
 **/
#pragma CMOD amend generic_selection _NOEVAL0 tosigned _NOEVAL0
#pragma CMOD amend foreach T = char short int long long\ long
unsigned ${T}:                  tolowest((signed ${T})0)
#pragma CMOD done
char:                         tolowest((signed char)0)
default:                      tolowest(_NOEVAL0)
#pragma CMOD done

/*
 * The implementation of the min feature is particularly difficult
 * because the system to compute a common type is not favorable. There
 * are 3 signed, 3 unsigned and 3 floating types: computing all
 * possible combinations would need 81 prototypes.
 */

#define _Core_min [](auto a, auto b){ return (a < b) ? a : b; }

#pragma CMOD amend generic_selection +(_NOEVALX) _Core_mintypeYul _NOEVALX _NOEVALY
    unsigned:  0u
    default:   0ul
#pragma CMOD done

#pragma CMOD amend generic_selection +(_NOEVALY) _Core_mintypeXuYs _NOEVALX _NOEVALY
    signed: 0                   /* X unsigned, Y unsigned */
    signed long: 0l
    signed long long: 0ll
    unsigned:  0u               /* X unsigned, Y unsigned */
    unsigned long:  _Core_mintypeYul(_NOEVALX, _NOEVALY)
    unsigned long long:  (generic_type(_NOEVALX))0
#pragma CMOD done

#pragma CMOD amend generic_selection +(_NOEVALX) _Core_mintypeXs _NOEVALX _NOEVALY
  default: _Core_mintypeXuYs(_NOEVALX, _NOEVALY)
    signed: 0                  /* X signed, Y unsigned */
    signed long: 0l
    signed long long: 0ll
#pragma CMOD done

#pragma CMOD amend generic_selection (_NOEVALX)+(_NOEVALY) _Core_mintype _NOEVALX _NOEVALY
  default: _Core_mintypeXs(_NOEVALX, _NOEVALY)
    float:  0.0f
    double:  0.0
    long double:  0.0l
    signed: 0                  /* X signed, Y signed */
    signed long: 0l
    signed long long: 0ll
#pragma CMOD done

#pragma CMOD amend oneline
#define min
[](auto b, auto a) {
  decltype(_Core_mintype(a, b)) ret;
  /* We only have to be precocious if the types have different
     signedness. */
  if (isinteger(ret) ∧ (isunsigned(a) ≠ isunsigned(b))) {
    /* If one is negative, the other is not because the type is an
       unsigned type. So the negative one is the minimum. Only one
       of two if selections may trigger, and the one that possibly
       could can be detected at compile time. */
    if (issigned(a) ∧ a < 0) {
      ret = a;
    } else {
      if (issigned(b) ∧ b < 0) {
        ret = b;
      } else {
        /* Here both values are positive so the conversion does the
           right thing */
        ret = _Core_min(tounsigned(a), tounsigned(b));
      }
    }
  } else {
    /* Here both types have the same signedness, so the conversions
       do the right thing */
    ret = _Core_min((decltype(ret))a, (decltype(ret))b);
  }
  return ret;
}
#pragma CMOD done

template< typename _T0 >
constexpr bool isarray(_T0) { return false; }

template< typename _T0, size_t N >
constexpr bool isarray(const _T0(&)[N]) { return true; }
