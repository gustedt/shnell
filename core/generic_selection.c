#include <core-math.h>

#include <stdio.h>

complex_type(double) const a0 = true*I;
complex_type(double) const a1 = I*3.0;

constexpr bool f(int x) { return ‼x; }

extern float (*const myabsf)(complex_type(float)) = abs;
extern double (*const myabs)(complex_type(double)) = abs;
extern unsigned (*const myabsu)(unsigned) = abs;
extern unsigned (*const myabss)(signed) = abs;

extern float (*const myabs²f)(complex_type(float)) = abs²;
extern double (*const myabs²)(complex_type(double)) = abs²;
extern double (*const myabs²u)(unsigned) = abs²;
extern double (*const myabs²s)(signed) = abs²;

extern long double (*const myscalbnl)(long double, int) = scalbn;
extern double (*const myscalbn)(double, int) = scalbn;
extern float (*const myscalbnf)(float, int) = scalbn;
extern long double (*const myscalblnl)(long double, long) = scalbn;
extern double (*const myscalbln)(double, long) = scalbn;
extern float (*const myscalblnf)(float, long) = scalbn;

extern signed (*const mymaxbns)(signed, int) = max;
extern unsigned (*const mymaxbnu)(unsigned, int) = max;
extern long double (*const mymaxbnl)(long double, int) = max;
extern double (*const mymaxbn)(double, int) = max;
extern float (*const mymaxbnf)(float, int) = max;
extern long (*const mymaxblns)(signed, long) = max;
extern long (*const mymaxblnu)(unsigned, long) = max;
extern long (*const mymaxblnls)(long, long) = max;
extern unsigned long (*const mymaxblnlu)(unsigned long, long) = max;
extern long double (*const mymaxblnl)(long double, long) = max;
extern double (*const mymaxbln)(double, long) = max;
extern float (*const mymaxblnf)(float, long) = max;

extern signed (*const myminbns)(signed, int) = min;
extern signed (*const myminbnsu)(signed, unsigned) = min;
extern signed (*const myminbnu)(unsigned, int) = min;
extern unsigned (*const myminbnuu)(unsigned, unsigned) = min;
extern long (*const myminblns)(signed, long) = min;
extern long (*const myminblnu)(unsigned, long) = min;
extern long (*const myminblnls)(long, long) = min;
extern long (*const myminblnlu)(unsigned long, long) = min;
extern float (*const myminf)(float, float) = min;
extern double (*const mymin)(double, double) = min;
extern long double (*const myminl)(long double, long double) = min;

#pragma CMOD amend foreach F =                  \
  atan2                                         \
  fdim                                          \
  fmax                                          \
  fmin                                          \
  fmod                                          \
  hypot                                         \
  pow                                           \
  remainder
extern double (*const my##${F})(double, double) = ${F};
extern float (*const my##${F}##f)(float, float) = ${F};
#pragma CMOD done


/*
 * Current state of VLA support in C++ compilers:
 *
 * Gcc and clang have VLA themselves, that is they implement the least
 * useful part of it :(
 *
 * Both do not have support for VM types as function parameters,
 * because they don't allow other parameters to be accessed within the
 * declaration.
 *
 * Clang is even worse than that, here VM simply don't work at all
 * because pointers to VLA cannot be assigned. So basically, VLA
 * support on clang is mostly useless.
 *
 */
#ifdef TEST_VLA
void fVLA(size_t nmemb, void* parm) {
  double (*ARRAY)[nmemb] = (double(*)[nmemb])parm;
  printf("%s: VLA transferred with size %zu\n", __func__, sizeof(*ARRAY));
}
#endif

#ifdef TEST_VLA_PARAMETER
void hVLA(size_t nmemb, void* parm) {
  double (*ARRAY)[nmemb] = [nmemb, parm]() → double(*)[nmemb]{ return (double(*)[nmemb])parm; };
  printf("%s: VLA transferred with size %zu\n", __func__, sizeof(*ARRAY));
}
void gVLA(size_t nmemb, double (*ARRAY)[nmemb]) {
  printf("%s: VM parameter transferred with size %zu\n", __func__, sizeof(*ARRAY));
}
#endif

int main(int argc, char* argv[]) {
#ifdef TEST_VLA
  double VLA[argc];
  double (*ARRAY)[argc] = &VLA;
  printf("VLA accepted with size %zu, pointed to %zu\n", sizeof(VLA), sizeof(*ARRAY));
  fVLA(sizeof VLA/sizeof VLA[0], &VLA);
#ifdef TEST_VLA_PARAMETER
  gVLA(sizeof VLA/sizeof VLA[0], &VLA);
#endif
#endif
#pragma CMOD amend foreach F = lowest highest minus1
#pragma CMOD amend foreach V = bool             \
  char                                          \
  signed\ char                                  \
  unsigned\ char                                \
  short                                         \
  unsigned\ short                               \
  int                                           \
  unsigned                                      \
  long                                          \
  unsigned\ long                                \
  long\ long                                    \
  unsigned\ long\ long                          \
  decltype(_Core_iswidest)                      \
  float                                         \
  double
   printf(#${V} "\thas " #${F} "\t%.50Lg\n", (long double)to##${F}((${V})0));
#pragma CMOD done
   puts("--------------------------------------------------------");
#pragma CMOD done
   printf("real part %g\n", a0.real());
   printf("real part %d\n", real_value(1));
   printf("imaginary part %d\n", imaginary_value(1));
   printf("real part %g\n", real_value(1.if));
   printf("imaginary part %g\n", imaginary_value(1.if));
   printf("argc (%d) is ice %d\n", argc-1, isice(argc-1));
   printf("char has width %zu\n", towidth((char)0));
   printf("uchar has width %zu\n", towidth((unsigned char)0));
   printf("short has width %zu\n", towidth((unsigned short)0));
   printf("bool has width %zu\n", towidth(true));
   printf("0 has width %zu\n", towidth(0));
   printf("0ll has width %zu\n", towidth(0ll));
   printf("0u has width %zu\n", towidth(0u));
   printf("0ull has width %zu\n", towidth(0ull));
   printf("0 and 1 are the same generic type %d\n", issame(0, 1));
   printf("0 and 0u are the same generic type %d\n", issame(0, 0u));
   printf("0.0 and 1.0 are the same generic type %d\n", issame(0.0, 1.0));
   printf("0.0 and 0.0f are the same generic type %d\n", issame(0.0, 0.0f));
   extern int f(double);
   float (*g)(float) = std∷cos;
   double A[2] = {};
   double B[3] = {};
   double* p = true ? tonullptr('\0') : nullptr;
#pragma CMOD amend foreach F = ice integer signed unsigned function pointer narrow wide constant extended
   puts("--------------------------------------------------------");
#pragma CMOD amend foreach V = 0.0 0 (short)0 (unsigned\ char)0 (decltype(_Core_iswidest))0 argc argc-argc f g A p
   printf(#${V} "\tis " #${F} "\t%d\n", is##${F}(${V}));
#pragma CMOD done
#pragma CMOD done
   printf("double[2] and double* are the same generic type %d\n", issame(A, (double*){}));
   printf("double[2] and double[3] are the same generic type %d\n", issame(A, B));
   printf("double[2] and double* are the same generic type %d\n", issame(A, &A));
#pragma CMOD amend foreach F =                  \
  acos                                          \
  acosh                                         \
  asin                                          \
  asinh                                         \
  atan                                          \
  atanh                                         \
  cos                                           \
  cosh                                          \
  exp                                           \
  log                                           \
  log10                                         \
  sin                                           \
  sinh                                          \
  sqrt                                          \
  tan                                           \
  tanh
   printf(#${F}"(%g) is %g\n", 1.0f, (double)${F}(1.0f));
   printf(#${F}"(%gi) is %g\n", 1.0f, (double)real_value(${F}(1.0if)));
#pragma CMOD done
#pragma CMOD amend foreach F =                  \
  cbrt                                          \
  ceil                                          \
  erf                                           \
  erfc                                          \
  exp2                                          \
  expm1                                         \
  floor                                         \
  ilogb                                         \
  lgamma                                        \
  llrint                                        \
  llround                                       \
  log1p                                         \
  log2                                          \
  logb                                          \
  lrint                                         \
  lround                                        \
  nearbyint                                     \
  rint                                          \
  round                                         \
  tgamma                                        \
  trunc
   printf(#${F}"(%g) is %g\n", 1.0f, (double)${F}(1.0f));
#pragma CMOD done
#pragma CMOD amend foreach F =                  \
  atan2                                         \
  fdim                                          \
  fmax                                          \
  fmin                                          \
  fmod                                          \
  hypot                                         \
  pow                                           \
  remainder
   printf(#${F}"(%g, %g) is %g\n", 1.0f, 0.7f, (double)${F}(1.0f, 0.7f));
#pragma CMOD done
#pragma CMOD amend foreach F =                  \
  carg                                          \
  conj                                          \
  cproj
   printf(#${F}"(%g,%gi) is %g\n", 0.5f, 1.0f, myabs²(${F}(0.5f+1.0if)));
#pragma CMOD done
   printf("abs²(argc) is %g\n",  abs²(argc));
   printf("max 1 -1 is %d\n", max(1, -1));
   printf("max 1 -1u is %u\n", max(1, -1u));
   printf("max 0u -1u is %u\n", max(0u, -1u));
   printf("max 1 -1.0 is %g\n", max(1, -1.0));
   printf("min 1 -1 is %d\n", min(1, -1));
   printf("min 1 -1u is %d\n", min(1, -1u));
   printf("min 0u -1u is %u\n", min(0u, -1u));
   printf("min 0u -1ul is %u\n", min(0u, -1ul));
   printf("min 0ul -1ull is %lu\n", min(0ul, -1ull));
   printf("min 0ul -1u is %u\n", min(0ul, -1u));
   printf("min 0ull -1ul is %lu\n", min(0ull, -1ul));
   printf("min 1 -1.0 is %g\n", min(1, -1.0));
   printf("min -1 1 is %d\n", min(-1, 1));
   printf("min -1u 1 is %d\n", min(-1u, 1));
   printf("min -1.0 1 is %g\n", min(-1.0, 1));
}
