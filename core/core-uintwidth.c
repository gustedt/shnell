
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#ifdef __cplusplus
#include <type_traits>
#include <stdexcept>
#endif

// shnell: stuff needed for the "let" directive, these special
// characters are not allowed in directives
#define OP_LSHIFT <<=
#define OP_RSHIFT >>=
#define OP_INTERSECTION &=
// shnell end

#pragma CMOD amend foreach UINT = uint int
/**
 ** @brief an error string that provides compile time information on
 ** the type, the width, and the bounds
 **/
template< unsigned M >
constexpr char const* _Core_##${UINT}##_error(void);
// generate typedefs for all fixed width types that we have
#pragma CMOD amend foreach N = 8 16 32 64
typedef ${UINT}##${N}##_t _Core_##${UINT}##${N}##_t;
#pragma CMOD done
typedef __##${UINT}##128_t _Core_##${UINT}##128_t;
#pragma CMOD done
// UINT

#pragma CMOD amend foreach N = 8 16 32 64 128
#pragma CMOD amend foreach UINT = uint int
// For each of the fixed width types, define a template class that
// encapsulates it and provides an unsigned integer storage with mask.
template< unsigned M >
class _Core_##${UINT}##width##${N} {
 private:
  _Core_##${UINT}##${N}##_t data;
 public:
  static constexpr _Core_##${UINT}##${N}##_t one = 1;
  static constexpr _Core_##${UINT}##${N}##_t mask = (((one ⌫ (M-1))-1)⌫1) + one;
  static constexpr _Core_##${UINT}##${N}##_t minval();
  static constexpr _Core_##${UINT}##${N}##_t maxval();
  constexpr operator _Core_##${UINT}##${N}##_t() const {
    return data;
  }
  constexpr _Core_##${UINT}##${N}##_t operator+ () const {
    return data;
  }
  /**
   ** @brief test if @a val is in the range of the type and throw up
   ** if it isn't.
   **/
  template< typename T >
    static constexpr _Core_##${UINT}##${N}##_t test_range_and_throw(T val);

  template< typename T >
    constexpr _Core_##${UINT}##width##${N}(T val);
  template< typename T >
    _Core_##${UINT}##width##${N}& operator=(T val);
  // nothing particular for the increment and decrement operators, we
  // just have to apply the mask
#pragma CMOD amend foreach OP = ++ --

  _Core_##${UINT}##width##${N}& operator ${OP} () {
    ${OP}data;
    data ∩= mask;
    return *this;
  }

  _Core_##${UINT}##${N}##_t operator ${OP} (int) {
    _Core_##${UINT}##${N}##_t old = (data${OP});
    data ∩= mask;
    return old;
  }

#pragma CMOD done

  // compound assignments for arithmetic operations are synthesized
  // from the underlying operation, we just have to apply the mask
#pragma CMOD amend foreach OP = *= /= += -= OP_LSHIFT OP_RSHIFT
  template< typename T >
    _Core_##${UINT}##width##${N}& operator ${OP} (T val) {
    data ${OP} val;
    data ∩= mask;
    return *this;
  }
#pragma CMOD done
  // compound assignments for bitwise operations are synthesized by
  // masking the other operand
#pragma CMOD amend foreach OP = OP_INTERSECTION |= ^=
  template< typename T >
    _Core_##${UINT}##width##${N}& operator ${OP} (T val) {
    auto m = mask ∩ val;
    data ${OP} m;
    return *this;
  }
#pragma CMOD done
};

#pragma CMOD done
// UINT

#pragma CMOD amend bind UINT=uint

template< unsigned M >
constexpr _Core_##${UINT}##${N}##_t _Core_##${UINT}##width##${N}< M >::maxval() {
  return _Core_##${UINT}##width##${N}< M >::mask;
}

template< unsigned M >
constexpr _Core_##${UINT}##${N}##_t _Core_##${UINT}##width##${N}< M >::minval() {
  return 0;
}

template< unsigned M >
template< typename T >
constexpr _Core_##${UINT}##${N}##_t _Core_##${UINT}##width##${N}< M >::test_range_and_throw(T val) {
  const decltype(val+maxval()) ual = val;
  if ((val < 0) ∨ (ual > maxval())) {
    throw std∷range_error(_Core_##${UINT}##_error< M >());
  }
  return val;
}

/**
 ** @brief Construction from integer is wrap around
 **/
template< unsigned M >
template< typename T >
constexpr _Core_##${UINT}##width##${N}< M >::_Core_##${UINT}##width##${N}(T val) : data(mask ∩ static_cast< _Core_##${UINT}##${N}##_t >(val)) {
#ifndef NDEBUG
  test_range_and_throw(val);
#endif
}

/**
 ** @brief Assignment from integer is wrap around
 **/
template< unsigned M >
template< typename T >
_Core_##${UINT}##width##${N}< M >& _Core_##${UINT}##width##${N}< M >::operator=(T val) {
 data = (mask ∩ static_cast< _Core_##${UINT}##${N}##_t >(val));
 return *this;
}
#pragma CMOD done
// uint

#pragma CMOD amend bind UINT=int

template< unsigned M >
constexpr _Core_##${UINT}##${N}##_t _Core_##${UINT}##width##${N}< M >::maxval() {
 return ((_Core_##${UINT}##width##${N}< M >::one ⌫ (M-1))-1);
}

template< unsigned M >
constexpr _Core_##${UINT}##${N}##_t _Core_##${UINT}##width##${N}< M >::minval() {
 return -_Core_##${UINT}##width##${N}< M >::maxval()-1;
}

template< unsigned M >
template< typename T >
constexpr _Core_##${UINT}##${N}##_t _Core_##${UINT}##width##${N}< M >::test_range_and_throw(T val) {
  const decltype(val+maxval()) ual = val;
  constexpr decltype(val+maxval()) mal = maxval();
  if (((val < 0) ∧ val < minval()) ∨ ((val > 0) ∧ (ual > mal))) {
    throw std∷range_error(_Core_##${UINT}##_error< M >());
  }
  return val;
}


/**
 ** @brief Construction throws up when @a val is out of range.
 **
 ** If the object to be constructed is `constexpr`, this has the
 ** effect that an out-of-range initialization will abort
 ** compilation. (The error message may be a bit weird, though.)
 **/
template< unsigned M >
template< typename T >
constexpr _Core_##${UINT}##width##${N}< M >::_Core_##${UINT}##width##${N}(T val) : data(_Core_##${UINT}##width##${N}< M >::test_range_and_throw< T >(val)) {
 /* empty */
}

/**
 ** @brief Assignment is undefined if the value is out of range.
 **
 ** Check manually by using test_range_and_throw().
 **/
template< unsigned M >
template< typename T >
_Core_##${UINT}##width##${N}< M >& _Core_##${UINT}##width##${N}< M >::operator=(T val) {
  data = val;
  return *this;
}
#pragma CMOD done
// int
#pragma CMOD done
// N

#pragma CMOD amend foreach UINT = uint int

template< unsigned M > struct _Core_##${UINT}##width;

// Create class wrappers for typedefs for each possible width.
#pragma CMOD amend eval EVAL0 EVAL1
#pragma CMOD amend foreach B = 3 4 5 6 7
#pragma EVAL0 amend let N = 1 -ls ${B}
#pragma EVAL0 amend let LO = 1 + ( ( ${B} == 3 ) -ter 0 -nar ( 1 -ls ${B} )/ 2 )
#pragma EVAL1 amend do I = ${LO} ${N} 1
#pragma EVAL0 done
template< > struct _Core_##${UINT}##width< ${I} > {
  typedef _Core_##${UINT}##width##${N}< ${I} > type;
};
#pragma EVAL1 done
// Those widths that correspond to a fixed width type take those
// directly.
template< >
struct _Core_##${UINT}##width< ${N} > {
  typedef _Core_##${UINT}##${N}##_t type;
};

#pragma EVAL0 done

#pragma CMOD done
// B
#pragma CMOD done
// EVAL0 EVAL1

/**
 ** @brief The core macros for fixed-width integer types.
 **
 ** They don't have arithmetic defined by themselves, but only
 ** conversion to and from the type `[u]intXX_t` where `XX` is the
 ** next power of 2 that is larger than @a N.
 **
 ** For the unsigned types, such a conversion wraps around mod \$2^{N}\$.
 **
 ** For the signed types, provisions to capture overflow are only made
 ** for the constructor. For automatic or modifiable objects, a
 ** `range_error` is thrown. For `constexpr` objects, compilation is
 ** aborted.
 **/
#define ${UINT}##width(N) _Core_##${UINT}##width< N >::type

#pragma CMOD done
// UINT

#pragma CMOD amend eval EVAL2
#pragma CMOD amend eval EVAL1
#pragma CMOD amend eval EVAL0
#pragma CMOD amend foreach B = 3 4 5 6 7

#pragma EVAL0 amend let N = 1 -ls ${B}
#pragma EVAL0 amend let LO = 1 + ( ( ${B} == 3 ) -ter 0 -nar ( 1 -ls ${B} )/ 2 )
#pragma EVAL1 amend do I = ${LO} ${N} 1
#pragma EVAL2 amend let I1 = ${I} - 1
#pragma EVAL0 done
#pragma EVAL0 done

template< >
constexpr char const* _Core_int_error< ${I} >(void) {
  return "intwidth(" #${I} ") range error [-2^(" #${I1} "), (2^(" #${I1} "))-1]";
}
#pragma EVAL2 done

template< >
constexpr char const* _Core_uint_error< ${I} >(void) {
  return "uintwidth(" #${I} ") range error [0, (2^" #${I} ")-1]";
}

#pragma EVAL1 done
#pragma CMOD done
// B
#pragma CMOD done
#pragma CMOD done
#pragma CMOD done

int main(int argc, char* argv[]) {
  printf("mask %#" PRIx32 "\n", uintwidth(4)::mask);
  printf("mask %#" PRIx32 "\n", uintwidth(31)::mask);
  printf("mask %#" PRIx64 "\n", uintwidth(47)::mask);
  printf("mask %#" PRIx32 "\n", +uintwidth(7)(128));
  printf("mask %#" PRIx32 "\n", +uintwidth(7)(129));
  printf("mask %#" PRIx32 "\n", +uintwidth(7)(129ULL));

  uintwidth(3) a = 0x1005ULL;
  uintwidth(12) b = 0x1006ULL;
  uintwidth(23) c = 0x1007ULL;
  uintwidth(61) d = 0xF00000000003L;

  printf("a %#" PRIx8 "\n", +a);
  printf("b %#" PRIx16 "\n", +b);
  printf("c %#" PRIx32 "\n", +c);
  printf("d %#" PRIx64 "\n", +d);

  a += 1;
  printf("a+1 %#" PRIx32 "\n", a+1);
  ++b;
  printf("b %#" PRIx16 "\n", +b);
  c = c+d;
  printf("c+d %#" PRIx64 "\n", c+d);
  printf("d %#" PRIx64 "\n", d++);
  printf("d %#" PRIx64 "\n", +d);

  // Test the compile time overflow detection.
#ifdef NDEBUG
  static constexpr intwidth(3) e = -4;
#else
  static constexpr intwidth(3) e = -5;
#endif
  printf("e %" PRId8 "\n", +e);
  intwidth(3) f = 3;
  printf("f %" PRId8 "\n", +f);
}
