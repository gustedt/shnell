#pragma CMOD load TRADE CONSTEXPR
#pragma CMOD insert hello before loading
#pragma CMOD insert gitID GIT
#pragma CMOD amend getconf                      \
  BC_BASE_MAX                                   \
  BC_DIM_MAX                                    \
  BC_SCALE_MAX                                  \
  BC_STRING_MAX

#if ${POSIX_VERSION} < 0
#error "we need a POSIX environment"
#endif

#include <stdio.h>

// Default values for ${DUM} and ${DIM}
#pragma VAR0 bind DUM=4 DIM=4
// This overwrites the existing value only if the environment variable
// exists.
#pragma VAR0 env DUM DIM

// This pragma uses meta-variables so it can only be evaluated in a
// second scan.
#pragma VAR1 let DD = ${DIM} + ${DUM} + 1

// Now all CONSTEXPR directives can use all levels of such defined
// variables.

#pragma CONSTEXPR factor
#pragma CONSTEXPR bc ⦃ ⦄

enum {
      r,
#pragma CONSTEXPR foreach A = a b c
      ${A} = (⦃${DD} >= 2⦄ ? ⦃2^${DD}⦄ : ⦃3+2⦄+r+⦃7+${DIM}⦄),
#pragma CONSTEXPR done
};

// Compute a constant up to a certain precision. A suffix that
// immediately follows the closing bracket without space will be glued
// to the resulting number. Here this should result in a valid
// constant of type `long double`. An expression that extends over
// multiple lines is possible. It is accounted in the physical line of
// the opening brace, empty lines are added thereafter to maintain the
// line structure of the source.
long double PI = ⦃scale=30 ;
                  r=4*atan(1)⦄L;
// Add a function to the bc state: ⦃define q(x) { return a(x); }⦄
double pi = ⦃4*q(1)⦄;

// Use bc variables, meta-variables and more complex bc programs:

long val8 = ⦃auto i; i=7; i += 1; r=i⦄;

// This will never be a prime number.
long val100 = ⦃auto i;
               for (i=0; i < ${DIM}; i++) {
                 r += 2*i + 1;
               }⦄;

// This should be left intact by bc, but still factored afterwards:

long val512 = 1<<9;

// Matrix indices are handled naturally as it should in C, that is the
// outer do loop has the row index and the inner do loop has the
// column index.
double A[${DIM}][${DUM}] = {
#pragma CONSTEXPR do I ${DIM}
 [${I}] = {
#pragma CONSTEXPR do J ${DUM}
           [${J}] = ⦃${I}+${J}⦄,
#pragma CONSTEXPR done
 },
#pragma CONSTEXPR done
};
#pragma CONSTEXPR done
#pragma CONSTEXPR done

int stdc::main(int argc, char* argv[argc+1]) {

#pragma CONSTEXPR specialize argc 1 2 3
  static_assert(${argc} < ${DIM}, #${argc} " is too large compared to " #${DIM} " (${DIM})");
  static_assert(${argc} < ${DUM}, #${argc} " is too large compared to " #${DIM} " (${DUM})");
  printf("diagonal element A[" #${argc} "][" #${argc} "]: %g\n", A[argc][argc]);
#pragma CONSTEXPR done
  else {
    if (argc < ${DIM} && (argc < ${DUM}))
      printf("diagonal element A[%d][%d]: %g\n", argc, argc, A[argc][argc]);
    else
      printf("diagonal element A[%d][%d] does not exist\n", argc, argc);
  }

}


#pragma CMOD insert hello end of file subject to loading
