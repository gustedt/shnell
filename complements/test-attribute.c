#pragma CMOD amend attribute

[[gnu::const]] double funct(double a){
  return 5.0;
}

[[deprecated("don't dare")]] enum ooo {
  aaa,
  [[deprecated("allowed")] ] bbb,
};

/* 1: a comment with a } */
/* 2: a comment with a { */

 ()
 { [0] = { [(a[0])], } }

// mismatches:
 {[}]}

 [[deprecated(]])

  
