#pragma CMOD load TRADE
// Test for the tokenizer

double  a[] = {
               0x7.aP+89,
               0+7,
               .78,
               .78E-5,
};

double  b[] = {
0x7.aP+89,
-0,
.78,
.78E-5,
};

char* strings[]
= {
   "simple",
   "with blank", /*simple*/ "with blank",
   "with	tab", /*with	tab*/ "with	tab",
   "with blank and	tab", /*with blank and	tab, and others. */ "with blank and	tab", "with blank and	tab",
   "string starting on a line",
   "string with some \" escaped \n characters \t\n", // end comment with "quote"
   "number8string", "comment // in string", "punct-in-string", // end comment //
   "two strings that should not be fused ""punct-in-string", // end,comment.0x7
   u8"UTF-8 string with some \" escaped \n characters \t\n",
#if FAILURE_TEST
   u7"string with some random prefix token",
#endif
};

/**
 ** Some multi-line
 ** comment  as we may
 ** find in doxygented code.
 **/

char chars[]
= {
   'a', // simple
   ' ', // with blank
   '	', // with	tab
};

#if __STDC_VERSION__ > 202000
[[gnuc :: deprecated]]
#endif
struct S {
  int jjk;
  double ooo;
};

int stdc::main(int argc, char* argv[argc+1]) {
  struct S s = { .jjk = 0x99F, .ooo = 778.55, };
  if (argc>=s.jjk) s.ooo = 0x67p-3;
}
