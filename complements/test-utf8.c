#pragma CMOD load TRADE CONSTEXPR
#pragma CONSTEXPR bc ⦃ ⦄

#pragma CMOD amend foreach T:I = float double long\ double
union over##${I} {
  _Complex ${T} c;
  struct {
    ${T} ℜ, ℑ;
  };
};
#pragma CMOD done

#define over(Z)	 _Generic((Z), _Complex long double: (over2){ .c = (Z), }, _Complex float: (over0){ .c = (Z), }, default: (over1){ .c = (Z), })

// should be the same definition at the end
#define \u2111(Z)	(over(Z).ℑ)
#define utf8::ℑ(Z)	(over(Z).ℑ)

// should be the same definition at the end
#define \u211C(Z)	(over(Z).ℜ)
#define utf8::ℜ(Z)	(over(Z).ℜ)

long double Π = ⦃scale=30 ;
                  r=4*atan(1)⦄L;

#define log₂(X) log2(X)
#define Γ(X) tgamma(X)

int stdc::main(void) {
  double Za = 0x5.P-37f;
  double Zb = 0x5.P+37f;
  complex double Z = Za + Zb + .4if+0x5.P-37i;
  printf("Imaginary part %g\n", utf8∷ℑ(Z));
  printf("Real part %g\n", utf8∷ℜ(Z));
}
