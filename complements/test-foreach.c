#pragma CMOD load TRADE CONSTEXPR
#pragma CMOD insert hello before loading

// Use environment variables with default values, and abort if there
// is no HOME.
#pragma VAR0 env DUM=DIM:-4 DIM=DUM:-4 HOME:?what

// This pragma uses meta-variables so it can only be evaluated in a
// second scan.
#pragma VAR1 let DD = ${DIM} + ${DUM} + 1

int const dd = ${DIM};

// Now all CONSTEXPR directives can use all levels of such defined
// variables.

#pragma CONSTEXPR factor
#pragma CONSTEXPR compute

enum {
      r,
      // Make these names public to see if the :: operator works in
      // front of a meta-variable
#pragma CONSTEXPR foreach A = a b c
      foreach::${A} = (${DD} >= (!0 + !0) ? (1 << ${DD}) : 3+2+r-(7+${DIM})),
#pragma CONSTEXPR done
};

char const* names[] = {
#pragma CONSTEXPR foreach A = r a b c
                       #${A},
#pragma CONSTEXPR done
};

// Matrix indices are handled naturally as it should in C, that is the
// outer do loop has the row index and the inner do loop has the
// column index.
double A[${DIM}][${DUM}] = {
#pragma CONSTEXPR do I ${DIM}
 [${I}] = {
#pragma CONSTEXPR do J ${DUM}
           [${J}] = ${I}+${J},
#pragma CONSTEXPR done
 },
#pragma CONSTEXPR done
};
#pragma CONSTEXPR done
#pragma CONSTEXPR done

#pragma CONSTEXPR foreach OPERATION:I = + -
#define OP##${I} ${OPERATION} ## =
#pragma CONSTEXPR done

#pragma CMOD amend foreach EXT = L U
#pragma PRETRADE local LUa7
#define LUa7 7##${EXT}
#pragma CMOD amend do I:NUM = 1 6 2
long ${EXT}##${NUM} = ${I}##${EXT} + sizeof LUa7;
#pragma CMOD done
#pragma PRETRADE done
#pragma CMOD done

char const* fullnames[] ={
#pragma CONSTEXPR foreach FN:NUMBER = Heinz\ Müller Frau\ Elster
                    [${NUMBER}] = #${FN},
#pragma CONSTEXPR done
};


// Iterate something over all known string prefixes. These could
// separated by "\ " such that they all land together in the
// meta-variable PREFIXES, but also words that don't contain a `=`
// character do the trick. So this defines `${PREFIXES}` to expand to
// a list of prefixes and `${OTHER}` to expand to the token `HEI`.
#pragma VAR0 bind PREFIXES= u\ U u8 L OTHER=HEI

// Iterate once over these to create strings. We use the ## operator
// in conjunction with ${NAME} expansion to glue it to the left and to
// the right int an identifier token, and to the right to glue it to a
// string as a prefix.
#pragma CONSTEXPR foreach PREFIX = ${PREFIXES}
void* string_ ## ${PREFIX} ## _var = ${PREFIX} ## "a string, prefixed with one of " #${PREFIXES};
void* string_ ## ${PREFIX} ## _vur = ${PREFIX} ###${PREFIXES};
#pragma CONSTEXPR done

#pragma CONSTEXPR foreach PREFIX = ${PREFIXES}
// The following initialization must be protected, because u8
// character literals is only a feature to come.
#ifndef __STDC_NO_##${PREFIX}##_CHAR_LITERAL__
unsigned long long char_ ## ${PREFIX} ## ${OTHER} ## _var = ${PREFIX} ## 'a';
#endif
#pragma CONSTEXPR done

#pragma VAR0 done

int stdc::main(int argc, char* argv[argc+1]) {

#pragma CONSTEXPR specialize argc 1 2 3
  static_assert(${argc} < ${DIM}, #${argc} " is too large compared to " #${DIM} " (${DIM})");
  static_assert(${argc} < ${DUM}, #${argc} " is too large compared to " #${DIM} " (${DUM})");
  printf("diagonal element A[" #${argc} "][" #${argc} "]: %g\n", A[argc][argc]);
#pragma CONSTEXPR done
  else {
    if (argc < ${DIM} && (argc < ${DUM}))
      printf("diagonal element A[%d][%d]: %g\n", argc, argc, A[argc][argc]);
    else
      printf("diagonal element A[%d][%d] does not exist\n", argc, argc);
  }

}


#pragma CMOD insert hello end of file subject to loading
