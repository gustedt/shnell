/**
 ** A test file for the `holdback` directive
 **
 ** For C, this needs lambdas, so this will probably not work unless
 ** lambdas are integrated in C23 and you have an implementation of
 ** that.
 **
 ** Also, up to now the state of type-generic lambdas (with `auto`
 ** parameters) is not yet clarified. I will try to keep this test
 ** up-to-date with the evolution of lambdas in C.
 **/


/** Simple maximum macro that always returns a type that can hold the
    maximum value of the two arguments. */
#pragma CMOD amend holdback max
[](auto a, auto b){
  return (a < 0)
    ? (b < 0) ? (a < b) ? b : a : b
    : (0 < b) ? (b < a) ? a : b : a;
}
#pragma CMOD done

/** Test default capture and other capture */
#pragma CMOD amend holdback first
[= , yf = 5](auto a, auto b){ return yf + a + b; }
#pragma CMOD done

/** Test other capture and array parameter rewriting .*/
#pragma CMOD amend holdback second
[x0 ,
 yf = 5](double (a)[const volatile 34], typeof(float) b){ return x0; }
#pragma CMOD done

/** Test default capture, other capture and pointer-to-array parameter
    non-rewritinġ */
#pragma CMOD amend holdback third
[&, x0 ,
 yf = 5](typeof(double) (*a)[34], float b)[[unsequenced]][[IMPL::whatever]]{ return second(a[1], b); }
#pragma CMOD done

/** Test default capture and array parameter rewriting. */
#pragma CMOD amend holdback fourth
[&](double a[__restrict__ 34], float b){ return x3; }
#pragma CMOD done

/** Test array parameter rewritinġ */
#pragma CMOD amend holdback fifth
[](double a[const 34], float b){ return a[0] + b; }
#pragma CMOD done

/** Test default capture, other capture and array-of-pointer-to-array
    parameter rewriting. */
#pragma CMOD amend holdback sixth
[&, x3 ,
 yf = 5](double (*a[42])[34], float b){
  /* Uncommenting this will lead to an error in NO_LAMBDA mode. */
  /* if (0) return a[0][2][4] + b; */
  auto blo = a[0][2][4] + b;
  return blo;
} oooo
#pragma CMOD done


int main(int argc, char* argv[]) {

  static int x1 = first(4, 7);
  int x0 = 65;
  double A[34] = { 0 };
  int x2 = second(A, 7);
  int x3 = third(&A, 7);
  int x4 = fourth(A, 7);
  int x5 = fifth(A, 7);
  double (*B[42])[34] = { 0 };
  int x6 = sixth(B, 7);
  auto y1 = first;
  auto y2 = second;
  auto y3 = third;
  auto y4 = fourth;
  auto y5 = fifth;
  auto y6 = sixth;

  double (*maxpDD)(double, double) = max;
  double (*maxpDF)(double, float) = max;
  /* uncommenting this produces an error because the return type is not correct */
  /* double (*maxpFF)(float, float) = max; */
  unsigned (*maxpUI)(unsigned, signed) = max;
  unsigned (*maxpIU)(signed, unsigned) = max;
  unsigned (*maxpUU)(unsigned, unsigned) = max;
  signed (*maxpII)(signed, signed) = max;
  /* uncommenting this produces an error because the return type is not correct */
  /* long (*maxpFF)(signed, signed) = max; */
  signed (*maxpISUC)(signed short, unsigned char) = max;

  return max(argc, 5);
}
