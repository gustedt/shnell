BSOURCES =					\
	test-POSIX.c				\
	test-alpha.c				\
	test-bc.c				\
	test-enum-expanded.c			\
	test-enum.c				\
	test-export.c				\
	test-foreach.c				\
	test-lambda-sort.c			\
	test-ranges.c				\
	test-utf8.c				\
	test-specialize.c			\
	test-tokenize.c

LSOURCES =					\
	test-string.c

OSOURCES =					\
	test-attribute.c			\
	test-module.c

OBJECTS = ${BSOURCES:.c=.o} ${LSOURCES:.c=.o}

INCLUDES ?= .include
MAKEFS  ?= .makef

PREOBJECTS = $(patsubst %.c, ${INCLUDES}/%.preo, ${BSOURCES} ${LSOURCES})

MFILES = $(patsubst %.c, ${MAKEFS}/%.mk, ${BSOURCES} ${LSOURCES})

IFILES = $(patsubst %.c, ${INCLUDES}/%.h, ${BSOURCES} ${LSOURCES})

BINS = ${BSOURCES:.c=}

ifeq (${MAKERECURSE},)
export MAKERECURSE = 1

ifeq (${SHNELL_STATIC},)
CFLAGS	+= -fPIC
LDSHARED ?= -shared -Bdynamic -lrt -lgcc
export SHNELL_RPATH = -Wl,-rpath,
else
LDFLAGS += -static
endif

COPT ?= -O3 -march=native -Wall

CFLAGS += -ffunction-sections -fdata-sections ${COPT}
LDFLAGS += -Wl,-gc-sections

SHNELLER = ../bin/shneller
POSIX = ../bin/posix
LEGACY = ../legacy/

SHCC := ${SHNELLER} ${CC}
PCC := ${POSIX} ${CC}

export CFLAGS
export LDSHARED
export LDFLAGS
export COPT
export SHNELLER
export SHCC
export PCC

endif

target :
	${MAKE} ${OBJECTS}
	${MAKE} -j 1 libtest.a
	${MAKE} -j 1 libtest.so
	${MAKE} ${BINS}

libtest.a(${OBJECTS}) : ${OBJECTS}

libtest.a : libtest.a(${OBJECTS})

libtest.so : ${OBJECTS}
	${SHCC} ${LDSHARED} ${OBJECTS} $(LDFLAGS) $(TARGET_ARCH) -o libtest.so

% : %.o
	${SHCC} $< $(LDFLAGS) $(TARGET_ARCH) -L. -ltest -o $@

%.o : %.c
	$(SHCC) $(CFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c $<

${MAKEFS}/%.mk ${INCLUDES}/%.h : %.c ${MAKEFS} ${INCLUDES}
	$(SHCC) $(CFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -M $< -o /dev/null
	@touch ${MAKEFS}/$*.mk ${INCLUDES}/$*.h

${MAKEFS} :
	mkdir ${MAKEFS}

${INCLUDES} :
	mkdir ${INCLUDES}

test-POSIX.o : test-POSIX.c
	$(PCC) $(CFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c $<

${MAKEFS}/test-POSIX.mk ${INCLUDES}/test-POSIX.h : %.c
	@-test -d ${MAKEFS} || mkdir ${MAKEFS}
	@-test -d ${INCLUDES} || mkdir ${INCLUDES}
	$(PCC) $(CFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -M $< -o /dev/null

headers : ${PREOBJECTS}

%.preo : %.h
	${CC} -x c -c $< $(CFLAGS) -I${LEGACY} $(TARGET_ARCH) -o $@

export ARFLAGS := ${ARFLAGS}U

test.a(${OBJECTS}) : ${OBJECTS}

clean :
	rm -f ${OBJECTS} ${PREOBJECTS} libtest.a libtest.so
	rm -rf ${MAKEFS}
	rm -rf ${INCLUDES}

-include ${MFILES}
