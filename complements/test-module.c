#pragma CMOD	load	module
#pragma MODULE	using	std::io		printf
#pragma MODULE	using	knoe		i j
#pragma MODULE	alias	ho::ho		hui
#pragma MODULE	alias	std::io		io
#pragma MODULE	intern			number2 abc
#pragma MODULE	end	toto::ui	number1 ppp type
#pragma CMOD	load	CONSTEXPR

int toto::o = 37;
int knoe::i = 13;

// identifiers on a single line
int
abc
[4];

// identifiers that terminate a line
int def=ppp
  ;

typedef hui::type type;

type ppp;

enum {
// generated idenfiers
#pragma CONSTEXPR do I 3
      number${I},
#pragma CONSTEXPR done
};

int main(int argc, char* argv[argc+1]) {
  std::io::printf("hey there: toto::o+knoe::i is %d\n", toto::o+knoe :: i);
  io::printf("hey there: toto::o+knoe::i is %d\n", toto::o+knoe :: i);
  printf("hey there: toto::o+knoe::i is %d\n", toto::o+knoe :: i);
}
