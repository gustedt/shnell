#pragma CMOD amend lambda

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

/* A function literal as a sort function. */
static const __auto_type sortDouble = [](size_t len, double arr[len]) -> void {
  qsort(arr, len, sizeof arr[0],
        /* A simple function literal as comparison function */
        [](void const* a, void const* b) -> int {
          double const* A = a;
          double const* B = b;
          if (*A < *B) return -1;
          else if (*A > *B) return 1;
          else return 0;
        }
        );
};

int test(int a) {
  if (a) return printf("A simple test with %d\n", a);
  else return puts("A simple test with 0");
}

struct tt {
  int a;
};

struct tt function(void) {
  return (struct tt){ 1 };
}

int main(void) {
  double A[] = { 4, 2, 5, 7, 1, -3, 1E7, 35, 0.7f, -9, 13, 0.4, -1E-97, };
  size_t const len = sizeof A/sizeof A[0];
  sortDouble(len, A);
  /* create bodies for the for loop */
  const __auto_type bod0
    = [  a = &(*A) /* make A a pointer */ ](size_t i) -> void
    {
     printf("%g\n", a[i]);
    };
  const __auto_type bod1
    = [ &A ](size_t i) -> void
    {
     printf("%g\n", A[i]);
    };
  puts("no loop:");
  if (1) if (0) { } else bod0(1); else bod1(2);
  if (1) if (0) { } else { bod0(3); }
  if (2) do bod0(4); while (false);
  if (2) do { bod0(5); } while (false);
  if (2) do [ &A ](size_t i) -> void { printf("%g\n", A[i]); }(6); while (false);
  puts("first loop:");
  for (size_t i = 0; i < len; ++i )
    bod0(i);
  puts("second loop:");
  for (size_t i = 0; i < len; ++i )
    bod1(i);
  puts("third loop:");
  for (size_t i = 0; i < len; ++i )
    [ &A ](size_t i) -> void { printf("%g\n", A[i]); }(i);
}
