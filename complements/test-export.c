#pragma CMOD load TRADE
#pragma PRETRADE private b
#pragma PRETRADE alias                          \
  user                                          \
  string=test::string                           \
  io=stdc

#pragma PRETRADE using test::string concat

// This should be mangled internally.
#define SOMETHING EMPTY
// This is external
#define user::SOMEELSE EMPTY

typedef enum toto {
    toto_one=-3,
    toto_two,
    toto_three,
    toto_four=43,
    toto_five=-2147483648,
    toto_six,
} tata;


_Thread_local int a = { 9 } ;
int b = 7, c, d = 34;

static string S = string::INIT("string, should not be replaced."), T, R = { 0 };

// This is to check if we correctly identify array *names*.
string Arr[sizeof(int)] = { 0 } ;
union string Brr[sizeof(int)] = { 0 };

// This is a plain static function and should stay out of the header.
static int function(void) {
  /* nothing sensible */
  return 0;
}

// An inline static function only makes sense in the header.
inline static int fonction(void) {
  /* nothing sensible */
  return 0;
}
// Now comes the conventional main, which is an entry point.
int stdc::main(int argc, char* argv[argc+1]) {
  io::printf("%d: %s\n", argc, S.s);
  string T = string::INIT("hei: ");
  // use concat with and without prefixes
  io::printf("\"%s\"\n", string::concat(T, S).s);
  io::printf("\"%s\"\n", concat(S, T).s);
  io::printf("\"%s\"\n", test::string::concat(string::EMPTY, T).s);
  io::printf("\"%s\"\n", string::concat(string::EMPTY, S).s);
  // return from main follows the usual C rules for main
}
