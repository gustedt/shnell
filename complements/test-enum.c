#pragma CMOD load TRADE

// The last naming component is "enum", so defining such a symbol
// clashes. Use another name.
#pragma PRETRADE alias here

#pragma TRADE enum here::toto                   \
  here::one=-3                                  \
  here::two                                     \
  here::three                                   \
  four=43                                       \
  five=-2147483648                              \
  six

int stdc::main(void) {
  stdc::printf("enumeration has %d elements, min %d, max %d, min of type %d, max of type %d\n",
         toto_number, toto_min, toto_max, toto_typemin, toto_typemax);
  for (unsigned i = 0; i <= toto_number; ++i) {
    stdc::printf("%u: %s = %d (%d)\n", i, toto_names(toto_elem(i)), toto_elem(i), toto_pos(toto_elem(i)));
  }
}

#pragma CMOD done


