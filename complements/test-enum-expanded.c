#pragma CMOD load TRADE


// @brief An enumeration type that has been generated with an enum directive.
//
// Additional tools are available for this type:
// - toto_min and toto_max are the minimum and maximum values, respectively.
// - toto_inv() returns the smallest int value that is not covered by the type.
// - toto_names() returns a string with the name of the value
// - toto_pos() and toto_elem give the mapping between positions in the declaration order
//   and enumeration values
enum toto {
    toto_one=-3,
    toto_two,
    toto_three,
    toto_four=43,
    toto_five=-2147483648,
    toto_six,
};
typedef enum toto toto;

enum {
    toto_number = 6,
// Since we now know that we have two's complement,
// specify INT_MAX and INT_MIN like this.
    toto_typemax = -1U/2,
    toto_typemin = -toto_typemax-1,
    toto_nmin = toto_typemin+6,
    toto_val0 = toto_one,
    toto_val1 = toto_two,
    toto_val2 = toto_three,
    toto_val3 = toto_four,
    toto_val4 = toto_five,
    toto_val5 = toto_six,
    toto_min0 = toto_val0,
    toto_max0 = toto_val0,
    toto_max1 = ( toto_val1 < toto_max0 ? toto_max0 : toto_val1 ),
    toto_min1 = ( toto_val1 > toto_min0 ? toto_min0 : toto_val1 ),
    toto_max2 = ( toto_val2 < toto_max1 ? toto_max1 : toto_val2 ),
    toto_min2 = ( toto_val2 > toto_min1 ? toto_min1 : toto_val2 ),
    toto_max3 = ( toto_val3 < toto_max2 ? toto_max2 : toto_val3 ),
    toto_min3 = ( toto_val3 > toto_min2 ? toto_min2 : toto_val3 ),
    toto_max4 = ( toto_val4 < toto_max3 ? toto_max3 : toto_val4 ),
    toto_min4 = ( toto_val4 > toto_min3 ? toto_min3 : toto_val4 ),
    toto_max5 = ( toto_val5 < toto_max4 ? toto_max4 : toto_val5 ),
    toto_min5 = ( toto_val5 > toto_min4 ? toto_min4 : toto_val5 ),
    toto_min = toto_min5,
    toto_max = toto_max5,
};

// @brief This is a pure function that returns that smallest `int`
// value that is not in the enumeration toto.
inline int toto_inv(void) {
    _Bool toto_arr[6+1] = { 0 };

    /* The acrobatic in the index expressions ensures that the
            * compiler does not falsely report an arithmetic overflow.*/
    if (toto_val0 < toto_nmin) toto_arr[(unsigned)toto_val0 - toto_typemax -1u] = 1;
    if (toto_val1 < toto_nmin) toto_arr[(unsigned)toto_val1 - toto_typemax -1u] = 1;
    if (toto_val2 < toto_nmin) toto_arr[(unsigned)toto_val2 - toto_typemax -1u] = 1;
    if (toto_val3 < toto_nmin) toto_arr[(unsigned)toto_val3 - toto_typemax -1u] = 1;
    if (toto_val4 < toto_nmin) toto_arr[(unsigned)toto_val4 - toto_typemax -1u] = 1;
    if (toto_val5 < toto_nmin) toto_arr[(unsigned)toto_val5 - toto_typemax -1u] = 1;
    if (!toto_arr[0]) return toto_typemin + 0;
    if (!toto_arr[1]) return toto_typemin + 1;
    if (!toto_arr[2]) return toto_typemin + 2;
    if (!toto_arr[3]) return toto_typemin + 3;
    if (!toto_arr[4]) return toto_typemin + 4;
    if (!toto_arr[5]) return toto_typemin + 5;
    return toto_nmin;
}

// @brief A pure function that returns the px' value of toto in order of
// declaration.
inline toto toto_elem(unsigned px) {
    switch (px) {
    case 0:
        return toto_one;
    case 1:
        return toto_two;
    case 2:
        return toto_three;
    case 3:
        return toto_four;
    case 4:
        return toto_five;
    case 5:
        return toto_six;
    default:
        return toto_inv();
    }
};

// @brief Return a string with the name of each toto value.
//
// If the value is not a valid value for toto but the specific
// value returned by toto_inv() this returns "<toto invalid>"
// and otherwise "<toto unknown>".
inline char const* toto_names(toto x) {
    if (x == toto_inv()) return "<toto invalid>";
    switch (x) {
    case toto_one:
        return"toto_one";
    case toto_two:
        return"toto_two";
    case toto_three:
        return"toto_three";
    case toto_four:
        return"toto_four";
    case toto_five:
        return"toto_five";
    case toto_six:
        return"toto_six";
    default:
        return "<toto unknown>";
    }
}

// @brief Return the position in the declaration of each toto
// value.
inline unsigned toto_pos(toto x) {
    switch (x) {
    case toto_one:
        return 0;
    case toto_two:
        return 1;
    case toto_three:
        return 2;
    case toto_four:
        return 3;
    case toto_five:
        return 4;
    case toto_six:
        return 5;
    default:
        return -1;
    }
}

int stdc::main(void) {
    printf("toto has %d elements, min %d, max %d, min of type %d, max of type %d\n",
           toto_number, toto_min, toto_max, toto_typemin, toto_typemax);
    printf("0: ""toto_one" " (%s) = %d (%d)\n", toto_names(toto_one),
           toto_one, toto_elem(0));
    printf("1: ""toto_two" " (%s) = %d (%d)\n", toto_names(toto_two),
           toto_two, toto_elem(1));
    printf("2: ""toto_three" " (%s) = %d (%d)\n", toto_names(toto_three),
           toto_three, toto_elem(2));
    printf("3: ""toto_four" " (%s) = %d (%d)\n", toto_names(toto_four),
           toto_four, toto_elem(3));
    printf("4: ""toto_five" " (%s) = %d (%d)\n", toto_names(toto_five),
           toto_five, toto_elem(4));
    printf("5: ""toto_six" " (%s) = %d (%d)\n", toto_names(toto_six),
           toto_six, toto_elem(5));
    printf("6: ""75" " (%s) = %d (%d)\n", toto_names(75),
           75, toto_elem(6));
    printf("7: ""83" " (%s) = %d (%d)\n", toto_names(83),
           83, toto_elem(7));
    printf("8: ""44" " (%s) = %d (%d)\n", toto_names(44),
           44, toto_elem(8));
    printf("9: ""-2147483646" " (%s) = %d (%d)\n", toto_names(-2147483646),
           -2147483646, toto_elem(9));
}
