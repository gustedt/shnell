/* Create a list of al POSIX variables that might interest us. */
#pragma CMOD amend bind LIST=POSIX_VERSION POSIX2_VERSION               \
  INT_MAX DUMMY POSIX_PATH                                              \
  POSIX_SHELL                                                           \
  POSIX_V7_ILP32_OFF32 POSIX_V7_LP64_OFF64 POSIX_V7_ILP32_OFFBIG POSIX_V7_LP64_OFFBIG \
  POSIX_V7_ILP32_OFF32_CFLAGS POSIX_V7_ILP32_OFF32_LDFLAGS              \
  POSIX_V7_LP64_OFF64_CFLAGS POSIX_V7_LP64_OFF64_LDFLAGS                \
  POSIX_V7_THREADS_CFLAGS POSIX_V7_THREADS_LDFLAGS                      \
  GLIBC

/* Capture values from POSIX variables. Those with = provide defaults,
   unknown variables are set to `-1`. */
#pragma POSIX getconf INT_MAX                                           \
  DUMMY POSIX_PATH=PATH                                                 \
  POSIX_SHELL                                                           \
  POSIX_V7_ILP32_OFF32 POSIX_V7_LP64_OFF64 POSIX_V7_ILP32_OFFBIG POSIX_V7_LP64_OFFBIG \
  POSIX_V7_ILP32_OFF32_CFLAGS POSIX_V7_ILP32_OFF32_LDFLAGS              \
  POSIX_V7_LP64_OFF64_CFLAGS POSIX_V7_LP64_OFF64_LDFLAGS                \
  POSIX_V7_THREADS_CFLAGS POSIX_V7_THREADS_LDFLAGS                      \
  GLIBC=GNU_LIBC_VERSION

long const int_max = ${INT_MAX};

char const* POSIX::ARRAY[] =  {

#pragma POSIX foreach F = ${LIST}
  // the first #... is a simple stringification of the variable name
  // the second is a stringification of the contents of that variable
  #${F} " is " #${${F}},
#pragma POSIX done

};

#pragma POSIX foreach F = ${LIST}
#define POSIX::${F} #${${F}}
#pragma POSIX done

int stdc::main(int argc, char* argv[argc+1]) {
#pragma POSIX foreach F:I = ${LIST}
  stdc::printf(#${F} " has number " #${I} ", %s\n", ARRAY[${I}]);
#pragma POSIX done
}
