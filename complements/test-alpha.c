#pragma CMOD load TRADE

// Test some Unicode code points as names for functions. Not all of
// them are detected as they should, yet.


unsigned α(int b){
  unsigned ret = 8;
  ret ∪= 3;
  return ret;
}
int אβ\u05d0(int b){
  return 5.≤+6;
}
unsigned eß(int b){
  return 1;
}
unsigned e\u0073(int b){
  return 1;
}
unsigned oe\u006f2(int b){
  return 1;
}
unsigned a(double b) {
  return 1;
}
double Ᵽa\u2c63(double b) {
  return 1;
}

int 😈😈\U0001F608(void) {
  return 0;
}

int \U0001F600a(void) {
  return 0;
}

int 𐇪1(void) {
  return 0;
}

int 𐇪\U000101EA(void) {
  return 0;
}

int 𐌲(void) {
  return 1;
}
