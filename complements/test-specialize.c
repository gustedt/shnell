#pragma CMOD load TRADE

// A test case for the specialize directive

int stdc::main(int argc, char* argv[argc+1]) {

  static char buf[] = "***********************";

  // General specialization including an "else" clause. Because of
  // that else clause, we have to pass the precision argument
  // (corresponding to the "*") dynamically via the len variable.
#pragma CMOD amend specialize argc 1 2 8 3 else 4 7 6
  int len = (${argc} < sizeof buf ? ${argc} : sizeof buf);
  printf("this\tis specific (with else) of length " #${argc} ": %.*s\n", len, buf);
#pragma CMOD done

  // Specialization including without an else clause. Here we can to
  // the check statically in a _Static_assert and just replace the "*"
  // that we had previously by the decimal constant.
#pragma CMOD amend specialize argc 1 2 8 3 4 7 23 11
  _Static_assert(${argc} < sizeof buf, "argc too small");
  printf("this\tis specific (without else) of length ${argc}: %." #${argc} "s\n", buf);
#pragma CMOD done
  // The else clause then can be really completely generic and also
  // print the argc value.
  else {
    int len = (argc < sizeof buf ? argc : sizeof buf);
    printf("this\tis specific (separate else) of length %d: %.*s\n", argc, len, buf);
  }
}

