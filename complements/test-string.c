#pragma CMOD load POSIX
#pragma TRADE private lenp
// The first arguments to the export directive are empty. That means that
//
// - the long internal prefix is taken from the filename, so `test::string`
// - the short internal prefix is taken from that, so `string`
// - the only additional private symbol is `lenp`
//
// Note that the long external prefix and mangling depends on the
// environment variable `EXPORT_SEPARATOR`.

enum secret {
  string::size = 32,
  string::length = size-1,
};

union string {
  struct {
    char s[length];
    char const last;
  };
  unsigned long long w[size/sizeof(unsigned long long)];
};

_Thread_local string myown;

#define string::INIT(S) { .s = S, }
#define string::EMPTY ((string)INIT(""))

inline size_t (lenp)(string const s[restrict static 1]) {
#pragma CMOD amend do I = 4
  {
    stdc::uint64_t w = s->w[${I}];
    if (!(w&0xFF)) return 8*${I};
#pragma CMOD amend do J = 1 8
    w >>= 8;
    if (!(w&0xFF)) return 8*${I}+${J};
#pragma CMOD done
  }
#pragma CMOD done
  return length;
}

inline unsigned long (len)(string const s) {
  return lenp(&s);
}

#define len(X)                                                          \
  _Generic((X),                                                         \
           string*: (lenp)(_Generic((X), string*: (X), default: (string*)0)), \
           string const*: (lenp)(_Generic((X), string const*: (X), default: (string const*)0)), \
           string: (len)(_Generic((X), string: (X), default: EMPTY)))

inline string (concat)(string a, string b) {
  size_t leng = len(a);
  if (!leng) {
    return b;
  } else if (leng < length) {
    string ret;
    stdc::memcpy(ret.s, a.s, leng);
    stdc::memcpy(ret.s+leng, b.s, length-leng);
    return ret;
  } else {
    return a;
  }
}

inline string (get)(char const*restrict s) {
  string ret;
  size_t i=0;
  for (; i < length && s[i]; ++i) {
    ret.s[i] = s[i];
  }
  ret.s[i] = 0;
  return ret;
}

inline string (cpy)(string s) {
  return s;
}

#pragma CMOD amend foreach FUNC = spn cspn
inline size_t (${FUNC})(string const s, string const accept){
  return POSIX::str##${FUNC}(s.s, accept.s);
}
#pragma CMOD done

#define GET(X) _Generic((X), string: cpy, default: get)(X)

#pragma CMOD amend foreach FUNC = concat spn cspn
#define ${FUNC}(A, B) (${FUNC})(GET(A), GET(B))
#pragma CMOD done

/* a final comment should still come through */
