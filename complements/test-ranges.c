#pragma CMOD load TRADE

// A test case for the range directive

#pragma CMOD amend let SEQ -seq 1 16 ×2

char SEQ[] = #${SEQ};

int stdc::main(int argc, char* argv[argc+1]) {

  static char buf[] = "***********************";

  // General range specialization including an "else" clause. Because
  // of that else clause, we have to pass the precision argument
  // (corresponding to the "*") dynamically via the len variable.
#pragma TRADE ranges argc ${SEQ} else
  int len = (${argc} < sizeof buf ? ${argc} : sizeof buf);
  printf("this\tis specific (with else) of range < ${argc}: %.*s\n", len, buf);
#pragma TRADE done

  // Range specialization without an else clause. Here we can to the
  // check statically in a _Static_assert and just replace the "*"
  // that we had previously by the decimal constant.
#pragma TRADE ranges argc ${SEQ} 11 23
  static_assert(${argc} < sizeof buf, "argc too small");
  printf("this\tis specific (without else) of range < ${argc}: %.##${argc}s\n", buf);
#pragma TRADE done
  // The else clause then can be really completely generic and also
  // print the argc value.
  else {
    int len = (argc < sizeof buf ? argc : sizeof buf);
    printf("this\tis specific (separate else) of length %d: %.*s\n", argc, len, buf);
  }
}

