/* -*- C -*-

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## The CONSTEXPR dialect -- multiple levels of compile time evaluation

# This is a tool that allows you to organize several scans of the C
# source to establish meta-variables with depending evaluation. Per
# default there are three `#pragma` labels, `VAR0`, `VAR1` and
# `CONSTEXPR`, but other labels `VAR2`, ... , `VARx` can also be
# created if needed.

# As their names suggest `VAR0`, `VAR1`, ... are to be used to define
# meta-variables. Any such meta-variable that is defined with `VAR0`
# can be used when defining new variables with `VAR1` and so on. These
# labels only allow to define variables with `let`, `bind`, `env` and
# `getconf`, all other directives are suppressed.

# The last level, `CONSTEXPR` then serves to use these meta-variables
# to `amend` and `insert` source code.

### Example

# Set variables `${DUM}` and `${DIM}` to values found
# in the environment or to the value `4` if not
# found. (The syntax `:-` here is inherited from sh's
# default initialization of variables.)

#pragma VAR0 env DUM=MYDUM:-4 DIM:-4

# The following directive uses meta-variables so it
# can only be evaluated in a second scan, namely
# `VAR1`:

#pragma VAR1 let DD = ${DIM} + ${DUM} + 1

# Use this for code unrolling, here array initialization:
double A[] = {
#pragma CONSTEXPR do I = ${DD}
[${I}] = 2×${I},
#pragma CONSTEXPR done
};

### Adjusting the number of levels

# This tool itself can be adopted by an environment
# variable, `CONSTEXPR_MAX`. If set to a higher
# level that its default, `1`, as many levels of
# `VARx` labels are created. Be reasonable when you
# augment this value: each new label implies a
# complete scan of the source for that label, so
# this might have an important impact on your
# compile times.

#endPreamble $*

*/

#pragma CMOD // -*- C -*-
#pragma CMOD amend dialect CONSTEXPR_TMP0 dialect amend= insert=
#pragma CMOD // the label CONSTEXPR is provided for user loop constructs
#pragma CMOD // this is evaluated in one last scan after all variables have been assigned
#pragma CMOD // labels VAR0, VAR1, ... are provided for user constexpr variables
#pragma CMOD // a special label CONSTEXPR_TMP0 is needed for the preamble
#pragma CONSTEXPR_TMP0 dialect COND IF amend= insert=
#pragma CONSTEXPR_TMP0 dialect CONSTEXPR do foreach specialize ranges factor compute bc amend= insert=
#pragma CMOD // ---------------------------------------------------------------
#pragma CMOD // all expansion is limited to here, only CONSTEXPR_TMP0 survives
#pragma CMOD amend dialect CONSTEXPR_TMP1 do amend= insert=
#pragma CMOD // default value for CONSTEXPR_MAX is 1
#pragma CMOD amend env CONSTEXPR_MAX:-1
#pragma CONSTEXPR_TMP1 // today's value of CONSTEXPR_MAX is ${CONSTEXPR_MAX}
#pragma CONSTEXPR_TMP1 do I ${CONSTEXPR_MAX} -1 -1
#pragma CONSTEXPR_TMP0 // label VAR${I} is provided for user constexpr variable evaluation on level ${I}
#pragma CONSTEXPR_TMP0 dialect VAR${I} env getconf let bind amend= insert=
#pragma CONSTEXPR_TMP1 done
#pragma CMOD done
#pragma CMOD done
#pragma CMOD // ---------------------------------------------------------------

