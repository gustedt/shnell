/* -*- C -*-

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2022

## The HOLDBACK extension for type-generic lambdas and constexpr lambdas

# This enables the definition of holdbacks, that are macro definitions
# of lambda expressions that are expanded differently if they are
# encountered with or without arguments.

# If encountered in a macro call with arguments the expansion first
# squeezes all evaluations of the arguments into evaluations that are
# captured with value captures and then defines local variables that
# mimic the parameters. If these parameters have `auto` types, this
# lets you define calls to type-generic lambdas, even if your compiler
# doesn't support these.

# If such a holdup is encountered otherwise, it is just replaced by
# the lambda expression as specified. This can be used for compilers
# that support type-generic lambdas expression only in contexts where
# they are converted to a function pointer with a known prototype.

#endPreamble $*

*/

#pragma CMOD amend eval _HOLDBACK1
#pragma CMOD amend preprocess
#pragma CMOD amend eval _HOLDBACK0
