/* -*- C -*-

# part of shnell -- a source to source compiler enhancement tool
# © Jens Gustedt, 2019

## The MODERN supplement -- avoid composed names for language features

# This is a list of simple `#define` and `typedef` that provide the
# features from `stddef.h` and similar basic headers.

*/

#pragma CMOD insert legacy stdc

#pragma CMOD insert private                     \
  EXIT_FAILURE                                  \
  EXIT_SUCCESS                                  \
  __STDC_NO_L_CHAR_LITERAL__                    \
  __STDC_NO_U_CHAR_LITERAL__                    \
  __STDC_NO_u8_CHAR_LITERAL__                   \
  __STDC_NO_u_CHAR_LITERAL__                    \
  alignas                                       \
  alignof                                       \
  bool                                          \
  char16_t                                      \
  char32_t                                      \
  false                                         \
  nullptr                                       \
  offsetof                                      \
  ptrdiff_t                                     \
  static_assert                                 \
  true                                          \
  wchar_t

#ifndef EXIT_FAILURE
#define EXIT_FAILURE stdc::EXIT_FAILURE
#define EXIT_SUCCESS stdc::EXIT_SUCCESS
#define alignas stdc::_Alignas
#define alignof stdc::_Alignof
#define bool stdc::_Bool
#define false ((stdc::_Bool)+0)
#define nullptr ((void*)0)
#ifndef offsetof
# define offsetof stdc::offsetof
#endif
#ifndef static_assert
# define static_assert stdc::_Static_assert
#endif
#define true ((stdc::_Bool)+1)
#ifndef __STDC_NO_COMPLEX__
# ifndef _Imaginary_I
#  define I _Complex_I
# else
#  define I _Imaginary_I
# endif
#endif
typedef stdc::char16_t char16_t;
typedef stdc::char32_t char32_t;
typedef stdc::ptrdiff_t ptrdiff_t;
typedef stdc::wchar_t wchar_t;
typedef struct stdc::timespec stdc::timespec;
#if __STDC_VERSION__ < 202000 && ! defined __STDC_NO_u8_CHAR_LITERAL__
// utf-8 character constants AKA ASCII are only coming in C2x
#define __STDC_NO_u8_CHAR_LITERAL__ 1
#endif
#endif
