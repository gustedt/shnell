// A replacement for a "POSIX" header TU.
//
// This just includes all headers from the POSIX standard that are
// supposed to exist.
//
// All of this seems to be negligible for the performance. If one day
// this turns to be harmful, we should implement this in a more
// sophisticated way.

#ifndef _ZN2_C5POSIX7__guardE
# define _ZN2_C5POSIX7__guardE 1
# include "stdc.h"
# if _XOPEN_SOURCE
#  include <aio.h>
#  include <arpa/inet.h>
#  include <cpio.h>
#  include <dirent.h>
#  include <dlfcn.h>
#  include <fcntl.h>
#  include <fnmatch.h>
#  include <glob.h>
#  include <grp.h>
#  include <iconv.h>
#  include <langinfo.h>
#  include <monetary.h>
#  include <net/if.h>
#  include <netdb.h>
#  include <netinet/in.h>
#  include <netinet/tcp.h>
#  include <nl_types.h>
#  include <poll.h>
#  include <pthread.h>
#  include <pwd.h>
#  include <regex.h>
#  include <sched.h>
#  include <semaphore.h>
#  include <strings.h>
#  include <sys/mman.h>
#  include <sys/select.h>
#  include <sys/socket.h>
#  include <sys/stat.h>
#  include <sys/statvfs.h>
#  include <sys/times.h>
#  include <sys/types.h>
#  include <sys/un.h>
#  include <sys/utsname.h>
#  include <sys/wait.h>
#  include <tar.h>
#  include <termios.h>
#  include <ulimit.h>
#  include <unistd.h>
#  include <utime.h> // obsolescent
#  include <wordexp.h>
// The following feature tests are provided by unistd.h, so they must
// come after that.
#  if _XOPEN_UNIX // XSI
#   include <fmtmsg.h>
#   include <ftw.h>
#   include <libgen.h>
#   include <ndbm.h>
#   include <search.h>
#   include <sys/ipc.h>
#   include <sys/msg.h>
#   include <sys/resource.h>
#   include <sys/sem.h>
#   include <sys/shm.h>
#   include <sys/time.h>
#   include <sys/uio.h>
#   include <syslog.h>
#   include <utmpx.h>
#  endif
#  if _POSIX_MESSAGE_PASSING // MSG
#   include <mqueue.h>
#  endif
#  if _POSIX_SPAWN // SPN
#   include <spawn.h>
#  endif
#  if _POSIX_TRACE // TRC
#   include <trace.h> // obsolescent
#  endif
#  if _XOPEN_STREAMS // XSR
#   include <stropts.h> // obsolescent
#  endif
# endif
#endif
