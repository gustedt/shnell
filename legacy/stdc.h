// A replacement for a "stdc" header TU.
//
// This just includes all headers from the C standard that are
// supposed to exist.
//
// Up to now using all of these is negligible for the performance. If
// one day this turns to be harmful, we should implement this in a
// more sophisticated way.

#ifndef _ZN2_C4stdc7__guardE
#define _ZN2_C4stdc7__guardE 1
#include <float.h>
#include <iso646.h>
#include <limits.h>
//#include <stdalign.h>
#include <stdarg.h>
//#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
//#include <stdnoreturn.h>
#if __STDC_HOSTED__    // __STDC_HOSTED__ is defined to be 0 or 1
# include <assert.h>
# ifndef __STDC_NO_COMPLEX__
#  include <complex.h>
# endif
# include <ctype.h>
# include <errno.h>
# include <fenv.h>
# include <inttypes.h>
# include <locale.h>
# include <math.h>
# include <setjmp.h>
# include <signal.h>
# if __STDC_VERSION__ > 201100L && ! defined __STDC_NO_ATOMICS__
#  include <stdatomic.h>
# endif
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <tgmath.h>
# if __STDC_VERSION__ > 201100L && ! defined __STDC_NO_THREADS__
#  include <threads.h>
# endif
# include <time.h>
# include <uchar.h>
# include <wchar.h>
# include <wctype.h>
# endif
#endif
