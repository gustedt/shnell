

target :
	make -C bin
	+make -C tools-c
	+make -C complements

clean :
	make -C bin          clean
	+make -C tools-c      clean
	+make -C complements  clean
